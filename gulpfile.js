var gulp = require('gulp'),
  yargs = require('yargs'),
  del = require('del'),
  sass = require('gulp-sass'),
  gulpif = require('gulp-if'),
  uglify = require('gulp-uglify'),
  minifyCss = require('gulp-minify-css'),
  urlAdjuster = require('gulp-css-url-adjuster'),
  base64 = require('gulp-base64-inline'),
  debug = require('gulp-debug'),
  autoprefixer = require('gulp-autoprefixer'),
  livereload = require('gulp-livereload'),
  runSequence = require('run-sequence'),
  watch = require('gulp-watch'),
  bower = require('main-bower-files'),
  concat = require('gulp-concat'),
  rev = require('gulp-rev'),
  sourcemaps = require('gulp-sourcemaps'),
  babel = require("gulp-babel"),
  browserify = require("browserify"),
  babelify = require("babelify"),
  source = require('vinyl-source-stream'),
  gutil = require('gulp-util'),
  eslint = require('gulp-eslint'),
  sassLint = require('gulp-sass-lint'),
  bootlint = require('gulp-bootlint'),
  minifyJs = require('gulp-minify'),
  iconfont = require('gulp-iconfont'),
  iconfontCss = require('gulp-iconfont-css'),
  mergeStream = require('merge-stream'),
  buffer = require('vinyl-buffer'),
  paths = require('./package.json').paths,
  revcss = require('gulp-rev-css-url'),
  revdel = require('gulp-rev-delete-original'),
  jscpd = require('gulp-jscpd'),
  args,
  isDev;

args = yargs
  .default('env', 'dev')
  .alias('env', 'environment')
  .argv;

isDev = args.env !== 'prod';

gulp.task('default', (callback) => {
  if (isDev) {
    runSequence('clean', 'icons', 'fonts', 'images', ['styles', 'styles:libs', 'scripts:libs', 'scripts'], callback);
  } else {
    runSequence('clean', 'icons', 'fonts', 'images', ['styles', 'styles:libs', 'scripts:libs', 'scripts'], 'version', callback);
  }
});

gulp.task('fonts', () => {
   return gulp.src(paths.src + 'fonts/*')
       .pipe(gulp.dest(paths.dst + 'fonts'));
});

gulp.task('images', () => {
  return gulp.src(paths.src + 'img/**/*')
    .pipe(gulp.dest(paths.dst + 'img'));
});

gulp.task('clean', () => {
  return del([paths.dst + '**/*']);
});

gulp.task('styles', () => {
  return gulp.src([paths.src + 'styles/*.scss', '!' + paths.src + 'styles/_*.scss'])
    .pipe(gulpif(isDev, sourcemaps.init()))
      .pipe(sass()).on('error', sass.logError)
      .pipe(base64('../'))
      // TODO: url changer destroy inline svg paths
      .pipe(urlAdjuster({prependRelative: '/build/'}))
      .pipe(autoprefixer())
      .pipe(gulpif(!isDev, minifyCss()))
    .pipe(gulpif(isDev, sourcemaps.write('.')))
    .pipe(gulp.dest(paths.dst + 'css'));
});

gulp.task('scripts', () => {
  var streams = [], files = [
    'mobilezone.js',
    'polyfill.js'
  ];

  files.forEach(file => {
    streams.push(
      browserify({debug: isDev})
        .transform(babelify)
        .require(paths.src + 'scripts/' + file, {entry: true})
        .bundle()
        .on('error', gutil.log)
        .pipe(source(file))
        .pipe(buffer())
        .pipe(gulpif(!isDev, minifyJs({
          ext: {
            src: '-debug.js',
            min: '.js'
          }
        })))
        .pipe(gulp.dest(paths.dst + 'scripts/'))
    );
  });

  // TODO: streams aborting building process, avoid it
  return mergeStream(streams);
});

gulp.task('version', () => {
  return gulp.src([
      paths.dst + '**/*.{js,css}',
      paths.dst + '**/iconfont.*'
    ])

    // create content hashed filenames
    .pipe(rev())
    .pipe(revcss())
    .pipe(revdel())
    .pipe(gulp.dest(paths.dst))

    // create asset manifest
    .pipe(rev.manifest())
    .pipe(gulp.dest(paths.dst));
});

/**
 * generate the icon font and the icon styles
 */
gulp.task('iconfont', function() {
  return gulp.src([paths.src + 'icons/*.svg'])
    .pipe(iconfontCss({
      fontName: 'iconfont',
      // template which will be taken to generate the scss file
      path: paths.src + 'styles/common/icons/_font_template.ejs',
      // where to put the generated scss (path is relative to the dest path and the very end of this task
      targetPath: '_font.scss',
      // will be placed into the font styles
      fontPath: 'fonts/icons/'
    }))
    .pipe(iconfont({
      fontName: 'iconfont',
      formats: ['svg', 'ttf', 'eot', 'woff'],
      normalize: true,
      prependUnicode: true,
      // this timestamp set to 1 will force the font not to change on every rebuild but some icons has changed
      timestamp: 1,
      // prevent from logging on to the console
      log: function(){}
    }))
    .pipe(gulp.dest(paths.dst + 'fonts/icons/'))
    .pipe(livereload());
});

// TODO: iconfont:move & iconfont:remove is a workaround, bevause 'targetPath' does not work properly
gulp.task('iconfont:move', () => {
  return gulp.src(paths.dst + 'fonts/icons/_font.scss')
    .pipe(gulp.dest(paths.src + 'styles/common/icons'));
});

gulp.task('iconfont:remove', () => {
  return del(paths.dst + 'fonts/icons/_font.scss');
});

gulp.task('icons', (callback) => {
  // run clean before starting the build process
  runSequence('iconfont', 'iconfont:move', 'iconfont:remove', callback);
});

gulp.task('styles:libs', () => {
  return gulp.src(bower('**/*.css'))
    .pipe(concat('libs.css'))
    .pipe(gulpif(!isDev, minifyCss()))
    .pipe(gulp.dest(paths.dst + 'css'));
});

gulp.task('scripts:libs', () => {
  return gulp.src(bower('**/*.js'))
    .pipe(gulpif(isDev, sourcemaps.init()))
      .pipe(concat('libs.js'))
      .pipe(gulpif(!isDev, uglify()))
    .pipe(gulpif(isDev, sourcemaps.write('.')))
    .pipe(gulp.dest(paths.dst + 'scripts'));
});

gulp.task('icons:build', (callback) => {
  runSequence('clean', 'icons', 'styles', callback);
});

gulp.task('watch', () => {
  // use watch instead of gulp.watch - gulp.watch is not catching changing new files
  watch(paths.src + 'icons/**/*', () => gulp.start('icons:build'));
  watch(paths.src + 'styles/**/*.scss', () => gulp.start('styles'));
  watch(paths.src + 'scripts/**/*.js', () => gulp.start('scripts'));
  watch(paths.src + 'img/**/*', () => gulp.start('images'));
  watch(paths.modules + '*/assets/scripts/**/*.js', () => gulp.start('scripts'));

  livereload.listen();

  gulp.watch(paths.dst + 'css/mobilezone.css').on('change', livereload.changed);
  gulp.watch(paths.dst + 'scripts/mobilezone.js').on('change', livereload.changed);
  gulp.watch('./app/modules/**/*.twig').on('change', livereload.changed);
  gulp.watch('./app/views/**/*.twig').on('change', livereload.changed);
});

gulp.task('lint:js', () => {
  return gulp.src([paths.src + 'scripts/**/*.js'])
    .pipe(eslint())
    .pipe(eslint.format())
    .pipe(eslint.failAfterError());
});

gulp.task('lint:sass', function () {
  return gulp.src(paths.src + 'styles/**/*.scss')
    .pipe(sassLint())
    .pipe(sassLint.format())
    .pipe(sassLint.failOnError())
});

gulp.task('lint:copy-paste', function() {
  return gulp.src(['app/**/*.php', '!app/runtime/**', paths.src + '**/*.js', paths.src + '**/*.scss'])
    .pipe(jscpd());
});

gulp.task('lint:html', function () {
  let issues = [];
  return gulp.src([
    'app/modules/**/*.twig',
    'app/views/**/*.twig'
  ])
    .pipe(bootlint({
      stoponerror: true,
      stoponwarning: false,
      loglevel: 'notice',
      issues: issues,
      reportFn: function(file, lint, isError, isWarning, errorLocation) {
        var message = (isError) ? "ERROR - " : "WARN - ";
        if (errorLocation) {
          message += file.path + ' (line:' + (errorLocation.line + 1) + ', col:' + (errorLocation.column + 1) + ') [' + lint.id + '] ' + lint.message;
        } else {
          message += file.path + ': ' + lint.id + ' ' + lint.message;
        }
        console.log(message);
      },
      summaryReportFn: function(file, errorCount, warningCount) {
        if (errorCount > 0 || warningCount > 0) {
          console.log("There are " + errorCount + " errors and "+ warningCount + " warnings in " + file.path);
          console.log('Total Errors/Warnings: ' + issues.length + "\n");
        }
      },
      disabledIds: [
        'E001', // DOCTYPE check not necessary
        'E003', // X-UA-Compatible check not necessary
        'E013', // compatibility issue (BS3 vs BS4)
        'E033', // we create the "alert" classes dynamically, therefor not lintable
        'E034', // we create the "alert" classes dynamically, therefor not lintable
        'E041', // .carousel-item is the new .item, bootlint needs to be updated for bootstrap 4
        'W001',
        'W002',
        'W003',
        'W005',
      ]
    }));
});
