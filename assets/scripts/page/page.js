class Page {
  constructor() {
    //Static Page Sidebar Navi
    const $url = window.location.toString();
    const $sideNav = $('.page .side-navi');
    const $langSwitch = $('.page .language-switch');
    const $customerAccount = $('.customer-account .list-group');
    const $tvTrigger = $('.page-contract-configurator .trigger-wrapper');

    // Current Language
    if ($url.indexOf('about-us') >= 0) {
      $langSwitch.find('.lang-en').addClass('active');
    } else {
      $langSwitch.find('.lang-en').next().addClass('active');
    }

    //Active Nav Element
    $sideNav.find('a').each((index, element) => {
      const $myHref = $(element).attr('href');
      const $this = $(element);
      const $collapse = $this.parents('.collapse');
      const $header = $collapse.prev().find('a');
      if ($url == $myHref) {
        $this.addClass('active');
        $collapse.addClass('show');
        $header.removeClass('collapsed');
        $header.attr('aria-expanded', 'true');
      }
    });

    // Active Nav Element for Feeds
    if ($url.indexOf('/feed?id=') >= 0) {
      $sideNav.find('a.news').addClass('active');
      $('#headingTwo').find('a').attr('aria-expanded', 'true');
      $('#collapseTwo').addClass('in')
    }

    // Active Nav Element Customer Account
    $customerAccount.find('a').each((index, element) => {
      const $this = $(element);
      const $myHref = $this.attr('href');
      if ($url.indexOf($myHref) >= 0) {
        $this.addClass('current');
      }
    });

    if($url.indexOf('customer-account/customer-account/view-order?orderID=') >=0) {
      $customerAccount.find('a:last-child').addClass('current');
    }

    // TV Page
    $tvTrigger.on('click', (e) => {
      // .parents.find necessary for IE11
      const $input = $(e.target).parents('.custom-control').find('.custom-control-input');
      const $target = $('[data-toggle="' + $input.val() + '"]');

      if($input.is(':checked')) {
        $target.addClass('d-block');
      } else {
        $target.removeClass('d-block');
      }
    });
  }
}

export default Page;
