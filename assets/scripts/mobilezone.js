import './base/modernizr';
import './base/scroll-pane';
import './base/datepicker';
import './base/slider';
import './base/smooth-scroll';
import './base/multi-level-accordion';
import './base/form';
import './base/mobile-header';
import './base/banner-tracking';
import './base/nav';
import './polyfill/ie/customCheckboxIE';

import Category from './category/category';
import Product from './product/product';
import Checkout from './checkout/checkout';
import Page from './page/page';
import ShopFinder from './shopFinder/shopFinder';
import PassportBehavior from './base/passport-behavior';
import ProductCard from './tariff/productCard';
import Overview from './tariff/overview';
import StickyBar from './stickyBar/stickyBar';
import Animations from './animations/animations';

accounting.settings.currency = {
  symbol: $('html').data('currency'),
  format: '%s %v',
  decimal: ',',
  thousand: '.',
  precision: 2,
};

$(() => {
  // flag to mark available js
  $('html').addClass('js');
  new Category();
  new Product();
  new Checkout();
  new Page();
  new ShopFinder();
  new PassportBehavior();
  new ProductCard();
  new Overview();
  new StickyBar();
  new Animations();
});
