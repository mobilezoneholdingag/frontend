import CustomerStep from './steps/customer';
import CartStep from './steps/cart';
import PaymentRedirectStep from './steps/payment-redirect';

class Checkout {
  constructor() {
    new CustomerStep();
    new CartStep();
    new PaymentRedirectStep();
  }
}

export default Checkout;
