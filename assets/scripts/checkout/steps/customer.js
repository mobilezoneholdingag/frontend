class Customer {
  constructor() {
    const $loginAttempt = $('#customermodel-loginattempt');
    const $registerAttempt = $('#customermodel-registerattempt');
    const $tabLogin = $('#checkout-tab-login');
    const $deliveryAddress = $('#checkout-delivery-address').find('.card-block');
    const $deliveryAddressInput = $('#customermodel-usedeliveryaddress');
    const $trigger = $('#checkout-steps-customer');
    const $content = $('.tab-content');

    $deliveryAddressInput.on('change', () => {
      $deliveryAddress.toggleClass('hidden-xs-up');
    });

    $trigger.find('a').click(function(e) {
      e.preventDefault();

      if($(this).hasClass('active')) {
        return;
      }

      $trigger.find('a').each(function() {
        $(this).toggleClass('active');
      });

      $content.find('.tab-pane').each(function() {
        $(this).toggleClass('active');
      });

      if($tabLogin.hasClass('active')) {
        $loginAttempt.val(1);
        $registerAttempt.val(0);
      } else {
        $loginAttempt.val(0);
        $registerAttempt.val(1);
      }
    });
  }
}

export default Customer;
