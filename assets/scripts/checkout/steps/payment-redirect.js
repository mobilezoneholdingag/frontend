class Customer {
  constructor() {
    const $checkout = $('.checkout');
    const $button = $('#Postfinancesubmit');

    if($checkout.hasClass('payment-redirect')) {
      $button.trigger('click');
    }
  }
}

export default Customer;
