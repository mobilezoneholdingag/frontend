class Cart {
  constructor() {
    this.$insuranceForm = $('.form-insurance');
    this.$insuranceActive = $('.insurance-active');
    this.$insuranceSelected = this.$insuranceForm.find('.insurance-options');
    this.$insuranceInformationToggle = $('.toggle-information');
    this.updateInsuranceUrl = '/checkout/cart/update-insurance';

    this.updateInsuranceHighlights();

    this.$insuranceActive.on('change', (event) => {
      const $target = $(event.target).closest('.form-insurance');
      const $checkedOption = $target.find('.insurance-options:checked');
      const $selectionTarget = $checkedOption.length ? $checkedOption : $target.find('.insurance-active');

      this.updateInsuranceSelection($selectionTarget);
      Cart.updatePriceLabel($target);
      Cart.updateInsuranceOptionsState($(event.target));
    });

    this.$insuranceSelected.on('change', (event) => {
      const $target = $(event.target).closest('.form-insurance');

      this.updateInsuranceSelection($target.find('.insurance-options:checked'));
      Cart.updatePriceLabel($target);
      Cart.updateInsuranceInformation($target);
      this.updateInsuranceHighlights();
    });

    this.$insuranceInformationToggle.on('click', (event) => {
      $(event.currentTarget).toggleClass('active');
      Cart.updateInsuranceInformation($(event.target).closest('.form-insurance'));
    });
  }

  /**
   * update state of the insurance options. if insurance is not active, then the options will be disbaled
   */
  static updateInsuranceOptionsState($target) {
    const $thisInsurance = $target.parents('.form-insurance');
    const $isActive = $thisInsurance.find('.insurance-active').is(':checked');
    $thisInsurance.find('.insurance-options').prop('disabled', !$isActive);
  }

  /**
   * will update the current insurance selection by sending request to server
   */
  updateInsuranceSelection($target) {
    const insuranceChecked = $target.closest('.form-insurance').find('.insurance-active').is(':checked');
    const insuranceId = $target.is(':checked') && insuranceChecked
      ? $target.val()
      : undefined;
    const articleId = $target.closest('.form-insurance').data('article-id');

    this.updateInsurance(insuranceId, articleId);
  }

  /**
   * update the price label by of the current insurance
   */
  static updatePriceLabel($thisInsurance) {
    const $input = $thisInsurance.find('.insurance-active');
    let price = $thisInsurance.find('.insurance-active').data('price-empty');

    if ($input.is(':checked')) {
      const $multipleOptions = $thisInsurance.find('.insurance-options').filter(':checked');
      const $singleOption = $thisInsurance.find('.insurance-active');
      const $selectedInsurance = $multipleOptions.length > 0 ? $multipleOptions : $singleOption;
      price = $selectedInsurance.data('price');
    }

    $thisInsurance.find('.insurance-price .price').html(price);
  }

  getSelectedInsurance() {
    return this.$insuranceSelected.filter(':checked');
  }

  updateInsurance(insuranceId, articleId) {
    const data = {
      'insurance_id': insuranceId,
      'article_id': articleId,
    };

    $.post(this.updateInsuranceUrl, data)
      .done((response) => {
        $('.price-total-once').find('.price').html(response.total.once);
      });
  }

  /**
   * Only show the highlights of active insurance
   */
  updateInsuranceHighlights() {
    this.$insuranceForm.each((index, element) => {
      const $thisInsurance = $(element).find('.insurance-options');
      const $thisInsuranceOptions = $thisInsurance.length;

      if($thisInsuranceOptions > 1) {
        const $active = $(element).find('.insurance-options:checked').val();
        $(element).find('.list-info').hide();
        $(element).find('.list-info-for-' + $active).show();
      }
    })
  }

  static updateInsuranceInformation($thisInsurance) {
    const $selectedInsuranceVal = $thisInsurance.find('.insurance-options:checked').val();
    const $targetId = $thisInsurance.next().find('#additionalInformationFor' + $selectedInsuranceVal);

    if($selectedInsuranceVal) {
      $thisInsurance.next().find('.additional-insurance-information').hide();
      $targetId.show();
    }
  }
}

export default Cart;
