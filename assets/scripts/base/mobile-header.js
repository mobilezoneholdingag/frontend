class Slider {
  constructor() {
    $(() => {
      const $mobileNaviLink = $('.mobile-level1');

      // Mobile Navi Toggle
      $mobileNaviLink.on('click', (element) => {
        const $target = $(element.currentTarget);

        $target.next('.navi-layer').toggleClass('d-block');
        $target.find('.icon').toggleClass('open');
      });
    });
  }
}

export default new Slider;
