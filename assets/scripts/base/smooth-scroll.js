class SmoothScroll {
  constructor() {
    $(() => {
      const $root = $('html, body');
      const $trigger = $('.smooth-scroll');

      $trigger.click((e) => {
        const $target = $($(e.currentTarget).attr('href'));

        $root.animate({
          scrollTop: $target.offset().top - 50
        }, 500);
        return false;
      });
    });
  }
}

export default new SmoothScroll;
