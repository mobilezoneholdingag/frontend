class Form {
  constructor() {
    $(() => {
      const $form = $('form');

      $form.submit((event)=> {
        $(event.target).find('[type="submit"]').prop('disabled', true);
      });
    });
  }
}

export default new Form;
