class Datepicker {
  constructor() {
    $(() => {
      const $selector = $('.datepicker');

      $selector.each(function () {
        /* Create a hidden clone, which will contain the actual value */
        const $this = $(this);
        const $classes = $this.attr('class');
        const $placeholder = $this.attr('data-placeholder');
        let $readonly = $this.attr('readonly');
        if ($readonly === undefined) {
          $readonly = '';
        } else {
          $readonly = 'readonly="' + $readonly + '"';
        }
        const $value = $this.val().split('-');
        const $clone = '<input type="text" class="'+$classes+'" placeholder="'+$placeholder+'" value="'+$value[2]+$value[1]+$value[0]+'" '+$readonly+'>';
        $this.after($clone);
        $this.hide();

        /* Mask the displayed input field */
        $this.next().mask('AB.CB.DBBB', {
          translation: {
            A: {pattern: /[0-3]/},
            B: {pattern: /[0-9]/},
            C: {pattern: /[0,1]/},
            D: {pattern: /[1,2]/},
          }
        });

        /* Finally, parse the value and change it to the output format */
        $this.next().keyup(function() {
          const $date = $(this).val().split('.');
          $this.attr('value', $date[2]+'-'+$date[1]+'-'+$date[0]);
        });
      });
    });
  }
}

export default new Datepicker;
