class MultiLevelAccordion {
  constructor() {
    $(() => {
      $('[class*="-level-header"]:not(".disable-toggle")').click(function(e) {
        e.preventDefault();
        const $this = $(this);
        const $next = $this.next();
        const $firstBody = $this.parents().find('.first-level-body.show');
        const $secondBody = $this.parents().find('.second-level-body.show');
        if ($next.hasClass('show')) {
          $next.removeClass('show');
          $next.slideUp(350);
          $this.find('span').removeClass('turn');
        } else {
          if ($this.hasClass('first-level-header')) {
            $firstBody.slideUp(350);
            $firstBody.removeClass('show');
            $firstBody.removeClass('turn');
          } else {
            $secondBody.slideUp(350);
            $secondBody.removeClass('show');
            $secondBody.removeClass('turn');
          }
          $next.slideDown(350);
          $next.addClass('show');
          $this.find('span').addClass('turn');
        }
      });
    });
  }
}

export default new MultiLevelAccordion;
