/** global: dataLayer */

class BannerTracking {
  constructor() {
    this.$banner = $('.banner-impression');

    $(() => {
      const $navItem = $('.nav-item');
      const $slider = $('#homepageTeaserCarousel');

      this.$banner.on('click', (e) => {
        this.eePromotionClick($(e.currentTarget));
      });

      this.checkForImpression();

      $navItem.on('mouseenter', () => {
        this.checkForImpression();
      });

      $slider.on('slide.bs.carousel', () => {
        this.checkForImpression();
      });

      $(window).scroll(() => {
        this.checkForImpression();
      });
    });
  }

  eePromotionClick($element) {
    dataLayer.push({
      'event': 'eePromotionClick',
      'ecommerce': {
        'promoClick': {
          'promotions': [
            {
              'id': $element.data('item-id'),
              'name': $element.data('item-name'),
              'creative': $element.data('item-tag'),
              'position': $element.data('item-position'),
            }
          ]
        }
      }
    });
  }

  checkForImpression() {
    const $win = $(window);
    const $viewport = {
      top: $win.scrollTop(),
      left: $win.scrollLeft(),
    };
    $viewport.right = $viewport.left + $win.width();
    $viewport.bottom = $viewport.top + $win.height();
    const $elements = [];
    const $idContainer = $('#bannerTrackingId');
    const $oldId = $idContainer.attr('data-id');
    let $newId = '';

    if($idContainer.length === 0) {
      $('body').append('<div id="bannerTrackingId" data-id class="hidden-xs-up"></div>');
    }

    this.$banner.each((index, e) => {
      const $this = $(e);
      const $bounds = $this.offset();
      $bounds.right = $bounds.left + $this.outerWidth();
      $bounds.bottom = $bounds.top + $this.outerHeight();

      if ((!(
          $viewport.right < $bounds.left ||
          $viewport.left > $bounds.right ||
          $viewport.bottom < $bounds.top ||
          $viewport.top > $bounds.bottom)
        )
        && $this.is(':visible')
      ) {
        $elements.push($this);
        $newId = $newId + $this.data('item-id');
      }
    });

    if(($oldId !== $newId) && ($elements.length !== 0)) {
      this.eePromotionImpression($elements);

      $('#bannerTrackingId').attr('data-id', $newId);
    }
  }

  eePromotionImpression($elements) {
    let $i;
    const $promotions = [];

    for($i = 0; $i < $elements.length; $i++) {
      $promotions.push(
        {
          'id': $elements[$i].data('item-id'),
          'name': $elements[$i].data('item-name'),
          'creative': $elements[$i].data('item-tag'),
          'position': $elements[$i].data('item-position'),
        }
      );
    }

    dataLayer.push({
      'event': 'eePromotionImpression',
      'ecommerce': {
        'promoView': {
          'promotions': $promotions
        }
      }
    });
  }
}

export default new BannerTracking;
