class History {
  constructor() {
    this.listeners = []
  }

  /**
   * register listener
   *
   * @param eventName
   * @param callback
   * @returns {History}
   */
  on(eventName, callback) {
    this.listeners.push({eventName, callback});

    return this;
  }

  /**
   * removes all listeners which are listening to the given event
   * can be namespaced by . (dot).
   *
   * @param eventName
   * @returns {History}
   */
  off(eventName) {
    this.listeners = this.listeners
        .find(listener => listener.eventName !== eventName);

    return this;
  }

  /**
   * push state, delegates to native browser history api
   *
   * @param args
   */
  push(...args) {
    window.history.pushState(...args);
    this.listeners
      .filter(listener => listener.eventName.search('push') === 0)
      .forEach(listener => listener.callback(args[0]));
  }
}

export default new History();
