import * as isMobile from "../../../bower_components/isMobile/isMobile";

class Nav {
  /** @namespace isMobile.any */

  constructor() {
    $(() => {
      const $navItem = $('#navigation-main > .nav-item');

      if(isMobile.any) {
        $navItem.doubleTapToGo();
      }
    });

    $('.customsearch').click(function () {
      $('.icon-menu').click();
      $(":input[name=q]").focus();
    });
  }
}

export default new Nav;
