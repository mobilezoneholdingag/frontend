class ScrollPane {
  constructor() {
    $(() => {
      $('.scroll-pane').jScrollPane();

      $(window).resize($.debounce(250, () => {
        $('.scroll-pane').jScrollPane();
      }));
    });
  }
}

export default new ScrollPane;
