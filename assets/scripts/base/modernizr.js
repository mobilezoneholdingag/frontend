/**
 * available tests: https://github.com/jnordberg/browsernizr/tree/master/test
 * pick what tests you need
 */
import 'browsernizr/test/geolocation';
import 'browsernizr/test/css/flexbox';

// make sure to do this _after_ importing the tests
import 'browsernizr';
