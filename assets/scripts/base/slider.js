class Slider {
  constructor() {
    $(() => {
      $('.nstSlider').each((index, element) => {
        const $element = $(element),
          $min = $element.find('.range-value-min'),
          $max = $element.find('.range-value-max'),
          settings = {
            left_grip_selector: '.range-grip-left',
            value_changed_callback: function(cause, min, max) {
              if (cause === 'refresh') {
                return;
              }

              // do not react on refresh because it does not change the slider value, it just updates the
              // position of the grips etc when you eg resize the window
              const $this = this,
                $range = $this.closest('.range'),
                format = $this.attr('data-slider-format');

              $this.trigger('change', [min, max]);
              $min.val(min).trigger('change');
              $max.val(max).trigger('change');

              $range.find('.range-display-left').html(format ? format.replace('%v', min) : accounting.formatMoney(min));
              $range.find('.range-display-right').html(format ? format.replace('%v', max) : accounting.formatMoney(max));
            }
          };

        if ($element.find('.range-grip-right').length == 1) {
          settings.right_grip_selector = '.range-grip-right';
          settings.value_bar_selector = '.range-bar';
        }

        $element.nstSlider(settings);
      });

      $(window).resize($.debounce(50, () => {
        $('.nstSlider:not(.disabled)').each(function() {
          $(this).nstSlider('refresh')
        });
      }));
    });
  }
}

export default new Slider;
