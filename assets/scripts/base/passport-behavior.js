class PassportBehavior {
  constructor() {
    const $nationality = $('#passportmodel-nationality');
    const $passportOptions = $('#passportmodel-passport_type option');
    let $passportTypes;

    $nationality.on("change", () => {
      const $countryCode = $nationality.val();

      if($countryCode == 'ch' || $countryCode == 'li') {
        $passportTypes = [4, 5, 6, 7, 8, 9];
      } else {
        $passportTypes = [1, 2, 3, 10];
      }

      // Show all options before filtering again
      $passportOptions.show();

      // Hide passport types that do not belong to selected country
      $.each($passportTypes, (index, type) => {
        $('#passportmodel-passport_type option[value="'+type+'"]').hide();
      });
    });

    // Duplicate countries and paste them to top
    const $duplicates = ['at', 'it', 'fr', 'de', 'ch'];
    const $firstOption = $nationality.find('option:first-child');

    $.each($duplicates, (index, key) => {
      $firstOption.after(() => {
        return $nationality.find('option[value="'+key+'"]').clone().addClass('clone');
      });
    });

    // Include separator between duplicates and originals
    const $lastClone = $nationality.find('.clone').last();
    const $separator = '<option disabled="disabled">────────────────────</option>';
    $lastClone.after($separator);
  }
}

export default PassportBehavior;
