import Assets from '../helpers/asset';
import Geo from '../helpers/geo';

const apiKey = 'AIzaSyD4FBM_itUdRDn-nGBAdWqEzdfz9TSbgag';

class ShopFinder {

  constructor() {
    this.markerWithoutHelpercenter = [];

    if ($('#map-locations').length > 0) {
      const url = `https://maps.googleapis.com/maps/api/js?key=${apiKey}&signed_in=true&libraries=places`;

      $.getScript(url, () => {
        this.initMap();
        this.initForm();
        this.initMarkerToggler();
      });
    }
  }

  /**
   * workaround to decode html entities
   * @param str
   * @returns {*}
   */
  static decodeHtml(str) {
    const elem = document.createElement('textarea');
    elem.innerHTML = str;
    str = elem.value;

    return str;
  }

  initForm() {
    const $form = $('#shop-finder');
    const $mylocation = $('#google-maps-mylocation');
    const $latitude = $('#shopfinderform-latitude');
    const $longitude = $('#shopfinderform-longitude');
    const $searchQuery = $('#google-maps-query');

    $form.on('submit', (e) => {
      $('#loader').fadeIn();
      const searchQuery = $searchQuery.val() + ',Switzerland';

      // if no coordinates set, need to look for entered search query
      if (!$form.data('coords-fetched')) {
        e.preventDefault();
        $.get('https://maps.google.com/maps/api/geocode/json?address=' + searchQuery, (res) => {

          if(res.results.length !== 0) {
            const cords = res.results[0].geometry.location;
            $latitude.val(cords.lat);
            $longitude.val(cords.lng);

            $form.data('coords-fetched', true);
            $form.attr('action', '?store=' + $searchQuery.val());

            $form.submit();
          } else {
            $('#loader').fadeOut();
            $('button').prop('disabled', false);
          }
        });
      }
    });

    $mylocation.on('click', () => {
      Geo.getPosition({
        success: (position) => {
          $latitude.val(position.coords.latitude);
          $longitude.val(position.coords.longitude);

          $form.data('coords-fetched', true);
          // send form
          $form.submit();
        }
      });
    });
  }

  initMarkerToggler() {
    const $helperCenter = $('#shopfinderform-helpcenter');

    $helperCenter.on('change', () => {
      this.markerWithoutHelpercenter.forEach((marker) => marker.setVisible(!$helperCenter.is(':checked')));
    });
  }

  initMap() {
    const data = JSON.parse($('#map-locations').html());
    const settings = data.settings;
    const locations = data.shops;
    let marker;
    let i;
    const infowindow = new google.maps.InfoWindow();
    let bounds  = new google.maps.LatLngBounds();
    let loc;

    const map = new google.maps.Map(document.getElementById('map'), {
      zoom: 12,
      center: new google.maps.LatLng(settings.latitude, settings.longitude),
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      mapTypeControl: false,
    });

    locations.forEach((location) => {

      loc = new google.maps.LatLng(location.latitude, location.longitude);

      const markerConfig = {
        position: loc,
        map: map,
        icon: Assets.url(location.is_helpcenter ? 'img/shop-finder/marker-helpcenter.png' : 'img/shop-finder/marker.png'),
      };

      marker = new google.maps.Marker(markerConfig);

      if (!location.is_helpcenter) {
        this.markerWithoutHelpercenter.push(marker);
      }

      google.maps.event.addListener(marker, 'click', ((marker) => {
        return function () {
          infowindow.setContent(ShopFinder.decodeHtml(location.html));
          infowindow.open(map, marker);
        }
      })(marker, i));

      bounds.extend(loc);

    });

    if(settings.fitbounds == true) {
      map.fitBounds(bounds);
    }

  }
}

export default ShopFinder;


