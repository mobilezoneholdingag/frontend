import History from '../base/history';

class ContractBox {
  constructor() {
    this.addEventListener();
  }

  addEventListener() {
    $('.contract-box-switch').on('click', (e) => {
      e.preventDefault();
      const $switch = $(e.currentTarget),
        href = $switch.attr('href'),
        box = href.split('=')[1];

      History.push({box}, null, href);
    });

    // listen to url changes and toggle boxes
    History.on('push.box', (state) => {
      const $contractBoxes = $('.contract-box'),
        $subscriptions = $('.subscriptions');

      $contractBoxes
        .addClass('hidden-xs-up')
        .filter(`.${state.box}`)
        .removeClass('hidden-xs-up');

      // hide subscriptions only on no-contract box
      $subscriptions.toggleClass('hidden-xs-up', state.box === 'no-contract');
    });

    $('.new-number').on('click', () => {
      this.loadSubscriptionList({ offer_type: 1 });
    });

    $('.keep-number').on('click', () => {
      this.loadSubscriptionList({ offer_type: 3 });
    });

    $('.contract-box [name="RenewalRequestForm[provider_id]"]').on('click', (el) => {
      this.loadSubscriptionList({ provider: el.target.value, offer_type: 3 });
    });

    $('#CurrentProviderSelection').on('change', (el) => {
      const provider_excluded = $(el.target).val();
      this.loadSubscriptionList({ provider_id_excluded: [provider_excluded], offer_type: 1 });
      $('.subscription-box-porting').removeClass('hidden-xs-up')
    }).trigger('change');

    $('.contract-box [data-current-provider-id]').on('click', (el) => {
      const provider_excluded = $(el.target).data('current-provider-id');
      this.loadSubscriptionList({ provider_id_excluded: [provider_excluded], offer_type: 1 });
    });

    $('#CurrentProviderSelectionRenewal').on('change', (el) => {
      const provider_excluded = $(el.target).val();
      if(provider_excluded == 2) {
        $('.subscription-box-renewal').addClass('hidden-xs-up');
        window.location = $('#swisscom_url').val();
        return true;
      }
      if (parseInt(provider_excluded) > 0) {
        $('.subscription-box-renewal').removeClass('hidden-xs-up');
        $('.subscription-box-swisscom').addClass('hidden-md-up');
      }
    }).trigger('change');
  }

  loadSubscriptionList(options = {}) {
    $('body').trigger('filter:change.product', options);
    $('.subscriptions').show();
  }

  getUrlVars()
  {
    const vars = [];
    let hash;
    const hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(let i = 0; i < hashes.length; i++)
    {
      hash = hashes[i].split('=');
      vars.push(hash[0]);
      vars[hash[0]] = hash[1];
    }

    return vars;
  }

}

export default ContractBox;
