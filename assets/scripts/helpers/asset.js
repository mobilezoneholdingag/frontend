const Asset = {
  url: (path) => {
    return '/build/' + path;
  }
};

export default Asset;
