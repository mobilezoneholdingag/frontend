const Browser = {
  isIE() {
    const ua = window.navigator.userAgent;
    const msie = ua.indexOf('MSIE '); // IE 10 or older
    const trident = ua.indexOf('Trident/'); // IE 11
    const edge = ua.indexOf('Edge/'); // IE 12 or newer

    return (msie > 0 || trident > 0 || edge > 0);
  }
};

export default Browser;
