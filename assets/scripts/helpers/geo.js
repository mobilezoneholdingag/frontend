const Geo = {
  isEnabled() {
    return ('geolocation' in navigator);
  },
  getPosition({success = () => {}, error = () => {}}) {
    navigator.geolocation.getCurrentPosition(success, error);
  }
};

export default Geo;
