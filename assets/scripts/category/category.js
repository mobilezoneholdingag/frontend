import ProductFilter from '../filters/filterLord';
import Filter from '../filters/filter';
import Renderer from './productFilter/renderer';
import Sort from './productFilter/sort';
import Requestor from './productFilter/requestor';
import CountChecked from './countCecked';

class Category {
  constructor() {
    const eventNamespace = 'category';
    const $filterGroups = $('.category .filter-group input');
    const $products = $('.product-cards .row');
    const $noFilterResult = $('.no-filter-result');

    if (($filterGroups.length && $products.length) || $('.tariff-articles').length) {
      new ProductFilter(
        eventNamespace,

        new Filter(
          eventNamespace,
          '.filter-groups-mobile .filter-group, .filter-groups-mobile .filter-group-slider',
          '.filter-groups .filter-group, .filter-groups .filter-group-slider'
        ),

        new Sort(eventNamespace),
        new Requestor(),
        new Renderer()
      );
    }

    new CountChecked();

    $filterGroups.on('change', () => {
      new CountChecked();
    });

    $('.filter-groups-mobile #product-filter').on('change', (e) => {
      const $activeSlider = $(e.target).parent().find('.nstSlider:not(.disabled)');

      $activeSlider.each(function() {
        $(this).nstSlider('refresh');
      });
    });

    $products.bind("DOMSubtreeModified", () => {
      if($products.is(':empty')) {
        $noFilterResult.show();
      } else {
        $noFilterResult.hide();
      }
    });
  }
}

export default Category;
