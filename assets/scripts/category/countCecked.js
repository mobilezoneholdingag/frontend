class CountChecked {
  constructor() {
    const $thisFilterGroup = $('.filter-group');
    const $manufacturerBanner = $('.img-manufacturer-banner');
    const $checkedManufacturer = $('[data-filter-group="manufacturer"]').find('.custom-control-input:checked');
    const $manufacturerLength = $checkedManufacturer.length;
    const $manufacturerId = $checkedManufacturer.val();
    const $pathPieces = window.location.pathname.split('/');
    const $activeId = $pathPieces[$pathPieces.length-1];

    $thisFilterGroup.each((index, element) => {
      const $this = $(element);
      const $checked = $this.find('.custom-control-input:checked').length;
      const $suffix = $this.find('.checked-filter-count');

      if($checked > 0) {
        $this.addClass('active');
        $suffix.html(' ('+$checked+')');
      } else {
        $this.removeClass('active');
        $suffix.html('');
      }
    });

    if($manufacturerLength === 1 && $manufacturerId === $activeId) {
      $manufacturerBanner.removeClass('hidden-xs-up');
    } else {
      $manufacturerBanner.addClass('hidden-xs-up');
    }
  }
}

export default CountChecked;
