class Renderer {
  add(productCard) {
    return $(`<div class="col-xs-12 col-sm-6 col-lg-4 col-xl-3">${productCard}</div>`);
  }

  update(result) {
    const products = [];
    for (const article of result) {
      products.push(this.add(article));
    }
    $('.product-cards .row:first').append(products);
  }

  clear() {
    $('.product-cards .row').empty();
  }
}

export default Renderer;
