class Requestor {
  constructor() {
    this.skipRequest = true;
  }

  request(payload) {
    return new Promise((resolve, reject) => {
      const data = {
        search: payload.search,
        page: payload.page,
        sort_by: payload.sort || 'position',
        type: payload.type || 'product',
      };

      Object.keys(payload.filter).forEach((name) => {
        data[name] = payload.filter[name];
      });

      const $pagination = $('#pagination-url');
      const customAction = $pagination.length ? $pagination.data('url') : false;

      $.ajax({
        url: customAction ? customAction : '/category/category/filtered-products',
        method: 'GET',
        data: data,
        beforeSend: () => {
          $('#loader').fadeIn();
        },
        error: (e) => {
          reject(e);
        },
        success: (result) => {
          resolve(result);
          $('#loader').fadeOut();
        },
        dataType: 'json'
      });
    });
  }
}

export default Requestor;
