let sortKey = null;

class Sort {
  constructor(eventNamespace) {
    this.eventNamespace = eventNamespace;
    this.init();
  }

  init() {
    // detect available sort options and attach events
    for (const option of $('.sort-bar .sort-option')) {
      const sortOptionValue = $(option).data('sort-option');
      $(option).on('click', () => {
        $('.sort-bar .sort-option').removeClass('active');
        $(option).addClass('active');
        this.sortBy(sortOptionValue);
      });
    }
  }

  sortBy(value) {
    sortKey = value;
    $('body').trigger('filter:change.' + this.eventNamespace, { sort: sortKey });
  }

  getSortKey() {
    return sortKey;
  }

  reset() {
    sortKey = null;
    $('.sort-bar .sort-option').removeClass('active');
    $('.sort-bar .sort-option').first().addClass('active');
  }
}

export default Sort;
