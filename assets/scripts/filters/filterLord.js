import Search from './search';
import Pagination from '/app/app/modules/pagination/assets/scripts/pagination';
import CountChecked from '../category/countCecked'

class FilterLord {
  constructor(EventNamespace, Filter, Sort, Requestor, Renderer) {
    this.eventNamespace = EventNamespace;

    this.filter = Filter;
    this.sort = Sort;
    this.requestor = Requestor;
    this.renderer = Renderer;
    this.changeEventDisabled = false;
    this.noFilterSelected = false;

    this.initComponents();
    this.addEventListener();
    this.initialData = this.collectRequestData();
    $('body').trigger('filter:change.' + this.eventNamespace);
  }

  initComponents() {
    this.search = new Search();
    this.paginate = new Pagination(this);
  }

  addEventListener() {
    const $body = $('body');

    $body.on('filter:change.' + this.eventNamespace, (event, data) => {
      if (this.changeEventDisabled) {
        return;
      }
      this.filter.setAdditionalFilters(data);
      this.request();
    });

    $body.on('filter:disable.' + this.eventNamespace, () => {
      this.changeEventDisabled = true;
    });

    $body.on('filter:enable.' + this.eventNamespace, () => {
      this.changeEventDisabled = false;
      new CountChecked();
    });

    $('.filter-reset a').on('click', (event) => {
      event.preventDefault();
      this.resetFilters();
    });
  }

  resetFilters() {
    this.filter.reset();
    if ($('.sort-bar').length > 0) {
      this.sort.reset();
    }
  }

  request() {
    if (!this.requestor) {
      return;
    }

    const data = this.collectRequestData();

    this.noFilterSelected = JSON.stringify(data.filter) === JSON.stringify(this.initialData.filter);

    $('body').trigger('filter:selected.' + this.eventNamespace);

    if (typeof this.requestor.skipRequest !== 'undefined' && this.requestor.skipRequest === true) {
      this.requestor.skipRequest = false;
      return;
    }

    this.requestor.request(data)
      .then((result) => {
        this.renderer.clear();
        this.renderer.update(result);
      })
      .catch((e) => {
        console.log('oops', e);
      });
  }

  collectRequestData() {
    const hiddenFilterJson = $('#hidden-filter').val() || "{}";
    const hiddenFilters = (JSON.parse(hiddenFilterJson));
    return {
      search: this.search.getQuery(),
      filter: this.filter.getSelectedValues(),
      sort: this.sort.getSortKey(),
      page: this.paginate.getPage(),
      type: $('.filter-groups[data-type]').data('type'),
      hiddenFilters: hiddenFilters,
    };
  }
}

export default FilterLord;
