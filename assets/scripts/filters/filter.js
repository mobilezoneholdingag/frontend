import SelectFilter from './filterTypes/selectFilter';
import MultiSelectFilter from './filterTypes/multiSelectFilter';
import DoubleSliderFilter from './filterTypes/doubleSliderFilter';
import ChoiceFilter from './filterTypes/choiceFilter';

class Filter {
  constructor(eventNamespace, selectorsMobile, selectorsDesktop, additionalFilters = {}) {
    this.filters = [];
    this.additionalFilters = {};
    this.eventNamespace = eventNamespace;

    this.selectors = {
      mobile: selectorsMobile,
      desktop: selectorsDesktop,
    };

    this.mapping = {
      choice: ChoiceFilter,
      single_select: SelectFilter,
      multi_select: MultiSelectFilter,
      double_slider: DoubleSliderFilter,
    };

    this.setAdditionalFilters(additionalFilters);

    this.init();
  }

  init() {
    const selector = $(window).width() < 768 ? this.selectors.mobile : this.selectors.desktop;

    // detect all available filters, store internally and attach events
    for (const group of $(selector)) {
      const $filterGroup = $(group),
        filterType = $filterGroup.data('filter-group-type'),
        filterGroupName = $filterGroup.data('filter-group');

      if (this.mapping[filterType]) {
        this.filters.push(new this.mapping[filterType]($filterGroup, filterGroupName, this.eventNamespace));
      }
    }
  }

  reset() {
    $('body').trigger('filter:disable.' + this.eventNamespace);
    this.filters.forEach(filter => filter.reset());
    setTimeout(() => {
      const $body = $('body');

      $body.trigger('filter:enable.' + this.eventNamespace);
      $body.trigger('filter:change.' + this.eventNamespace);
    }, 50)
  }

  getSelectedValues() {
    const result = {};

    this.filters.forEach(filter => {
      if (!filter.isEmpty()) {
        result[filter.name] = filter.getAllSelectedFilters();
      }
    });

    Object.keys(this.additionalFilters).forEach((key) => {
      result[key] = this.additionalFilters[key];
    });

    return result;
  }

  setAdditionalFilters(filters = {}) {
    this.additionalFilters = Object.assign(this.additionalFilters, filters);
  }
}

export default Filter;
