import AbstractSliderFilter from './abstractSliderFilter';

class SingleSliderFilter extends AbstractSliderFilter {
  updateSelection() {
    this.selection = this.$slider.nstSlider('get_current_min_value');
  }

  reset() {
    // reset slider to min/max
    this.$slider.nstSlider('set_position', this.$slider.nstSlider('get_range_max'));
  }
}

export default SingleSliderFilter;
