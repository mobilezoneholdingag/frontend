import AbstractFilter from './abstractFilter';

class SelectFilter extends AbstractFilter {
  init() {
    this.selectedFilters = [];
    this.$select = this.container.find('select');
    this.selectedFilters = this.$select.val() ? [this.$select.val()] : [];
  }

  attachEvents() {
    this.$select.on('change', () => {
      this.selectedFilters = this.$select.val() ? [this.$select.val()] : [];
      this.fireFilterChanged();
    });
  }

  isEmpty() {
    return this.selectedFilters.length == 0;
  }

  reset() {
    this.selectedFilters = [];
    this.$select.val(null);
  }

  getAllSelectedFilters() {
    return this.selectedFilters;
  }
}

export default SelectFilter;
