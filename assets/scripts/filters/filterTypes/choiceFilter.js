import AbstractFilter from './abstractFilter';

class ChoiceFilter extends AbstractFilter {
  init() {
    this.selectedFilters = null;
    this.$radio = this.container.find('input');
    this.initial = this.$radio.find(':checked').val();
  }

  attachEvents() {
    this.$radio.on('change', () => {
      this.selectedFilters = this.$radio.filter(':checked').val();
      this.fireFilterChanged();
    });
  }

  isEmpty() {
    return this.selectedFilters == null;
  }

  reset() {
    this.selectedFilters = null;
    if (this.initial) {
      this.$radio.val(this.initial);
    } else {
      this.$radio.prop('checked', false);
    }
  }

  getAllSelectedFilters() {
    return this.selectedFilters;
  }
}

export default ChoiceFilter;
