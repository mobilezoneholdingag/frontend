import AbstractFilter from './abstractFilter';

class AbstractSliderFilter extends AbstractFilter {
  init() {
    this.$slider = this.container.find('.nstSlider');
    this.updateSelection();
  }

  attachEvents() {
    const onChange = $.debounce(350, () => {
      this.updateSelection();
      this.fireFilterChanged();
    });

    this.$slider.on('change', onChange);
  }

  updateSelection() {
    // implement abstract method
  }

  getAllSelectedFilters() {
    return this.selection;
  }
}

export default AbstractSliderFilter;
