class AbstractFilter {
  constructor(container, name, eventNamespace) {
    this.container = container;
    this.name = name;
    this.eventNamespace = eventNamespace;

    this.init();
    this.attachEvents();
  }

  fireFilterChanged() {
    $('body').trigger('filter:change.' + this.eventNamespace);
  }

  init() {
    // override in child class
  }

  attachEvents() {
    // override in child class
  }

  reset() {
    // override in child class
  }

  isEmpty() {
    return true;
  }
}

export default AbstractFilter;
