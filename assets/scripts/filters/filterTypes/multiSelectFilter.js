import AbstractFilter from './abstractFilter';

class MultiSelectFilter extends AbstractFilter {
  init() {
    this.selectedFilters = [];
    this.$checked = this.container.find('input:checked');
    this.$checked.each((index, item) => {
      this.toggleFilter(item.value);
    });
  }

  attachEvents() {
    // desktop view
    this.container.find('li input[type=checkbox]').on('change', (event) => {
      this.toggleFilter(event.target.value);
      this.fireFilterChanged();
    });
  }

  isEmpty() {
    return this.selectedFilters.length == 0;
  }

  reset() {
    this.selectedFilters = [];
    this.container.find('li input[type=checkbox]').prop('checked', false);
  }

  getAllSelectedFilters() {
    return this.selectedFilters;
  }

  setSelectedFilter(value) {
    this.selectedFilters.push(value);
  }

  removeSelectedFilter(value) {
    this.selectedFilters = this.selectedFilters.filter(filterValue => filterValue !== value)
  }

  toggleFilter(value) {
    if (this.selectedFilters.indexOf(value) > -1) {
      this.removeSelectedFilter(value);
    } else {
      this.setSelectedFilter(value);
    }
  }
}

export default MultiSelectFilter;
