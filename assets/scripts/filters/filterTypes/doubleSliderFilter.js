import AbstractSliderFilter from './abstractSliderFilter';

class DoubleSliderFilter extends AbstractSliderFilter {
  updateSelection() {
    const from = this.$slider.nstSlider('get_current_min_value');
    const to = this.$slider.nstSlider('get_current_max_value');

    this.selection = {from, to};
  }

  isEmpty() {
    return false;
  }

  reset() {
    // reset slider to min/max
    this.$slider.nstSlider('set_position',
      this.$slider.nstSlider('get_range_min'),
      this.$slider.nstSlider('get_range_max')
    );
  }
}

export default DoubleSliderFilter;
