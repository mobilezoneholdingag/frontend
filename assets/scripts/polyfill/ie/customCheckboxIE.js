import Browser from '../../helpers/browser';

class CustomCheckboxIE {
  constructor() {
    $(() => {
      const $label = $('.custom-checkbox');

      if (Browser.isIE()) {
        $label.on('mouseup', function () { //mouseup instead of click which fires the event twice
          const $this = $(this);
          const $input = $this.find('input:nth-child(2)');
          const $isChecked = $input.is(':checked');

          $input.prop('checked', !$isChecked);
          $input.trigger('change');
        });
      }
    });
  }
}

export default new CustomCheckboxIE;
