import FilterLord from '../filters/filterLord';
import Filter from '../filters/filter';
import Renderer from './subscriptionFilter/renderer';
import Sort from './subscriptionFilter/sort';
import Requestor from './subscriptionFilter/requestor';

class Overview {
  constructor() {
    const eventNamespace = 'tariff';
    const fullPath = window.location.href;
    const sitesWithoutFilter = [/\/smartwatches\//i, /\/tablets\//i, /offer_type=3/i, /\/abo\/\d+\//i];

    if(!this.pageHasFilter(fullPath, sitesWithoutFilter)) {
      return;
    }

    new FilterLord(
      eventNamespace,

      new Filter(
        eventNamespace,
        '.subs-filter-mobile .filter-group, .subs-filter-mobile .filter-group-slider',
        '.subs-filter .filter-group, .subs-filter .filter-group-slider'
      ),

      new Sort(),
      new Requestor(),
      new Renderer()
    );
  }

  pageHasFilter(fullPath, sitesWithoutFilter) {
    let hasFilter = true;
    $(sitesWithoutFilter).each((key, value) => {
      if (value.test(fullPath)) {
        return hasFilter = false;
      }
    });
    return hasFilter;
  }
}

export default Overview;
