class Renderer {
  update(result, clear = false) {
    if (clear) {
      this.clear();
    }
    $('.tariffs-wrapper').append(result);
  }

  clear() {
    $('.tariffs-wrapper').empty();
  }
}

export default Renderer;
