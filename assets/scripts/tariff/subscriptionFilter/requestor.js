class Requestor {
  constructor() {
    this.skipRequest = true;
  }

  request(payload) {
    return new Promise((resolve, reject) => {
      const data = $.extend({
        search: payload.search,
        page: payload.page,
        sort: payload.sort
      }, payload.hiddenFilters);

      Object.keys(payload.filter).forEach((name) => {
        data[name] = payload.filter[name];
      });

      $.ajax({
        url: '/tariff/tariff/filtered-tariffs',
        method: 'GET',
        data: data,
        error: () => {
          reject(-1);
        },
        success: (result) => {
          resolve(result);
        },
        dataType: 'html'
      });
    });
  }
}

export default Requestor;
