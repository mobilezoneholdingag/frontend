class ProductCard {
  constructor() {
    this.$container = $('.tariff-articles');
    this.tariffId = this.$container.data('tariff-id');

    this.addEventListener();
  }

  addEventListener() {
    this.$container.on('click', '.product-card .options a', (event) => {
      event.preventDefault();
      const $target = $(event.currentTarget);
      const $productCard = $target.closest('.product-card');
      const link = $target.attr('href');
      this.loadProductCard($productCard, link);
    });

    this.$container.on('click', '.product-image a', (event) => {
      event.preventDefault();
      const $target = $(event.currentTarget);
      const $productCard = $target.closest('.product-card');
      const $form = $productCard.find('form');
      $form.submit();
    });
  }

  loadProductCard($productCard, link) {
    $.get(link, { tariff_id: this.tariffId, product_card: true })
      .done(function(response) {
        $productCard.replaceWith(response);
      });
  }
}

export default ProductCard;
