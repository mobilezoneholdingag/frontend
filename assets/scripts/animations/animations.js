class Animations
{
    constructor()
    {
        $('.animate').appear();

        $(document.body).on('appear', '.animate', function(e, $affected) {

            var delay = 0;
            $affected.each(function (i, e) {
                if($(e).hasClass('visible') == false) {
                    delay = delay + 1;
                    setTimeout(function (elmn) {
                        $(e).addClass('visible');
                    }, 10 * delay);
                }
            });
        });

        $.force_appear();

    }
}
export default Animations;