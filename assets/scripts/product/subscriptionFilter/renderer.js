class Renderer {
  constructor() {
    this.initPopovers();
  }

  update(result, clear = false) {
    if (clear) {
      this.clear();
    }
    $('.subs-wrapper').append(result);
    this.initPopovers();
  }

  clear() {
    $('.subs-wrapper').empty();
  }

  initPopovers() {
    const $trigger = $('.subs-wrapper [data-toggle="popover"], .tariff-articles [data-toggle="popover"]');

    $trigger.popover({
      html: true,
      placement: 'bottom'
    });
  }
}

export default Renderer;
