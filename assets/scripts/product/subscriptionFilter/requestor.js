import Product from "../product";


class Requestor {
  constructor() {
    this.skipRequest = true;
  }

  request(payload) {
    return new Promise((resolve, reject) => {
      const data = {
        search: payload.search,
        page: payload.page,
        sort: payload.sort
      };

      Object.keys(payload.filter).forEach((name) => {
        data[name] = payload.filter[name];
      });

      data['article'] = Product.getArticleId();

      $.ajax({
        url: '/product/product/filtered-tariffs',
        method: 'GET',
        data: data,
        error: () => {
          reject(-1);
        },
        success: (result) => {
          resolve(result);
        },
        dataType: 'html'
      });
    });
  }
}

export default Requestor;
