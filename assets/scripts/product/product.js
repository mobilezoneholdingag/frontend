import ContractBox from '../contractBox/contractBox';
import ImageGallery from './imageGallery/imageGallery';
import FilterLord from '../filters/filterLord';
import Filter from '../filters/filter';
import Renderer from './subscriptionFilter/renderer';
import Sort from './subscriptionFilter/sort';
import Requestor from './subscriptionFilter/requestor';


class Product {
  constructor() {
    new ContractBox();
    new ImageGallery();

    const eventNamespace = 'product';
    const $mobileFilter = $('#product-filter');

    if (Product.hasTariffs()) {
      new FilterLord(
        eventNamespace,

        new Filter(
          eventNamespace,
          '.subs-filter-mobile .filter-group, .subs-filter-mobile .filter-group-slider',
          '.subs-filter .filter-group, .subs-filter .filter-group-slider',
          {article: Product.getArticleId()}
        ),

        new Sort(),
        new Requestor(),
        new Renderer()
      );
    }

    $mobileFilter.on('click', () => {
      $(document).scrollTop($('.subs-filter-mobile').offset().top-60 );
    });

    $mobileFilter.on('change', (e) => {
      const $activeSlider = $(e.target).parent().find('.nstSlider:not(.disabled)');

      $activeSlider.each(function() {
        $(this).nstSlider('refresh')
      });
    });

    $('.contract-renewal-sliders').find('input[type="checkbox"]').on('click', (e) => {
      const $checkbox = $(e.target);
      const $slider = $checkbox.parents('.form-group').find('.range');

      $slider.toggleClass('disabled');
    });
  }

  static getArticleId() {
    return $("[name='item_id']").val();
  }

  static hasTariffs() {
    return $('.subs-wrapper').length > 0;
  }
}

export default Product;
