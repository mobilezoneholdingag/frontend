class ImageGallery {
  constructor() {
    this.markFirstThumbnailAsActive();
    this.addEventListener();
  }

  addEventListener() {
    this.getThumbnails().on('click', (thumbnail) => {
      this.exchangeMainImage(thumbnail.target);
      (this.getThumbnails()).removeClass('active');
      this.getThumbnailImage(thumbnail.target).parent('.thumbnail').addClass('active');
    });
  }

  getMainImage() {
    return $('.product-gallery .main-image img');
  }

  getThumbnails() {
    return $('.product-gallery .thumbnail');
  }

  exchangeMainImage(thumbnail) {
    const imageUrl = this.getThumbnailImage(thumbnail).prop('src');
    this.getMainImage().prop('src', imageUrl);
  }

  getThumbnailImage(thumbnail) {
    if ($(thumbnail).is('img')) {
      return $(thumbnail);
    }

    return $(thumbnail).find('img').first();
  }

  markFirstThumbnailAsActive() {
    this.getThumbnails().first().addClass('active');
  }
}

export default ImageGallery;
