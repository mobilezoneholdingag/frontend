class StickyBar
{
    constructor() {
        this.$stickyBar = $('#sticky-bar')[0];
        this.$stickyBarStarter = $('#sticky-bar-starter')[0];

        if(this.$stickyBar !== undefined) {
            this.initBar();
        }
    }

    initBar() {
        this.$scrollPoint = 400;

        if(this.$stickyBarStarter !== undefined) {
            this.$scrollPoint = $(this.$stickyBarStarter).offset().top;
        }

        this.$window = $(window);

        this.check();

        $(window).scroll((event) => {
            this.check();
        });

    }

    check() {
        if(this.$scrollPoint < this.$window.scrollTop()) {
            $(this.$stickyBar).fadeIn();
        } else {
            $(this.$stickyBar).fadeOut();
        }
    }

}

export default StickyBar;