#!/bin/sh
cd ../../
cp docker-frontend/code/Dockerfile .
docker build -t deinhandy/${1}stardust-mobilezone-frontend .
rm Dockerfile