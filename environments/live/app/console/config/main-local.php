<?php
return [
	'components' => [
		'redis' => [
			'database' => 2,
		],
		'redis-session' => [
			'database' => 3,
		],
	],
];
