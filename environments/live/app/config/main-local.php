<?php
return [
	'components' => [
		'urlManager' => [
			'class' => app\services\i18n\UrlManager::class,
			'hostInfo' => 'http://www.mobilezone.ch'
		],
		'redis' => [
			'database' => 2,
		],
		'redis-session' => [
			'database' => 3,
		],
	],
];
