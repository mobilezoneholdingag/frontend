<?php
return [
	'components' => [
		'urlManager' => [
			'class' => app\services\i18n\UrlManager::class,
			'hostInfo' => 'http://www.vokky.net'
		],
	],
];
