<?php

return [
	'localeMatchName' => 'vokky',
	'apiClient' => [
		'logger' => \app\services\api\ApiClientConsoleLogger::class,
		'caching' => [
			'entities' => [
				\app\models\LandingPageModel::API_ENTITY_NAME => false,
			],
		],
	],
	'environment_stage' => 'staging',
];
