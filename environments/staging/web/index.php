<?php
defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'prod');

require(__DIR__ . '/../vendor/autoload.php');
require(__DIR__ . '/../vendor/yiisoft/yii2/Yii.php');
require(__DIR__ . '/../app/config/bootstrap.php');

$config = yii\helpers\ArrayHelper::merge(
	require app_path('config/main.php'),
	require app_path('config/main-local.php')
);

$application = new \app\LocalizedApplication($config);
$application->run();
