<?php
return [
	'bootstrap' => [
		[
			'class' => \app\twig\TwigNamespaceGenerator::class,
		],
	],
	'components' => [
		'urlManager' => [
			'class' => app\services\i18n\UrlManager::class,
			'hostInfo' => 'http://www.stardust.dev',
		],
	],
];
