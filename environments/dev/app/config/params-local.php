<?php

return [
	'localeMatchName' => 'stardust',
	'language_cache_duration' => 10,
	'browserLiveReload' => true,
	'assets' => [
		'path' => '/build/',
		'cdn' => [
			'url' => 'http://0f83e797ea84f6a76701-e6b47ff6c39afef955a5084761be7cb6.r82.cf3.rackcdn.com/images/',
			'url_secure' => 'https://e18cdd5126ab0efb3a1e-e6b47ff6c39afef955a5084761be7cb6.ssl.cf3.rackcdn.com/images/',
		]
	],
	'apiClient' => [
		'logger' => \app\services\api\ApiClientConsoleLogger::class,
	],
	'environment_stage' => 'dev',
    'luya' => [
        'url' => 'http://172.18.0.1:5000/',
        'token' => 'test',
    ]
];
