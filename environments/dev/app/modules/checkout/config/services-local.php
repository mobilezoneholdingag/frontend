<?php

use app\modules\checkout\services\cart\PaymentBillDataExtractor;
use app\modules\checkout\services\cart\PaymentBillMockDataExtractor;

Yii::$container->setSingleton(PaymentBillDataExtractor::class, PaymentBillMockDataExtractor::class);
