<?php
return [
	'components' => [
		'redis' => [
			'database' => 4,
		],
		'redis-session' => [
			'database' => 5,
		],
	],
];
