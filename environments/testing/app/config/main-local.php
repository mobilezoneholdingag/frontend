<?php
return [
	'components' => [
		'urlManager' => [
			'class' => app\services\i18n\UrlManager::class,
			'hostInfo' => 'http://www.mztesting.ch'
		],
		'redis' => [
			'database' => 4,
		],
		'redis-session' => [
			'database' => 5,
		],
	],
];
