<?php
return [
    'components' => [
        'urlManager' => [
            'class' => app\services\i18n\UrlManager::class,
            'hostInfo' => 'http://www.mztesting2.ch'
        ],
        'redis' => [
            'database' => 6,
        ],
        'redis-session' => [
            'database' => 7,
        ],
    ],
];