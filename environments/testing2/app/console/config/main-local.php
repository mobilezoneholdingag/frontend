<?php
return [
    'components' => [
        'redis' => [
            'database' => 6,
        ],
        'redis-session' => [
            'database' => 7,
        ],
    ],
];
