<?php
namespace tests\codeception\frontend;
use tests\codeception\frontend\Page\HardwareOnlyProduct;
use tests\codeception\frontend\Page\Product;
use tests\codeception\frontend\Page\RegularProduct;
use tests\codeception\frontend\Page\SimOnlyProduct;
use tests\codeception\frontend\Page\Address;

/**
 * All these tests are supposed to do a fully successful checkout with different configuration.
 *
 * @package tests\codeception\frontend
 */
class CheckoutSuccessfullyCest
{
	/**
	 * This test enters a different-to-the-billing-address delivery address, and verifies, that it is properly
	 * shown on the overview page.
	 *
	 * @group germany
	 * @param AcceptanceTester $I
	 */
	public function checkoutWithDeliveryAddressAndSeeItOnOverview(AcceptanceTester $I)
	{
		$fixtures = [
			// address data
			'salutation' => Address::SALUTATION_MR,
			'firstname' => 'Jochen',
			'lastname' => 'Wookie',
			'street' => 'Galaktische Straße',
			'houseNumber' => '1337',
			'zipCode' => '78956',
			'city' => 'Coruscant',
			'phoneNumber' => '01729384738479',
			'email' => 'test@deinhandy.de',
			// delivery address data
			'delivery_company' => 'Planet Express',
			'delivery_street' => 'Broadway 123',
			'delivery_houseNumber' => '23 C/8',
			'delivery_zipCode' => '85695',
			'delivery_city' => 'New York, NY',
			// contract data
			'dateOfBirth' => (date('Y') - 20) . '-03-22',
			'idNumber' => '1220001297',
			'bankAccountNumber' => '7237985',
			'bankCode' => '76026000',
			'placeOfBirth' => 'Entenhausen',
			'validityDate' => (date('Y') + 2) . '-05-08',
			'placeOfIssue' => 'Entenhausen City',
		];

		$product = new RegularProduct($I, Product::REGULAR_PRODUCT_URL);

		$cart = $product->putIntoCart();
		$cart->isCorrectPage();

		$address = $cart->clickCheckoutButton();
		$address->isCorrectPage();
		$address->fillAddress($fixtures);
		$address->fillDeliveryAddress($fixtures);

		$contract = $address->clickContinue();
		$contract->isCorrectPage();
		$contract->fillIdNumber($fixtures['idNumber']);
		$contract->selectDateOfBirth($fixtures['dateOfBirth']);
		$contract->fillPlaceOfBirth($fixtures['placeOfBirth']);
		$contract->selectValidityDate($fixtures['validityDate']);
		$contract->fillPlaceOfIssue($fixtures['placeOfIssue']);
		$contract->checkAcceptYoungTariffSpecialStatus();
		$contract->selectBankAccountTypeAccountAndBankCode();
		$contract->fillBankAccountNumber($fixtures['bankAccountNumber']);
		$contract->fillBankAccountBankCode($fixtures['bankCode']);

		$overview = $contract->clickContinue();
		$overview->isCorrectPage();
		$overview->seeDeliveryAddress($fixtures);
	}

	/**
	 * Tests the successful checkout process with a sim only product.
	 *
	 * @group germany
	 * @param AcceptanceTester $I
	 */
	public function checkoutWithSimOnlySuccessfully(AcceptanceTester $I)
	{
		$fixtures = [
			// address data
			'salutation' => Address::SALUTATION_MR,
			'firstname' => 'Horst',
			'lastname' => 'Kevin',
			'street' => 'Imbastraße',
			'houseNumber' => '42 A',
			'zipCode' => '10551',
			'city' => 'Berlin',
			'phoneNumber' => '01511234567689',
			'email' => 'test@deinhandy.de',
			// contract data
			'idNumber' => '1220001297',
			'bankAccountNumber' => '7237985',
			'bankCode' => '76026000',
			'dateOfBirth' => '1987-02-01',
			'placeOfBirth' => 'Berlin',
			'validityDate' => (date('Y') + 5) . '-09-23',
			'placeOfIssue' => 'Berlin-Reinickendorf',
		];

		$soProduct = new SimOnlyProduct($I, Product::SIM_ONLY_PRODUCT_URL);

		$cart = $soProduct->putIntoCart();
		$cart->isCorrectPage();
		$cart->seePricesForProduct($soProduct);
		$cart->seeNamesOfProduct($I, $soProduct);

		$address = $cart->clickCheckoutButton();
		$address->isCorrectPage();
		$address->seeNamesOfProduct($I, $soProduct);
		$address->fillAddress($fixtures);

		$contract = $address->clickContinue();
		$contract->isCorrectPage();
		$contract->seeNamesOfProduct($I, $soProduct);
		$contract->fillIdNumber($fixtures['idNumber']);
		$contract->selectDateOfBirth($fixtures['dateOfBirth']);
		$contract->fillPlaceOfBirth($fixtures['placeOfBirth']);
		$contract->selectValidityDate($fixtures['validityDate']);
		$contract->fillPlaceOfIssue($fixtures['placeOfIssue']);
		$contract->selectBankAccountTypeAccountAndBankCode();
		$contract->fillBankAccountNumber($fixtures['bankAccountNumber']);
		$contract->fillBankAccountBankCode($fixtures['bankCode']);

		$overview = $contract->clickContinue();
		$overview->isCorrectPage();
		$overview->seeNamesOfProduct($I, $soProduct);
		$overview->seePricesForProduct($soProduct);
		$overview->seePersonalAddressData($fixtures);
		$overview->seeDeliveryAddressByAddress($fixtures);
		$overview->seeDateOfBirth($fixtures['dateOfBirth']);
		$overview->seePassportIdNumber($fixtures['idNumber']);
		$overview->seeBankAccountDetails($fixtures);
		$overview->fillProviderPasswordAndConfirmation('r4nd0mp455w0rd.!');
		$overview->checkAcceptTos();
		$overview->checkAcceptProviderTos();
		$overview->checkAcceptSolvencyCheck();
		
		$success = $overview->clickBuyNow();
		$success->isCorrectPage();
	}

	/**
	 * Tests the successful checkout process with a regular (sim & hardware) product in Germany.
	 *
	 * @group germany
	 * @param AcceptanceTester $I
	 */
	public function checkoutWithRegularProductSuccessfullyInGermany(AcceptanceTester $I)
	{
		$fixtures = [
			// address data
			'salutation' => Address::SALUTATION_MRS,
			'firstname' => 'Bart',
			'lastname' => 'Simpson',
			'street' => 'Evergreen terrace',
			'houseNumber' => '1337 C',
			'zipCode' => '78956',
			'city' => 'Springfield',
			'phoneNumber' => '01621234567689',
			'email' => 'test@deinhandy.de',
			// contract data
			'idNumber' => '1220001297',
			'bankAccountNumber' => '7237985',
			'bankCode' => '76026000',
			'dateOfBirth' => '1956-08-23',
			'placeOfBirth' => 'Warschau',
			'validityDate' => (date('Y') + 3) . '-02-12',
			'placeOfIssue' => 'Nirvana',
		];

		$product = new RegularProduct($I, Product::REGULAR_PRODUCT_URL);

		$cart = $product->putIntoCart();
		$cart->isCorrectPage();
		$cart->seePricesForProduct($product);
		$cart->seeNamesOfProduct($I, $product);

		$address = $cart->clickCheckoutButton();
		$address->isCorrectPage();
		$address->seeNamesOfProduct($I, $product);
		$address->fillAddress($fixtures);

		$contract = $address->clickContinue();
		$contract->isCorrectPage();
		$contract->seeNamesOfProduct($I, $product);
		$contract->fillIdNumber($fixtures['idNumber']);
		$contract->selectDateOfBirth($fixtures['dateOfBirth']);
		$contract->fillPlaceOfBirth($fixtures['placeOfBirth']);
		$contract->selectValidityDate($fixtures['validityDate']);
		$contract->fillPlaceOfIssue($fixtures['placeOfIssue']);
		$contract->checkAcceptYoungTariffSpecialStatus();
		$contract->selectBankAccountTypeAccountAndBankCode();
		$contract->fillBankAccountNumber($fixtures['bankAccountNumber']);
		$contract->fillBankAccountBankCode($fixtures['bankCode']);

		$overview = $contract->clickContinue();
		$overview->isCorrectPage();
		$overview->seeNamesOfProduct($I, $product);
		$overview->seePricesForProduct($product);
		$overview->seePersonalAddressData($fixtures);
		$overview->seeDeliveryAddressByAddress($fixtures);
		$overview->seeDateOfBirth($fixtures['dateOfBirth']);
		$overview->seePassportIdNumber($fixtures['idNumber']);
		$overview->seeBankAccountDetails($fixtures);
		$overview->selectPaymentTypePrepayment();
		$overview->checkAcceptTos();
		$overview->checkAcceptProviderTos();
		$overview->checkAcceptSolvencyCheck();

		$success = $overview->clickBuyNow();
		$success->isCorrectPage();
	}

	/**
	 * Tests the successful checkout process with a regular (sim & hardware) product in Switzerland.
	 *
	 * @group switzerland
	 * @param AcceptanceTester $I
	 */
	public function checkoutWithRegularProductSuccessfullyInSwitzerland(AcceptanceTester $I)
	{
		$fixtures = [
			// address data
			'salutation' => Address::SALUTATION_MR,
			'firstname' => 'Fred',
			'lastname' => 'Flintstone',
			'street' => 'Lange Strasse',
			'houseNumber' => '85',
			'zipCode' => '5685',
			'city' => 'Puchheim',
			'phoneNumber' => '089582998',
			'email' => 'test@deinhandy.de',
			// contract data
			'idNumber' => 'tolleSchweizerPassId',
			'dateOfBirth' => (date('Y') - 19) . '-04-05',
			'placeOfBirth' => 'Aquarius',
			'validityDate' => (date('Y') + 3) . '-02-12',
			'placeOfIssue' => 'Tweat',
		];

		$product = new RegularProduct($I, Product::REGULAR_PRODUCT_URL_CH);

		$cart = $product->putIntoCart();
		$cart->isCorrectPage();
		$cart->seeNamesOfProduct($I, $product);
		// This test does intentionally not check for prices as of now, because

		$address = $cart->clickCheckoutButton();
		$address->isCorrectPage();
		$address->seeNamesOfProduct($I, $product);
		$address->fillAddress($fixtures);

		$contract = $address->clickContinue();
		$contract->isCorrectPage();
		$contract->seeNamesOfProduct($I, $product);
		$contract->fillIdNumber($fixtures['idNumber']);
		$contract->selectDateOfBirth($fixtures['dateOfBirth']);
		$contract->fillPlaceOfBirth($fixtures['placeOfBirth']);
		$contract->selectValidityDate($fixtures['validityDate']);
		$contract->fillPlaceOfIssue($fixtures['placeOfIssue']);
		$contract->checkAcceptYoungTariffSpecialStatus();

		$overview = $contract->clickContinue();
		$overview->isCorrectPage();
		$overview->seeNamesOfProduct($I, $product);
		$overview->seePersonalAddressData($fixtures);
		$overview->seeDeliveryAddressByAddress($fixtures);
		$overview->seeDateOfBirth($fixtures['dateOfBirth']);
		$overview->seePassportIdNumber($fixtures['idNumber']);
		$overview->checkAcceptTos();
		$overview->checkAcceptProviderTos();
		$overview->checkAcceptSolvencyCheck();

		$success = $overview->clickBuyNow();
		$success->isCorrectPage();
	}

	/**
	 * Tests the successful checkout process with a hardware only product.
	 *
	 * @group germany
	 * @param AcceptanceTester $I
	 */
	public function checkoutWithHardwareOnlySuccessfully(AcceptanceTester $I)
	{
		$fixtures = [
			// address data
			'salutation' => Address::SALUTATION_MRS,
			'firstname' => 'Peter',
			'lastname' => 'Griffon',
			'street' => 'Rhode island ave',
			'houseNumber' => '90210',
			'zipCode' => '85685',
			'city' => 'Quahog',
			'phoneNumber' => '01731234567689',
			'email' => 'test@deinhandy.de',
			// contract data
			'idNumber' => '1220001297',
			'bankAccountNumber' => '7237985',
			'bankCode' => '76026000',
			'dateOfBirth' => '1956-08-23',
			'placeOfBirth' => 'New England',
			'validityDate' => (date('Y') + 7) . '-02-12',
			'placeOfIssue' => 'New York',
		];

		$hoProduct = new HardwareOnlyProduct($I, Product::HARDWARE_ONLY_PRODUCT_URL);

		$cart = $hoProduct->putIntoCart();
		$cart->isCorrectPage();
		$cart->seePricesForProduct($hoProduct);
		$cart->seeNamesOfProduct($I, $hoProduct);

		$address = $cart->clickCheckoutButton();
		$address->isCorrectPage();
		$address->seeNamesOfProduct($I, $hoProduct);
		$address->fillAddress($fixtures);

		$overview = $address->clickContinue();
		$overview->isCorrectPage();
		$overview->seePricesForProduct($hoProduct);
		$overview->seeNamesOfProduct($I, $hoProduct);
		$overview->seePersonalAddressData($fixtures);
		$overview->seeDeliveryAddressByAddress($fixtures);
		$overview->selectPaymentTypePrepayment();
		$overview->checkAcceptTos();

		$success = $overview->clickBuyNow();
		$success->isCorrectPage();
	}
}
