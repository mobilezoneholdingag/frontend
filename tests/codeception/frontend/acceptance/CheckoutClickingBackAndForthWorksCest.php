<?php
namespace tests\codeception\frontend;

use tests\codeception\frontend\Page\Overview;
use tests\codeception\frontend\Page\Product;
use tests\codeception\frontend\Page\RegularProduct;
use tests\codeception\frontend\Page\SimOnlyProduct;
use tests\codeception\frontend\Page\Address;


/**
 * These tests are supposed to check all back-and-forth clicking functionality within the checkout.
 *
 * @package tests\codeception\frontend
 */
class CheckoutClickingBackAndForthWorksCest
{
	/**
	 * Checks that clicking "back" from the cart leads back to the product you selected previously.
	 *
	 * This is itentionally separated from the bigger test in clickBackAndForth(), as the form data in the checkout
	 * would get lost here otherwise.
	 *
	 * @group germany
	 * @param AcceptanceTester $I
	 */
	public function backButtonFromCartLeadsToPreviousProductInGermany(AcceptanceTester $I)
	{
		$soProduct = new SimOnlyProduct($I, Product::SIM_ONLY_PRODUCT_URL);
		$this->testBackButtonFromCartLeadsToPreviousProduct($soProduct, $I);
	}

	/**
	 * @todo Interim solution until we have proper test products. Group "common" should be used instead.
	 * @see backButtonFromCartLeadsToPreviousProductInGermany
	 * @group switzerland
	 * @param AcceptanceTester $I
	 */
	public function backButtonFromCartLeadsToPreviousProductInSwitzerland(AcceptanceTester $I)
	{
		$product = new RegularProduct($I, Product::REGULAR_PRODUCT_URL_CH);
		$this->testBackButtonFromCartLeadsToPreviousProduct($product, $I);
	}

	/**
	 * @see backButtonFromCartLeadsToPreviousProductInGermany
	 * @param Product $product
	 * @param AcceptanceTester $I
	 */
	private function testBackButtonFromCartLeadsToPreviousProduct(Product $product, AcceptanceTester $I)
	{
		$cart = $product->putIntoCart();
		$cart->isCorrectPage();

		$sameSoProduct = $cart->clickBackButton($product);
		$sameSoProduct->isCorrectPage();
	}


	/**
	 * This test checks all back and continue buttons within the checkout, from cart to overview page. It does not
	 * actually trigger an order, as this is covered by other tests.
	 *
	 * Test steps:
	 * 1. Fill all required form data in all steps
	 * 		1.1. Continue in cart to reach the address page
	 * 		1.2. Go to address page, fill in address
	 * 		1.3. Go to contract page, fill in contract details
	 * 		1.4. Go to overview page (to submit the contract details page)
	 * 2. Click back through all steps until the cart is reached
	 * 3. Click forward through all steps until the overview is reached again
	 * 4. Click "change now" for address
	 * 5. Click forward through all steps until the overview is reached again
	 * 6. Click "change now" for delivery address
	 * 7. Click forward through all steps until the overview is reached again
	 * 8. Click "change now" for contract details
	 * 9. Click continue
	 * 10. Verify overview page, done!
	 *
	 * @group germany
	 * @param AcceptanceTester $I
	 */
	public function clickBackAndForthInGermany(AcceptanceTester $I)
	{
		/**
		 * Step 1. Fill all required form data in all steps
		 */
		$fixtures = [
			// address data
			'salutation' => Address::SALUTATION_MRS,
			'firstname' => 'Jutta',
			'lastname' => 'Butter',
			'street' => 'Keksweg',
			'houseNumber' => '1200',
			'zipCode' => '13511',
			'city' => 'Bielefeld',
			'phoneNumber' => '0151098762638',
			'email' => 'test@deinhandy.de',
			// contract data
			'idNumber' => '1220001297',
			'bankAccountNumber' => '7237985',
			'bankCode' => '76026000',
			'dateOfBirth' => '1956-08-22',
			'placeOfBirth' => 'Hamburg',
			'validityDate' => (date('Y') + 5) . '-02-21',
			'placeOfIssue' => 'Eisenhüttenstadt',
		];

		$soProduct = new SimOnlyProduct($I, Product::SIM_ONLY_PRODUCT_URL);

		$cart = $soProduct->putIntoCart();
		$cart->isCorrectPage();

		$address = $cart->clickCheckoutButton();
		$address->isCorrectPage();
		$address->fillAddress($fixtures);

		$contract = $address->clickContinue();
		$contract->isCorrectPage();
		$contract->fillIdNumber($fixtures['idNumber']);
		$contract->selectDateOfBirth($fixtures['dateOfBirth']);
		$contract->fillPlaceOfBirth($fixtures['placeOfBirth']);
		$contract->selectValidityDate($fixtures['validityDate']);
		$contract->fillPlaceOfIssue($fixtures['placeOfIssue']);
		$contract->selectBankAccountTypeAccountAndBankCode();
		$contract->fillBankAccountNumber($fixtures['bankAccountNumber']);
		$contract->fillBankAccountBankCode($fixtures['bankCode']);

		$overview = $contract->clickContinue();
		$overview->isCorrectPage();

		// Steps 2 to 10 are separated as they are the same in both countries
		$this->testClickingBackAndForthFromOverviewPage($overview);
	}

	/**
	 * This test checks all back and continue buttons within the checkout, from cart to overview page. It does not
	 * actually trigger an order, as this is covered by other tests.
	 *
	 * @see clickBackAndForthInGermany
	 *
	 * @group switzerland
	 * @param AcceptanceTester $I
	 */
	public function clickBackAndForthInSwitzerland(AcceptanceTester $I)
	{
		/**
		 * Step 1. Fill all required form data in all steps
		 */
		$fixtures = [
			// address data
			'salutation' => Address::SALUTATION_MRS,
			'firstname' => 'Jutta',
			'lastname' => 'Butter',
			'street' => 'Keksweg',
			'houseNumber' => '1200',
			'zipCode' => '0815',
			'city' => 'Bielefeld',
			'phoneNumber' => '0151098762638',
			'email' => 'test@deinhandy.de',
			// contract data
			'idNumber' => '1220001297',
			'bankAccountNumber' => '7237985',
			'bankCode' => '76026000',
			'dateOfBirth' => (date('Y') - 20) . '-08-22',
			'placeOfBirth' => 'Hamburg',
			'validityDate' => (date('Y') + 5) . '-02-21',
			'placeOfIssue' => 'Eisenhüttenstadt',
		];

		$soProduct = new RegularProduct($I, Product::REGULAR_PRODUCT_URL_CH);

		$cart = $soProduct->putIntoCart();
		$cart->isCorrectPage();

		$address = $cart->clickCheckoutButton();
		$address->isCorrectPage();
		$address->fillAddress($fixtures);

		$contract = $address->clickContinue();
		$contract->isCorrectPage();
		$contract->fillIdNumber($fixtures['idNumber']);
		$contract->selectDateOfBirth($fixtures['dateOfBirth']);
		$contract->fillPlaceOfBirth($fixtures['placeOfBirth']);
		$contract->selectValidityDate($fixtures['validityDate']);
		$contract->fillPlaceOfIssue($fixtures['placeOfIssue']);
		$contract->checkAcceptYoungTariffSpecialStatus();

		$overview = $contract->clickContinue();
		$overview->isCorrectPage();
	}

	/**
	 * DRY-decoupling of clicking back and forth tests from the overview.
	 *
	 * @param Overview $overview
	 */
	private function testClickingBackAndForthFromOverviewPage(Overview $overview)
	{
		/**
		 * Step 2. Click back through all steps until the cart is reached
		 */
		$contract = $overview->clickBackButton();
		$contract->isCorrectPage();

		$address = $contract->clickBackButton();
		$address->isCorrectPage();

		$cart = $address->clickBackButton();
		$cart->isCorrectPage();

		/**
		 * Step 3. Click forward through all steps until the overview is reached again
		 */
		$address = $cart->clickCheckoutButton();
		$address->isCorrectPage();

		$contract = $address->clickContinue();
		$contract->isCorrectPage();

		$overview = $contract->clickContinue();
		$overview->isCorrectPage();

		/**
		 * Step 4. Click "change now" for address
		 */
		$address = $overview->clickChangeAddress();
		$address->isCorrectPage();

		/**
		 * Step 5. Click forward through all steps until the overview is reached again
		 */
		$contract = $address->clickContinue();
		$contract->isCorrectPage();

		$overview = $contract->clickContinue();
		$overview->isCorrectPage();

		/**
		 * Step 6. Click "change now" for delivery address
		 */
		$address = $overview->clickChangeDeliveryAddress();
		$address->isCorrectPage();

		/**
		 * Step 7. Click forward through all steps until the overview is reached again
		 */
		$contract = $address->clickContinue();
		$contract->isCorrectPage();

		$overview = $contract->clickContinue();
		$overview->isCorrectPage();

		/**
		 * Step 8. Click "change now" for contract details
		 */
		$contract = $overview->clickChangeContractDetails();
		$contract->isCorrectPage();

		/**
		 * Step 9. Click continue
		 */
		$overview = $contract->clickContinue();

		/**
		 * Step 10. Verify overview page, done!
		 */
		$overview->isCorrectPage();
	}
}
