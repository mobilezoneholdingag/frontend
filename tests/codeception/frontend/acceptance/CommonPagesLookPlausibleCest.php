<?php
namespace tests\codeception\frontend;

use tests\codeception\frontend\Page\Home;
use tests\codeception\frontend\Page\Pathfinder;
use tests\codeception\frontend\Page\Smartphones;

/**
 * These tests are supposed to check common pages for plausible appearance and availabilty.
 *
 * @package tests\codeception\frontend
 */
class CommonPagesLookPlausibleCest
{
	/**
	 * @group common
	 * @param AcceptanceTester $I
	 */
	public function commonPagesLookPlausible(AcceptanceTester $I)
	{
		$home = new Home($I);
		$home->goToPage();
		$home->isCorrectPage();

		$smartphones = new Smartphones($I);
		$smartphones->goToPage();
		$smartphones->isCorrectPage();
		$smartphones->seeFirstProductImage();

		$home->goToPage();
		$tariffs = $home->clickTariffOverviewLink();
		$tariffs->isCorrectPage();
		$tariffs->seeFirstProductImage();
	}

	/**
	 * @group germany
	 * @param AcceptanceTester $I
	 */
	public function pathfinderPageLooksPlausible(AcceptanceTester $I)
	{
		$pathfinder = new Pathfinder($I);
		$pathfinder->goToPage();
		$pathfinder->isCorrectPage();
	}
}
