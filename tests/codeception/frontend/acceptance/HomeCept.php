<?php
use tests\codeception\frontend\AcceptanceTester;

/** @var CodeCeption\Scenario $scenario */

$I = new AcceptanceTester($scenario);
$I->wantTo('ensure that home page works');
$I->amOnPage('/');
$I->seeInTitle('Dein Handy. Dein Tarif. Ganz Einfach.');
$I->see('Unsere Top-10-Handys');
$I->see('Unsere Provider');
$I->see('Handys nach Herstellern');
$I->seeLink('Alle anzeigen', '/smartphones');