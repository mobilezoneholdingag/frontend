<?php

namespace tests\codeception\frontend;

use tests\codeception\frontend\Page\Address;
use tests\codeception\frontend\Page\Cart;
use tests\codeception\frontend\Page\Contract;
use tests\codeception\frontend\Page\Product;
use tests\codeception\frontend\Page\RegularProduct;

/**
 * These tests are supposed to check all checkout input server validation, in order to be safe about invalid input
 * getting caught before an order can be created.
 *
 * @package tests\codeception\frontend
 */
class CheckoutFailsWithInvalidInputCest
{
	/**
	 * @group switzerland
	 * @param AcceptanceTester $I
	 */
	public function failValidationForRegularProductInSwitzerland(AcceptanceTester $I)
	{
		$fixtures = [
			// address data
			'salutation' => Address::SALUTATION_MRS,
			'firstname' => 'Bart',
			'lastname' => 'Simpson',
			'street' => 'Evergreen terrace',
			'houseNumber' => '1337 C',
			'zipCode' => '7895',
			'city' => 'Springfield',
			'phoneNumber' => '01621234567689',
			'email' => 'test@deinhandy.de',
			// contract data
			'idNumber' => '1220001297',
			'bankAccountNumber' => '7237985',
			'bankCode' => '76026000',
			'dateOfBirth' => (date('Y') - 20) . '-08-23',
			'placeOfBirth' => 'Warschau',
			'validityDate' => (date('Y') + 3) . '-02-12',
			'placeOfIssue' => 'Nirvana',
		];

		$product = new RegularProduct($I, Product::REGULAR_PRODUCT_URL_CH);

		$cart = $product->putIntoCart();
		$cart->isCorrectPage();

		$address = $cart->clickCheckoutButton();
		$address->isCorrectPage();

		foreach ($this->getInvalidAddresses($fixtures['zipCode']) as $invalidAddress) {
			$address->fillAddress($invalidAddress);
			$address = $address->clickContinue();
			$address->isCorrectPage();
			$address->seeValidationError($I);
		}

		$address->fillAddress($fixtures);

		$contract = $address->clickContinue();
		$contract->isCorrectPage();

		$invalidContractData = $this->getInvalidContractData(
			$fixtures,
			[
				'idNumber',
				'dateOfBirth',
				'placeOfBirth',
				'validityDate',
				'placeOfIssue',
			]
		);

		foreach ($invalidContractData as $contractData) {
			$this->fillPassportData($contract, $contractData);
			$contract->checkAcceptYoungTariffSpecialStatus();

			$contract = $contract->clickContinue();
			$contract->isCorrectPage();
			$contract->seeValidationError($I);
		}

		$this->fillPassportData($contract, $fixtures);
		$contract->checkAcceptYoungTariffSpecialStatus();

		$overview = $contract->clickContinue();
		$overview->isCorrectPage();

		// TOS check missing
		$overview->checkAcceptProviderTos();
		$overview->checkAcceptSolvencyCheck();
		$overview->clickBuyNow();
		$overview->seeValidationError($I);

		// Provider TOS check missing
		$overview->checkAcceptTos();
		$overview->checkAcceptSolvencyCheck();
		$overview->clickBuyNow();
		$overview->seeValidationError($I);

		// Solvency check check missing
		$overview->checkAcceptTos();
		$overview->checkAcceptProviderTos();
		$overview->clickBuyNow();
		$overview->seeValidationError($I);

		// This test does intentionally not actually order a product, as its sole purpose is to check that the
		// validation is working. Checking that the checkout works sucessfully has a dedicated test.
	}

	/**
	 * @group germany
	 * @param AcceptanceTester $I
	 */
	public function failValidationForRegularProductInGermany(AcceptanceTester $I)
	{
		$fixtures = [
			// address data
			'salutation' => Address::SALUTATION_MRS,
			'firstname' => 'Bart',
			'lastname' => 'Simpson',
			'street' => 'Evergreen terrace',
			'houseNumber' => '1337 C',
			'zipCode' => '78958',
			'city' => 'Springfield',
			'phoneNumber' => '01621234567689',
			'email' => 'test@deinhandy.de',
			// contract data
			'idNumber' => '1220001297',
			'bankAccountNumber' => '7237985',
			'bankCode' => '76026000',
			'dateOfBirth' => '1956-08-23',
			'placeOfBirth' => 'Warschau',
			'validityDate' => (date('Y') + 3) . '-02-12',
			'placeOfIssue' => 'Nirvana',
		];

		$product = new RegularProduct($I, Product::REGULAR_PRODUCT_URL);

		$cart = $product->putIntoCart();
		$cart->isCorrectPage();

		$this->runCouponTests($I, $cart);

		$address = $cart->clickCheckoutButton();
		$address->isCorrectPage();

		foreach ($this->getInvalidAddresses($fixtures['zipCode']) as $invalidAddress) {
			$address->fillAddress($invalidAddress);
			$address = $address->clickContinue();
			$address->isCorrectPage();
			$address->seeValidationError($I);
		}

		$address->fillAddress($fixtures);

		$contract = $address->clickContinue();
		$contract->isCorrectPage();

		$invalidContractData = $this->getInvalidContractData(
			$fixtures,
			[
				'idNumber',
				'dateOfBirth',
				'placeOfBirth',
				'validityDate',
				'placeOfIssue',
				'bankAccountNumber',
				'bankCode'
			]
		);

		foreach ($invalidContractData as $contractData) {
			$this->fillPassportData($contract, $contractData);
			$contract->checkAcceptYoungTariffSpecialStatus();
			$this->fillBankAccountData($contract, $contractData);

			$contract = $contract->clickContinue();
			$contract->isCorrectPage();
			$contract->seeValidationError($I);
		}

		$this->fillPassportData($contract, $fixtures);
		$contract->checkAcceptYoungTariffSpecialStatus();
		$this->fillBankAccountData($contract, $fixtures);

		$overview = $contract->clickContinue();
		$overview->isCorrectPage();

		// Payment type selection missing
		$overview->checkAcceptTos();
		$overview->checkAcceptProviderTos();
		$overview->checkAcceptSolvencyCheck();
		$overview->clickBuyNow();
		$overview->seeValidationError($I);

		// TOS check missing
		$overview->selectPaymentTypePrepayment();
		$overview->checkAcceptProviderTos();
		$overview->checkAcceptSolvencyCheck();
		$overview->clickBuyNow();
		$overview->seeValidationError($I);

		// Provider TOS check missing
		$overview->selectPaymentTypePrepayment();
		$overview->checkAcceptTos();
		$overview->checkAcceptSolvencyCheck();
		$overview->clickBuyNow();
		$overview->seeValidationError($I);

		// Solvency check check missing
		$overview->selectPaymentTypePrepayment();
		$overview->checkAcceptTos();
		$overview->checkAcceptProviderTos();
		$overview->clickBuyNow();
		$overview->seeValidationError($I);

		// This test does intentionally not actually order a product, as its sole purpose is to check that the
		// validation is working. Checking that the checkout works sucessfully has a dedicated test.
	}

	/**
	 * @param array $fixtures
	 * @param array $attributesToUse
	 * @return array
	 */
	private function getInvalidContractData(array $fixtures, array $attributesToUse)
	{
		$contractData = [];
		$originalData = [];

		// Cut out slice of contract data
		foreach ($attributesToUse as $index) {
			$originalData[$index] = $fixtures[$index];
		}
		$reverseOriginalData = $originalData;

		// One element missing from the back
		foreach ($originalData as $moot) {
			array_pop($originalData);
			$contractData[] = $originalData;
		}

		// One element missing from the start
		foreach ($reverseOriginalData as $moot) {
			array_shift($reverseOriginalData);
			$contractData[] = $reverseOriginalData;
		}

		return $contractData;
	}

	/**
	 * @param string $zipCode
	 * @return array 2-dimensional array. Each element is an array of invalid address data.
	 */
	private function getInvalidAddresses($zipCode)
	{
		$addresses = [];

		$validAdress = [
			'salutation' => Address::SALUTATION_MRS,
			'firstname' => 'Bart',
			'lastname' => 'Simpson',
			'street' => 'Evergreen terrace',
			'houseNumber' => '1337 C',
			'zipCode' => $zipCode,
			'city' => 'Springfield',
			'phoneNumber' => '01621234567689',
			'email' => 'test@deinhandy.de'
		];
		$reverseValidAddress = $validAdress;

		// One or more elements missing, starting from the end
		while (empty($validAdress) == false) {
			array_pop($validAdress);
			$addresses[] = $validAdress;
		}

		// One or more elements missing, starting from the front
		while (empty($reverseValidAddress) == false) {
			array_shift($reverseValidAddress);
			$addresses[] = $reverseValidAddress;
		}

		// Everything too short
		$addresses[] = [
			'salutation' => Address::SALUTATION_MRS,
			'firstname' => 'B',
			'lastname' => 'S',
			'street' => 'E',
			'houseNumber' => '1',
			'zipCode' => '7',
			'city' => 'S',
			'phoneNumber' => '0',
			'email' => 't'
		];

		// Invalid salutation
		$addresses[] = [
			'salutation' => 'X',
			'firstname' => 'Bart',
			'lastname' => 'Simpson',
			'street' => 'Evergreen terrace',
			'houseNumber' => '1337 C',
			'zipCode' => $zipCode,
			'city' => 'Springfield',
			'phoneNumber' => '01621234567689',
			'email' => 'test@deinhandy.de'
		];

		return $addresses;
	}

	/**
	 * Enters invalid coupon codes on the cart page.
	 *
	 * @param AcceptanceTester $I
	 * @param Cart $cart
	 */
	private function runCouponTests(AcceptanceTester $I, $cart)
	{
		foreach (['invalidCodeThatWillNeverExist', '.,()§/$(/', '\'', ' '] as $invalidCode) {
			$cart->enterAndSubmitCouponCode($invalidCode);
			$cart->isCorrectPage();
			$cart->seeValidationError($I);
		}
	}

	/**
	 * @param Contract $contract
	 * @param array $contractData
	 * @return mixed
	 */
	private function fillPassportData($contract, array $contractData)
	{
		$contract->fillIdNumber(isset($contractData['idNumber']) ? $contractData['idNumber'] : '');
		if (isset($contractData['dateOfBirth'])) {
			$contract->selectDateOfBirth($contractData['dateOfBirth']);
		}
		$contract->fillPlaceOfBirth(isset($contractData['placeOfBirth']) ? $contractData['placeOfBirth'] : '');
		if (isset($contractData['validityDate'])) {
			$contract->selectValidityDate($contractData['validityDate']);
		}
		$contract->fillPlaceOfIssue(isset($contractData['placeOfIssue']) ? $contractData['placeOfIssue'] : '');
	}

	/**
	 * @param Contract $contract
	 * @param array $contractData
	 * @return mixed
	 */
	private function fillBankAccountData($contract, array $contractData)
	{
		$contract->selectBankAccountTypeAccountAndBankCode();
		$contract->fillBankAccountNumber(isset($contractData['bankAccountNumber']) ? $contractData['bankAccountNumber'] : '');
		$contract->fillBankAccountBankCode(isset($contractData['bankCode']) ? $contractData['bankCode'] : '');
	}
}
