<?php
namespace tests\codeception\frontend;
use Codeception\Exception\ElementNotFound;

/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method \Codeception\Lib\Friend haveFriend($name, $actorClass = NULL)
 *
 * @SuppressWarnings(PHPMD)
*/
class AcceptanceTester extends \Codeception\Actor
{
	use _generated\AcceptanceTesterActions;

   /**
	* Define custom actions here
	*
	* @return string
	*/
	public function grabPrice($selector)
	{
		$text = $this->grabTextOptionallyFrom($selector);
		return $this->extractPrice($text);
	}

	/**
	 * Convenience wrapper for grabbing text from the page without failing hard if there is no such element.
	 * This should only be used in cases where it is okay for the test that this element does not exist.
	 *
	 * @param string $selector
	 * @return string Empty string if the element was not found.
	 */
	public function grabTextOptionallyFrom($selector)
	{
		// We have use cases where either the monthly or the one-time price are not available (e.g. SO does not have
		// one-time and HO does not have monthly price), hence, the elements may not be found in those cases,
		// which is actually fine
		// Another use case are product and/or tariff names on the PDP and in the checkout. For both hardware only
		// and sim only, one of them is missing, which is also fine.
		try {
			$text = $this->grabTextFrom($selector);
		} catch (ElementNotFound $e) {
			$text = '';
		}
		return $text;
	}

	/**
	 * Extracts a price (formatted as either "1.234,56 €" or "CHF 1,234.56") from a string.
	 *
	 * @param string $string
	 * @return string
	 */
	public function extractPrice($string)
	{
		$price = $this->extractPriceByRegex($string, '/([0-9\,\.]+)[^0-9]*€/');
		if (empty($price) === false) {
			return $price;
		}

		$price = $this->extractPriceByRegex($string, '/CHF[^0-9]*?([0-9\,\.]+)/');
		if (empty($price) === false) {
			return $price;
		}

		return 0.0;
	}

	/**
	 * @param string $string
	 * @param string $regex
	 * @return string|null
	 */
	private function extractPriceByRegex($string, $regex)
	{
		preg_match($regex, $string, $matches);
		if (isset($matches[1])) {
			return $matches[1];
		}
		return null;
	}

	/**
	 * @param string $price Formatted price, without currency sign.
	 * @param string $selector
	 */
	public function seePrice($price, $selector)
	{
		$text = $this->grabTextFrom($selector);
		$this->seeMatches('/' . $price . '.*?€|CHF.*?' . $price . '/', $text);
	}

	/**
	 * @todo This should probably be separated into a proper action/step class, like so:
	 *  $this->getScenario()->runStep(new \Codeception\Step\Action('grabTextFrom', func_get_args()));
	 *  This would be especially nice for the debug output with the --steps or --debug parameter,
	 *  as it right now only shows "I grab text from [...]".
	 *  see also for this todo: \tests\codeception\frontend\_generated\AcceptanceTesterActions::grabTextFrom()
	 *
	 * @param $pattern
	 * @param $value
	 */
	public function seeMatches($pattern, $value)
	{
		\PHPUnit_Framework_Assert::assertRegExp($pattern, $value);
	}
}
