<?php

namespace tests\codeception\frontend\Page;

use tests\codeception\frontend\AcceptanceTester;

/**
 * Checkout validation error reading trait.
 *
 * @package tests\codeception\frontend\Page
 */
trait CanHaveValidationErrors
{
	/**
	 * Sees any or a specific error on the page.
	 *
	 * @param AcceptanceTester $I
	 * @param string|null $specificText [optional] In case one wants to check for a specific error message.
	 */
	public function seeValidationError(AcceptanceTester $I, $specificText = null)
	{
		if (empty($specificText)) {
			$I->seeElement('.feedback.errors .error');
		} else {
			$I->see($specificText, '.feedback.errors .error');
		}
	}
}
