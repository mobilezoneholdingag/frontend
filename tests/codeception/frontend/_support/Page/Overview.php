<?php
namespace tests\codeception\frontend\Page;

use tests\codeception\frontend\AcceptanceTester;

/**
 * PageObject for the checkout overview page.
 *
 * @package tests\codeception\frontend\Page
 */
class Overview
{
	use HasNamesOfProduct;
	use CanHaveValidationErrors;

	/**
	 * @var AcceptanceTester
	 */
	private $tester;

	/**
	 * Overview constructor.
	 *
	 * @param AcceptanceTester $acceptanceTester
	 */
	public function __construct(AcceptanceTester $acceptanceTester)
	{
		$this->tester = $acceptanceTester;
	}

	/**
	 * Checks whether the current page is the checkout overview page or not.
	 */
	public function isCorrectPage()
	{
		$I = $this->tester;
		$I->seeCurrentUrlMatches('/kasse(_|-)?3/');
		$I->see('Rechtliches', 'h3');
		$I->see('Jetzt verbindlich kaufen');
	}

	/**
	 * Verifies that the given address data is in the right place on the page.
	 *
	 * Supported array indexes (and according fields) are:
	 *
	 *  - firstname & lastname (needs both)
	 *  - street & houseNumber (needs both)
	 *  - zipCode & city (needs both)
	 *  - email
	 *
	 * **Please note** that the supported index names are case-sensitive.
	 *
	 * All passed indexes that are not supported are just ignored, so feel free to pass them along!
	 *
	 * @param array $address
	 */
	public function seePersonalAddressData($address)
	{
		if (isset($address['firstname']) && isset($address['lastname'])) $this->seeInPersonalData($address['firstname'] . ' ' . $address['lastname']);
		if (isset($address['street']) && isset($address['houseNumber'])) $this->seeInPersonalData($address['street'] . ' ' . $address['houseNumber']);
		if (isset($address['zipCode']) && isset($address['city'])) $this->seeInPersonalData($address['zipCode'] . ' ' . $address['city']);
		if (isset($address['email'])) $this->seeInPersonalData($address['email']);
	}

	/**
	 * Verfies the date of birth on the page.
	 *
	 * @param string $dateOfBirth Format: YYYY-MM-DD
	 */
	public function seeDateOfBirth($dateOfBirth)
	{
		$formattedDob = date('d.m.Y', strtotime($dateOfBirth));
		$this->seeInPersonalData($formattedDob);
	}

	/**
	 * @param string $string
	 */
	private function seeInPersonalData($string)
	{
		$this->tester->see($string, '#personal-data p');
	}

	/**
	 * Verifies that the given delivery address data is in the right place on the page.
	 *
	 * Supported array indexes (and according fields) are:
	 *
	 *  - delivery_street & delivery_houseNumber (needs both)
	 *  - delivery_zipCode & delivery_city (needs both)
	 *
	 * **Please note** that the supported index names are case-sensitive.
	 *
	 * All passed indexes that are not supported are just ignored, so feel free to pass them along!
	 *
	 * @param array $address
	 */
	public function seeDeliveryAddress($address)
	{
		if (isset($address['delivery_company']))
			$this->seeInDeliveryAddress($address['delivery_company']);
		if (isset($address['delivery_street']) && isset($address['delivery_houseNumber']))
			$this->seeInDeliveryAddress($address['delivery_street'] . ' ' . $address['delivery_houseNumber']);
		if (isset($address['delivery_zipCode']) && isset($address['delivery_city']))
			$this->seeInDeliveryAddress($address['delivery_zipCode'] . ' ' . $address['delivery_city']);
	}

	/**
	 * @param string $string
	 */
	private function seeInDeliveryAddress($string)
	{
		$this->tester->see($string, '#delivery-address p');
	}

	/**
	 * Convenience wrapper to have a delivery address checked by default address. This is intended primarily
	 * for usage of checking the delivery adress when there was no different delivery address specified, and
	 * thus is the same as the default address.
	 *
	 * @see Overview::seeDeliveryAddress
	 * @param array $address
	 */
	public function seeDeliveryAddressByAddress($address)
	{
		$deliveryAddress = [];
		foreach (['street', 'houseNumber', 'zipCode', 'city'] as $attribute) {
			if (isset($address[$attribute])) $deliveryAddress['delivery_' . $attribute] = $address[$attribute];
		}
		$this->seeDeliveryAddress($deliveryAddress);
	}

	/**
	 * @param string $idNumber
	 */
	public function seePassportIdNumber($idNumber)
	{
		$this->tester->see($idNumber, '#passport-number p');
	}

	/**
	 * Verifies that the bank account details are in the right place on the page.
	 *
	 * Supported array indexes (and according fields) are:
	 *
	 *  - firstname & lastname (needs both)
	 *  - bankAccountNumber
	 *  - bankCode
	 *
	 * **Please note** that the supported index names are case-sensitive.
	 *
	 * All passed indexes that are not supported are just ignored, so feel free to pass them along!
	 *
	 * @param $details
	 */
	public function seeBankAccountDetails($details)
	{
		if (isset($details['firstname']) && isset($details['lastname'])) $this->seeInBankAccountDetails($details['firstname'] . ' ' . $details['lastname']);
		if (isset($details['bankAccountNumber'])) $this->seeInBankAccountDetails($details['bankAccountNumber']);
		if (isset($details['bankCode'])) $this->seeInBankAccountDetails($details['bankCode']);
	}

	/**
	 * @param string $string
	 */
	private function seeInBankAccountDetails($string)
	{
		$this->tester->see($string, '#bankaccount-details p');
	}

	/**
	 * Selects prepayment ("Vorkasse) as payment method for the ontime payment.
	 */
	public function selectPaymentTypePrepayment()
	{
		$this->tester->selectOption('#paymentTypePrepaid', 0);
	}

	/**
	 * Checks the checkbox to accept the terms of service.
	 */
	public function checkAcceptTos()
	{
		$this->tester->checkOption('confirmationTos');
	}

	/**
	 * Checks the checkbox to accept the terms of service of the contract provider.
	 */
	public function checkAcceptProviderTos()
	{
		$this->tester->checkOption('confirmationTosProvider');
	}

	/**
	 * Checks the checkbox to accept the passing of data to third parties for a solvency check.
	 */
	public function checkAcceptSolvencyCheck()
	{
		$this->tester->checkOption('confirmationSolvencyCheck');
	}

	/**
	 * Convenience wrapper to fill both, the password and the password confirmation inputs with the same password.
	 *
	 * @param string $password
	 */
	public function fillProviderPasswordAndConfirmation($password)
	{
		$this->fillProviderPassword($password);
		$this->fillProviderPasswordConfirmation($password);
	}

	/**
	 * @param string $password
	 */
	public function fillProviderPassword($password)
	{
		$this->tester->fillField('password', $password);
	}

	/**
	 * @param string $password
	 */
	public function fillProviderPasswordConfirmation($password)
	{
		$this->tester->fillField('passwordRepeat', $password);
	}

	/**
	 * Clicks the "buy now" link, and thus places the order.
	 *
	 * @return Overview|Success
	 */
	public function clickBuyNow()
	{
		$I = $this->tester;
		// ToDo: Remove the duration checking part, once the checkout is actually faster (or we decide that it will stay like this).
		// ToDo: When doing this, be sure to also remove the extended cURL timeout CURLOPT_TIMEOUT setting in
		// tests/codeception/frontend/acceptance.suite.yml, so it defaults back to 30 seconds
		$clickStartTime = microtime(true);
		$I->click('#buy-now-button');
		$clickDuration = microtime(true) - $clickStartTime;
		if ($clickDuration > 5) {
			echo "\e[1;37m \e[41m Clicking buy now took very long (" . $clickDuration . " seconds)! We should really think about " .
				"steps to make the purchase step quicker! \e[0m\n";
		}

		$nextStep = $I->grabFromCurrentUrl('/kasse_?-?(3|4)/');
		switch ($nextStep) {
			case 3: return $this;
			case 4: return new Success($I);
		}

		throw new \RuntimeException(
			'Next step after clicking continue on overview page seems to be neither overview nor success step.'
		);
	}

	/**
	 * @return Contract
	 */
	public function clickBackButton()
	{
		$I = $this->tester;
		$I->click('#back-button', 'a');
		return new Contract($I);
	}

	/**
	 * @return Address
	 */
	public function clickChangeAddress()
	{
		$I = $this->tester;
		$I->click('#personal-data + a.change-data');
		return new Address($I);
	}

	/**
	 * @return Address
	 */
	public function clickChangeDeliveryAddress()
	{
		$I = $this->tester;
		$I->click('#delivery-address + a.change-data');
		return new Address($I);
	}

	/**
	 * @return Contract
	 */
	public function clickChangeContractDetails()
	{
		$I = $this->tester;
		$I->click('#change-contract-details-link');
		return new Contract($I);
	}

	/**
	 * Checks that all the right prices are in the right places.
	 *
	 * @param Product $product
	 */
	public function seePricesForProduct(Product $product)
	{
		$I = $this->tester;

		$expectedOneTime = $product->getExpectedPriceOneTime();
		if (empty($expectedOneTime) === false) {
			$I->seePrice($expectedOneTime, '#device-price');
			$I->seePrice($expectedOneTime, '#total-price-one-time');
		}

		$expectedMonthly = $product->getExpectedPriceMonthly();
		if (empty($expectedMonthly) === false) {
			$I->seePrice($expectedMonthly, '#price-monthly');
			$I->seePrice($expectedMonthly, '#total-price-monthly');
		}

		$expectedConnectionFee = $product->getExpectedPriceConnectionFee();
		if (empty($expectedConnectionFee) === false) {
			$I->seePrice($expectedConnectionFee, '#price-connection-fee');
		}
	}
}
