<?php
namespace tests\codeception\frontend\Page;

/**
 * Specific product PageObject for regular (sim & hardware) products.
 *
 * @package tests\codeception\frontend\Page
 */
class RegularProduct extends Product
{
	/**
	 * @var string Base URL for sim only products.
	 */
	protected $baseUrl = '/smartphones';
}
