<?php
namespace tests\codeception\frontend\Page;

use tests\codeception\frontend\AcceptanceTester;

/**
 * PageObject for checkout contract details page.
 *
 * @package tests\codeception\frontend\Page
 */
class Contract
{
	use HasNamesOfProduct;
	use CanHaveValidationErrors;

	/**
	 * @var AcceptanceTester
	 */
	private $tester;

	/**
	 * @var int Bank account entry by account number and bankcode.
	 */
	const ACCOUNT_TYPE_ACCOUNT_AND_BANKCODE = 1;
	/**
	 * @var int Bank account entry by IBAN.
	 */
	const ACCOUNT_TYPE_IBAN = 2;

	/**
	 * Contract constructor.
	 *
	 * @param AcceptanceTester $acceptanceTester
	 */
	public function __construct(AcceptanceTester $acceptanceTester)
	{
		$this->tester = $acceptanceTester;
	}

	/**
	 * Checks whether the current page is the checkout contract details page or not.
	 */
	public function isCorrectPage()
	{
		$I = $this->tester;
		$I->seeCurrentUrlMatches('/kasse(_|-)?2/');
		$I->see('Deine Ausweisdaten');
		$I->see('Weiter zur Übersicht');
	}

	/**
	 * Fills the identification number ("Ausweisnummer").
	 *
	 * @param string $idNumber
	 */
	public function fillIdNumber($idNumber)
	{
		$this->tester->fillField('idnumber', $idNumber);
	}

	/**
	 * Convenience wrapper to fill the date of birth easily.
	 *
	 * @param string $date Format: YYYY-MM-DD
	 */
	public function selectDateOfBirth($date)
	{
		$parts = explode('-', $date);
		$this->selectDateOfBirthDay($parts[2]);
		$this->selectDateOfBrithMonth($parts[1]);
		$this->selectDateOfBirthYear($parts[0]);
	}

	/**
	 * @param string|int $day Number from 1-31.
	 */
	public function selectDateOfBirthDay($day)
	{
		$this->tester->selectOption('dateofbirthDay', $day);
	}

	/**
	 * @param string|int $month Number from 1-12.
	 */
	public function selectDateOfBrithMonth($month)
	{
		$this->tester->selectOption('dateofbirthMonth', $month);
	}

	/**
	 * @param string|int $year Format: YYYY
	 */
	public function selectDateOfBirthYear($year)
	{
		$this->tester->selectOption('dateofbirthYear', $year);
	}

	/**
	 * @param string $place
	 */
	public function fillPlaceOfBirth($place)
	{
		$this->tester->fillField('placeofbirth', $place);
	}

	/**
	 * Convenience wrapper to fill the validity date of the identification easily.
	 *
	 * @param string $date Format: YYYY-MM-DD
	 */
	public function selectValidityDate($date)
	{
		$parts = explode('-', $date);
		$this->selectValidityDateDay($parts[2]);
		$this->selectValidityDateMonth($parts[1]);
		$this->selectValidityDateYear($parts[0]);

	}

	/**
	 * @param string|int $day Number from 1-31.
	 */
	public function selectValidityDateDay($day)
	{
		$this->tester->selectOption('validtillDay', $day);
	}

	/**
	 * @param string|int $month Number from 1-12.
	 */
	public function selectValidityDateMonth($month)
	{
		$this->tester->selectOption('validtillMonth', $month);
	}

	/**
	 * @param string|int $year Format: YYYY
	 */
	public function selectValidityDateYear($year)
	{
		$this->tester->selectOption('validtillYear', $year);
	}

	/**
	 * @param string $place
	 */
	public function fillPlaceOfIssue($place)
	{
		$this->tester->fillField('idplace', $place);
	}

	/**
	 * Checks the special checkbox that confirms that the user acknowledges that he is valid for a young tariff.
	 */
	public function checkAcceptYoungTariffSpecialStatus()
	{
		$this->tester->checkOption('#youngTariffSpecialStatus');
	}

	/**
	 * Selects bank account entry by account number ("Kontonr.") and bank code ("BLZ").
	 */
	public function selectBankAccountTypeAccountAndBankCode()
	{
		$this->selectBankAccountType(self::ACCOUNT_TYPE_ACCOUNT_AND_BANKCODE);
	}

	/**
	 * Selects bank account entry by IBAN.
	 */
	public function selectBankAccountTypeIban()
	{
		$this->selectBankAccountType(self::ACCOUNT_TYPE_IBAN);
	}

	/**
	 * @param int $type One of self::ACCOUNT_TYPE constants.
	 */
	private function selectBankAccountType($type)
	{
		$this->tester->selectOption('accountType', $type);
	}

	/**
	 * @param string $number
	 */
	public function fillBankAccountNumber($number)
	{
		$this->tester->fillField('accountNumber', $number);
	}

	/**
	 * @param string $code
	 */
	public function fillBankAccountBankCode($code)
	{
		$this->tester->fillField('accountBLZ', $code);
	}

	/**
	 * Clicks the "continue to overview" button.
	 *
	 * @return Contract|Overview
	 */
	public function clickContinue()
	{
		$I = $this->tester;
		$I->click('#continue-overview-button');

		$nextStep = $I->grabFromCurrentUrl('/kasse_?-?(2|3)/');
		switch ($nextStep) {
			case 2: return $this;
			case 3: return new Overview($I);
		}

		throw new \RuntimeException(
			'Next step after clicking continue on contract page seems to be neither contract nor overview step.'
		);
	}

	/**
	 * @return Address
	 */
	public function clickBackButton()
	{
		$I = $this->tester;
		$I->click('#back-button', 'a');
		return new Address($I);
	}
}
