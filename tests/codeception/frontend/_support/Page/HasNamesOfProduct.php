<?php

namespace tests\codeception\frontend\Page;

use tests\codeception\frontend\AcceptanceTester;

/**
 * Name checking trait for those PageObjects that have the product names (hardware name and tariff name) displayed
 * on them; or at least one of them.
 *
 * @package tests\codeception\frontend\Page
 */
trait HasNamesOfProduct
{
	public function seeNamesOfProduct(AcceptanceTester $I, Product $product)
	{
		$expectedHardwareName = $product->getExpectedHardwareName();
		if (empty($expectedHardwareName) === false) {
			$I->see($expectedHardwareName, '#device-name');
		}

		$expectedTariffName = $product->getExpectedTariffName();
		if (empty($expectedTariffName) === false) {
			$I->see($expectedTariffName, '#tariff-name');
		}
	}
}
