<?php
namespace tests\codeception\frontend\Page;

use tests\codeception\frontend\AcceptanceTester;

/**
 * PageObject for tariffs overview page.
 *
 * @package tests\codeception\frontend\Page
 */
class Tariffs
{
	use CanSeeImages;

	/**
	 * @var AcceptanceTester
	 */
	private $tester;

	/**
	 * Tariffs overview constructor.
	 *
	 * @param AcceptanceTester $acceptanceTester
	 */
	public function __construct(AcceptanceTester $acceptanceTester)
	{
		$this->tester = $acceptanceTester;
	}

	/**
	 * Checks whether the current page is the tariffs overview page or not.
	 */
	public function isCorrectPage()
	{
		$I = $this->tester;
		$I->seeResponseCodeIs(200);
		$I->see('Unsere', 'h1');
		$I->seeElement('#result-container .result');
		$I->see('Impressum', 'a');
	}

	/**
	 * Gets the relative path of the image of the first product box.
	 *
	 * @return string
	 */
	public function getFirstProductImagePath()
	{
		$I = $this->tester;
		return $I->grabAttributeFrom('#result-container .tarif-box .provider img', 'src');
	}

	/**
	 * Sees the image of the first product.
	 */
	public function seeFirstProductImage()
	{
		$imagePath = $this->getFirstProductImagePath();
		$this->seePngImage($this->tester, $imagePath);
	}
}
