<?php

namespace tests\codeception\frontend\Page;

use tests\codeception\frontend\AcceptanceTester;

/**
 * Image reading and checking trait.
 *
 * @package tests\codeception\frontend\Page
 */
trait CanSeeImages
{
	/**
	 * Sees a PNG image.
	 *
	 * @param AcceptanceTester $I
	 * @param string $path Relative path from domain root.
	 */
	public function seePngImage(AcceptanceTester $I, $path)
	{
		$I->amOnPage($path);
		$I->see('PNG');
		$I->moveBack();
	}
}
