<?php
namespace tests\codeception\frontend\Page;

use tests\codeception\frontend\AcceptanceTester;

/**
 * PageObject for checkout success page.
 *
 * @package tests\codeception\frontend\Page
 */
class Success
{
	/**
	 * @var AcceptanceTester
	 */
	private $tester;

	/**
	 * Success constructor.
	 *
	 * @param AcceptanceTester $acceptanceTester
	 */
	public function __construct(AcceptanceTester $acceptanceTester)
	{
		$this->tester = $acceptanceTester;
	}

	/**
	 * Checks whether the current page is the checkout success page or not.
	 */
	public function isCorrectPage()
	{
		$I = $this->tester;
		$I->seeCurrentUrlMatches('/kasse(_|-)?4/');
		$I->seeLink('Deine Bestätigung');
		$I->see('Deine Bestellung war erfolgreich!', 'h1.text-success');
		$I->see('Zurück zum Shop');
	}
}
