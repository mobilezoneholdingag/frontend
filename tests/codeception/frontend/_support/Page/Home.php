<?php
namespace tests\codeception\frontend\Page;

use tests\codeception\frontend\AcceptanceTester;

/**
 * PageObject for home page.
 *
 * @package tests\codeception\frontend\Page
 */
class Home
{
	/**
	 * @var AcceptanceTester
	 */
	private $tester;

	/**
	 * Home page constructor.
	 *
	 * @param AcceptanceTester $acceptanceTester
	 */
	public function __construct(AcceptanceTester $acceptanceTester)
	{
		$this->tester = $acceptanceTester;
	}

	/**
	 * Opens the home page.
	 */
	public function goToPage()
	{
		$I = $this->tester;
		$I->amOnPage('/');
	}

	/**
	 * Checks whether the current page is the home page or not.
	 */
	public function isCorrectPage()
	{
		$I = $this->tester;
		$I->seeResponseCodeIs(200);
		$I->seeInCurrentUrl('/');
		$I->seeElement('#dynamicSlider');
		$I->seeElement('#horizontal-scroll-box');
		$I->see('Unsere Provider', 'h2');
		$I->see('Handys nach Herstellern', 'h2');
		$I->see('Impressum', 'a');
	}

	/**
	 * @return Tariffs
	 */
	public function clickTariffOverviewLink()
	{
		$I = $this->tester;
		$I->click('#tariffs-overview-link', 'a');
		return new Tariffs($I);
	}

}
