<?php
namespace tests\codeception\frontend\Page;

use tests\codeception\frontend\AcceptanceTester;

/**
 * PageObject for checkout address page.
 *
 * @package tests\codeception\frontend\Page
 */
class Address
{
	use HasNamesOfProduct;
	use CanHaveValidationErrors;

	/**
	 * @var AcceptanceTester
	 */
	private $tester;

	/**
	 * @var int Salutation ID for Mr. ("Herr").
	 */
	const SALUTATION_MR = 2;
	/**
	 * @var int Salutation ID for Mrs. ("Frau").
	 */
	const SALUTATION_MRS = 1;

	/**
	 * Address constructor.
	 *
	 * @param AcceptanceTester $acceptanceTester
	 */
	public function __construct(AcceptanceTester $acceptanceTester)
	{
		$this->tester = $acceptanceTester;
	}

	/**
	 * Checks whether the current page is the checkout address page or not.
	 */
	public function isCorrectPage()
	{
		$I = $this->tester;
		$I->seeCurrentUrlMatches('/kasse(_|-)?1/');
		$I->see('Persönliche Daten');
		$I->see('Weiter zu', 'button[type=submit]');
	}

	/**
	 * Selects the salutation for "Mr." ("Herr").
	 */
	public function selectSalutationMr()
	{
		$this->tester->selectOption('gender', self::SALUTATION_MR);
	}

	/**
	 * Selects the salutation for "Mrs." ("Frau").
	 */
	public function selectSalutationMrs()
	{
		$this->tester->selectOption('gender', self::SALUTATION_MRS);
	}

	/**
	 * @param string $firstname
	 */
	public function fillFirstname($firstname)
	{
		$this->tester->fillField('prename', $firstname);
	}

	/**
	 * @param string $lastname
	 */
	public function fillLastname($lastname)
	{
		$this->tester->fillField('surname', $lastname);
	}

	/**
	 * @param string $street Streetname without house number.
	 */
	public function fillStreet($street)
	{
		$this->tester->fillField('street', $street);
	}

	/**
	 * @param int|string $number House number to be filled, e.g. "23" or "42 A"
	 */
	public function fillHouseNumber($number)
	{
		$this->tester->fillField('streetnumber', $number);
	}

	/**
	 * Fills in the ZIP (postal code).
	 *
	 * @param string $zipCode
	 */
	public function fillZipCode($zipCode)
	{
		$this->tester->fillField('zipcode', $zipCode);
	}

	/**
	 * @param string $city
	 */
	public function fillCity($city)
	{
		$this->tester->fillField('city', $city);
	}

	/**
	 * @param string $number
	 */
	public function fillPhoneNumber($number)
	{
		$this->tester->fillField('telephone', $number);
	}

	/**
	 * @param string $email
	 */
	public function fillEmail($email)
	{
		$this->tester->fillField('email', $email);
	}

	/**
	 * Convenience wrapper to fill plenty of address data at once by passing an array.
	 *
	 * Supported array indexes (and according fields) are:
	 *
	 *  - salutation (one of Address::SALUTATION_ constants)
	 *  - firstname
	 *  - lastname
	 *  - street
	 *  - houseNumber
	 *  - zipCode
	 *  - city
	 *  - phoneNumber
	 *  - email
	 *
	 * **Please note** that the supported index names are case-sensitive.
	 *
	 * All passed indexes that are not supported are just ignored, so feel free to pass them along!
	 *
	 * @param array $address
	 */
	public function fillAddress(array $address)
	{
		if (isset($address['salutation'])) {
			if ($address['salutation'] == self::SALUTATION_MR) $this->selectSalutationMr();
			elseif ($address['salutation'] == self::SALUTATION_MRS) $this->selectSalutationMrs();
		}
		if (isset($address['firstname'])) $this->fillFirstname($address['firstname']);
		if (isset($address['lastname'])) $this->fillLastname($address['lastname']);
		if (isset($address['street'])) $this->fillStreet($address['street']);
		if (isset($address['houseNumber'])) $this->fillHouseNumber($address['houseNumber']);
		if (isset($address['zipCode'])) $this->fillZipCode($address['zipCode']);
		if (isset($address['city'])) $this->fillCity($address['city']);
		if (isset($address['phoneNumber'])) $this->fillPhoneNumber($address['phoneNumber']);
		if (isset($address['email'])) $this->fillEmail($address['email']);
	}

	/**
	 * Checks the checkbox for "different delivery address".
	 */
	public function checkDifferentDeliveryAddressOption()
	{
		$this->tester->checkOption('#delivery-address-checkbox');
	}

	/**
	 * @param string $company
	 */
	public function fillDeliveryCompany($company)
	{
		$this->tester->fillField('#delivery_company', $company);
	}

	/**
	 * @param string $street
	 */
	public function fillDeliveryStreet($street)
	{
		$this->tester->fillField('#delivery_street', $street);
	}

	/**
	 * @param int|string $number
	 */
	public function fillDeliveryHouseNumber($number)
	{
		$this->tester->fillField('#delivery_streetnumber', $number);
	}

	/**
	 * @param string $zipCode
	 */
	public function fillDeliveryZipCode($zipCode)
	{
		$this->tester->fillField('#delivery_zipcode', $zipCode);
	}

	/**
	 * @param string $city
	 */
	public function fillDeliveryCity($city)
	{
		$this->tester->fillField('#delivery_city', $city);
	}

	/**
	 * Convenience wrapper to fill plenty of delivery address data at once by passing an array.
	 *
	 * Supported array indexes (and according fields) are:
	 *
	 *  - delivery_company
	 *  - delivery_street
	 *  - delivery_houseNumber
	 *  - delivery_zipCode
	 *  - delivery_city
	 *
	 * **Please note** that the supported index names are case-sensitive.
	 *
	 * All passed indexes that are not supported are just ignored, so feel free to pass them along!
	 *
	 * @param array $address
	 */
	public function fillDeliveryAddress($address)
	{
		if (empty($address) === false) $this->checkDifferentDeliveryAddressOption();
		if (isset($address['delivery_company'])) $this->fillDeliveryCompany($address['delivery_company']);
		if (isset($address['delivery_street'])) $this->fillDeliveryStreet($address['delivery_street']);
		if (isset($address['delivery_houseNumber'])) $this->fillDeliveryHouseNumber($address['delivery_houseNumber']);
		if (isset($address['delivery_zipCode'])) $this->fillDeliveryZipCode($address['delivery_zipCode']);
		if (isset($address['delivery_city'])) $this->fillDeliveryCity($address['delivery_city']);
	}

	/**
	 * Clicks the "continue to contract details" button.
	 *
	 * @return Contract|Overview|Address
	 */
	public function clickContinue()
	{
		$I = $this->tester;
		$I->click('#continue-button');

		$nextStep = $I->grabFromCurrentUrl('/kasse_?-?(1|2|3)/');
		switch ($nextStep) {
			case 1: return $this;
			case 2: return new Contract($I);
			case 3: return new Overview($I);
		}

		throw new \RuntimeException(
			'Next step after clicking continue on address page seems to be neither address, contract nor overview step.'
		);
	}

	/**
	 * @return Cart
	 */
	public function clickBackButton()
	{
		$I = $this->tester;
		$I->click('#back-button', 'a');
		return new Cart($I);
	}
}
