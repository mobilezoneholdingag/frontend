<?php
namespace tests\codeception\frontend\Page;

use tests\codeception\frontend\AcceptanceTester;

/**
 * PageObject for tariffs overview page.
 *
 * @package tests\codeception\frontend\Page
 */
class Pathfinder
{
	/**
	 * @var AcceptanceTester
	 */
	private $tester;

	/**
	 * Pathfinder page constructor.
	 *
	 * @param AcceptanceTester $acceptanceTester
	 */
	public function __construct(AcceptanceTester $acceptanceTester)
	{
		$this->tester = $acceptanceTester;
	}

	/**
	 * Opens the pathfinder page.
	 */
	public function goToPage()
	{
		$I = $this->tester;
		$I->amOnPage('/wegweiser');
	}

	/**
	 * Checks whether the current page is the pathfinder page or not.
	 */
	public function isCorrectPage()
	{
		$I = $this->tester;
		$I->seeResponseCodeIs(200);
		$I->see('Dein Wegweiser', 'h1');
		$I->see('Nächste Frage', 'button');
		$I->see('Impressum', 'a');
	}
}
