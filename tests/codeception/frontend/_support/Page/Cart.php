<?php
namespace tests\codeception\frontend\Page;

use tests\codeception\frontend\AcceptanceTester;

/**
 * PageObject for checkout cart page.
 *
 * @package tests\codeception\frontend\Page
 */
class Cart
{
	use HasNamesOfProduct;
	use CanHaveValidationErrors;

	/**
	 * @var AcceptanceTester
	 */
	private $tester;

	/**
	 * Cart constructor.
	 *
	 * @param AcceptanceTester $acceptanceTester
	 */
	public function __construct(AcceptanceTester $acceptanceTester)
	{
		$this->tester = $acceptanceTester;
	}

	/**
	 * Checks whether the current page is the checkout cart page or not.
	 */
	public function isCorrectPage()
	{
		$I = $this->tester;
		$I->seeCurrentUrlMatches('/warenkorb(1|_1)?/');
		$I->seeLink('Deine Auswahl');
		$I->see('Jetzt zur Kasse');
	}

	/**
	 * Clicks the button to continue to the checkout step.
	 *
	 * @return Address
	 */
	public function clickCheckoutButton()
	{
		$I = $this->tester;
		$I->click('#checkout-button-desktop', 'button');
		return new Address($I);
	}

	/**
	 * @param Product $expectedProduct
	 * @return Product
	 */
	public function clickBackButton(Product $expectedProduct)
	{
		$I = $this->tester;
		$I->click('#back-button-desktop', 'a');
		return $expectedProduct;
	}

	/**
	 * Checks that all the right prices are in the right places.
	 *
	 * @param Product $product
	 */
	public function seePricesForProduct(Product $product)
	{
		$I = $this->tester;

		$expectedOneTime = $product->getExpectedPriceOneTime();
		if (empty($expectedOneTime) === false) {
			$I->seePrice($expectedOneTime, '#device-price');
			$I->seePrice($expectedOneTime, '#total-price-one-time');
		}

		$expectedMonthly = $product->getExpectedPriceMonthly();
		if (empty($expectedMonthly) === false) {
			$I->seePrice($expectedMonthly, '#price-monthly');
			$I->seePrice($expectedMonthly, '#total-price-monthly');
		}

		$expectedConnectionFee = $product->getExpectedPriceConnectionFee();
		if (empty($expectedConnectionFee) === false) {
			$I->seePrice($expectedConnectionFee, '#price-connection-fee');
		}
	}

	/**
	 * @param string $code
	 */
	public function enterCouponCode($code)
	{
		$this->tester->fillField('coupon_code', $code);
	}

	/**
	 * @return Cart $this
	 */
	public function submitCouponCode()
	{
		$this->tester->click('Gutschein einlösen', 'button[type=submit]');
		return $this;
	}

	/**
	 * @param $code
	 * @return Cart $this
	 */
	public function enterAndSubmitCouponCode($code)
	{
		$this->enterCouponCode($code);
		return $this->submitCouponCode();
	}
}
