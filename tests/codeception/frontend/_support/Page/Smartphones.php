<?php
namespace tests\codeception\frontend\Page;

use tests\codeception\frontend\AcceptanceTester;

/**
 * PageObject for smartphones overview page.
 *
 * @package tests\codeception\frontend\Page
 */
class Smartphones
{
	use CanSeeImages;

	/**
	 * @var AcceptanceTester
	 */
	private $tester;

	/**
	 * Smartphones overview constructor.
	 *
	 * @param AcceptanceTester $acceptanceTester
	 */
	public function __construct(AcceptanceTester $acceptanceTester)
	{
		$this->tester = $acceptanceTester;
	}

	/**
	 * Opens the smartphones overview page.
	 */
	public function goToPage()
	{
		$I = $this->tester;
		$I->amOnPage('/smartphones');
	}

	/**
	 * Checks whether the current page is the smartphones overview page or not.
	 */
	public function isCorrectPage()
	{
		$I = $this->tester;
		$I->seeResponseCodeIs(200);
		$I->seeInCurrentUrl('/smartphones');
		$I->see('Unsere Smartphones', 'h1');
		$I->seeElement('#deviceFilter');
		$I->seeElement('.results .artikel-box');
		$I->see('Impressum', 'a');
	}

	/**
	 * Gets the relative path of the image of the first product box.
	 *
	 * @return string
	 */
	public function getFirstProductImagePath()
	{
		$I = $this->tester;
		return $I->grabAttributeFrom('.results .artikel-box .artikelbild-box img.artikelbild', 'src');
	}

	/**
	 * Sees the image of the first product.
	 */
	public function seeFirstProductImage()
	{
		$imagePath = $this->getFirstProductImagePath();
		$this->seePngImage($this->tester, $imagePath);
	}
}
