<?php
namespace tests\codeception\frontend\Page;

use tests\codeception\frontend\AcceptanceTester;

/**
 * PageObject for product page.
 *
 * @package tests\codeception\frontend\Page
 */
abstract class Product
{
	/**
	 * @var string Specific URL of the product.
	 */
	private $url = '';

	/**
	 * @var string Base URL, differs per product type.
	 */
	protected $baseUrl = '';

	/**
	 * @var AcceptanceTester
	 */
	protected $tester;

	/**
	 * The expected price for one time payment, as seen on the PDP.
	 *
	 * @var string
	 */
	private $expectedPriceOneTime;

	/**
	 * The expected price for monthly payment, as seen on the PDP.
	 *
	 * @var string
	 */
	private $expectedPriceMonthly;

	/**
	 * The expected price for the connection fee, as seen on the PDP.
	 *
	 * @var string
	 */
	private $expectedPriceConnectionFee;

	/**
	 * The expected hardware name, as seen on the PDP.
	 *
	 * @var string
	 */
	private $expectedHardwareName;

	/**
	 * The expected tariff name, as seen on the PDP.
	 *
	 * @var string
	 */
	private $expectedTariffName;

	/**
	 * @var string URL of the product to be tested with for the sim only checkout.
	 */
	const SIM_ONLY_PRODUCT_URL = '/winsim/lte-all-3-gb';

	/**
	 * @var string URL of the product to be tested with for regular checkout (sim & hardware).
	 */
	const REGULAR_PRODUCT_URL = '/samsung/galaxy-s7-edge/schwarz_32gb/o2/blue-all-in-m-young';

	/**
	 * @todo This is an interim solution, until we have proper test products that are the same in all stores.
	 * @var string URL of the product to be tested with for regular checkout (sim & hardware) in switzerland.
	 */
	const REGULAR_PRODUCT_URL_CH = '/samsung/galaxy-s7-edge/schwarz_32gb/swisscom/natel-xtra-infinity-20-m';

	/**
	 * @var string URL of the product to be tested with for the hardware only checkout.
	 */
	const HARDWARE_ONLY_PRODUCT_URL = '/samsung/galaxy-tab-s2-80-lte/schwarz_32gb/ohne-vertrag';

	/**
	 * Product constructor.
	 *
	 * @param AcceptanceTester $acceptanceTester
	 * @param string $productUrl Specific URL of the product, relative to the types base URL.
	 */
	public function __construct(AcceptanceTester $acceptanceTester, $productUrl)
	{
		$this->tester = $acceptanceTester;
		$this->url = $productUrl;
	}

	/**
	 * Determines the full product URL, relative to the domains root.
	 *
	 * @return string
	 */
	private function getRelativeUrl()
	{
		return $this->baseUrl . $this->url;
	}

	/**
	 * Determines the URL with only the product contained (not including the tariff part)
	 *
	 * @return string
	 */
	private function getProductOnlyUrl()
	{
		return implode('/', array_slice(explode('/', $this->getRelativeUrl()), 0, 4));
	}

	/**
	 * Opens and verifies the product page.
	 */
	public function goToPage()
	{
		$I = $this->tester;
		$I->amOnPage($this->getRelativeUrl());
		$this->isCorrectPage();
	}

	/**
	 * Checks whether the current page is the product page or not.
	 */
	public function isCorrectPage()
	{
		$I = $this->tester;
		$I->seeInCurrentUrl($this->getProductOnlyUrl());
		$I->seeElement('div.productDetail');
		$I->see('Jetzt bestellen', 'button');
	}

	/**
	 * Clicks the buy now button, putting the product into the cart.
	 *
	 * @return Cart
	 */
	public function clickBuyNowButton()
	{
		$I = $this->tester;
		$I->click('#buy-now-button');
		return new Cart($I);
	}

	/**
	 * Convenience wrapper to go to the product page, verify it, and put it into the cart.
	 *
	 * @return Cart
	 */
	public function putIntoCart()
	{
		$this->goToPage();
		$this->grabExpectedPrices();
		$this->grabExpectedNames();
		return $this->clickBuyNowButton();
	}

	/**
	 * Grabs the expected prices from the PDP.
	 */
	private function grabExpectedPrices()
	{
		$I = $this->tester;
		$this->expectedPriceOneTime = $I->grabPrice('#price-one-time');
		$this->expectedPriceMonthly = $I->grabPrice('#monthly-tariff-price');
		$this->expectedPriceConnectionFee = $I->grabPrice('#price-connection-fee');
	}

	/**
	 * @return string Locale formatted price.
	 */
	public function getExpectedPriceOneTime()
	{
		return $this->expectedPriceOneTime;
	}

	/**
	 * @return string Locale formatted price.
	 */
	public function getExpectedPriceMonthly()
	{
		return $this->expectedPriceMonthly;
	}

	/**
	 * @return string Locale formatted price.
	 */
	public function getExpectedPriceConnectionFee()
	{
		return $this->expectedPriceConnectionFee;
	}

	/**
	 * Grabs the expected names from the PDP.
	 */
	private function grabExpectedNames()
	{
		$I = $this->tester;
		$this->expectedHardwareName = $I->grabTextOptionallyFrom('#device-name');
		$this->expectedTariffName = $I->grabTextOptionallyFrom('#tariff-name');
	}

	/**
	 * @return string
	 */
	public function getExpectedHardwareName()
	{
		return $this->expectedHardwareName;
	}

	/**
	 * @return string
	 */
	public function getExpectedTariffName()
	{
		return $this->expectedTariffName;
	}
}
