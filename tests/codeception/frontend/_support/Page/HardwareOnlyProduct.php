<?php
namespace tests\codeception\frontend\Page;

/**
 * Specific product PageObject for sim only products.
 *
 * @package tests\codeception\frontend\Page
 */
class HardwareOnlyProduct extends Product
{
	/**
	 * @var string Base URL for sim only products.
	 */
	protected $baseUrl = '/tablets';
}
