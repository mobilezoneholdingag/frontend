<?php

namespace app\validators;

use app\services\Basket;
use DateTime;
use yii\validators\Validator;

class OfAgeValidator extends Validator
{
	/**
	 * @param \yii\base\Model $model
	 * @param string $attribute
	 * @return void
	 */
	public function validateAttribute($model, $attribute)
	{
		$this->validateDateFormat($model, $attribute);
		$this->validateIsAdult($model, $attribute);
		$this->validateAgeLimit($model, $attribute);
		$this->validateYoungPeople($model, $attribute);
	}

	private function validateYoungPeople($model, $attribute)
	{
		$tariff = Basket::getTariff();
		if ($tariff && $tariff->young_people && $tariff->provider->young_age_limit > 0) {
			$youngAge = new DateTime($model->$attribute);
			$youngAge = $youngAge->modify('+' . $tariff->provider->young_age_limit . ' years')->format('Y-m-d');
			if (strtotime($youngAge) < time()) {
				$this->addError(
					$model,
					$attribute,
					\Yii::t(
						'checkout',
						'Young Tariff Age Error {ageLimit}',
						['ageLimit' => $tariff->provider->young_age_limit]
					)
				);
			}
		}
	}

	private function validateIsAdult ($model, $attribute) {
		$ofAge = new DateTime($model->$attribute);
		$ofAge = $ofAge->modify('+18 years')->format('Y-m-d');

		if (strtotime($ofAge) > time()) {
			$this->addError($model, $attribute, \Yii::t('checkout', 'Of_Age_Validator'));
		}
	}

	private function validateAgeLimit($model, $attribute) {
		$overAge = new DateTime($model->$attribute);
		$overAge = $overAge->modify('+100 years')->format('Y-m-d');

		if (strtotime($overAge) < time()) {
			$this->addError($model, $attribute, \Yii::t('checkout', 'Correct_Format_Message'));
		}
	}

	private function validateDateFormat($model, $attribute) {
		if (!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $model->$attribute)) {
			$this->addError($model, $attribute, \Yii::t('checkout', 'Correct_Format_Message'));
		}
	}

}
