<?php

namespace app\validators;

use yii\validators\Validator;

class ConfirmedValidator extends Validator
{
	/**
	 * @param \yii\base\Model $model
	 * @param string $attribute
	 * @return bool|void
	 */
	public function validateAttribute($model, $attribute)
	{
		if (!(bool) $model->$attribute) {
			$this->addError($model, $attribute, \Yii::t('app', 'You need to confirm') . ': "' . strip_tags($model->getAttributeLabel($attribute)) . '".');
		}
	}
}
