<?php

namespace app\actions;

use Yii;
use yii\web\ErrorAction as BaseErrorAction;

class ErrorAction extends BaseErrorAction
{
	/**
	 * @override
	 * @return string
	 */
	public function run()
	{
		$statusCode = Yii::$app->getErrorHandler()->exception->statusCode;

		if ($statusCode === 404) {
			return $this->controller->renderFile('@views/errors/404.twig');
		}

		return $this->controller->renderFile('@views/errors/500.twig');
	}

}
