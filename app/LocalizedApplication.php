<?php
namespace app;

use app\services\Locale;
use deinhandy\yii2events\WebApplication;

class LocalizedApplication extends WebApplication
{
	public function bootstrap()
	{
		Locale::setEnvironmentConfigurations(base_path());
		parent::bootstrap();
	}
}