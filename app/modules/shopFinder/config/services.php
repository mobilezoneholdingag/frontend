<?php

/** @var Container $container */
use app\modules\shopFinder\services\Geocoder;
use app\modules\shopFinder\services\GeocoderInterface;
use deinhandy\yii2container\Container;

$container = di();

$container->setSingleton(GeocoderInterface::class, function(){
	return new Geocoder(\Yii::$app->cache);
});
