<?php

namespace app\modules\shopFinder\controllers;

use app\modules\BaseModuleController;
use app\modules\shopFinder\models\ShopFinderForm;
use app\modules\shopFinder\models\ShopRepository;
use app\modules\shopFinder\services\GeocoderInterface;
use Illuminate\Support\Collection;
use Yii;
use yii\web\NotFoundHttpException;

class ShopFinderController extends BaseModuleController
{
	protected $repository;
	protected $geocoder;

	const SHOP_NEARBY_COUNT = 3;

	public function __construct($id, $module, ShopRepository $repository, GeocoderInterface $geocoder, $config = [])
	{
		parent::__construct($id, $module, $config);

		$this->repository = $repository;
		$this->geocoder = $geocoder;
	}

	/**
	 * @return string
	 */
	public function actionIndex()
	{
		/** @var Collection $shops */
		$request = Yii::$app->request;
		$shopFinder = new ShopFinderForm();
		$shops = [];

		// process post request carrying coords
		if ($request->isPost) {
			$shopFinder->load($request->post());

		// if entering by the query parameter in a get request
		} else if ($query = $request->get('store')) {
			$coords = $this->geocoder->search("{$query},Switzerland");

			// only set coords if there are some hits, otherwise default will be displayed
			if ($coords) {
				$shopFinder->latitude = $coords['lat'];
				$shopFinder->longitude = $coords['lng'];
			} else {
                $shopFinder->fitbounds = true;
            }
		} else {
		    $shopFinder->fitbounds = true;
        }

		if ($shopFinder->validate()) {
			$shops = $this->repository->search($shopFinder);
		}

		$shops->each(function($shop) {
			$shop->html = $this->renderFile('@shop-finder/google-maps-marker.twig', compact('shop'));
		});

		return $this->renderFile('@shop-finder/index.twig', [
			'shop_finder' => $shopFinder,
			'shops' => $shops,
		]);
	}

	public function actionView($id = null)
	{
		if (!$shop = $this->repository->find($id)) {
			throw new NotFoundHttpException();
		}

		$shops = $this->repository->findNearBy($shop, static::SHOP_NEARBY_COUNT);

		return $this->renderFile('@shop-finder/shop/index.twig', compact('shop', 'shops'));
	}
}
