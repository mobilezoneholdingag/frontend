<?php

namespace app\modules\shopFinder\models;

use yii\base\Model;

class ShopFinderForm extends Model
{
	public $latitude = 47.37174;
	public $longitude = 8.54226;
    public $fitbounds = false;

	public function rules()
	{
		return [
			[['latitude', 'longitude'], 'required'],
			[['latitude', 'longitude'], 'number'],
		];
	}

	public function attributeLabels()
	{
		return [
			'latitude' => 'Latitude',
			'longitude' => 'Longitude',
            'fitbounds' => 'FitBounds',
		];
	}
}
