<?php

namespace app\modules\shopFinder\models;

use app\models\AbstractApiModel;
use deinhandy\yii2picasso\models\Image;
use Illuminate\Support\Collection;
use Yii;

/**
 * @property Image $manager_image
 * @property Image $shop_image_1
 * @property Image $shop_image_2
 * @property Image $shop_image_3
 * @property Collection|ShopHourModel[] $shop_hours
 * @property Collection|ShopHourModel[] $todays_openings
 * @property bool $closed_today
 * @property bool $already_closed_today
 */
class ShopModel extends AbstractApiModel
{
	const API_ENTITY_NAME = 'shop';
	const MANAGER_IMAGE_TYPE = 'manager_image';
	const STORE_IMAGE_TYPES = ['shop_image_1', 'shop_image_2', 'shop_image_3'];

	protected $relations = [
		'shop_hours' => ['class' => ShopHourModel::class, 'many' => true, 'field' => 'shop_hours', 'lazy' => false],
	];

	public $weekdays = [
		0 => 'Sunday',
		1 => 'Monday',
		2 => 'Tuesday',
		3 => 'Wednesday',
		4 => 'Thursday',
		5 => 'Friday',
		6 => 'Saturday',
	];

	public function fields()
	{
		$fields = parent::fields();

		$fields[] = 'latitude';
		$fields[] = 'longitude';

		return $fields;
	}

	public function getManagerImage()
	{
		return $this->getAttributes([self::MANAGER_IMAGE_TYPE]);
	}

	public function isClosedToday()
	{
		return $this->todays_openings->count();
	}

	public function isAlreadyClosedToday()
	{
		$timezone = Yii::$app->params['timezone'];

		$now = new \DateTime('now', new \DateTimeZone($timezone));

		foreach ($this->todays_openings as $hour) {
			$closedAt = new \DateTime($hour['closed_at'], new \DateTimeZone($timezone));

			if ($now < $closedAt) {
				return false;
			}
		}

		return true;
	}

	public function getTodaysOpenings()
	{
		return $this->shop_hours->where('day', (int) date('w'));
	}
}
