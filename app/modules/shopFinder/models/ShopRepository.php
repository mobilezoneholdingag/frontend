<?php

namespace app\modules\shopFinder\models;

use app\models\ApiRepository;
use app\services\api\ApiClientInterface;

class ShopRepository extends ApiRepository
{
	public function __construct(ApiClientInterface $apiClient)
	{
		parent::__construct($apiClient);

		$this->className = ShopModel::class;
	}

	/**
	 * @param ShopFinderForm $shopFinderForm
	 *
	 * @return ShopModel[]|\Illuminate\Support\Collection
	 */
	public function search($shopFinderForm)
	{
		$entity = constant("{$this->className}::API_ENTITY_NAME");
		$params = array_merge(
			$shopFinderForm->toArray(),
			$this->getParams()
		);

		$data = $this->apiClient->get($entity, null, $params);
		$result = collect();

		foreach ($data as $row) {
			$result[] = new $this->className($row);
		}

		return $result;
	}

	/**
	 * @param ShopModel $shopModel
	 * @param null $limit
	 *
	 * @return ShopModel[]|\Illuminate\Support\Collection
	 */
	public function findNearBy(ShopModel $shopModel, $limit = null)
	{
		$entity = constant("{$this->className}::API_ENTITY_NAME");
		$params = array_merge(
			$shopModel->toArray(['latitude', 'longitude']),
			$this->getParams()
		);

		if ($limit) {
			$params['limit'] = $limit;
		}

		// increase limit by one because first hit is the current shop
		$params['limit']++;

		$data = $this->apiClient->get($entity, null, $params);
		$result = collect();

		// remove first store from result because it is the current shop
		array_shift($data);

		foreach ($data as $row) {
			$result[] = new $this->className($row);
		}

		return $result;
	}
}
