<?php

namespace app\modules\shopFinder\models;

use app\models\AbstractApiModel;

class ShopHourModel extends AbstractApiModel
{
	const API_ENTITY_NAME = 'shop-hour';
}
