<?php

namespace app\modules\shopFinder\services;

use yii\caching\Cache;

class Geocoder implements GeocoderInterface
{
	protected $cache;

	const CACHE_PREFIX = 'googlemaps.geocoder';

	public function __construct(Cache $cache)
	{
		$this->cache = $cache;
	}

	public function search($query)
	{
		$key = $this->getKey($query);

		if (!$this->cache->exists($key)) {
			$data = json_decode(file_get_contents("http://maps.google.com/maps/api/geocode/json?address={$query}"), true);
			$coords = $data['results'][0]['geometry']['location'] ?? null;

			$this->cache->set($key, $coords);
		}

		return $this->cache->get($key);
	}

	protected function getKey(string $query): string
	{
		return implode([static::CACHE_PREFIX, $query]);
	}
}
