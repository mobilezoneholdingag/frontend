<?php

namespace app\modules\shopFinder\services;

interface GeocoderInterface
{
	public function search($query);
}
