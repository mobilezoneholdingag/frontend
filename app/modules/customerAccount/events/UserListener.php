<?php

namespace app\modules\customerAccount\events;

use app\models\CustomerModel;
use app\models\RestApiAdapter;
use app\modules\product\models\RenewalRequestModel;
use app\services\api\ApiClient;
use app\services\auth\AuthProvider;
use deinhandy\yii2events\listener\AbstractEventListener;
use yii\web\User;

class UserListener extends AbstractEventListener
{
	use RestApiAdapter;

	public function getClassEvents(): array
	{
		return [
			User::class => [
				User::EVENT_AFTER_LOGIN => [$this, 'onLogin']
			]
		];
	}

	public function onLogin()
	{
		/** @var CustomerModel $customer */
		$customer = di(AuthProvider::class)->user();

		$session = \Yii::$app->session;
		if (!($renewalRequestId = $session->get('renewal_request_id'))) {
			return;
		}

		/** @var ApiClient $ApiClient */
		$ApiClient = di('api.client');
		$ApiClient->put(RenewalRequestModel::API_ENTITY_NAME, $renewalRequestId, ['customer_id' => $customer->id]);
	}
}
