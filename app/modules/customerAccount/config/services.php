<?php

use app\modules\customerAccount\events\UserListener;
use deinhandy\yii2container\Container;

/** @var Container $container */
$container = di();

$container->addTags(UserListener::class, ['events.listener']);
