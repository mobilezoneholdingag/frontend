<?php

namespace app\modules\customerAccount\controllers;

use app\models\CustomerModel;
use app\models\CustomerRepository;
use app\services\api\ApiClientInterface;
use app\services\auth\AuthProviderInterface;
use app\services\SessionInterface;
use Yii;
use yii\filters\AccessControl;

class CustomerAccountController extends BaseController
{
	/**
	 * @var AuthProviderInterface
	 */
	protected $authProvider;
	/**
	 * @var SessionInterface
	 */
	protected $session;
	protected $apiClient;
	protected $repository;

	public function __construct(
		$id,
		$module,
		AuthProviderInterface $authProvider,
		SessionInterface $session,
		ApiClientInterface $apiClient,
		CustomerRepository $repository,
		$config = []
	) {
		$this->apiClient = $apiClient;
		$this->repository = $repository;

		parent::__construct($id, $module, $authProvider, $session, $config);
	}

	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::class,
				'only' => ['index', 'logout', 'information', 'change-password'],
				'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
		];
	}

	public function actionIndex()
	{
		$customer = $this->authProvider->user();
		$customer->fetch($customer->id);

		return $this->renderFile('@customer-account/account/index.twig', compact('customer'));
	}

	public function actionLogin()
	{
		$request = Yii::$app->request;
		$customer = new CustomerModel(['loginAttempt' => true]);

		if ($request->isPost) {
			$customer->scenario = CustomerModel::SCENARIO_LOGIN;
			$customer->load($request->post());

			if ($customer->validate()) {
				if (!$this->authProvider->check($customer)) {
					$this->session->addFlash('danger', $this->trans('Login_Failed'));
				} else {
					$this->authProvider->login($customer);

					return $this->redirect('/customer-account/customer-account/index');
				}
			}
		}

		return $this->renderFile('@customer-account/account/login.twig', compact('customer'));
	}

	public function actionLogout()
	{
		if ($customer = $this->authProvider->user()) {
			$this->authProvider->logout();
		}

		return $this->redirect('/customer-account/customer-account/index');
	}

	public function actionChangePassword()
	{
		$request = Yii::$app->request;
		$customer = $this->authProvider->user();

		if ($request->isPost) {
			$customer->scenario = CustomerModel::SCENARIO_CHANGE_PASSWORD;
			$customer->load($request->post());

			if ($customer->validate() && $this->checkPassword($customer)) {
				$this->repository->changePassword($customer);

				$this->session->addFlash('success', $this->trans('Password_Change_Successful'));
			}
		}

		return $this->renderFile('@customer-account/account/change-password.twig', compact('customer'));
	}

	private function checkPassword(CustomerModel $customer)
	{
		$customer->fetch($customer->id);

		if (!$customer->validatePassword($customer->password)) {
			$customer->addError('password', $this->trans('Password_Change_Wrong_Password'));

			return false;
		}

		return true;
	}

	public function actionInformation()
	{
		$request = Yii::$app->request;
		$customer = $this->authProvider->user();
		$customer->fetch($customer->id);

		if ($request->isPut) {
			$customer->scenario = CustomerModel::SCENARIO_INFORMATION;
			$customer->load($request->post());

			if ($customer->validate()) {
				$this->repository->save($customer);

				$this->session->addFlash('success', $this->trans('Account_Information_Saved'));
			}
		}

		return $this->renderFile('@customer-account/account/information.twig', [
			'customer' => $customer,
			'languages' => $this->repository->getAllLanguages()
		]);
	}

	public function actionForgotPassword()
	{
		$request = Yii::$app->request;
		$customer = new CustomerModel();

		if ($request->isPost) {
			$customer->scenario = CustomerModel::SCENARIO_FORGOT_PASSWORD;
			$customer->load($request->post());

			if ($customer->validate()) {
				if ($this->repository->forgotPassword($customer)) {
					$this->session->addFlash('success', $this->trans('Reset_Password_Email_Sent'));

					return $this->redirect('/customer-account/customer-account/login');
				} else {
					$this->session->addFlash('danger', $this->trans('Reset_Password_User_Does_Not_Exist'));
				}
			}
		}

		return $this->renderFile('@customer-account/account/forgot-password.twig', compact('customer'));
	}

	public function actionResetPassword($token = null)
	{
		$request = Yii::$app->request;
		$customer = new CustomerModel();

		if ($request->isPost) {
			$customer->scenario = CustomerModel::SCENARIO_RESET_PASSWORD;
			$customer->load($request->post());

			if (!$token) {
				$this->session->addFlash('danger', $this->trans('Password_Reset_Link_Broken'));
			} else {
				if ($customer->validate()) {
					if ($this->repository->resetPassword($token, $customer)) {
						$this->session->addFlash('success', $this->trans('Password_Reset_Success'));

						return $this->redirect('/customer-account/customer-account/login');
					} else {
						$this->session->addFlash('danger', $this->trans('Password_Reset_Failed'));
					}
				}
			}
		}

		return $this->renderFile('@customer-account/account/reset-password.twig', compact('customer'));
	}
}
