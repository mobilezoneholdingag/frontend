<?php

namespace app\modules\customerAccount\controllers;

use app\modules\BaseModuleController;
use app\services\auth\AuthProviderInterface;
use app\services\SessionInterface;

class BaseController extends BaseModuleController
{
	/**
	 * @var AuthProviderInterface
	 */
	protected $authProvider;
	/**
	 * @var SessionInterface
	 */
	protected $session;

	public function __construct(
		$id,
		$module,
		AuthProviderInterface $authProvider,
		SessionInterface $session,
		$config = []
	) {
		$this->authProvider = $authProvider;
		$this->session = $session;

		parent::__construct($id, $module, $config);
	}
}
