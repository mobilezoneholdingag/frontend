<?php

namespace app\modules\customerAccount\controllers;

use app\models\ApiRepository;
use app\models\OrderModel;
use Yii;
use yii\filters\AccessControl;

class OrderController extends BaseController
{
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::class,
				'only' => ['index', 'view'],
				'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
		];
	}

	public function actionIndex()
	{
		$customer = $this->authProvider->user();
		$customer->fetch($customer->id);

		return $this->renderFile('@customer-account/order/index.twig', [
			'orders' => $customer->getOrders(),
		]);
	}

	public function actionView()
	{
		/** @var ApiRepository $repository */
		$repository = di(ApiRepository::class);
		$customer = $this->authProvider->user();
		$customer->fetch($customer->id);
		$order = $repository->model(OrderModel::class)->findOne([
			'id' => Yii::$app->request->get('id'),
			'customer_id' => $customer->id,
		]);

		return $this->renderFile('@customer-account/order/view.twig', ['order' => $order]);
	}
}
