<?php

namespace app\modules\customerAccount\controllers;

use app\models\CustomerModel;
use app\models\CustomerRepository;
use app\models\NewsletterModel;
use app\models\NewsletterRepository;
use app\modules\customerAccount\CustomerAccountModule;
use Snowcap\Emarsys\Client;
use Snowcap\Emarsys\CurlClient;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

class NewsletterController extends BaseController
{

    private $emarsysLangMapping = [
        'de-CH' => 2,
        'en-EN' => 1,
        'fr-FR' => 3,
        'it-IT' => 4
    ];

	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::class,
				'only' => ['index'],
				'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
			'verbs' => [
				'class' => VerbFilter::class,
				'actions' => [
					'index' => ['get', 'put'],
					'create' => ['post'],
				],
			],
		];
	}

	/**
	 * Status Codes for getting Newsletters:
	 * 0 = Inactive, 1 = Signed Up, 2 = Double Opt In confirmed, 3 = Unsubscribed
	 *
	 * @return string
	 */
	public function actionIndex()
	{
		$request = Yii::$app->request;
		$customer = $this->authProvider->user();
		$customer->fetch($customer->id);

		$newsletter = $customer->newsletters->first();

		if (!$newsletter) {
			$newsletter = new NewsletterModel();
			$newsletter->customer = $customer;
		}

		$newsletter->scenario = NewsletterModel::SCENARIO_CUSTOMER_ACCOUNT;
		$newsletter->sex = $customer->gender;
		$newsletter->firstname = $customer->first_name;
		$newsletter->lastname = $customer->last_name;

		$newsletterPostData = $request->post('NewsletterModel');
		if ($request->isPut && isset($newsletterPostData['active']) && $newsletter->active != $newsletterPostData['active']) {
			$newsletter->load($request->post());

			if ($newsletter->validate()) {
				/** @var NewsletterRepository $repository */
				$repository = di(NewsletterRepository::class);
				$repository->save($newsletter);

				if($newsletter->data['status'] == NewsletterModel::STATUS_UNSUBSCRIBED) {
					$this->session->addFlash('success', $this->trans('Newsletter_Subscription_Success'));
				} elseif($newsletter->data['status'] == NewsletterModel::STATUS_DOI_STARTED) {
					$this->session->addFlash('success', $this->trans('Newsletter_Subscription_Saved'));
				}
			}
		}

		return $this->renderFile('@customer-account/newsletter/index.twig', compact('newsletter'));
	}

	/**
	 * @deprecated use actionSignup() instead
	 *
	 * @return \yii\web\Response
	 */
	public function actionCreate()
	{
		$data = Yii::$app->request->post();
		/** @var CustomerRepository $customerRepository */
		$customerRepository = di(CustomerRepository::class);

		// create customer if no customer found
		if (!$customer = $customerRepository->findOne(['email' => $data['email']])) {
			$customer = new CustomerModel();
			$customer->scenario = CustomerModel::SCENARIO_NEWSLETTER;
			$customer->setAttributes($data);

			if (!$customer->validate()) {
				$this->session->addFlash('danger', $this->trans('Newsletter_Subscription_Failed'));
				return $this->redirect(Yii::$app->request->referrer);
			}

			/** @var CustomerRepository $customerRepository */
			$customerRepository = di(CustomerRepository::class);
			$customerRepository->save($customer);
		}

		/** @var NewsletterModel|null $newsletter */
		$newsletter = $customer->newsletters->where('standard', '!=', null)->first();

		if (!$newsletter) {
			// create new newsletter entry
			$newsletter = new NewsletterModel();
			$newsletter->customer = $customer;
		} else {
			// if newsletter entry already exists, then just put it to active
			$newsletter->scenario = NewsletterModel::SCENARIO_REGISTER;
			$newsletter->active = true;
		}

		/** @var NewsletterRepository $newsletterRepository */
		$newsletterRepository = di(NewsletterRepository::class);
		$newsletterRepository->save($newsletter);

		$this->session->addFlash('success', $this->trans('Newsletter_Subscription_Success'));

		return $this->redirect(Yii::$app->request->referrer);
	}

	/**
	 * TODO for the moment this method just covers the newsletter signup initiiated from the homepage. needs to be
	 * adjusted to work from everywhere. after that, actionCreate will be obsolete.
	 *
	 * @return \yii\web\Response
	 */
	public function actionSignup()
	{
		$newsletter =  $this->getNewsletterModel();
		$customer = null;

		if (!$this->validateInputData($newsletter)) {
			return $this->redirect(Yii::$app->request->referrer);
		}

		$module = CustomerAccountModule::getInstance();

        $emarsys = new Client(new CurlClient(), $module->emarsys['user'], $module->emarsys['secret']);

        $emarsys->addFieldsMapping([
            'quelle' => 14670,
        ]);

        $test = $emarsys->createContact([
            'email' => $newsletter->email,
            'firstName' => $newsletter->firstname,
            'lastName' => $newsletter->lastname,
            'gender' => $emarsys->getChoiceId('gender', ($newsletter->sex == 1) ? 'male' : 'female'),
            'language' => $this->emarsysLangMapping[Yii::$app->language],
            'optin' => 1,
            'salutation' => $emarsys->getChoiceId('salutation', ($newsletter->sex == 1) ? 'mr' : 'mrs'), // mr und mrs
            'quelle' => 'Subscriber_Webshop'
        ]);

        if($test->getReplyCode() !== 0) {
            $this->session->addFlash('danger', $this->trans('Newsletter_Subscription_Already_Subscribed'));
            return $this->redirect(Yii::$app->request->referrer);
        }

        if (!$this->isUserAlreadyACustomer($newsletter->email)) {
			$customer = $this->createCustomer($this->getNewsletterModel()->getData());

			if (!$customer) {
				return $this->redirect(Yii::$app->request->referrer);
			}

			$this->registerCustomerForNewsletter($customer, $newsletter);
			$this->session['mkt-conversion'] = 'newsletter_sign_up';
			$this->session->addFlash('success', $this->trans('Newsletter_Subscription_Success'));
		} else {
			$this->session->addFlash('danger', $this->trans('Newsletter_Subscription_Already_Subscribed'));
		}

		return $this->redirect(Yii::$app->request->referrer);
	}

	private function getNewsletterModel()
	{
		$inputData = Yii::$app->request->post();
		$newsletter = new NewsletterModel($inputData['NewsletterModel'] ?? null);
		$newsletter->setData($inputData['NewsletterModel'] ?? null); // TODO why is setData() necessary? (already done in constructor)
		$newsletter->scenario = $newsletter::SCENARIO_HOMEPAGE;

		return $newsletter;
	}

	private function validateInputData(NewsletterModel $newsletter)
	{
		if(!$newsletter->validate()) {
			foreach ($newsletter->getErrors() as $error) {
				$this->session->addFlash('danger', implode('.', $error));
			}

			return false;
		}

		return true;
	}

	private function isUserAlreadyACustomer(string $email)
	{
		$customer = $this->getCustomerByEmail($email);

		return $customer !== null;
	}

	private function getCustomerByEmail($email)
	{
		$customer = di(CustomerRepository::class)->findOne([
			'email' => $email
		]);

		return $customer;
	}

	private function createCustomer(array $data)
	{
		// TODO here is not the right place for mapping the newsletter data structure to customer data structure
		$customerData = [
			'gender' => $data['sex'],
			'first_name' => $data['firstname'],
			'last_name' => $data['lastname'],
			'email' => $data['email'],
			'via_newsletter' => true,
		];

		$customer = new CustomerModel($customerData);
		$customer->setData($customerData);
		$customer->scenario = CustomerModel::SCENARIO_NEWSLETTER; // TODO temporary solution. see comment of method actionSignup

		if (!$customer->validate()) {
			foreach ($customer->getErrors() as $error) {
				$this->session->addFlash('danger', implode('.', $error));
			}

			return null;
		}

		// TODO the language is a required field, but should not be handled here
		$customer->language = Yii::$app->language;

		/** @var CustomerRepository $customerRepository */
		$customerRepository = di(CustomerRepository::class);
		$customerRepository->save($customer); // TODO what happens ehen the customer couldn't be created???

		return $customer;
	}

	private function registerCustomerForNewsletter(CustomerModel $customer, NewsletterModel $newsletter)
	{
		$newsletter->customer = $customer;
		$newsletter->status = NewsletterModel::STATUS_DOI_STARTED;

		/** @var NewsletterRepository $newsletterRepository */
		$newsletterRepository = di(NewsletterRepository::class);
		$newsletterRepository->save($newsletter);
	}
}
