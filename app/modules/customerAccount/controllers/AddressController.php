<?php

namespace app\modules\customerAccount\controllers;

use app\models\AddressModel;
use app\models\AddressRepository;
use app\models\ApiRepository;
use Yii;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;

class AddressController extends BaseController
{
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::class,
				'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
		];
	}

	public function actionDelete()
	{
		$request = Yii::$app->request;
		$id = $request->post('id');

		$customer = $this->authProvider->user();
		$customer->fetch($customer->id);

		$address = $customer
			->addresses
			->where('id', '==', $id)
			->first();

		if (!$address) {
			throw new NotFoundHttpException();
		}

		di(ApiRepository::class)->delete($address);
		$this->session->addFlash('success', $this->trans('Address_Deleted_Successfully'));

		return $this->redirect('index');
	}

	public function actionIndex()
	{
		$customer = $this->authProvider->user();
		$customer->fetch($customer->id);

		return $this->renderFile('@customer-account/address/index.twig', [
			'default_address' => $customer->address,
			'other_addresses' => $customer->other_addresses,
		]);
	}

	public function actionEdit()
	{
		$request = Yii::$app->request;
		$id = $request->get('id');

		// TODO would be nice if this where only one command
		$customer = $this->authProvider->user();
		$customer->fetch($customer->id);

		// TODO combine this and the previous command, so we load via repository, but also verify that this address really belongs to the logged in user
		$address = di(AddressRepository::class)->load($id);

		// TODO extract this block to a dedicated 'update' method
		if ($request->isPut) {
			$address = new AddressModel();
			$address->load($request->post());
			$address->id = $id; // TODO HACK! HACK! HACK!

			if ($address->validate()) {
				di(AddressRepository::class)->save($address, $customer->id);
				$this->session->addFlash('success', $this->trans('Address_Saved_Successfully'));

				return $this->redirect('index');
			}
		}

		return $this->renderFile('@customer-account/address/edit.twig', compact('address'));
	}

	public function actionAdd()
	{
		$request = Yii::$app->request;
		$customer = $this->authProvider->user();
		$address = new AddressModel();
		$address->customer = $customer;

		if ($request->isPost) {
			$address->load($request->post());

			if ($address->validate()) {
				/** @var AddressRepository $repository */
				$repository = di(AddressRepository::class);
				$repository->save($address, $customer->id);
				$this->session->addFlash('success', $this->trans('Address_Created_Successfully'));

				return $this->redirect('index');
			}
		}

		return $this->renderFile('@customer-account/address/create.twig', compact('address'));
	}
}
