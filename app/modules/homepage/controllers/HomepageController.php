<?php

namespace app\modules\homepage\controllers;

use app\behaviors\DataLayerBehavior;
use app\models\ApiRepository;
use app\models\ManufacturerModel;
use app\models\NewsletterModel;
use app\models\ProviderModel;
use app\models\SetModel;
use app\models\MarketingImagesModel;
use app\modules\BaseModuleController;
use Illuminate\Support\Collection;

/**
 * Class HomepageController
 *
 * @package frontend\controllers
 */
class HomepageController extends BaseModuleController
{
	/**
	 * @return string
	 */
	public function actionIndex()
	{
		/** @var Collection|ProviderModel[] $provider */
		$repository = di(ApiRepository::class);
		$provider = $repository->model(ProviderModel::class)->find();

		/** @var Collection|ManufacturerModel[] $manufacturer */
		$repository = di(ApiRepository::class);
		$manufacturer = $repository->model(ManufacturerModel::class)->find();

		/** @var Collection|SetModel[] $set */
		$repository = di(ApiRepository::class);
		/** @var SetModel $set */
		$topTenSliderItems = $repository->model(SetModel::class)->find('homepage-slider');
		$accessorySliderItems = $repository->model(SetModel::class)->find('accessory-homepage-slider');

		$params = [
			'top_ten_slider_items' => $topTenSliderItems->getArticlesOnly(),
			'accessory_slider_items' => $accessorySliderItems->getArticlesOnly(),
			//'providers' => $provider->take(6),
            'providers' => $provider->where('visible_homepage', '=', 1),
			'manufacturers' => $manufacturer->where('visible_homepage', '=', 1),
            'newsletter' => new NewsletterModel(),
			'newsletter_image' => 'newsletter_' . \Yii::$app->language . '.png',
		];

		return $this->renderFile('@homepage/homepage.twig', $params);
	}

	private function createMarketingImage($tag)
	{
		return (new MarketingImagesModel())->fetch($tag);
	}
}
