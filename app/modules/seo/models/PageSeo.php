<?php

namespace app\modules\seo\models;

use Yii;

class PageSeo implements Seo
{
	public static function getEntityInformation($id = null): array
	{
		$mapping = [
			//DE - Support Static Pages
			'support' => 'Page_Tree_Service_Support',
			'support/reparatur-garantie' => 'Page_Tree_Reparatur_Garantie',
			'support/express-reparatur' => 'Page_Tree_Express_Reparatur',
			'support/canon-reparaturannahme' => 'Page_Tree_Canon_Reparaturannahme',
			'support/service-garantie' => 'Page_Tree_Service_Garantie',
			'support/handyversicherung' => 'Page_Tree_Handyversicherung',
			'agb' => 'Page_Tree_Terms',
			'datenschutz' => 'Page_Tree_Datenschutz',
			'impressum' => 'Page_Tree_Imprint',


            'support/reparatur' => 'Page_Tree_Express_Reparatur',
            'support/reparations' => 'Page_Tree_Express_Reparatur',
            'support/riparazioni' => 'Page_Tree_Express_Reparatur',
            'versand' => 'Page_Shipment_Headline',
            'transport' => 'Page_Shipment_Headline',
            'spedizione' => 'Page_Shipment_Headline',
            'tv/configurator' => 'Page_CC_Title',
            'abo' => 'Our_Tariffs',
            'contrats' => 'Our_Tariffs',
            'contratti' => 'Our_Tariffs',
            'abo/neuvertrag' => 'Tariff_Category_Title',
            'contrats/nouvel-abonnement' => 'Tariff_Category_Title',
            'contratti/nuovo-abbonamento' => 'Tariff_Category_Title',
            'abo/portierung' => 'Tariff_Selection_Porting',
            'contrats/reprendre-abonnement' => 'Tariff_Selection_Porting',
            'contratti/transferire-abbonamento' => 'Tariff_Selection_Porting',
            'abo/vertragsverlaengerung' => 'Contract_Renewal',
            'contrats/prolongation' => 'Contract_Renewal',
            'contratti/prolungamento' => 'Contract_Renewal',
            'abo/neuvertrag' => 'Tariff_Category_Title',
            'contrats/nouvel-abonnement' => 'Tariff_Category_Title',
            'contratti/nuovo-abbonamento' => 'Tariff_Category_Title',
            'checkout/checkout/index' => 'Checkout',
            'checkout/checkout/' => 'Checkout',
            'customer-account/customer-account/login' => 'Login',


			//FR
			'support/reparation-garantie' => 'Page_Tree_Reparatur_Garantie',
			'support/reparations-express' => 'Page_Tree_Express_Reparatur',
			'support/reparation-de-canon' => 'Page_Tree_Canon_Reparaturannahme',
			'support/garantie-de-service' => 'Page_Tree_Service_Garantie',
			'support/assurance-du-portable' => 'Page_Tree_Handyversicherung',
			'conditions-generales' => 'Page_Tree_Terms',
			'protection-des-donnees' => 'Page_Tree_Datenschutz',
			'mentions-legales' => 'Page_Tree_Imprint',

			//IT
			'support/riparazione-garanzia' => 'Page_Tree_Reparatur_Garantie',
			'support/riparazioni-express' => 'Page_Tree_Express_Reparatur',
			'support/accettazione-riparazioni-canon' => 'Page_Tree_Canon_Reparaturannahme',
			'support/garanzia-di-servizio' => 'Page_Tree_Service_Garantie',
			'support/assicurazione' => 'Page_Tree_Handyversicherung',
			'condizioni-generali' => 'Page_Tree_Terms',
			'protezione-dei-dati' => 'Page_Tree_Datenschutz',
			'informazioni-legali' => 'Page_Tree_Imprint',

			//DE - About Static Pages
			'ueber-uns/unternehmen/ueber-uns' => 'Page_About_Us_Title',
			'ueber-uns/unternehmen/geschaeftsbereiche' => 'Page_Business_Unit_Title',
			'ueber-uns/medien/mitteilungen' => 'Page_Tree_News',
			'ueber-uns/medien/medienservice' => 'Page_Tree_Media_Service',
			'ueber-uns/investoren/kennzahlen' => 'Page_Tree_Key_Figures',
			'ueber-uns/investoren/finanzkalender' => 'Page_Tree_Calendar',
			'ueber-uns/investoren/berichte' => 'Page_Tree_Reports',
			'ueber-uns/investoren/ir-service' => 'Page_Tree_IR_Service',
			'ueber-uns/governance/struktur' => 'Page_Tree_Structure',
			'ueber-uns/governance/verwaltungsrat' => 'Page_Tree_Board_of_Directors',
			'ueber-uns/governance/konzernleitung' => 'Page_Tree_Group_Management',
			'ueber-uns/governance/management-team' => 'Page_Tree_Management_Team',
			'ueber-uns/governance/downloads' => 'Page_Tree_Principles',
			'ueber-uns/kontakt' => 'Page_Tree_Contact',

			//FR
			'portrait/entreprise/portrait' => 'Page_About_Us_Title',
			'portrait/entreprise/domains-activite' => 'Page_Business_Unit_Title',
			'portrait/media/news-et-communiques' => 'Page_Tree_News',
			'portrait/media/media-service' => 'Page_Tree_Media_Service',
			'portrait/investisseurs/chiffres-cles' => 'Page_Tree_Key_Figures',
			'portrait/investisseurs/calendrier' => 'Page_Tree_Calendar',
			'portrait/investisseurs/rapports' => 'Page_Tree_Reports',
			'portrait/investisseurs/ir-service' => 'Page_Tree_IR_Service',
			'portrait/gouvernement/structure' => 'Page_Tree_Structure',
			'portrait/gouvernement/conseil-administration' => 'Page_Tree_Board_of_Directors',
			'portrait/gouvernement/direction' => 'Page_Tree_Group_Management',
			'portrait/gouvernement/management-team' => 'Page_Tree_Management_Team',
			'portrait/gouvernement/downloads' => 'Page_Tree_Principles',
			'portrait/contact' => 'Page_Tree_Contact',

			//IT
			'chi-siamo/azienda/chi-siamo' => 'Page_About_Us_Title',
			'chi-siamo/azienda/settori-di-attivita' => 'Page_Business_Unit_Title',
			'chi-siamo/media/news-e-communicati-stampa' => 'Page_Tree_News',
			'chi-siamo/media/media-service' => 'Page_Tree_Media_Service',
			'chi-siamo/investitori/indicatori' => 'Page_Tree_Key_Figures',
			'chi-siamo/investitori/calendario' => 'Page_Tree_Calendar',
			'chi-siamo/investitori/rapporti' => 'Page_Tree_Reports',
			'chi-siamo/investitori/ir-service' => 'Page_Tree_IR_Service',
			'chi-siamo/governance/struttura' => 'Page_Tree_Structure',
			'chi-siamo/governance/consiglio-amministrazione' => 'Page_Tree_Board_of_Directors',
			'chi-siamo/governance/direzione' => 'Page_Tree_Group_Management',
			'chi-siamo/governance/management-team' => 'Page_Tree_Management_Team',
			'chi-siamo/governance/downloads' => 'Page_Tree_Principles',
			'chi-siamo/contatto' => 'Page_Tree_Contact',

			//Annual Report Static Pages
			'ueber-uns/investoren/geschaeftsberichte-16/start' => 'Page_Business_Unit_Start',
			'ueber-uns/investoren/geschaeftsberichte-16/kennzahlen' => 'Page_Business_Unit_Key_Figures',
			'ueber-uns/investoren/geschaeftsberichte-16/portrait' => 'Page_Business_Unit_Portrait',
			'ueber-uns/investoren/geschaeftsberichte-16/aktionaersbrief' => 'Page_Business_Unit_Letter_To_Shareholders',
			'ueber-uns/investoren/geschaeftsberichte-16/highlights' => 'Page_Business_Unit_Highlights',
			'ueber-uns/investoren/geschaeftsberichte-16/corporate-governance' => 'Page_Business_Unit_Corporate_Governance',
			'ueber-uns/investoren/geschaeftsberichte-16/verguetungsbericht' => 'Page_Business_Unit_Compensation_Report',
			'ueber-uns/investoren/geschaeftsberichte-16/finanzbericht' => 'Page_Business_Unit_Financial_Report',

			//EN Static Pages
			'about-us/company/about-us' => 'Page_About_Us_Title',
			'about-us/company/business-units' => 'Page_Business_Unit_Title',
			'about-us/media/news' => 'Page_Tree_News',
			'about-us/media/media-service' => 'Page_Tree_Media_Service',
			'about-us/investors/key-figures' => 'Page_Tree_Key_Figures',
			'about-us/investors/reports' => 'Page_Tree_Reports',
			'about-us/investors/calendar' => 'Page_Tree_Calendar',
			'about-us/investors/ir-service' => 'Page_Tree_IR_Service',
			'about-us/governance/structure' => 'Page_Tree_Structure',
			'about-us/governance/board-of-directors' => 'Page_Tree_Board_of_Directors',
			'about-us/governance/group-management' => 'Page_Tree_Group_Management',
			'about-us/governance/management-team' => 'Page_Tree_Management_Team',
			'about-us/governance/downloads' => 'Page_Tree_Principles',
			'about-us/contact' => 'Page_Tree_Contact',

			//EN Annual Report Static Pages
			'about-us/investors/annual-reports-16/start' => 'Page_Business_Unit_Start',
			'about-us/investors/annual-reports-16/key-figures' => 'Page_Business_Unit_Key_Figures',
			'about-us/investors/annual-reports-16/portrait' => 'Page_Business_Unit_Portrait',
			'about-us/investors/annual-reports-16/letter-to-shareholders' => 'Page_Business_Unit_Letter_To_Shareholders',
			'about-us/investors/annual-reports-16/highlights' => 'Page_Business_Unit_Highlights',
			'about-us/investors/annual-reports-16/corporate-governance' => 'Page_Business_Unit_Corporate_Governance',
			'about-us/investors/annual-reports-16/compensation-report' => 'Page_Business_Unit_Compensation_Report',
			'about-us/investors/annual-reports-16/financial-report' => 'Page_Business_Unit_Financial_Report',

			//DE - Jobs
			'karriere' => 'Page_Jobs_Title',
			'karriere/arbeiten-bei-mobilezone' => 'Page_Work_Title',
			'karriere/ausbildung' => 'Page_Education_Title',
			'karriere/offene-stellen' => 'Page_Positions_Title',

			//FR
			'carriere' => 'Page_Jobs_Title',
			'carriere/travailler-chez-mobilezone' => 'Page_Work_Title',
			'carriere/formation' => 'Page_Education_Title',
			'carriere/positions-vacantes' => 'Page_Positions_Title',

			//IT
			'carriera' => 'Page_Jobs_Title',
			'carriera/lavorare-con-mobilezone' => 'Page_Work_Title',
			'carriera/tirocinio' => 'Page_Education_Title',
			'carriera/posizioni-aperte' => 'Page_Positions_Title',
		];

        //Yii::t("staticcontent",$mapping[$id] ?? null)

		$entityInformation = [
			'page_name' => self::findlangStr($mapping, $id),
		];

		return $entityInformation;
	}
    public static function findlangStr($mapping, $id){
        $str = "";
        $id = parse_url($id)["path"];
        if(isset($mapping[$id])) {
            $str = $mapping[$id];
            $categories = ["staticcontent", "view", "product", "customer-account"];
            foreach ($categories as $category) {
                $string = Yii::t($category,$str ?? null);
                if($string != $str){
                    $str = $string;
                    break;
                }
            }
        }
        return $str;
    }

}
