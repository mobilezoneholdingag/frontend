<?php

namespace app\modules\seo\models;

class SeoService
{
	// TODO needs to be implemented: just an example for now
	public static function getCanonical($path)
	{
		$mapping = [
			'/homepage/homepage/index' => '/',
			'/page/page/tv' => '/tv',
			'/product/product/16505' => 'mobiltelefone/16505/iphone-7/',
			'/page/page/4244' => 'ueber-uns/investoren/kennzahlen',
		];

		return $mapping[$path] ?? $path;
	}

	// TODO needs to be implemented: just an example for now
	public static function getPath($canonical)
	{
		$mapping = [
			'/' => '/homepage/homepage/index',
			'/tv' => '/page/page/tv',
			'mobiltelefone/16505/iphone-7/' => '/product/product/16505',
			'ueber-uns/investoren/kennzahlen' => '/page/page/4244',
		];

		return $mapping[$canonical] ?? $canonical;
	}

	public static function getSeoInformation($canonical)
	{
		foreach (SeoUri::getDefindedRules() as $pattern => $content) {
			preg_match("@$pattern@", $canonical, $hits);

			if (!$hits) {
				continue;
			}

			$entityData = null;
			if ($content && isset($content['data-source']) && isset($hits[1])) {
				$entityData = $content['data-source']::getEntityInformation($hits[1]);
			}

			return new SeoInformation($content, $entityData);
		}

		$seo = new SeoInformation(SeoUri::getDefaultRules());

		return $seo;
	}
}
