<?php

namespace app\modules\seo\models;

use app\models\ApiRepository;
use app\models\SetSearchModel;

class CategoryTabletSeo implements Seo
{
	public static function getEntityInformation($id = null): array
	{
		if (!$id) {
			return [];
		}

		$set = self::getArticleSet($id);

		if (!$set) {
			return [];
		}

		if (!$set->items->first()) {
			return [];
		}

		$manufacturerTitle = $set->items->first()->manufacturer->title;
		$productType = self::getProductType($manufacturerTitle);

		return [
			'product_type' => $productType,
			'manufacturer_title' => $manufacturerTitle,
		];
	}

	private static function getArticleSet($id)
	{
		return di(ApiRepository::class)->model(SetSearchModel::class)->findOne(
			[
				'id' => 'category-tablets',
				'manufacturer' => $id,
			]
		);
	}

	private static function getProductType($manufacturerTitle)
	{
		if ($manufacturerTitle === 'Apple') {
			return 'iPad';
		}

		return 'Tablets';
	}
}
