<?php

namespace app\modules\seo\models;

// currently just a little stupid data container.
class SeoInformation {
	protected $metaRobots = null;
	protected $keywords;
	protected $title = null;
	protected $description = null;
	protected $seoText = null;
	protected $canonical = null;

	protected $entity = null;

	public function __construct($options = [], $getEntityInformation = [])
	{
		$this->metaRobots = $options['meta_robots'] ?? 'INDEX,FOLLOW';
		$this->keywords = $options['keywords'] ?? null;
		$this->title = $options['title'] ?? null;
		$this->description = $options['description'] ?? null;
		$this->seoText = $options['seo_text'] ?? null;
		$this->canonical = $options['canonical'] ?? null;
		$this->getEntityInformation = $getEntityInformation;
	}

	public function getMetaRobots()
	{
		return $this->metaRobots;
	}

	public function setMetaRobots($metaRobots)
	{
		$this->metaRobots = $metaRobots;
	}

	public function getKeywords()
	{
		return $this->keywords;
	}

	public function setKeywords($keywords)
	{
		$this->keywords = $keywords;
	}

	public function getTitle()
	{
		return $this->title;
	}

	public function setTitle($title)
	{
		$this->title = $title;
	}

	public function getDescription()
	{
		return $this->description;
	}

	public function setDescription($description)
	{
		$this->description = $description;
	}

	public function getSeoText()
	{
		return $this->seoText;
	}

	public function setSeoText($seoText)
	{
		$this->seoText = $seoText;
	}

	public function getCanonical()
	{
		return $this->canonical;
	}

	public function setCanonical($canonical)
	{
		$this->canonical = $canonical;
	}

	public function getEntityInformation()
	{
		return $this->getEntityInformation;
	}
}
