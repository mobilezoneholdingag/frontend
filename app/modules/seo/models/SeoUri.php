<?php

namespace app\modules\seo\models;

class SeoUri
{
	public static function getDefaultRules(): array
	{
		return [
			'canonical' => null,
			'title' => 'default_title',
			'description' => 'default_meta_description',
			'robots' => 'default_robots',
			'redirect' => false,
			'data-source' => null,
		];
	}

	public static function getDefindedRules(): array
	{
		return [
			// HOMEPAGE
			'^/$' => [ // same for all three languages
				'canonical' => '/',
				'title' => 'homepage_title',
				'description' => 'homepage_meta_description',
			],

			// STATIC PAGES
			'^/tv$' => [
				'canonical' => '/tv', // same for all three languages
				'title' => 'page_tv_title',
				'description' => 'page_tv_meta_description',
			],
			'^/(ueber-uns.*)' => [
				'canonical' => '$1',
				'title' => 'static_page_title',
				'description' => 'static_page_meta_description',
				'data-source' => PageSeo::class,
			],
			'^/(support.*)' => [
				'canonical' => '$1',
				'title' => 'static_page_title',
				'description' => 'static_page_meta_description',
				'data-source' => PageSeo::class,
			],


            '^/(versand.*)' => [
                'canonical' => '$1',
                'title' => 'static_page_title',
                'description' => 'static_page_meta_description',
                'data-source' => PageSeo::class,
            ],
            '^/(tv.*)' => [
                'canonical' => '$1',
                'title' => 'static_page_title',
                'description' => 'static_page_meta_description',
                'data-source' => PageSeo::class,
            ],
            '^/(abo.*)' => [
                'canonical' => '$1',
                'title' => 'static_page_title',
                'description' => 'static_page_meta_description',
                'data-source' => PageSeo::class,
            ],
            '^/(customer-account.*)' => [
                'canonical' => '$1',
                'title' => 'static_page_title',
                'description' => 'static_page_meta_description',
                'data-source' => PageSeo::class,
            ],
            '^/(checkout.*)' => [
                'canonical' => '$1',
                'title' => 'static_page_title',
                'description' => 'static_page_meta_description',
                'data-source' => PageSeo::class,
            ],





            '^/(about-us.*)' => [
				'canonical' => '$1',
				'title' => 'static_page_title',
				'description' => 'static_page_meta_description',
				'data-source' => PageSeo::class,
			],
			'^/(karriere.*)' => [
				'canonical' => '$1',
				'title' => 'static_page_title',
				'description' => 'static_page_meta_description',
				'data-source' => PageSeo::class,
			],
			'^/(agb.*)' => [
				'canonical' => '$1',
				'title' => 'static_page_title',
				'description' => 'static_page_meta_description',
				'data-source' => PageSeo::class,
			],
			'^/(datenschutz.*)' => [
				'canonical' => '$1',
				'title' => 'static_page_title',
				'description' => 'static_page_meta_description',
				'data-source' => PageSeo::class,
			],
			'^/(impressum.*)' => [
				'canonical' => '$1',
				'title' => 'static_page_title',
				'description' => 'static_page_meta_description',
				'data-source' => PageSeo::class,
			],
			// CUSTOMER ACCOUNT
			'^/mein-account$' => [
				'canonical' => '/mein-account',
				'robots' => 'NOINDEX,FOLLOW',
			],

			// SEARCH
			'^/suche.*' => [
				'canonical' => '/suche',
				'robots' => 'NOINDEX,FOLLOW',
				'title' => 'search_title',
				'description' => 'search_meta_description',
			],
			'^/recherche.*' => [
				'canonical' => '/suche',
				'robots' => 'NOINDEX,FOLLOW',
				'title' => 'search_title',
				'description' => 'search_meta_description',
			],
			'^/ricerca.*' => [
				'canonical' => '/suche',
				'robots' => 'NOINDEX,FOLLOW',
				'title' => 'search_title',
				'description' => 'search_meta_description',
			],

			// TARIFFS
			'^/abo/vertragsverlaengerung\?mode=contract-renewal&only=1$' => [
				'canonical' => '/abo/vertragsverlaengerung?mode=contract-renewal&only=1',
				'title' => 'renewal_request_title',
				'description' => 'renewal_request_meta_description',
			],
			'^/contrats/prolongation\?mode=contract-renewal&only=1$' => [
				'canonical' => 'contrats/prolongation?mode=contract-renewal&only=1',
				'title' => 'renewal_request_title',
				'description' => 'renewal_request_meta_description',
			],
			'^/contratti/prolungamento\?mode=contract-renewal&only=1$' => [
				'canonical' => 'contratti/prolungamento?mode=contract-renewal&only=1',
				'title' => 'renewal_request_title',
				'description' => 'renewal_request_meta_description',
			],

			// STORES
			'^/filialsuche.*' => [
				'canonical' => '/filialsuche',
				'title' => 'store_search_title',
				'description' => 'store_search_meta_description',
				'robots' => 'NOINDEX,FOLLOW',
			],
			'^/rechercher-magasin.*' => [
				'canonical' => '/rechercher-magasin',
				'title' => 'store_search_title',
				'description' => 'store_search_meta_description',
				'robots' => 'NOINDEX,FOLLOW',
			],
			'^/pronta-negozi.*' => [
				'canonical' => '/pronta-negozi',
				'title' => 'store_search_title',
				'description' => 'store_search_meta_description',
				'robots' => 'NOINDEX,FOLLOW',
			],
			'^/filiale/([0-9]+)/(.+)$' => [
				'canonical' => '/filiale/$1/$2',
				'title' => 'store_title',
				'description' => 'store_meta_description',
				'data-source' => StoreSeo::class,
			],
			'^/magasin/([0-9]+)/(.+)$' => [
				'canonical' => '/filiale/$1/$2',
				'title' => 'store_title',
				'description' => 'store_meta_description',
				'data-source' => StoreSeo::class,
			],
			'^/negozi/([0-9]+)/(.+)$' => [
				'canonical' => '/filiale/$1/$2',
				'title' => 'store_title',
				'description' => 'store_meta_description',
				'data-source' => StoreSeo::class,
			],

			// PDP
			'^/mobiltelefone/([0-9]+)$' => [ // strictly speaking is this an invalid url and should return a 404
				'canonical' => '/mobiltelefone/$1/$2/$3',
				'title' => 'pdp_title',
				'description' => 'pdp_meta_description',
				'data-source' => PdpSeo::class,
			],
			'^/mobiltelefone/([0-9]+)/(.+)/(.+)$' => [
				'canonical' => '/mobiltelefone/$1/$2/$3',
				'title' => 'pdp_title',
				'description' => 'pdp_meta_description',
				'data-source' => PdpSeo::class,
			],
			'^/mobiles/([0-9]+)$' => [ // strictly speaking is this an invalid url and should return a 404
				'canonical' => '/mobiles/$1/$2/$3',
				'title' => 'pdp_title',
				'description' => 'pdp_meta_description',
				'data-source' => PdpSeo::class,
			],
			'^/mobiles/([0-9]+)/(.+)/(.+)$' => [
				'canonical' => '/mobiles/$1/$2/$3',
				'title' => 'pdp_title',
				'description' => 'pdp_meta_description',
				'data-source' => PdpSeo::class,
			],
			'^/telefonicellulari/([0-9]+)$' => [ // strictly speaking is this an invalid url and should return a 404
				'canonical' => '/telefonicellulari/$1/$2/$3',
				'title' => 'pdp_title',
				'description' => 'pdp_meta_description',
				'data-source' => PdpSeo::class,
			],
			'^/telefonicellulari/([0-9]+)/(.+)/(.+)$' => [
				'canonical' => '/telefonicellulari/$1/$2/$3',
				'title' => 'pdp_title',
				'description' => 'pdp_meta_description',
				'data-source' => PdpSeo::class,
			],

			// CATEGORY
			'^/tablets$' => [ // same for all three languages
				'canonical' => '/tablets',
				'title' => 'category_tablets_title',
				'description' => 'category_tablets_meta_description',
			],
			'^/tablets/([0-9]+)$' => [ // same for all three languages
				'canonical' => '/tablets/$1',
				'title' => 'category_tab_title',
				'description' => 'category_tab_meta_description',
				'data-source' => CategoryTabletSeo::class,
			],
			'^/tablets/([0-9]+)/(.+)/(.+)$' => [ // same for all three languages
				'canonical' => '/mobiles/$1/$2/$3',
				'title' => 'pdp_title',
				'description' => 'pdp_meta_description',
				'data-source' => PdpSeo::class,
			],
            '^/gebraucht-secondhand$' => [ // same for all three languages
                'canonical' => '/gebraucht-secondhand',
                'title' => 'category_refurbished_title',
                'description' => 'category_refurbished_meta_description',
            ],
            '^/gebraucht-secondhand/([0-9]+)/(.+)$' => [ // same for all three languages
                'canonical' => '/gebraucht-secondhand/$1/$2',
                'title' => 'pdp_title',
                'description' => 'pdp_meta_description',
                'data-source' => PdpSeo::class,
            ],
            '^/usage-secondhand$' => [ // same for all three languages
                'canonical' => '/usage-secondhand',
                'title' => 'category_refurbished_title',
                'description' => 'category_refurbished_meta_description',
            ],
            '^/usage-secondhand/([0-9]+)/(.+)$' => [ // same for all three languages
                'canonical' => '/usage-secondhand/$1/$2',
                'title' => 'pdp_title',
                'description' => 'pdp_meta_description',
                'data-source' => PdpSeo::class,
            ],
            '^/usato-secondhand$' => [ // same for all three languages
                'canonical' => '/usato-secondhand',
                'title' => 'category_refurbished_title',
                'description' => 'category_refurbished_meta_description',
            ],
            '^/usato-secondhand/([0-9]+)/(.+)$' => [ // same for all three languages
                'canonical' => '/usato-secondhand/$1/$2',
                'title' => 'pdp_title',
                'description' => 'pdp_meta_description',
                'data-source' => PdpSeo::class,
            ],
            '^/smartwatches$' => [ // same for all three languages
				'canonical' => '/smartwatches',
				'title' => 'category_smartwatches_title',
				'description' => 'category_smartwatches_meta_description',
			],
			'^/smartwatches/([0-9]+)/(.+)$' => [ // same for all three languages
				'canonical' => '/smartwatches/$1/$2',
				'title' => 'pdp_title',
				'description' => 'pdp_meta_description',
				'data-source' => PdpSeo::class,
			],
			'^/smartwatches/([0-9]+)' => [ // same for all three languages
				'canonical' => '/smartwatches',
				'title' => 'category_smartw_title',
				'description' => 'category_smartw_meta_description',
				'data-source' => CategorySmartwatchSeo::class,
			],
			'^/mobiltelefone/alle$' => [
				'canonical' => '/mobiltelefone/alle',
				'title' => 'category_smartphones_title',
				'description' => 'category_smartphones_meta_description',
			],
			'^/mobiltelefone/alle/(\d+)$' => [
				'canonical' => '/mobiltelefone/alle/$1',
				'title' => 'category_smartp_title',
				'description' => 'category_smartp_meta_description',
				'data-source' => CategorySmartphoneSeo::class,
			],
			'^/mobiles/tous$' => [
				'canonical' => '/mobiles/tous',
				'title' => 'category_smartphones_title',
				'description' => 'category_smartphones_meta_description',
			],
			'^/mobiles/tous/(\d+)$' => [
				'canonical' => '/mobiles/tous/$1',
				'title' => 'category_smartp_title',
				'description' => 'category_smartp_meta_description',
				'data-source' => CategorySmartphoneSeo::class,
			],
			'^/telefonicellulari/tutto$' => [
				'canonical' => '/telefonicellulari/tutto',
				'title' => 'category_smartphones_title',
				'description' => 'category_smartphones_meta_description',
			],
			'^/telefonicellulari/tutto/(\d+)$' => [
				'canonical' => '/telefonicellulari/tutto/$1',
				'title' => 'category_smartp_title',
				'description' => 'category_smartp_meta_description',
				'data-source' => CategorySmartphoneSeo::class,
			],

			'^/zubehoer$' => [
				'canonical' => '/zubehoer',
				'title' => 'category_accessory_title',
				'description' => 'category_accessory_description',
			],
			'^/zubehoer/([0-9]+)/(.+)$' => [
				'canonical' => '/zubehoer$/$1/$2',
				'title' => 'pdp_title',
				'description' => 'pdp_meta_description',
				'data-source' => PdpSeo::class,
			],
			'^/zubehoer/(.+)$' => [
				'canonical' => '/zubehoer$/$1',
				'title' => 'category_acc_title',
				'description' => 'category_acc_description',
				'data-source' => CategoryAccessorySeo::class,
			],
			'^/accessoires$' => [
				'canonical' => '/accessoires',
				'title' => 'category_accessory_title',
				'description' => 'category_accessory_description',
			],
			'^/accessoires/([0-9]+)/(.+)$' => [
				'canonical' => '/accessoires$/$1/$2',
				'title' => 'pdp_title',
				'description' => 'pdp_meta_description',
				'data-source' => PdpSeo::class,
			],
			'^/accessoires/(.+)$' => [
				'canonical' => '/accessoires$/$1/$2',
				'title' => 'category_acc_title',
				'description' => 'category_acc_description',
				'data-source' => CategoryAccessorySeo::class,
			],
			'^/accessori$' => [
				'canonical' => '/accessori',
				'title' => 'category_accessory_title',
				'description' => 'category_accessory_description',
			],
			'^/accessori/([0-9]+)/(.+)$' => [
				'canonical' => '/accessori/$1/$2',
				'title' => 'pdp_title',
				'description' => 'pdp_meta_description',
				'data-source' => PdpSeo::class,
			],
			'^/accessori/(.+)$' => [
				'canonical' => '/accessori/$1',
				'title' => 'category_acc_title',
				'description' => 'category_acc_description',
				'data-source' => CategoryAccessorySeo::class,
			],

			'^/aktionen' => [
				'canonical' => '/aktionen',
				'title' => 'category_specials_title',
				'description' => 'category_specials_meta_description',
			],
			'^/promotions' => [
				'canonical' => '/promotions',
				'title' => 'category_specials_title',
				'description' => 'category_specials_meta_description',
			],
			'^/promozioni' => [
				'canonical' => '/promozioni',
				'title' => 'category_specials_title',
				'description' => 'category_specials_meta_description',
			],

			// CHECKOUT
			'^/checkout/checkout/index$' => [
				'canonical' => '/checkout/checkout/index',
				'robots' => 'NOINDEX,FOLLOW',
			],

			// CATCHALL
			'^(.*)$' => self::getDefaultRules(),
		];
	}
}
