<?php

namespace app\modules\seo\models;

use app\models\AccessoryCategoryModel;
use app\models\SetSearchRepository;
use Yii;

class CategoryAccessorySeo implements Seo
{
	public static function getEntityInformation($id = null): array
	{
		if (!$id) {
			return [];
		}

		if (AccessoryCategoryModel::isAccessoryCategory($id)) {
			return ['category_title' => Yii::t('seo', "cat-$id")];
		}

		$set = di(SetSearchRepository::class)->findAccessorySets($id);

		if (!$set) {
			return [];
		}

		$title = $set->items->first()->data['title'] ?? null;

		return ['category_title' => $title];
	}
}
