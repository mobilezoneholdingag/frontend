<?php

namespace app\modules\seo\models;

use app\models\ApiRepository;
use app\modules\shopFinder\models\ShopModel;

class StoreSeo implements Seo
{
	public static function getEntityInformation($id = null): array
	{
		if (!$id) {
			return [];
		}

		$entity = di(ApiRepository::class)->model(ShopModel::class)->find($id);

		return [
			'store_name' => $entity->name,
			'store_city' => $entity->city,
		];
	}
}
