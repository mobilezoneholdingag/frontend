<?php

namespace app\modules\seo\models;

use app\models\ApiRepository;
use app\models\ArticleModel;

class PdpSeo implements Seo
{
	public static function getEntityInformation($id = null): array
	{
		if (!$id) {
			return [];
		}

		$entity = di(ApiRepository::class)->model(ArticleModel::class)->find($id);

		if(empty($entity->group->short_name)){
			return [
				'article_name' => $entity->product_name,
			];
		}

		return [
			'article_name' => $entity->product_name,
			'article_group_name' => $entity->group ? $entity->group->short_name : null,
		];
	}
}
