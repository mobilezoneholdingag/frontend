<?php

namespace app\modules\seo\models;

interface Seo
{
	public static function getEntityInformation($id = null): array;
}
