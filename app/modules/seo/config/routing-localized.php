<?php return [
	'urlRules' => [
		'fr-FR' => [
			'rechercher-magasin' => 'shop-finder',
			[
				'pattern' => 'magasin/<id:[\d]+>/<city:[\w\-]+>',
				'route' => 'shop-finder/shop-finder/view',
				'defaults' => [
					'id' => 1,
					'city' => 'Regensdorf',
				],
			],
			'compte-client' => 'customer-account',
			'recherche' => 'search/search/index',
			'contrats/prolongation' => 'product/renewal-request/index',
			'contrats' => 'tariff/tariff/selection',
			'contrats/nouvel-abonnement' => 'tariff/tariff/overview',
			'contrats/reprendre-abonnement' => 'tariff/tariff/porting',

			//About Static Pages
			'portrait/entreprise/portrait' => 'page/about/about-us',
			'portrait/entreprise/organisation' => 'page/about/organisation',
			'portrait/entreprise/domains-activite' => 'page/about/business-units',
			'portrait/media/news-et-communiques' => 'page/about/news',
			'portrait/media/media-service' => 'page/about/media-service',
			'portrait/investisseurs/chiffres-cles' => 'page/about/key-figures',
			'portrait/investisseurs/calendrier' => 'page/about/calendar',
			'portrait/investisseurs/rapports' => 'page/about/reports',
			'portrait/investisseurs/ir-service' => 'page/about/ir-service',
			'portrait/gouvernement/structure' => 'page/about/structure',
			'portrait/gouvernement/conseil-administration' => 'page/about/board-of-directors',
			'portrait/gouvernement/direction' => 'page/about/group-management',
			'portrait/gouvernement/management-team' => 'page/about/management-team',

            'portrait/gouvernement/management-team-commerce' => 'page/about/management-team-trade',
            'portrait/gouvernement/management-team-service-providing' => 'page/about/management-team-service-providing',

			'portrait/gouvernement/downloads' => 'page/about/downloads',
			'portrait/contact' => 'page/about/contact',

			//Support Static Pages
			'support' => 'page/support/service-support',
			'support/reparations' => 'page/support/repair',
			'support/garantie-de-service' => 'page/support/service-garantie',
			'support/assurance-du-portable' => 'page/support/device-insurance',
			'conditions-generales' => 'page/support/terms',
			'transport' => 'page/support/shipment',
			'protection-des-donnees' => 'page/support/data-protection',
			'mentions-legales' => 'page/support/imprint',

			//EN Static Pages
			'about-us/company/about-us' => 'page/about/about-us-en',
			'about-us/company/business-units' => 'page/about/business-units-en',
			'about-us/media/news' => 'page/about/news-en',
			'about-us/media/media-service' => 'page/about/media-service-en',
			'about-us/investors/key-figures' => 'page/about/key-figures-en',
			'about-us/investors/reports' => 'page/about/reports-en',
			'about-us/investors/calendar' => 'page/about/calendar-en',
			'about-us/investors/ir-service' => 'page/about/ir-service-en',
			'about-us/governance/structure' => 'page/about/structure-en',
			'about-us/governance/board-of-directors' => 'page/about/board-of-directors-en',
			'about-us/governance/group-management' => 'page/about/group-management-en',
			'about-us/governance/management-team' => 'page/about/management-team-en',

            'about-us/governance/management-team-trade' => 'page/about/management-team-trade-en',
            'about-us/governance/management-team-service-providing' => 'page/about/management-team-service-providing-en',

			'about-us/governance/downloads' => 'page/about/downloads-en',
			'about-us/contact' => 'page/about/contact-en',
			'about-us/investors/annual-reports-16/start' => 'page/annual-report/start-de',
			'about-us/investors/annual-reports-16/key-figures' => 'page/annual-report/key-figures-de',
			'about-us/investors/annual-reports-16/portrait' => 'page/annual-report/portrait-de',
			'about-us/investors/annual-reports-16/letter-to-shareholders' => 'page/annual-report/letter-to-shareholders-de',
			'about-us/investors/annual-reports-16/highlights' => 'page/annual-report/highlights-de',
			'about-us/investors/annual-reports-16/corporate-governance' => 'page/annual-report/corporate-governance-de',
			'about-us/investors/annual-reports-16/compensation-report' => 'page/annual-report/compensation-report-de',
			'about-us/investors/annual-reports-16/financial-report' => 'page/annual-report/financial-report-de',

			'tv' => 'page/tv/tv',
			'tv/configurator' => 'page/tv/contract-configurator',

			//Jobs Static Pages
			'carriere' => 'page/jobs/jobs',
			'carriere/travailler-chez-mobilezone' => 'page/jobs/work',
			'carriere/formation' => 'page/jobs/education',
			'carriere/positions-vacantes' => 'page/jobs/positions',
			'carriere/positions-vacantes/<slug>' => 'page/jobs/job',

			[
				'pattern' => '<category:(tablets|smartwatches)>/<setName:[\w\-]+>',
				'route' => 'category/category/<category>',
				'defaults' => [
					'setName' => '',
				],
			],
			[
				'pattern' => 'accessoires/<setName:[\w\-]+>',
				'route' => 'category/category/accessories',
				'defaults' => [
					'setName' => '',
				],
			],
			[
				'pattern' => 'mobiles/tous/<manufacturerId:[\d]+>',
				'route' => 'category/category/smartphones',
				'defaults' => [
					'manufacturerId' => '',
				],
			],
			'<category:(tablets|smartwatches)>' => 'category/category/<category>',
			'promotions' => 'category/category/specials',
			'accessoires' => 'category/category/accessories',
            'usage-secondhand' => 'category/category/refurbished',
            [
                'pattern' => 'usage-secondhand/<setName:[\w\-]+>',
                'route' => 'category/category/refurbished',
                'defaults' => [
                    'setName' => '',
                ],
            ],
            [
                'pattern' => 'usage-secondhand/<id:[\d]+>/<manufacturerTitle>/<groupUrlText:[\w\-]+>/<variationName:[\w\-]+>/<providerTitle:[\w\-]+>/<tariffUrlText:[\w\-]+>',
                'route' => 'product/product/refurbished',
                'defaults' => [
                    'id' => null,
                    'manufacturerTitle' => '',
                    'groupUrlText' => '',
                    'variationName' => '',
                    'providerTitle' => '',
                    'tariffUrlText' => '',
                ],
            ],
			'mobiles' => 'category/category/smartphones',
			[
				'pattern' => '<category:(tablets|smartwatches)>/<id:[\d]+>/<manufacturerTitle>/<groupUrlText:[\w\-]+>/<variationName:[\w\-]+>/<providerTitle:[\w\-]+>/<tariffUrlText:[\w\-]+>',
				'route' => 'product/product/<category>',
				'defaults' => [
					'id' => null,
					'manufacturerTitle' => '',
					'groupUrlText' => '',
					'variationName' => '',
					'providerTitle' => '',
					'tariffUrlText' => '',
				],
			],
			[
				'pattern' => 'mobiles/<id:[\d]+>/<manufacturerTitle>/<groupUrlText:[\w\-]+>/<variationName:[\w\-]+>/<providerTitle:[\w\-]+>/<tariffUrlText:[\w\-]+>',
				'route' => 'product/product/smartphones',
				'defaults' => [
					'id' => null,
					'manufacturerTitle' => '',
					'groupUrlText' => '',
					'variationName' => '',
					'providerTitle' => '',
					'tariffUrlText' => '',
				],
			],
			[
				'pattern' => 'contrats/<id:[\d]+>/<providerTitle:[\w\-]+>/<tariffTitle:[\w\-]+>',
				'route' => 'tariff/tariff/tariff',
				'defaults' => [
					'id' => null,
					'providerTitle' => '',
					'tariffTitle' => '',
				],
			],
			[
				'pattern' => 'contrats-etendre/<hash:[\w\-]+>',
				'route' => 'tariff/tariff/renewal',
				'defaults' => [
					'hash' => null,
				],
			],
			[
				'pattern' => 'accessoires/<id:[\d]+>/<manufacturerTitle>/<groupUrlText:[\w\-]+>/<variationName:[\w\-]+>/<providerTitle:[\w\-]+>/<tariffUrlText:[\w\-]+>',
				'route' => 'product/product/accessories',
				'defaults' => [
					'id' => null,
					'manufacturerTitle' => '',
					'groupUrlText' => '',
					'variationName' => '',
					'providerTitle' => '',
					'tariffUrlText' => '',
				],
			],
			[
				'pattern' => 'portrait/media/news-et-communiques/feed<feedID:[\d]+>',
				'route' => 'page/about/feeds',
				'defaults' => [
					'feedID' => '',
				],
			],
			[
				'pattern' => 'about-us/media/news/feed<feedID:[\d]+>',
				'route' => 'page/about/feeds-en',
				'defaults' => [
					'feedID' => '',
				],
			],
		],
		'it-IT' => [
			'pronta-negozi' => 'shop-finder',
			[
				'pattern' => 'negozi/<id:[\d]+>/<city:[\w\-]+>',
				'route' => 'shop-finder/shop-finder/view',
				'defaults' => [
					'id' => 1,
					'city' => 'Regensdorf',
				],
			],
			'conto-cliente' => 'customer-account',
			'ricerca' => 'search/search/index',
			'contratti/prolungamento' => 'product/renewal-request/index',
			'contratti' => 'tariff/tariff/selection',
			'contratti/nuovo-abbonamento' => 'tariff/tariff/overview',
			'contratti/transferire-abbonamento' => 'tariff/tariff/porting',

			//About Static Pages
			'chi-siamo/azienda/chi-siamo' => 'page/about/about-us',
			'chi-siamo/azienda/settori-di-attivita' => 'page/about/business-units',
			'chi-siamo/media/news-e-communicati-stampa' => 'page/about/news',
			'chi-siamo/media/media-service' => 'page/about/media-service',
			'chi-siamo/investitori/indicatori' => 'page/about/key-figures',
			'chi-siamo/investitori/calendario' => 'page/about/calendar',
			'chi-siamo/investitori/rapporti' => 'page/about/reports',
			'chi-siamo/investitori/ir-service' => 'page/about/ir-service',
			'chi-siamo/governance/struttura' => 'page/about/structure',
			'chi-siamo/governance/consiglio-amministrazione' => 'page/about/board-of-directors',
			'chi-siamo/governance/direzione' => 'page/about/group-management',
			'chi-siamo/governance/management-team' => 'page/about/management-team',

            'chi-siamo/governance/management-team-commercio' => 'page/about/management-team-trade',
            'chi-siamo/governance/management-team-service-providing' => 'page/about/management-team-service-providing',

			'chi-siamo/governance/downloads' => 'page/about/downloads',
			'chi-siamo/contatto' => 'page/about/contact',

			//Support Static Pages
			'support' => 'page/support/service-support',
			'support/riparazioni' => 'page/support/repair',
			'support/garanzia-di-servizio' => 'page/support/service-garantie',
			'support/assicurazione' => 'page/support/device-insurance',
			'condizioni-generali' => 'page/support/terms',
			'spedizione' => 'page/support/shipment',
			'protezione-dei-dati' => 'page/support/data-protection',
			'informazioni-legali' => 'page/support/imprint',

			//EN Static Pages
			'about-us/company/about-us' => 'page/about/about-us-en',
			'about-us/company/business-units' => 'page/about/business-units-en',
			'about-us/media/news' => 'page/about/news-en',
			'about-us/media/media-service' => 'page/about/media-service-en',
			'about-us/investors/key-figures' => 'page/about/key-figures-en',
			'about-us/investors/reports' => 'page/about/reports-en',
			'about-us/investors/calendar' => 'page/about/calendar-en',
			'about-us/investors/ir-service' => 'page/about/ir-service-en',
			'about-us/governance/structure' => 'page/about/structure-en',
			'about-us/governance/board-of-directors' => 'page/about/board-of-directors-en',
			'about-us/governance/group-management' => 'page/about/group-management-en',
			'about-us/governance/management-team' => 'page/about/management-team-en',

            'about-us/governance/management-team-trade' => 'page/about/management-team-trade-en',
            'about-us/governance/management-team-service-providing' => 'page/about/management-team-service-providing-en',

			'about-us/governance/downloads' => 'page/about/downloads-en',
			'about-us/contact' => 'page/about/contact-en',
			'about-us/investors/annual-reports-16/start' => 'page/annual-report/start-de',
			'about-us/investors/annual-reports-16/key-figures' => 'page/annual-report/key-figures-de',
			'about-us/investors/annual-reports-16/portrait' => 'page/annual-report/portrait-de',
			'about-us/investors/annual-reports-16/letter-to-shareholders' => 'page/annual-report/letter-to-shareholders-de',
			'about-us/investors/annual-reports-16/highlights' => 'page/annual-report/highlights-de',
			'about-us/investors/annual-reports-16/corporate-governance' => 'page/annual-report/corporate-governance-de',
			'about-us/investors/annual-reports-16/compensation-report' => 'page/annual-report/compensation-report-de',
			'about-us/investors/annual-reports-16/financial-report' => 'page/annual-report/financial-report-de',

			'tv' => 'page/tv/tv',
			'tv/configurator' => 'page/tv/contract-configurator',

			//Jobs Static Pages
			'carriera' => 'page/jobs/jobs',
			'carriera/lavorare-con-mobilezone' => 'page/jobs/work',
			'carriera/tirocinio' => 'page/jobs/education',
			'carriera/posizioni-aperte' => 'page/jobs/positions',
			'carriera/posizioni-aperte/<slug>' => 'page/jobs/job',

			[
				'pattern' => '<category:(tablets|smartwatches)>/<setName:[\w\-]+>',
				'route' => 'category/category/<category>',
				'defaults' => [
					'setName' => '',
				],
			],
			[
				'pattern' => 'accessori/<setName:[\w\-]+>',
				'route' => 'category/category/accessories',
				'defaults' => [
					'setName' => '',
				],
			],
			[
				'pattern' => 'telefonicellulari/tutto/<manufacturerId:[\d]+>',
				'route' => 'category/category/smartphones',
				'defaults' => [
					'manufacturerId' => '',
				],
			],
			
			'<category:(tablets|smartwatches)>' => 'category/category/<category>',
			'promozioni' => 'category/category/specials',
			'accessori' => 'category/category/accessories',
            'usato-secondhand' => 'category/category/refurbished',
            [
                'pattern' => 'usato-secondhand/<setName:[\w\-]+>',
                'route' => 'category/category/refurbished',
                'defaults' => [
                    'setName' => '',
                ],
            ],
            [
                'pattern' => 'usato-secondhand/<id:[\d]+>/<manufacturerTitle>/<groupUrlText:[\w\-]+>/<variationName:[\w\-]+>/<providerTitle:[\w\-]+>/<tariffUrlText:[\w\-]+>',
                'route' => 'product/product/refurbished',
                'defaults' => [
                    'id' => null,
                    'manufacturerTitle' => '',
                    'groupUrlText' => '',
                    'variationName' => '',
                    'providerTitle' => '',
                    'tariffUrlText' => '',
                ],
            ],
			'telefonicellulari' => 'category/category/smartphones',
			[
				'pattern' => '<category:(tablets|smartwatches)>/<id:[\d]+>/<manufacturerTitle>/<groupUrlText:[\w\-]+>/<variationName:[\w\-]+>/<providerTitle:[\w\-]+>/<tariffUrlText:[\w\-]+>',
				'route' => 'product/product/<category>',
				'defaults' => [
					'id' => null,
					'manufacturerTitle' => '',
					'groupUrlText' => '',
					'variationName' => '',
					'providerTitle' => '',
					'tariffUrlText' => '',
				],
			],
			[
				'pattern' => 'telefonicellulari/<id:[\d]+>/<manufacturerTitle>/<groupUrlText:[\w\-]+>/<variationName:[\w\-]+>/<providerTitle:[\w\-]+>/<tariffUrlText:[\w\-]+>',
				'route' => 'product/product/smartphones',
				'defaults' => [
					'id' => null,
					'manufacturerTitle' => '',
					'groupUrlText' => '',
					'variationName' => '',
					'providerTitle' => '',
					'tariffUrlText' => '',
				],
			],
			[
				'pattern' => 'contratti/<id:[\d]+>/<providerTitle:[\w\-]+>/<tariffTitle:[\w\-]+>',
				'route' => 'tariff/tariff/tariff',
				'defaults' => [
					'id' => null,
					'providerTitle' => '',
					'tariffTitle' => '',
				],
			],
			[
				'pattern' => 'contratti-estendere/<hash:[\w\-]+>',
				'route' => 'tariff/tariff/renewal',
				'defaults' => [
					'hash' => null,
				],
			],
			[
				'pattern' => 'accessori/<id:[\d]+>/<manufacturerTitle>/<groupUrlText:[\w\-]+>/<variationName:[\w\-]+>/<providerTitle:[\w\-]+>/<tariffUrlText:[\w\-]+>',
				'route' => 'product/product/accessories',
				'defaults' => [
					'id' => null,
					'manufacturerTitle' => '',
					'groupUrlText' => '',
					'variationName' => '',
					'providerTitle' => '',
					'tariffUrlText' => '',
				],
			],
			[
				'pattern' => 'chi-siamo/media/news-e-communicati-stampa/feed<feedID:[\d]+>',
				'route' => 'page/about/feeds',
				'defaults' => [
					'feedID' => '',
				],
			],
			[
				'pattern' => 'about-us/media/news/feed<feedID:[\d]+>',
				'route' => 'page/about/feeds-en',
				'defaults' => [
					'feedID' => '',
				],
			],
		],
		'de-CH' => [
			'filialsuche' => 'shop-finder',
			[
				'pattern' => 'filiale/<id:[\d]+>/<city:[\w\-]+>',
				'route' => 'shop-finder/shop-finder/view',
				'defaults' => [
					'id' => 1,
					'city' => 'Regensdorf',
				],
			],
			'mein-account' => 'customer-account',
			'suche' => 'search/search/index',
			'abo/vertragsverlaengerung' => 'product/renewal-request/index',
			'abo' => 'tariff/tariff/selection',
			'abo/neuvertrag' => 'tariff/tariff/overview',
			'abo/portierung' => 'tariff/tariff/porting',

			//About Static Pages
			'ueber-uns/unternehmen/ueber-uns' => 'page/about/about-us',
			'ueber-uns/unternehmen/geschaeftsbereiche' => 'page/about/business-units',
			'ueber-uns/medien/mitteilungen' => 'page/about/news',
			'ueber-uns/medien/medienservice' => 'page/about/media-service',
			'ueber-uns/investoren/kennzahlen' => 'page/about/key-figures',
			'ueber-uns/investoren/finanzkalender' => 'page/about/calendar',
			'ueber-uns/investoren/berichte' => 'page/about/reports',
			'ueber-uns/investoren/ir-service' => 'page/about/ir-service',
			'ueber-uns/governance/struktur' => 'page/about/structure',
			'ueber-uns/governance/verwaltungsrat' => 'page/about/board-of-directors',
			'ueber-uns/governance/konzernleitung' => 'page/about/group-management',
			'ueber-uns/governance/management-team' => 'page/about/management-team',

            'ueber-uns/governance/management-team-handel' => 'page/about/management-team-trade',
            'ueber-uns/governance/management-team-service-providing' => 'page/about/management-team-service-providing',

			'ueber-uns/governance/downloads' => 'page/about/downloads',
			'ueber-uns/kontakt' => 'page/about/contact',

			//Support Static Pages
			'support' => 'page/support/service-support',
			'support/reparatur' => 'page/support/repair',
			'support/service-garantie' => 'page/support/service-garantie',
			'support/handyversicherung' => 'page/support/device-insurance',
			'agb' => 'page/support/terms',
			'versand' => 'page/support/shipment',
			'datenschutz' => 'page/support/data-protection',
			'impressum' => 'page/support/imprint',

			//Annual Report Static Pages
			'ueber-uns/investoren/geschaeftsberichte-16/start' => 'page/annual-report/start-de',
			'ueber-uns/investoren/geschaeftsberichte-16/kennzahlen' => 'page/annual-report/key-figures-de',
			'ueber-uns/investoren/geschaeftsberichte-16/portrait' => 'page/annual-report/portrait-de',
			'ueber-uns/investoren/geschaeftsberichte-16/aktionaersbrief' => 'page/annual-report/letter-to-shareholders-de',
			'ueber-uns/investoren/geschaeftsberichte-16/highlights' => 'page/annual-report/highlights-de',
			'ueber-uns/investoren/geschaeftsberichte-16/corporate-governance' => 'page/annual-report/corporate-governance-de',
			'ueber-uns/investoren/geschaeftsberichte-16/verguetungsbericht' => 'page/annual-report/compensation-report-de',
			'ueber-uns/investoren/geschaeftsberichte-16/finanzbericht' => 'page/annual-report/financial-report-de',

			//EN Static Pages
			'about-us/company/about-us' => 'page/about/about-us-en',
			'about-us/company/business-units' => 'page/about/business-units-en',
			'about-us/media/news' => 'page/about/news-en',
			'about-us/media/media-service' => 'page/about/media-service-en',
			'about-us/investors/key-figures' => 'page/about/key-figures-en',
			'about-us/investors/reports' => 'page/about/reports-en',
			'about-us/investors/calendar' => 'page/about/calendar-en',
			'about-us/investors/ir-service' => 'page/about/ir-service-en',
			'about-us/governance/structure' => 'page/about/structure-en',
			'about-us/governance/board-of-directors' => 'page/about/board-of-directors-en',
			'about-us/governance/group-management' => 'page/about/group-management-en',
			'about-us/governance/management-team' => 'page/about/management-team-en',

            'about-us/governance/management-team-trade' => 'page/about/management-team-trade-en',
            'about-us/governance/management-team-service-providing' => 'page/about/management-team-service-providing-en',

			'about-us/governance/downloads' => 'page/about/downloads-en',
			'about-us/contact' => 'page/about/contact-en',
			'about-us/investors/annual-reports-16/start' => 'page/annual-report/start-en',
			'about-us/investors/annual-reports-16/key-figures' => 'page/annual-report/key-figures-en',
			'about-us/investors/annual-reports-16/portrait' => 'page/annual-report/portrait-en',
			'about-us/investors/annual-reports-16/letter-to-shareholders' => 'page/annual-report/letter-to-shareholders-en',
			'about-us/investors/annual-reports-16/highlights' => 'page/annual-report/highlights-en',
			'about-us/investors/annual-reports-16/corporate-governance' => 'page/annual-report/corporate-governance-en',
			'about-us/investors/annual-reports-16/compensation-report' => 'page/annual-report/compensation-report-en',
			'about-us/investors/annual-reports-16/financial-report' => 'page/annual-report/financial-report-en',

			'tv' => 'page/tv/tv',
			'tv/configurator' => 'page/tv/contract-configurator',

			'karriere' => 'page/jobs/jobs',
			'karriere/arbeiten-bei-mobilezone' => 'page/jobs/work',
			'karriere/ausbildung' => 'page/jobs/education',
			'karriere/offene-stellen' => 'page/jobs/positions',
			'karriere/jobs/<slug>' => 'page/jobs/job',

			[
				'pattern' => '<category:(tablets|smartwatches)>/<setName:[\w\-]+>',
				'route' => 'category/category/<category>',
				'defaults' => [
					'setName' => '',
				],
			],
			[
				'pattern' => 'zubehoer/<setName:[\w\-]+>',
				'route' => 'category/category/accessories',
				'defaults' => [
					'setName' => '',
				],
			],
			[
				'pattern' => 'mobiltelefone/alle/<manufacturerId:[\d]+>',
				'route' => 'category/category/smartphones',
				'defaults' => [
					'manufacturerId' => '',
				],
			],
			'<category:(tablets|smartwatches)>' => 'category/category/<category>',
			'aktionen' => 'category/category/specials',
			'zubehoer' => 'category/category/accessories',
			'mobiltelefone' => 'category/category/smartphones',
			[
				'pattern' => '<category:(tablets|smartwatches)>/<id:[\d]+>/<manufacturerTitle>/<groupUrlText:[\w\-]+>/<variationName:[\w\-]+>/<providerTitle:[\w\-]+>/<tariffUrlText:[\w\-]+>',
				'route' => 'product/product/<category>',
				'defaults' => [
					'id' => null,
					'manufacturerTitle' => '',
					'groupUrlText' => '',
					'variationName' => '',
					'providerTitle' => '',
					'tariffUrlText' => '',
				],
			],
			[
				'pattern' => 'mobiltelefone/<id:[\d]+>/<manufacturerTitle>/<groupUrlText:[\w\-]+>/<variationName:[\w\-]+>/<providerTitle:[\w\-]+>/<tariffUrlText:[\w\-]+>',
				'route' => 'product/product/smartphones',
				'defaults' => [
					'id' => null,
					'manufacturerTitle' => '',
					'groupUrlText' => '',
					'variationName' => '',
					'providerTitle' => '',
					'tariffUrlText' => '',
				],
			],
            'gebraucht-secondhand' => 'category/category/refurbished',
            [
                'pattern' => 'gebraucht-secondhand/<setName:[\w\-]+>',
                'route' => 'category/category/refurbished',
                'defaults' => [
                    'setName' => '',
                ],
            ],
            [
                'pattern' => 'gebraucht-secondhand/<id:[\d]+>/<manufacturerTitle>/<groupUrlText:[\w\-]+>/<variationName:[\w\-]+>/<providerTitle:[\w\-]+>/<tariffUrlText:[\w\-]+>',
                'route' => 'product/product/refurbished',
                'defaults' => [
                    'id' => null,
                    'manufacturerTitle' => '',
                    'groupUrlText' => '',
                    'variationName' => '',
                    'providerTitle' => '',
                    'tariffUrlText' => '',
                ],
            ],
			[
				'pattern' => 'aboverlaengern/<hash:[\w\-]+>',
				'route' => 'tariff/tariff/renewal',
				'defaults' => [
					'hash' => null,
				],
			],
			[
				'pattern' => 'abo/<id:[\d]+>/<providerTitle:[\w\-]+>/<tariffTitle:[\w\-]+>',
				'route' => 'tariff/tariff/tariff',
				'defaults' => [
					'id' => null,
					'providerTitle' => '',
					'tariffTitle' => '',
				],
			],
			[
				'pattern' => 'zubehoer/<id:[\d]+>/<manufacturerTitle>/<groupUrlText:[\w\-]+>/<variationName:[\w\-]+>/<providerTitle:[\w\-]+>/<tariffUrlText:[\w\-]+>',
				'route' => 'product/product/accessories',
				'defaults' => [
					'id' => null,
					'manufacturerTitle' => '',
					'groupUrlText' => '',
					'variationName' => '',
					'providerTitle' => '',
					'tariffUrlText' => '',
				],
			],
			[
				'pattern' => 'ueber-uns/medien/mitteilungen/feed<feedID:[\d]+>',
				'route' => 'page/about/feeds',
				'defaults' => [
					'feedID' => '',
				],
			],
			[
				'pattern' => 'about-us/media/news/feed<feedID:[\d]+>',
				'route' => 'page/about/feeds-en',
				'defaults' => [
					'feedID' => '',
				],
			],
		],
	],
];
