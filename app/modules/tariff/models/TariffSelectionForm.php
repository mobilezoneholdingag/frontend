<?php

namespace app\modules\tariff\models;

use app\helper\ArrayHelper;
use app\models\ProviderRepository;
use Illuminate\Support\Collection;
use Yii;
use yii\base\Model;

class TariffSelectionForm extends Model
{
	public $provider;
	public $type;

	public function rules()
	{
		return [
			[['provider'], 'required', 'message' => Yii::t('view', 'Please_select_your_previous_provider')],
			[['type'], 'required'],
			[['provider'], 'number'],
			[['type'], 'string'],
		];
	}

	public function attributeLabels()
	{
		return [
			'provider' => Yii::t('view', 'Please_Select'),
		];
	}

	public function getProviders(string $type = 'new-contract')
	{
		/** @var Collection $collection */
		$collection = di(ProviderRepository::class)->find();

		if ($type == 'renewal') {
			$collection = $collection->where('renewable', '==', true);
		}

		if ($type == 'porting') {
		    $collection = $collection->where('status', '==', true);
        }

		return ArrayHelper::map($collection->toArray(), 'id', 'title');
	}
}
