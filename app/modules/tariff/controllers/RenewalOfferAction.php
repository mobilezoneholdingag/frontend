<?php

namespace app\modules\tariff\controllers;

use app\models\ApiRepository;
use app\models\RenewalRequestOfferModel;
use app\models\TariffModel;
use Illuminate\Support\Collection;
use Yii;
use yii\base\Action;

/**
 * @property TariffController $controller
 */
class RenewalOfferAction extends Action
{
	public function run()
	{
		$request = Yii::$app->request;
		$hash = $request->get('hash');

		$offer = di(ApiRepository::class)->model(RenewalRequestOfferModel::class)->find($hash);
		$text = $this->controller->getOfferText($offer->renewal_request_id);

		/** @var Collection|TariffModel $tariff */
		$tariffId = $offer->tariff_id;
		$tariff = di(ApiRepository::class)->model(TariffModel::class)->find($tariffId);

		$articleId = $offer->article_id;
		$articleTariffs = $this->controller->getArticleTariffFilterModel($tariffId)->article_tariffs->take(19);
		$featuredArticleTariff = $this->controller
			->getArticleTariffFilterModel($tariffId, ['article' => $articleId])
			->article_tariffs
			->first();

		foreach ($articleTariffs as $key => $articleTariff) {
			if ($articleTariff->id == $featuredArticleTariff->id) {
				$articleTariffs->forget($key);
			}
		}

		$articleTariffs->prepend($featuredArticleTariff);

		$params = [
			'tariff' => $tariff,
			'article_tariffs' => $articleTariffs,
			'contract_mode' => 'contract-renewal',
			'featured_article' => $articleId,
			'text' => $text,
			'hash' => $hash,
		];

		return $this->controller->renderFile('@tariff/tariff.twig', $params);
	}
}
