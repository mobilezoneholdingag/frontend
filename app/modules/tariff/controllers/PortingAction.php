<?php

namespace app\modules\tariff\controllers;

use app\models\TariffModel;
use Yii;

class PortingAction extends OverviewAction
{
	protected function getFilterParameters(): array
	{
		return array_merge($this->getRequestParameters(), [
			'per_page' => TariffController::ITEMS_PER_PAGE,
			'offer_type' => TariffModel::OFFER_TYPE_PORTING,
			'facets' => 1,
		]);
	}

	protected function getViewParameters(): array
	{
		return [
			'contract_mode' => 'porting',
		];
	}

	protected function getHiddenFilterParameters(): array
	{
		return [
			'contract_mode' => 'porting',
		];
	}

	private function getRequestParameters()
	{
		$request = Yii::$app->request;
		$parameters = $request->get();

		if (!isset($parameters['provider'])) {
			return $parameters;
		}

		$providerId = $parameters['provider'] ?? null;
		unset($parameters['provider']);
		$request->setQueryParams($parameters);
		$parameters['provider_id_excluded'] = $providerId;

		return $parameters;
	}
}
