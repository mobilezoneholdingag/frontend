<?php

namespace app\modules\tariff\controllers;

use app\models\TariffModel;
use Yii;

class ContractRenewalAction extends OverviewAction
{
	protected function getFilterParameters(): array
	{
		return array_merge(Yii::$app->request->get(), [
			'per_page' => TariffController::ITEMS_PER_PAGE,
			'offer_type' => TariffModel::OFFER_TYPE_RENEW,
			'facets' => 1,
		]);
	}

	protected function getViewParameters(): array
	{
		return [
			'contract_mode' => 'contract-renewal',
			'selected_provider' => ['id' => Yii::$app->request->get('provider')],
		];
	}
}
