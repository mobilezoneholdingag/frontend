<?php

namespace app\modules\tariff\controllers;

use app\models\ApiRepository;
use app\models\ArticleTariffFilterModel;
use app\models\ArticleTariffModel;
use app\models\ProviderModel;
use app\models\TariffFilterModel;
use app\models\TariffModel;
use app\modules\BaseModuleController;
use app\modules\product\models\RenewalRequestModel;
use DateTime;
use Yii;
use app\modules\tariff\models\TariffSelectionForm;
use yii\helpers\Url;

class TariffController extends BaseModuleController
{
	const ITEMS_PER_PAGE = 20;

	const SMARTPHONES_PER_PAGE = 16;

	public function actions()
	{
		$actions = parent::actions();

		$actions['overview']['class'] = OverviewAction::class;

		$actions['renewal-offer']['class'] = RenewalOfferAction::class;

		$actions['contract-renewal']['class'] = ContractRenewalAction::class;

		$actions['porting']['class'] = PortingAction::class;

		return $actions;
	}

	public function actionRenewal()
	{
		$params = Yii::$app->request->get();
		if (isset($params['hash'])) {
			return $this->runAction('renewal-offer', $params);
		}

		return $this->redirect(Url::to([
			'/product/renewal-request/index',
			'mode' => 'contract-renewal',
			'selected_provider' => $params['provider'] ?? null
		]));
	}

	public function actionSelection()
	{
		$tariffSelection = new TariffSelectionForm();

		$request = Yii::$app->request;
		if ($request->isPost) {
			$tariffSelection->load($request->post());

			if ($tariffSelection->validate()) {
				$this->redirect(Url::to([
					'/tariff/tariff/' . $tariffSelection->type,
					'provider' => $tariffSelection->provider
				]));
			}
		}

		$params = [
			'tariff_selection' => $tariffSelection,
		];

		return $this->renderFile('@tariff/selection/selection.twig', $params);
	}

	public function actionFilteredTariffs()
	{
		$request = Yii::$app->request;
		$filter = $request->get();
		$offerType = $this->getOfferType();
		/** @var ApiRepository $repository */
		$repository = di(ApiRepository::class)->model(TariffFilterModel::class);

		/** @var TariffFilterModel $filterModel */
		$filterModel = $repository->findOne(array_merge($filter, [
			'facets' => 0,
			'offer_type' => $offerType,
			'provider_id_excluded' => $request->get('renewal_provider') ?? null,
			'per_page' => self::ITEMS_PER_PAGE,
			'page' => $request->get('page', 1),
			'group_by' => 'tariff_id',
		]));

		$params = [
			'offer_type' => $offerType,
			'contract_mode' => $request->get('mode') ?? $request->get('contract_mode'),
			'tariffs' => $filterModel->tariffs,
			'per_page' => self::ITEMS_PER_PAGE,
			'page' => $filter['page'] ?? 1,
		];

		return $this->renderFile('@tariff/tariff-cards.twig', $params);
	}

	public function getOfferType()
	{
		$mode = Yii::$app->request->get('mode');

		// by "get" parameter
		if($offerType = Yii::$app->request->get('offer-type')) {
			return $offerType;
		}

		// by contract box
		switch ($mode) {
			case 'new-number':
				return TariffModel::OFFER_TYPE_NEW;
			case 'porting':
				return TariffModel::OFFER_TYPE_PORTING;
			case 'contract-renewal':
				return TariffModel::OFFER_TYPE_RENEW;
		}

		// default
		return TariffModel::OFFER_TYPE_NEW;
	}

	public function actionTariff(int $id)
	{
		$tariff = di(ApiRepository::class)->model(TariffModel::class)->find($id);
		$paginate = (bool)Yii::$app->request->get('paginate', false);
		$contractMode = Yii::$app->request->get('mode', null);
		$filterParams = [
			'per-page' => self::SMARTPHONES_PER_PAGE,
			'page' => Yii::$app->request->get('page', 1),
			'facets' => $paginate ? 0 : 1,
		];

        if( $contractMode == 'contract-renewal' ) {
            $filterParams['order_by'] = ['price' => SORT_DESC];
        }

		if ($contractMode === 'contract-renewal' && $tariff->provider_id == ProviderModel::SWISSCOM_ID) {
			$filterParams['swisscom_id'] = true;
		}

		if (is_null($contractMode)) {
			$contractMode = 'new-number';
		} else {
			Yii::$app->session->set('selected_contract_mode', $contractMode);
		}

		$filterModel = $this->getArticleTariffFilterModel($id, $filterParams);

		$params = [
			'tariff' => $tariff,
			'article_tariffs' => $filterModel->article_tariffs,
			'contract_mode' => $contractMode,
			'per_page' => self::SMARTPHONES_PER_PAGE,
			'page' => Yii::$app->request->get('page', 1),
			'total' => $filterModel->total ?? 0,
			'filters' => $filterModel->facets,
		];

		if ($paginate) {
			return $this->renderPaginated($filterModel->article_tariffs, $params);
		}

		return $this->renderFile('@tariff/tariff.twig' , $params);
	}

	/**
	 * @param ArticleTariffModel[]|array $articleTariffs
	 * @param array $params
	 *
	 * @return string
	 */
	private function renderPaginated($articleTariffs, array $params = [])
	{
		$renderedItems = [];
		foreach ($articleTariffs as $articleTariff) {
			$params['article_tariff'] = $articleTariff;
			$params['article'] = $articleTariff->article;
			$renderedItems[] = $this->renderFile('@category/product-card.twig', $params);
		}

		return json_encode($renderedItems);
	}

	public function getArticleTariffFilterModel(int $tariffId, array $filterParams = []): ArticleTariffFilterModel
	{
		$articleTariffFilterModel = di(ApiRepository::class)
			->model(ArticleTariffFilterModel::class)
			->findOne(array_merge($filterParams, [
				'tariff' => $tariffId,
				'group_by' => 'group_id',
			]), true);

		return $articleTariffFilterModel;
	}

	public function getOfferText($renewalRequest)
	{
		/** @var RenewalRequestModel $renewalRequest */
		$renewalRequest = di(ApiRepository::class)->model(RenewalRequestModel::class)->find($renewalRequest);

		$text = Yii::t(
			'view',
			'Contract Renewal for {fullname} - phone Number {phone_number} (valid until {validUntil})',
			[
				'fullname' => $renewalRequest->getFullName(),
				'validUntil' => (new DateTime($renewalRequest->valid_until_date))->format('d.m.Y'),
				'phone_number' => $renewalRequest->phone_number,
			]
		);

		return $text;
	}
}
