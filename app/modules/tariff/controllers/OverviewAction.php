<?php

namespace app\modules\tariff\controllers;

use app\models\ApiRepository;
use app\models\TariffFilterModel;
use Yii;
use yii\base\Action;

/**
 * @property TariffController $controller
 */
class OverviewAction extends Action
{
	public function run()
	{
		/** @var ApiRepository $repository */
		$repository = di(ApiRepository::class)->model(TariffFilterModel::class);
		/** @var TariffFilterModel $filterModel */
		$filterModel = $repository->findOne($this->getFilterParameters());
		$facetsModel = $filterModel->facets;
		// disable rate payment filter
		$facetsModel->deferred_payment_optional = null;

		$request = Yii::$app->request;
		$params = [
			'offer_type' => $this->controller->getOfferType(),
			'contract_mode' => $request->get('mode'),
			'provider' => $request->get('provider'),
			'tariffs' => $filterModel->tariffs,
			'filters' => $facetsModel,
			'total' => $filterModel->total ?? 0,
			'per_page' => TariffController::ITEMS_PER_PAGE,
			'page' => 1,
			'hidden_filter' => $this->getHiddenFilterParameters(),
		];

		return $this->controller->renderFile('@tariff/overview.twig', array_merge($params, $this->getViewParameters()));
	}

	protected function getViewParameters(): array
	{
		return [];
	}

	protected function getHiddenFilterParameters(): array
	{
		return [];
	}

	protected function getFilterParameters(): array
	{
		return array_merge(Yii::$app->request->get(), [
			'facets' => 1,
			'per_page' => TariffController::ITEMS_PER_PAGE,
			'offer_type' => $this->controller->getOfferType(),
		]);
	}
}
