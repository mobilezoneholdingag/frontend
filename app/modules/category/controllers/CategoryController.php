<?php

namespace app\modules\category\controllers;

use app\models\ApiRepository;
use app\models\ArticleModel;
use app\models\LandingPageModel;
use app\models\ManufacturerModel;
use app\models\SetSearchModel;
use app\models\SetSearchRepository;
use app\modules\BaseModuleController;
use app\services\Seo;
use Yii;

/**
 * Class CategoryController
 *
 * @package app\modules\category\controllers
 */
class CategoryController extends BaseModuleController
{
	protected $setSearchRepository;
	protected $apiRepository;

	const ITEMS_PER_PAGE = 48;

	public function __construct(
		$id,
		$module,
		SetSearchRepository $setSearchRepository,
		ApiRepository $apiRepository,
		array $config = []
	)
	{
		parent::__construct($id, $module, $config);

		$this->setSearchRepository = $setSearchRepository;
		$this->apiRepository = $apiRepository;
	}

	/**
	 * @param null $manufacturerId
	 *
	 * @return string
	 */
	public function actionSmartphones($manufacturerId = null)
	{
		$setId = $this->getArticleSetIdFromRequest('category-smartphones');

		$requestParams = !empty($manufacturerId) ? ['manufacturer_id' => $manufacturerId] : $this->getRequestParams();
		$requestParams['id'] = $setId;
		$requestParams['facets'] = 1;
		$requestParams['per-page'] = self::ITEMS_PER_PAGE;

		/** @var ApiRepository $repository */
		$repository = di(ApiRepository::class);
		/** @var SetSearchModel $articlesAndFilters */
		$articlesAndFilters = $repository->model(SetSearchModel::class)->findOne($requestParams);

		$manufacturer = $repository->model(ManufacturerModel::class)->find($manufacturerId) ?? null;

		$params = [
			'initial_articles' => $articlesAndFilters->getArticlesOnly(),
			'filters' => $articlesAndFilters->facets,
			'initialFilterId' => $manufacturerId, // TODO this is just a quickfix
			'category_type' => Yii::$app->controller->action->id,
			'per_page' => self::ITEMS_PER_PAGE,
			'total' => $articlesAndFilters->total ?? 0,
			'page' => 1,
			'manufacturer' => $manufacturer,
		];

		return $this->renderFile('@category/category/smartphones.twig', $params);
	}

    /**
     * @param null $manufacturerId
     *
     * @return string
     */
    public function actionRefurbished($manufacturerId = null)
    {
        if($manufacturerId == null) {
            Yii::$app->response->redirect('/occasion-handy-gebraucht', 301)->send();
            Yii::$app->end();

            return;
        }else{
            $setId = $this->getArticleSetIdFromRequest('gebrauchte-geraete');

            $requestParams = !empty($manufacturerId) ? ['manufacturer_id' => $manufacturerId] : $this->getRequestParams();
            $requestParams['id'] = $setId;
            $requestParams['facets'] = 1;
            $requestParams['per-page'] = self::ITEMS_PER_PAGE;

            /** @var ApiRepository $repository */
            $repository = di(ApiRepository::class);
            /** @var SetSearchModel $articlesAndFilters */
            $articlesAndFilters = $repository->model(SetSearchModel::class)->findOne($requestParams);

            $manufacturer = $repository->model(ManufacturerModel::class)->find($manufacturerId) ?? null;

            $params = [
                'hideFilter' => true,
                'initial_articles' => $articlesAndFilters->getArticlesOnly(),
                'filters' => $articlesAndFilters->facets,
                'initialFilterId' => $manufacturerId, // TODO this is just a quickfix
                'category_type' => Yii::$app->controller->action->id,
                'per_page' => self::ITEMS_PER_PAGE,
                'total' => $articlesAndFilters->total ?? 0,
                'page' => 1,
                'manufacturer' => $manufacturer,
            ];

            return $this->renderFile('@category/category/smartphones.twig', $params);
        }
    }


	/**
	 * @param null $setName
	 *
	 * @return string
	 */
	public function actionTablets($setName = null)
	{
		return $this->categoryWithManufacturerFilter('tablets', $setName);
	}

	/**
	 * @param null $setName
	 *
	 * @return string
	 */
	public function actionAccessories($setName = null)
	{

        $content = $this->getContent('accessories', $setName);

        if (!empty($content)) {

            if(substr($content->title, 0, 1) == "*") {
                Seo::setTitle(substr($content->title, 1));
            } else {
                Seo::setTitle($content->title);
            }

            Seo::setMetaDescription($content->meta_description);

            return $this->renderFile('@landingpage/index.twig', ['landingpage' => $content]);
        }

		$accessorySets = $this->setSearchRepository->findAccessorySets($setName)->items;
		$articlesAndFilters = $this->setSearchRepository->findByAccessorySets($accessorySets);
		$allSets = $this->setSearchRepository->findAccessorySets()->items;
		$systemTitles = $this->setSearchRepository->getAllSetsSystemTitles($accessorySets);

		$params = [
			'initial_articles' => $articlesAndFilters->articlesOnly,
			'filters' => $articlesAndFilters->facets,
			'category_type' => Yii::$app->controller->action->id,
			'all_sets' => $allSets,
			'all_set_ids' => $allSets->map(function($set) { return $set()->id; })->toArray(),
			'system_titles' => $setName ? $systemTitles : null,
			'subscription_not_available' => true,
			'per_page' => self::ITEMS_PER_PAGE,
			'total' => $articlesAndFilters->total ?? 0,
			'page' => 1,
		];

		return $this->renderFile('@category/category/accessories.twig', $params);
	}

	/**
	 * @param null $setName
	 *
	 * @return string
	 */
	public function actionSmartwatches($setName = null)
	{
		return $this->categoryWithManufacturerFilter('smartwatches', $setName);
	}

	/**
	 * @param $categoryName
	 * @param null $filter
	 *
	 * @return string
	 */
	protected function categoryWithManufacturerFilter($categoryName, $setName = null)
	{
		$setId = $this->getArticleSetIdFromRequest('category-'.$categoryName);
		$requestParams = !empty($setName) ? ['manufacturer' => $setName] : Yii::$app->request->get('manufacturer', null);
		$requestParams['id'] = $setId;
		$requestParams['facets'] = 1;
		$requestParams['per-page'] = self::ITEMS_PER_PAGE;

		/** @var ApiRepository $repository */
		$repository = di(ApiRepository::class);
		$articlesAndFilters = $repository->model(SetSearchModel::class)->findOne($requestParams);

		$params = [
			'initial_articles' => $articlesAndFilters->getArticlesOnly(),
			'subscription_not_available' => true,
			'category_type' => Yii::$app->controller->action->id,
		];

		return $this->renderFile('@category/category/'. $categoryName .'.twig', $params);
	}

	/**
	 * @return string
	 */
	public function actionSpecials()
	{
		/** @var ApiRepository $repository */
		$repository = di(ApiRepository::class);
		$articles = $repository->model(ArticleModel::class)->find(null, ['image_overlay_text_exists' => 1]);

		$params = [
			'initial_articles' => $articles,
			'subscription_not_available' => true,
			'category_type' => Yii::$app->controller->action->id,
		];

		return $this->renderFile('@category/category/specials.twig', $params);
	}

	public function actionFilteredProducts()
	{
		$renderedItems = [];

		$filter = Yii::$app->request->get();
		$setId = $this->getArticleSetIdFromRequest($this->getFallbackId());
		$filter['id'] = $setId;
		$filter['per-page'] = self::ITEMS_PER_PAGE;

		/** @var ApiRepository $repository */
		$repository = di(ApiRepository::class);
		/** @var SetSearchModel $set */
		$set = $repository->model(SetSearchModel::class)->findOne($filter);
		$articlesOnly = $set->getArticlesOnly();

		foreach ($articlesOnly as $item) {
			$renderedItems[] = $this->renderFile('@category/product-card.twig', ['article' => $item]);
		}

		return json_encode($renderedItems);
	}

	private function getFallbackId()
	{
		switch (Yii::$app->request->get('type')) {
			case 'accessories':
				return $this->getSystemTitles();
			case 'product':
				return 'category-smartphones';
			default:
				return 'category-smartphones';
		}
	}

	/**
	 * @param $fallbackId
	 *
	 * @return array|mixed
	 */
	protected function getArticleSetIdFromRequest($fallbackId)
	{
		$key = Yii::$app->request->get('product_type') ? 'product_type' : 'set';

		return Yii::$app->request->get($key, $fallbackId);
	}

	/**
	 * @return array
	 */
	protected function getRequestParams()
	{
		$params = [];
		$params['manufacturer_id'] = Yii::$app->request->get('manufacturer_id', null);

		return $params;
	}

	private function getSystemTitles(): array
	{
		$accessorySets = $this->setSearchRepository->findAccessorySets()->items;

		return $this->setSearchRepository->getAllSetsSystemTitles($accessorySets);
	}

	private function getContent($url, $setName)
    {

        if(empty($setName)) {
            return di(ApiRepository::class)->model(LandingPageModel::class)->find('content-' . $url);
        }

        return di(ApiRepository::class)->model(LandingPageModel::class)->find('content-' . $url . '-' . $setName);

    }
}
