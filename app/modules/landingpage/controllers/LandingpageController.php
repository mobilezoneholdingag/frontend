<?php

namespace app\modules\landingpage\controllers;

use app\models\ApiRepository;
use app\models\LandingPageModel;
use app\modules\BaseModuleController;
use app\services\Seo;

class LandingpageController extends BaseModuleController
{
	public function actionIndex($landingpageUrl = null)
	{
		$landingpage = di(ApiRepository::class)->model(LandingPageModel::class)->find($landingpageUrl);
		if (isset($landingpage) === false) {
			return $this->redirect('/');
		}


		if(substr($landingpage->title, 0, 1) == "*") {
            Seo::setTitle(substr($landingpage->title, 1));
        } else {
            Seo::setTitle($landingpage->title);
        }

		Seo::setMetaDescription($landingpage->meta_description);

		$isActive = $landingpage->active == LandingPageModel::IS_ACTIVE;

		$target = $isActive ? $this->renderFile('@landingpage/index.twig', ['landingpage' => $landingpage]) : $this->redirect('/');

		return $target;
	}
}
