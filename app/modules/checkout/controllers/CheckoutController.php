<?php

namespace app\modules\checkout\controllers;

use app\models\CountryInterface;
use app\models\OrderModel;
use app\models\PassportTypeInterface;
use app\models\PaymentModel;
use app\modules\BaseModuleController;
use app\modules\checkout\events\CheckoutEvent;
use app\modules\checkout\services\cart\CartInterface;
use app\modules\checkout\services\cart\SessionCart;
use app\modules\checkout\services\checkout\CheckoutBuilderInterface;
use app\modules\checkout\services\checkout\CheckoutInterface;
use app\modules\checkout\services\checkout\CheckoutStorageInterface;
use app\modules\checkout\services\checkout\step\CartStep;
use app\modules\checkout\services\checkout\step\PaymentStep;
use app\modules\checkout\services\checkout\step\StepInterface;
use app\modules\customerAccount\CustomerAccountModule;
use app\services\auth\AuthProviderInterface;
use app\services\SessionInterface;
use deinhandy\yii2events\dispatcher\EventDispatcher;
use Snowcap\Emarsys\Client;
use Snowcap\Emarsys\CurlClient;
use Yii;
use yii\base\Exception;
use yii\helpers\Url;

class CheckoutController extends BaseModuleController
{
	protected $cart;
	public $checkoutBuilder;
	public $checkoutStorage;
	protected $authProvider;
	protected $eventDispatcher;
	protected $session;


    private $emarsysLangMapping = [
        'de-CH' => 2,
        'en-EN' => 1,
        'fr-FR' => 3,
        'it-IT' => 4
    ];


	public function __construct(
		$id,
		$module,
		CartInterface $cart,
		CheckoutBuilderInterface $checkoutBuilder,
		CheckoutStorageInterface $checkoutStorage,
		AuthProviderInterface $authProvider,
		EventDispatcher $eventDispatcher,
		SessionInterface $session,
		$config = []
	) {
		$this->cart = $cart;
		$this->checkoutBuilder = $checkoutBuilder;
		$this->checkoutStorage = $checkoutStorage;
		$this->authProvider = $authProvider;
		$this->eventDispatcher = $eventDispatcher;
		$this->session = $session;

		if ($this->checkoutStorage->getCurrentStep() != 'overview') {
			$this->cart->removeCommisionArticle();
		}

		parent::__construct($id, $module, $config);
	}

	public function actionSelectStep($id)
	{
		$steps = $this->checkoutBuilder->build();
		foreach ($steps as $step) {
			if ($step->getIdentifier() === $id && $step->isAccessible()) {
				$this->checkoutStorage->setCurrentStep($step);

				break;
			}
		}

		return $this->redirect('/checkout/checkout');
	}

	public function actionTruncate()
	{
		$this->cart->truncate();
		$this->checkoutStorage->truncate();

		return $this->redirect('/checkout/checkout');
	}

	public function actionBack()
	{
		$this->checkoutBuilder->back();

		return $this->redirect('/checkout/checkout');
	}

	public function actionNext()
	{
		$this->checkoutBuilder->next();

		return $this->redirect('/checkout/checkout');
	}

	public function actionProcess()
	{
		/** @var StepInterface|null $activeStep */
		$activeStep = $this->checkoutBuilder->getCurrentStep();
		$activeStep->process(Yii::$app->request->bodyParams);
		$isValid = $activeStep->isValid();

		$this->checkoutStorage->store($activeStep);

		if ($isValid) {
			if ($this->checkoutBuilder->isFinished()) {

			    /*
			     * Dumb Copy from customerAccount/NewsletterController
			     */

			    if($activeStep::IDENTIFIER == 'overview' && $activeStep->newsletter && $activeStep->newsletter->register == 1) {

                    try {
                        $module = CustomerAccountModule::getInstance();
                        $emarsys = new Client(new CurlClient(), $module->emarsys['user'], $module->emarsys['secret']);

                        $emarsys->addFieldsMapping([
                            'quelle' => 14670,
                        ]);

                        $customer = $this->authProvider->user();
                        $customer->fetch($customer->id);

                        $emarsys->createContact([
                            'email' => $customer->email,
                            'firstName' => $customer->first_name,
                            'lastName' => $customer->last_name,
                            'gender' => $emarsys->getChoiceId('gender', ($customer->gender == 1) ? 'male' : 'female'),
                            'language' => $this->emarsysLangMapping[Yii::$app->language],
                            'optin' => 1,
                            'salutation' => $emarsys->getChoiceId('salutation', ($customer->gender == 1) ? 'mr' : 'mrs'), // mr und mrs
                            'quelle' => 'Subscriber_Webshop'
                        ]);

                    } catch (Exception $e) {

                    }

                }

				$checkoutEvent = new CheckoutEvent([
					'name' => CheckoutInterface::EVENT_COMPLETED,
				]);

				$this->eventDispatcher->dispatch($checkoutEvent);
				$orderData = $checkoutEvent->data;
				$this->session['payment_redirect_html'] = $orderData['payment_redirect_html'];

				return $this->redirect([
					$this->determineRedirectTarget($orderData),
					'orderId' => $orderData['order_id'],
				]);
			} else {
				$this->checkoutBuilder->next();
			}
		}

		return $this->redirect('/checkout/checkout');
	}

	private function determineRedirectTarget(array $orderData)
	{
		if ($orderData['payment_redirect_html']) {
			return '/checkout/checkout/payment-redirect';
		}

		return '/checkout/checkout/success';
	}

	public function actionPaymentRedirect($orderId)
	{
		return $this->renderFile('@checkout/payment-redirect.twig', [
			'order_id' => $orderId,
			'payment_redirect_html' => $this->session['payment_redirect_html'],
		]);
	}

	public function actionRetry($orderId)
	{
		$session = Yii::$app->session;
		$session->addFlash('danger', Yii::t('checkout', 'Payment_Failed'));

		$customer = $this->authProvider->user();
		$customer->fetch($customer->id);
		/** @var OrderModel $order */
		$order = $customer->getOrders()->where('id', $orderId)->first();
		$session->set(PaymentModel::FAILED_PAYMENT_METHODS_SESSION_KEY, array_merge(
			$session->get(PaymentModel::FAILED_PAYMENT_METHODS_SESSION_KEY, []),
			[$order->payment_method]
		));
		/** @var SessionCart $cart */
		$cart = di(CartInterface::class);
		$cart->restoreBackup();

		return $this->redirect('/checkout/checkout/select-step/payment');
	}

	public function actionSuccess($orderId = null)
	{
		if($orderId == null){
			return $this->redirect(Url::base(true));
		}

		$customer = $this->authProvider->user();

		if($customer == null){
			return $this->redirect(Url::base(true));
		}

		$customer->fetch($customer->id);
		/** @var OrderModel $order */
		$order = $customer->getOrders()->where('id', $orderId)->first();
		if ($order->status == OrderModel::STATUS_PAYMENT_INCOMPLETE) {
			return $this->redirect(['retry', 'orderId' => $orderId]);
		} else {
			$this->checkoutStorage->truncate();
		}
		$orderHasTariff = $order->orderHasTariff();
		$countries = CountryInterface::ALL;
		$nationality = isset($countries[$customer->nationality]) ? Yii::t('countries', $countries[$customer->nationality]) : '';
		$passportTypes = PassportTypeInterface::ALL;
		$passportType = isset($passportTypes[$customer->passport_type]) ? Yii::t('passport-types', $passportTypes[$customer->passport_type]) : '';

		if (Yii::$app->session->has('simCardIndeterminable')) {
			Yii::$app->session->addFlash('danger', Yii::t('view', 'Retention_Offers_New_Sim_Card_Needed'));
			Yii::$app->session->remove('simCardIndeterminable');
		}

		$params = [
			'order' => $order,
			'customer' => $customer,
			'orderHasTariff' => $orderHasTariff,
			'nationality' => $nationality,
			'passportType' => $passportType,
		];

		return $this->renderFile('@checkout/success.twig', $params);
	}

	public function actionError($orderId)
	{
		return $this->redirect(['retry', 'orderId' => $orderId]);
	}

	/**
	 * @param null|StepInterface $activeStep
	 *
	 * @return string
	 */
	public function actionIndex($activeStep = null)
	{
	    echo'1122';exit;
		/** @var StepInterface[] $steps */
		$steps = $this->checkoutBuilder->build();

		if (!$activeStep) {
			foreach ($steps as $step) {
				if ($step->isActive()) {
					$activeStep = $step;

					break;
				}
			}
		}

		if ($activeStep->isSubmitted()) {
			$activeStep->validate();
		}

		// @TODO this is a fast and hacky solution for zero total_once cart.
		// @TODO This should be removed by disabling the paymentstep in this case
		if ($activeStep->getIdentifier() == PaymentStep::IDENTIFIER) {
			$cartData = $this->checkoutStorage->get(CartStep::class)->getData();
			if (isset($cartData['totals']['total']['once']) && $cartData['totals']['total']['once'] == 0) {
				$paymentStepProcessData = [
					'PaymentModel' => [
						'type' => 'no-payment-required',
					],
				];
				/** @var PaymentStep $activeStep */
				$activeStep->processWithCommissionArticle = false;
				$activeStep->process($paymentStepProcessData);
				$this->checkoutStorage->store($activeStep);
				$this->checkoutBuilder->next();
				return $this->redirect('/checkout/checkout');
			}
		}

		return $this->renderFile('@checkout/index.twig', [
			'steps' => $steps,
			'active_step' => $activeStep,
			'cartHasTariff' => $this->cart->hasTariff(),
		]);
	}
}
