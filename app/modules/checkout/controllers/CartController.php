<?php

namespace app\modules\checkout\controllers;

use app\models\AbstractApiModel;
use app\models\ApiRepository;
use app\models\ArticleModel;
use app\models\ArticleTariffFilterModel;
use app\models\CustomerModel;
use app\models\OrderItemModel;
use app\models\OrderModel;
use app\modules\BaseModuleController;
use app\modules\checkout\services\cart\CartInterface;
use app\modules\checkout\services\cart\CartTotalsCalculator;
use app\modules\checkout\services\cart\ItemInterface;
use app\modules\checkout\services\checkout\CheckoutStorageInterface;
use app\modules\checkout\services\checkout\step\CartStep;
use app\services\auth\AuthProvider;
use app\twig\CurrencyExtension;
use Yii;
use yii\web\NotFoundHttpException;

class CartController extends BaseModuleController
{
	protected $cart;
	protected $checkoutStorage;

	protected $allowedTypes = [
		ItemInterface::TYPE_ARTICLE,
		ItemInterface::TYPE_ARTICLE_TARIFF,
		ItemInterface::TYPE_TARIFF,
		ItemInterface::TYPE_COUPON,
		ItemInterface::TYPE_ACCESSORIES,
	];

	public $enableCsrfValidation = false;

	public function __construct(
		$id,
		$module,
		CartInterface $cart,
		CheckoutStorageInterface $checkoutStorage,
		$config = []
	) {
		$this->cart = $cart;
		$this->checkoutStorage = $checkoutStorage;

		parent::__construct($id, $module, $config);
	}

	public function actionAdd()
	{
		$request = Yii::$app->request;

		$itemType = $request->post('item_type');
		$itemId = $request->post('item_id');
		$this->setArticleHash();
		$this->resetSteps();

		if (
			$itemType === ItemInterface::TYPE_ARTICLE_TARIFF
			&& is_array($itemId)
			&& isset($itemId[ItemInterface::TYPE_ARTICLE]) && isset($itemId[ItemInterface::TYPE_TARIFF])
		) {
			// try to find article tariffs
			$filterModel = di(ApiRepository::class)->model(ArticleTariffFilterModel::class)->findOne([
				ItemInterface::TYPE_ARTICLE => $itemId[ItemInterface::TYPE_ARTICLE],
				ItemInterface::TYPE_TARIFF => $itemId[ItemInterface::TYPE_TARIFF],
			]);

			if ($filterModel->article_tariffs->count() === 1) {
				$this->addItemToCart($itemType, $filterModel->article_tariffs->first()->id);
			// if not found, then put seperate article & tariff into the cart
			} else {
				$this->addItemToCart(ItemInterface::TYPE_ARTICLE, $itemId[ItemInterface::TYPE_ARTICLE]);
				$this->addItemToCart(ItemInterface::TYPE_TARIFF, $itemId[ItemInterface::TYPE_TARIFF]);
			}
		} else {
			$this->addItemToCart($itemType, $itemId);
		}

		return $this->redirect('/checkout/checkout');
	}

	private function resetSteps()
	{
		$cartStep = $this->checkoutStorage->get(CartStep::class);
		if ($cartStep instanceof CartStep) {
			$this->checkoutStorage->setCurrentStep($cartStep);
		}
	}

	private function setArticleHash()
	{
		$hash = Yii::$app->request->post('hash');

		Yii::$app->session->remove('article_hash');

		if (!is_null($hash)) {
			Yii::$app->session->set('article_hash', $hash);
		}
	}

	/**
	 * recently only updating insurances in article model
	 */
	public function actionUpdateInsurance()
	{
		$insuranceId = Yii::$app->request->post('insurance_id', null);
		$articleId = Yii::$app->request->post('article_id', null);

		/** @var ArticleModel $item */
		$item = $this->cart->getItem($articleId);
		$item->insurance_id = $insuranceId;
		$this->cart->updateItem($item);

		Yii::$app->response->format = 'json';

		return $this->getFormattedPrices();
	}

	private function getFormattedPrices(): array
	{
		/** @var CartTotalsCalculator $calculator */
		$calculator = di(CartTotalsCalculator::class);
		$prices = $calculator->calculate();

		array_walk_recursive($prices, function(&$price) {
			$price = CurrencyExtension::price($price);
	});

		return $prices;
	}

	public function actionRemove()
	{
		$identifier = Yii::$app->request->get('id');

		$this->resetSteps();
		$this->cart->removeItem($this->cart->getItem($identifier));
		Yii::$app->getSession()->addFlash('item-removed', $identifier);

		return $this->redirect('/checkout/checkout');
	}

	public function actionTruncate()
	{
		$this->cart->truncate();
	}

	/**
	 * @param $itemType
	 * @param $itemId
	 */
	protected function addItemToCart($itemType, $itemId)
	{
		if (in_array($itemType, $this->allowedTypes)) {
			$item = di($itemType);
			/** @var $item ItemInterface|AbstractApiModel */
			$item->fetch($itemId);

			if ($item->validate()) {
				$this->cart->addItem($item);
			}
		}
	}

	public function actionAcceptOffer($id)
	{
		/** @var ApiRepository $repository */
		$repository = di(ApiRepository::class)->model(OrderModel::class);
		/** @var OrderModel $order */
		$order = $repository->find($id, ['offered' => true]);

		if (!$order) {
			throw new NotFoundHttpException();
		}

		$this->cart->truncate();
		$this->checkoutStorage->truncate();

		/** @var OrderItemModel $item */
		foreach ($order->items as $item) {
			$this->addItemToCart($item->item_product_type_id, $item->item_id);
		}

		return $this->redirect('/checkout/checkout');
	}
}
