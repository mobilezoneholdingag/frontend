<?php

namespace app\modules\checkout\controllers;

use app\modules\BaseModuleController;
use app\modules\checkout\services\cart\CartInterface;
use app\modules\checkout\services\checkout\CheckoutBuilderInterface;
use app\modules\checkout\services\checkout\CheckoutStorageInterface;
use app\modules\checkout\services\checkout\step\CartStep;
use app\modules\checkout\services\checkout\step\PaymentStep;
use app\modules\checkout\services\checkout\step\StepInterface;

use app\services\auth\AuthProviderInterface;
use app\services\SessionInterface;
use deinhandy\yii2events\dispatcher\EventDispatcher;


class ApiController extends BaseModuleController
{
    protected $cart;
    public $checkoutBuilder;
    public $checkoutStorage;
    protected $authProvider;
    protected $eventDispatcher;
    protected $session;

    public function __construct(
        $id,
        $module,
        CartInterface $cart,
        CheckoutBuilderInterface $checkoutBuilder,
        CheckoutStorageInterface $checkoutStorage,
        AuthProviderInterface $authProvider,
        EventDispatcher $eventDispatcher,
        SessionInterface $session,
        $config = []
    ) {
        $this->cart = $cart;
        $this->checkoutBuilder = $checkoutBuilder;
        $this->checkoutStorage = $checkoutStorage;
        $this->authProvider = $authProvider;
        $this->eventDispatcher = $eventDispatcher;
        $this->session = $session;

        if ($this->checkoutStorage->getCurrentStep() != 'overview') {
            $this->cart->removeCommisionArticle();
        }

        parent::__construct($id, $module, $config);
    }

    /**
     * @return string
     */
    public function actionPayments()
    {
        $apiUrl = "http://digon-alb-498886375.eu-central-1.elb.amazonaws.com/cart-service";
        //$apiUrl = "http://mozo:mozo123456@http://cart.local.mztech.ch:8080";
        $config = new \Mozo\Digon\MsCartApi\V1\Configuration();
        $config->setHost($apiUrl);
        $client = new \Mozo\Digon\MsCartApi\V1\Client\DefaultApi(null, $config);

        try {
            $channel = '000014';
            $user = 'user123';

            echo '<pre>';

            $result = $client->cartGet($channel, $user);
            if (method_exists($result, 'getData')) {
                $dataItems = $result->getData();
                //make sort for array
                foreach ($dataItems as $dataItem) {
                    print_r($dataItem);
                }
            }
        } catch (\Exception $e) {
            print_r($e->getMessage());
            print_r($e->getCode());
        }




        echo'Hello World!!!';
        exit;

    }

}