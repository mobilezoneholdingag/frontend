<?php

use app\models\AccessoriesModel;
use app\models\ArticleModel;
use app\models\ArticleTariffModel;
use app\models\CouponModel;
use app\models\TariffModel;
use app\modules\checkout\services\cart\CartTotalsCalculator;
use app\modules\checkout\services\cart\CheckoutDataExtractor;
use app\modules\checkout\services\cart\ItemInterface;
use app\modules\checkout\services\cart\PaymentBillDataExtractor;
use app\modules\checkout\services\cart\PaymentBillMockDataExtractor;
use app\modules\checkout\services\cart\SessionCart;
use app\modules\checkout\services\cart\CartInterface;
use app\modules\checkout\services\checkout\CheckoutBuilder;
use app\modules\checkout\services\checkout\CheckoutBuilderInterface;
use app\modules\checkout\services\checkout\CheckoutStorageInterface;
use app\modules\checkout\services\checkout\SessionCheckoutStorage;
use deinhandy\yii2container\Container;

/** @var Container $container */
$container = di();

// common
$container->setSingleton(\app\modules\checkout\events\EventListener::class, function(Container $container) {
	return (new \app\modules\checkout\events\EventListener(
		$container->get(CartInterface::class),
		$container->get(CheckoutStorageInterface::class)
	))
		->setContainer($container);
});
$container->addTags(\app\modules\checkout\events\EventListener::class, ['events.listener']);

// cart
$container->addAlias('cart', SessionCart::class);
$container->addAlias(CartInterface::class, SessionCart::class);

$container->set(ItemInterface::TYPE_ARTICLE, ArticleModel::class);
$container->set(ItemInterface::TYPE_ARTICLE_TARIFF, ArticleTariffModel::class);
$container->set(ItemInterface::TYPE_TARIFF, TariffModel::class);
$container->set(ItemInterface::TYPE_COUPON, CouponModel::class);
$container->set(ItemInterface::TYPE_ACCESSORIES, AccessoriesModel::class);

$container->setSingleton(CartTotalsCalculator::class, CartTotalsCalculator::class);

// checkout
$container->setSingleton(CheckoutStorageInterface::class, function(Container $container) {
	$sessionCheckoutStorage = new SessionCheckoutStorage(
		$container->get('session')
	);

	return $sessionCheckoutStorage->setContainer($container);
});

$container->setSingleton(CheckoutBuilderInterface::class, function(Container $container) {
	$checkoutBuilder = new CheckoutBuilder(
		$container->get(CartInterface::class),
		$container->get(CheckoutStorageInterface::class)
	);

	return $checkoutBuilder->setContainer($container);
});

// test data mock for billing payment
$container->setSingleton(PaymentBillDataExtractor::class, PaymentBillDataExtractor::class);
$container->setSingleton(CheckoutDataExtractor::class, CheckoutDataExtractor::class);
