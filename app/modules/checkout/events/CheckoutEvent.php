<?php

namespace app\modules\checkout\events;

use app\modules\checkout\services\checkout\CheckoutBuilderInterface;
use app\modules\checkout\services\checkout\CheckoutInterface;
use app\modules\checkout\services\checkout\step\StepInterface;
use deinhandy\yii2events\event\ClassLevelEvent;

class CheckoutEvent extends ClassLevelEvent
{
	public $sender = CheckoutInterface::class;
	public $data;

	/**
	 * @return StepInterface[]
	 */
	public function getSteps()
	{
		/** @var CheckoutBuilderInterface $checkoutBuilder */
		$checkoutBuilder = di(CheckoutBuilderInterface::class);

		return $checkoutBuilder->build();
	}
}
