<?php

namespace app\modules\checkout\events;

use app\models\CouponModel;
use app\models\OrderModel;
use app\models\RestApiAdapter;
use app\modules\checkout\services\cart\CartInterface;
use app\modules\checkout\services\cart\CheckoutDataExtractor;
use app\modules\checkout\services\cart\InvoiceDataExtractor;
use app\modules\checkout\services\cart\ItemInterface;
use app\modules\checkout\services\checkout\CheckoutInterface;
use app\modules\checkout\services\checkout\CheckoutStorageInterface;
use app\modules\checkout\services\checkout\step\PaymentStep;
use app\services\di\ContainerAwareTrait;
use deinhandy\yii2events\listener\AbstractEventListener;
use Yii;

class EventListener extends AbstractEventListener
{
	use ContainerAwareTrait;
	use RestApiAdapter;

	protected $cart;
	protected $checkoutStorage;

	public function __construct(CartInterface $cart, CheckoutStorageInterface $checkoutStorage)
	{
		$this->cart = $cart;
		$this->checkoutStorage = $checkoutStorage;
	}

	public function getGlobalEvents() : array
	{
		return [];
	}

	public function getClassEvents() : array
	{
		return [
			CheckoutInterface::class => [
				CheckoutInterface::EVENT_COMPLETED => [$this, 'completeCheckout'],
			],
			CartInterface::class => [
				CartInterface::EVENT_ADD => [$this, 'addItemToCart'],
				CartInterface::EVENT_REMOVE => [$this, 'removeItem'],
			],
		];
	}

	public function removeItem(CartEvent $event)
	{
		// if article is removed, look for an tariff and remove it as well
		if ($event->getItem()->getItemType() === ItemInterface::TYPE_ARTICLE) {
			$cart = $event->getCart();

			foreach ($cart->getItems() as $item) {
				if ($item->getItemType() === ItemInterface::TYPE_TARIFF) {
					$cart->removeItem($item);
				}
			}
		}
	}

	public function addItemToCart(CartEvent $event)
	{
		$eventItem = $event->getItem();

		$uniqueTypes = [ItemInterface::TYPE_ARTICLE_TARIFF, ItemInterface::TYPE_TARIFF];
		if ($eventItem->getItemType() === ItemInterface::TYPE_COUPON) {
			/** @var CouponModel $eventItem */
			foreach ($event->getCart()->getItems() as $item) {
				if ($item->getItemType() === ItemInterface::TYPE_COUPON) {
					/** @var ItemInterface $item */

					if (
						// if another coupon is already in cart, remove it
						$item->getData()['code'] !== $eventItem->getData()['code']
					) {
						$event->getCart()->removeItem($item);
						di('session')->addFlash('danger', 'Cart_Coupon_Replaced');

						break;
					}
				}
			}
		// if item is of type, whcih can not be put multiple times into the cart
		} elseif (in_array($eventItem->getItemType(), $uniqueTypes)) {
			foreach ($event->getCart()->getItems() as $item) {
				if (
					(
						$eventItem->getItemType() === $item->getItemType()
						|| $item->getItemType() === ItemInterface::TYPE_ARTICLE_TARIFF
					) && $eventItem->getIdentifier() !== $item->getIdentifier()
				) {
					// removes item from the cart and displays an error message
					$event->getCart()->removeItem($eventItem);
					di('session')->addFlash('danger', Yii::t('checkout', 'Cart_Only_One_Type_Of_Item_Allowed'));

					// reset the current checkout step
					/** @var CheckoutStorageInterface $checkoutStorage */
					$checkoutStorage = di(CheckoutStorageInterface::class);
					$checkoutStorage->resetCurrentStep();

					break;
				}
			}
		}
	}

	public function completeCheckout(CheckoutEvent $event)
	{
		$data = array_merge(
			di(InvoiceDataExtractor::class)->extract(),
			di(CheckoutDataExtractor::class)->extract()
		);

		// TODO use order model instead of direct api call (and add missing "order place" method)
		$event->data = $this->getApiClient()->post('checkout/confirm-order', $data);

		// if order placement failed throw user back to the first step
		if ($event->data['order_status'] == OrderModel::STATUS_PAYMENT_INCOMPLETE) { // TODO this is a bloody workaround to detect if the order has been rejected by swissbilling
			/** @var CheckoutStorageInterface $checkoutStorage */
			$checkoutStorage = di(CheckoutStorageInterface::class);
			$checkoutStorage->resetCurrentStep();

			di('session')->addFlash('danger', Yii::t('checkout', 'Error_Title'));
		}

		// set step to payment-step as fallback when payment fails
		$this->checkoutStorage->setCurrentStep($this->checkoutStorage->get(PaymentStep::class));

		// truncate checkout
		$this->cart->saveBackup();
		$this->cart->truncate();
	}
}
