<?php

namespace app\modules\checkout\events;

use app\modules\checkout\services\cart\CartInterface;
use app\modules\checkout\services\cart\ItemInterface;
use deinhandy\yii2events\event\ClassLevelEvent;

class CartEvent extends ClassLevelEvent
{
	public $sender = CartInterface::class;

	/**
	 * @var ItemInterface
	 */
	protected $item;

	/**
	 * @return ItemInterface
	 */
	public function getItem()
	{
		return $this->item;
	}

	/**
	 * @param ItemInterface $item
	 * @return $this
	 */
	public function setItem($item)
	{
		$this->item = $item;

		return $this;
	}

	/**
	 * @return CartInterface
	 */
	public function getCart()
	{
		return di('cart');
	}
}
