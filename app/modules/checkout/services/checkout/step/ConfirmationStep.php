<?php

namespace app\modules\checkout\services\checkout\step;

class ConfirmationStep extends StepAbstract
{
	const IDENTIFIER = 'confirmation';

	public function getName()
	{
		return 'Confirmation';
	}
}
