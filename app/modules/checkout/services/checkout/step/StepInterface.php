<?php

namespace app\modules\checkout\services\checkout\step;

interface StepInterface extends \Serializable
{
	public function getName();
	public function getIdentifier();
	public function setActive($active);
	public function isActive(): bool;
	public function setSubmitted(bool $active);
	public function isSubmitted(): bool;
	public function getData();
	public function isAccessible(): bool;
	public function setAccessible(bool $accessible);

	/**
	 * exports data which can be processed and stored as order
	 *
	 * @return mixed
	 */
	public function export();

	/**
	 * validates the underlying models and creating errors on models
	 *
	 * @return $this
	 */
	public function validate();

	/**
	 * validates the underlying models without creating errors
	 *
	 * @return bool
	 */
	public function isValid(): bool;

	/**
	 * process the given data for step
	 *
	 * @param array $data
	 *
	 * @return $this
	 */
	public function process(array $data);
}
