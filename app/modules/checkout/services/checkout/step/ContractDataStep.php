<?php

namespace app\modules\checkout\services\checkout\step;

use app\models\CustomerPhoneNumberModel;
use app\models\PassportModel;
use app\modules\checkout\services\cart\CartInterface;
use app\services\auth\AuthProvider;
use app\services\auth\AuthProviderInterface;
use Yii;
use yii\base\Model;

class ContractDataStep extends StepAbstract
{
	/**
	 * @var CustomerPhoneNumberModel
	 */
	protected $customerPhoneNumberModel;
	/**
	 * @var PassportModel
	 */
	protected $passportModel;
	/**
	 * @var AuthProviderInterface
	 */
	protected $authProvider;
	protected $cart;

	/**
	 * @var string
	 */
	const IDENTIFIER = 'contract_data';

	public function __construct(AuthProviderInterface $authProvider, CartInterface $cart)
	{
		$this->authProvider = $authProvider;
		$this->cart = $cart;

		if ($currentCustomer = $this->authProvider->user()) {
			$data = $currentCustomer
				->getCustomerData()
				->getData();

			$this->passportModel = new PassportModel(
				[
					'customer_id' => $data['id'],
					'passport_id' => $data['passport_id'],
					'passport_type' => $data['passport_type'],
					'nationality' => $data['nationality'],
					'birthday' => $data['birthday'],
				]
			);

			$this->customerPhoneNumberModel = new CustomerPhoneNumberModel([
				'customer' => $currentCustomer,
			]);

			$this->customerPhoneNumberModel->first();
		} else {
			$this->passportModel = new PassportModel();
			$this->customerPhoneNumberModel = new CustomerPhoneNumberModel();
		}
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return 'Contract data';
	}

	/**
	 * @return Model[]
	 */
	public function getData()
	{
		return [
			'submitted' => $this->submitted,
			'porting' => $this->customerPhoneNumberModel,
			'showPorting' => $this->showPorting(),
			'passport' => $this->passportModel,
			'items' => $this->cart->getItems(),
		];
	}

	protected function showPorting(): bool
	{
		return Yii::$app->session->get('selected_contract_mode') === 'porting';
	}

	/**
	 * @param array $data
	 *
	 * @return $this
	 */
	public function process(array $data)
	{
		$this->setSubmitted(true);

		$this->customerPhoneNumberModel = new CustomerPhoneNumberModel($data['CustomerPhoneNumberModel'] ?? []);

		if ($this->authProvider instanceof AuthProvider) {
			$this->customerPhoneNumberModel->customer_id = $this->authProvider->user()->id;
		}

		$data['PassportModel']['customer_id'] = $this->customerPhoneNumberModel->customer_id;
		$this->customerPhoneNumberModel->create();
		$this->passportModel = new PassportModel($data['PassportModel']);
		$this->passportModel->create();

		return $this;
	}

	/**
	 * @param string $serialized
	 */
	public function unserialize($serialized)
	{
		$data = json_decode($serialized, true);

		$this->setSubmitted((bool) isset($data['submitted']) ? $data['submitted'] : false);
		$this->customerPhoneNumberModel = new CustomerPhoneNumberModel($data['porting']);
		$this->passportModel = new PassportModel($data['passport']);
	}
}
