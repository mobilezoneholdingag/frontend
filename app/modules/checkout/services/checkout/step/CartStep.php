<?php

namespace app\modules\checkout\services\checkout\step;

use app\models\CouponModel;
use app\modules\checkout\services\cart\ArticleExtractor;
use app\modules\checkout\services\cart\CartInterface;
use app\modules\checkout\services\cart\CartTotalsCalculator;
use app\modules\checkout\services\cart\ItemInterface;
use app\services\SessionInterface;
use Yii;

class CartStep extends StepAbstract
{
	/**
	 * @var CartInterface
	 */
	protected $cart;
	protected $session;
	protected $totalsCalculator;

	/**
	 * @var string
	 */
	const IDENTIFIER = 'cart';

	public function __construct(CartInterface $cart, SessionInterface $session, CartTotalsCalculator $totalsCalculator)
	{
		$this->cart = $cart;
		$this->session = $session;
		$this->totalsCalculator = $totalsCalculator;
	}

	public function unserialize($serialized)
	{
		$data = json_decode($serialized, true);
		$this->setSubmitted((bool) isset($data['submitted']) ? $data['submitted'] : false);
	}

	public function process(array $data)
	{
		$this->setSubmitted(true);

		$this->addCoupon($data);

		$this->validateCoupons();

		return parent::process($data);
	}



	private function addCoupon(array &$data)
	{
		if (isset($data['code']) && $data['code']) {
			$this->setSubmitted(false);

			$couponModel = new CouponModel([
				'code' => $data['code']
			]);

			if (!$couponModel->fetch()->validate()) {
				return;
			}

			if (!$this->couponAlreadyInCart($couponModel)) {
				$this->cart->addItem($couponModel);
			}
		}
	}

	private function validateCoupons()
	{
		$noCouponRemoved = true;
		foreach ($this->cart->getItems() as $item) {
			/** @var CouponModel $item */
			if ($item->getItemType() === ItemInterface::TYPE_COUPON) {
				if (!$item->validate()) {
					$this->cart->removeItem($item);
					$noCouponRemoved = false;
				}
			}
		}

		return $noCouponRemoved;
	}

	protected function couponAlreadyInCart(CouponModel $model)
	{
		foreach ($this->cart->getItems() as $item) {
			/** @var CouponModel $item */
			if ($item->getItemType() === ItemInterface::TYPE_COUPON && $item->getCode() === $model->getCode()) {
				return true;
			}
		}

		return false;
	}

	/**
	 * @return bool
	 */
	public function isValid(): bool
	{
		return $this->isSubmitted() && $this->cart->count() > 0;
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return 'Choice';
	}

	/**
	 * @return array
	 */
	public function getData()
	{
		/** @var ArticleExtractor $articleExtractor */
		$articleExtractor = di(ArticleExtractor::class);

		return [
			'items' => $this->cart->getItems(),
			'submitted' => $this->isSubmitted(),
			'totals' => $this->totalsCalculator->calculate(),
			'article' => $articleExtractor->extract(),
		];
	}

	public function export()
	{
		$items = $this->cart->getItems();
		
		return array_map(function(ItemInterface $item) {
			$item->getData();
		}, $items);
	}
}
