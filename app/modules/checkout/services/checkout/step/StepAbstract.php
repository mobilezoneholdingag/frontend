<?php

namespace app\modules\checkout\services\checkout\step;

use yii\base\Model;

abstract class StepAbstract implements StepInterface
{
	/**
	 * @var bool
	 */
	protected $active = false;
	/**
	 * @var bool
	 */
	protected $submitted = false;

	/**
	 * @var bool
	 */
	protected $accessible = false;

	/**
	 * needs to be a unique step identifier
	 */
	const IDENTIFIER = 'default';

	/**
	 * @return boolean
	 */
	public function isAccessible(): bool
	{
		return $this->accessible;
	}

	/**
	 * @param boolean $accessible
	 *
	 * @return $this
	 */
	public function setAccessible(bool $accessible)
	{
		$this->accessible = $accessible;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getIdentifier()
	{
		return static::IDENTIFIER;
	}

	/**
	 * @return boolean
	 */
	public function isSubmitted(): bool
	{
		return $this->submitted;
	}

	/**
	 * @param boolean $submitted
	 *
	 * @return $this
	 */
	public function setSubmitted(bool $submitted)
	{
		$this->submitted = $submitted;

		return $this;
	}

	/**
	 * @param $active
	 *
	 * @return $this
	 */
	public function setActive($active)
	{
		$this->active = (bool) $active;

		return $this;
	}

	/**
	 * @return bool
	 */
	public function isActive(): bool
	{
		return $this->active;
	}

	/**
	 * @return string
	 */
	public function serialize()
	{
		$data = $this->getData();

		$data = array_map(function($data) {
			if ($data instanceof Model) {
				return $data->toArray();
			}

			return $data;
		}, $data);

		return json_encode($data);
	}

	/**
	 * @param string $serialized
	 */
	public function unserialize($serialized)
	{
	}

	/**
	 * @return array
	 */
	public function getData()
	{
		return [];
	}

	public function validate()
	{
		foreach ($this->getData() as $model) {
			if ($model instanceof Model) {
				$model->validate();
			}
		}

		return $this;
	}

	/**
	 * validates all returned models by the getData() method
	 *
	 * @return bool
	 */
	public function isValid(): bool
	{
		$isValid = $this->isSubmitted();

		foreach ($this->getData() as $model) {
			if ($model instanceof Model) {
				// clone models so the errors will not be attached to the model
				$model = clone $model;
				$isValid = $isValid && $model->validate();
				unset($model);
			}
		}

		return $isValid;
	}

	/**
	 * @param array $data
	 *
	 * @return $this
	 */
	public function process(array $data)
	{
		return $this;
	}

	public function export()
	{
		return [];
	}

	protected function cloneAndValidate(Model $model)
	{
		return (bool) (clone $model)->validate();
	}
}
