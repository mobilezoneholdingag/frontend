<?php

namespace app\modules\checkout\services\checkout\step;

use app\models\CustomerModel;
use app\modules\checkout\services\cart\CartInterface;
use app\services\auth\AuthProviderInterface;
use Yii;

class CustomerDataStep extends StepAbstract
{
	/**
	 * @var CustomerModel
	 */
	protected $customerModel;
	protected $cart;
	protected $authProvider;

	const IDENTIFIER = 'customer_data';

	public function __construct(AuthProviderInterface $authProvider, CartInterface $cart)
	{
		$this->cart = $cart;
		$this->authProvider = $authProvider;

		if (!($this->customerModel = $authProvider->user())) {
			$this->customerModel = new CustomerModel();
		}
	}

	public function getName()
	{
		return 'Customer data';
	}

	public function unserialize($serialized)
	{
		$data = json_decode($serialized, true);

		$this->setSubmitted((bool) isset($data['submitted']) ? $data['submitted'] : false);

		$this->customerModel = new CustomerModel($data['customer']);
		$this->customerModel->scenario = $data['scenario'];
		$this->customerModel->setDefaultAddress($data['defaultAddress']);
		$this->customerModel->setBillingAddress($data['billingAddress']);
		$this->customerModel->setDeliveryAddress($data['deliveryAddress']);
	}

	public function getData()
	{
		return [
			'submitted' => $this->isSubmitted(),
			'customer' => $this->customerModel,
			'scenario' => $this->customerModel->scenario,
			'defaultAddress' => $this->customerModel->getBillingAddress(),
			'billingAddress' => $this->customerModel->getBillingAddress(),
			'deliveryAddress' => $this->customerModel->getDeliveryAddress(),
			'items' => $this->cart->getItems(),
		];
	}

	public function validate()
	{
		if ($this->customerModel->getRegisterAttempt()) {
			$this->customerModel->validate();
			$this->customerModel->getBillingAddress()->validate();

			if ($this->customerModel->getUseDeliveryAddress()) {
				$this->customerModel->getDeliveryAddress()->validate();
			}
		} elseif ($this->customerModel->getLoginAttempt()) {
			$this->customerModel->validate();
		} else {
			$this->customerModel->getBillingAddress()->validate();
		}
	}

	public function isValid(): bool
	{
		$valid = $this->isSubmitted();
		$customer = $this->customerModel;

		if ($customer->id && !$customer->getRegisterAttempt()) {
			$valid &= $customer->getDefaultAddressConfirmation();
		}

		if ($customer->getRegisterAttempt()) {
			$valid &= $this->cloneAndValidate($customer->getBillingAddress());

			if ($customer->getUseDeliveryAddress()) {
				$valid &= $this->cloneAndValidate($customer->getDeliveryAddress());
			}
		} elseif ($customer->getLoginAttempt()) {
			$valid &= $this->cloneAndValidate($customer);
		} else {
			if ($customer->id) {
				$valid &= $this->cloneAndValidate($customer->getBillingAddress());
			} else {
				$valid &= $this->cloneAndValidate($customer->getBillingAddress());
			}
		}

		return $valid && count($customer->adHocErrors) === 0;
	}

	public function process(array $data)
	{
		$this->setSubmitted(true);
		/* @var $customer CustomerModel */
		$customer = $this->customerModel;

		if (isset($data['CustomerModel'])) {
			foreach ($data['CustomerModel'] as $name => $val) {
				$customer->{$name} = $val;
			}

			if (isset($data['CustomerModel']['loginAttempt']) && $data['CustomerModel']['loginAttempt']) {
				$customer->setRegisterAttempt(false);
				unset($data['BillingAddress'], $data['DeliveryAddress']);
			}
		}

		if (isset($data['BillingAddress'])) {
			$customer->setBillingAddress($data['BillingAddress']);
			$customer->setDefaultAddress($customer->getBillingAddress());
		}

		if (isset($data['DeliveryAddress'])) {
			$customer->setDeliveryAddress($data['DeliveryAddress']);
		}

		// clear ad-hoc errors
		$customer->adHocErrors = [];

		if ($customer->getLoginAttempt() && !$customer->id) {
			$customer->scenario = CustomerModel::SCENARIO_LOGIN;

			// try to login the user and set default data
			if ($this->isValid()) {
				if ($this->authProvider->check($customer)) {
					$this->authProvider->login($customer);
					$customer->fetch($customer->id);
					$customer->setBillingAddress($customer->getDefaultAddress());
				} else {
					$customer->adHocErrors[] = ['email', \Yii::t('customer-account', 'Login_Failed {link}', [
						'link' => '/customer-account/customer-account/forgot-password'
					])];
				}
			}
		}

		if ($customer->getRegisterAttempt() && !$customer->id) {
			$customer->scenario = CustomerModel::SCENARIO_REGISTER;

			$customer->gender = $customer->billingAddress->sex;
			$customer->first_name = $customer->billingAddress->firstname;
			$customer->last_name = $customer->billingAddress->lastname;
			$customer->birthday = $customer->billingAddress->birthday;

			// try to create the customer, if successful, login them afterwards
			if ($this->isValid() && $customer->create()->id) {
				$this->authProvider->login($customer);
			} else {
				$customer->adHocErrors[] = ['email', \Yii::t('customer-account', 'Registration_Failed')];

				if($customer->emailExists()) {
					Yii::$app->session->addFlash('danger', Yii::t('checkout', 'Can_Not_Register'));
				}
			}
		}

		return $this;
	}
}
