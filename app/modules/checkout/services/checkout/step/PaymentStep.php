<?php

namespace app\modules\checkout\services\checkout\step;

use app\models\PaymentModel;
use app\modules\checkout\services\cart\CartInterface;
use app\modules\checkout\services\checkout\CheckoutStorageInterface;
use app\services\auth\AuthProviderInterface;
use app\services\SessionInterface;
use Yii;
use yii\base\Model;

class PaymentStep extends StepAbstract
{
	protected $paymentModel;
	protected $customerModel;
	protected $cart;
	protected $session;

	public $processWithCommissionArticle = true;

	const IDENTIFIER = 'payment';

	public function __construct(SessionInterface $session, AuthProviderInterface $authProvider, CartInterface $cart)
	{
		$this->cart = $cart;
		$this->session = $session;
		$this->paymentModel = new PaymentModel();

		if ($customer = $authProvider->user()) {
			$customerData = $customer->getCustomerData()
				->getData();

			if (isset($customerData['birthday'])) {
				$this->paymentModel->birthday = $customerData['birthday'];
			} else {
				$checkoutStorage = di(CheckoutStorageInterface::class);
				/** @var ContractDataStep $contractDataStep */
				$contractDataStep = $checkoutStorage->get(ContractDataStep::class);
				if (isset($contractDataStep)) {
					$contractDataStepData = $contractDataStep->getData();
					$this->paymentModel->birthday = $contractDataStepData['passport']['birthday'] ?? '';
				}
			}
		}
	}

	public function getName()
	{
		return 'Payment';
	}

	/**
	 * @return Model[]
	 */
	public function getData()
	{
		return [
			'submitted' => $this->submitted,
			'payment' => $this->paymentModel,
			'items' => $this->cart->getItems(),
		];
	}

	/**
	 * @param array $data
	 *
	 * @return $this
	 */
	public function process(array $data)
	{
		$this->setSubmitted(true);

		$this->paymentModel = new PaymentModel($data['PaymentModel']);

		// TODO: handle proper
		unset($this->session['payment_payload']);

		if ($this->processWithCommissionArticle) {
			$this->cart->addCommisionArticle();
		}

		if($this->paymentModel->getType() == PaymentModel::PREPAYMENT){
			$this->cart->removeCommisionArticle();
		}

		return $this;
	}

	/**
	 * @param string $serialized
	 */
	public function unserialize($serialized)
	{
		$data = json_decode($serialized, true);

		$this->setSubmitted((bool) isset($data['submitted']) ? $data['submitted'] : false);
		$this->paymentModel = new PaymentModel($data['payment']);
	}
}
