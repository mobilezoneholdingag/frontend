<?php

namespace app\modules\checkout\services\checkout\step;

use app\models\CheckoutEmarsysModel;
use app\models\ConditionsModel;
use app\models\CustomerModel;
use app\modules\checkout\services\cart\ArticleExtractor;
use app\modules\checkout\services\cart\CartInterface;
use app\modules\checkout\services\cart\CartTotalsCalculator;
use app\modules\checkout\services\cart\TariffProviderExtractor;
use app\modules\checkout\services\checkout\CheckoutBuilderInterface;

class OverviewStep extends StepAbstract
{
	/**
	 * @var CartInterface
	 */
	protected $cart;
	/**
	 * @var CheckoutBuilderInterface
	 */
	protected $builder;
	/**
	 * @var ConditionsModel
	 */
	protected $conditionsModel;

	protected $totalsCalculator;

	//public $newsletter;

	/**
	 * @var string
	 */
	const IDENTIFIER = 'overview';

	public function __construct(CartInterface $cart, CheckoutBuilderInterface $builder, CartTotalsCalculator $totalsCalculator)
	{
		$this->cart = $cart;
		$this->builder = $builder;
		$this->totalsCalculator = $totalsCalculator;

		$this->newsletter = new CheckoutEmarsysModel();

		$this->createConditionsModel();
	}

	protected function createConditionsModel($data = null)
	{
		$data = $data ?? [];
		$data['provider'] = di(TariffProviderExtractor::class)->extract();

		$this->conditionsModel = new ConditionsModel($data);
	}

	public function getName()
	{
		return 'Overview';
	}

	public function isValid(): bool
	{
		return $this->isSubmitted() && $this->cloneAndValidate($this->conditionsModel);
	}

	public function validate()
	{
		$this->conditionsModel->validate();
	}

	public function process(array $data)
	{

        $this->setSubmitted(true);
        $this->newsletter = new CheckoutEmarsysModel($data['CheckoutEmarsysModel']);
		$this->conditionsModel = new ConditionsModel($data['ConditionsModel']);
	}

	public function unserialize($serialized)
	{
		$data = json_decode($serialized, true);

		$this->setSubmitted((bool) isset($data['submitted']) ? $data['submitted'] : false);

		$this->createConditionsModel($data['conditions']);
	}

	public function getData()
	{
		/** @var ArticleExtractor $articleExtractor */
		$articleExtractor = di(ArticleExtractor::class);

		$data = [
			'items' => $this->cart->getItems(),
			'conditions' => $this->conditionsModel,
			'submitted' => $this->isSubmitted(),
			'totals' => $this->totalsCalculator->calculate(),
			'insurance_article' => $articleExtractor->extract(),
			'cartHasTariff' => $this->cart->hasTariff(),
            'newsletter' => $this->newsletter,
		];

		foreach ($this->builder->build() as $step) {
			switch ($step->getIdentifier()) {
				case CustomerDataStep::IDENTIFIER:
					$stepData = $step->getData();
					/** @var CustomerModel $customerModel */
					$customerModel = $stepData['customer'];
					$data['customer'] = $customerModel;

					break;
				case ContractDataStep::IDENTIFIER:
					$stepData = $step->getData();
					$data['passport'] = $stepData['passport'];
					$data['porting'] = $stepData['porting'];

					break;
			}
		}

		return $data;
	}
}
