<?php

namespace app\modules\checkout\services\checkout;

use app\modules\checkout\services\checkout\step\StepInterface;

interface CheckoutStorageInterface
{
	/**
	 * @param StepInterface $step
	 *
	 * @return CheckoutStorageInterface
	 */
	public function setCurrentStep(StepInterface $step): CheckoutStorageInterface;

	/**
	 * @return string
	 */
	public function getCurrentStep();

	/**
	 * @return $this
	 */
	public function resetCurrentStep();

	/**
	 * @param StepInterface $step
	 *
	 * @return CheckoutStorageInterface
	 */
	public function store(StepInterface $step): CheckoutStorageInterface;

	/**
	 * @param StepInterface $step
	 *
	 * @return CheckoutStorageInterface
	 */
	public function remove(StepInterface $step): CheckoutStorageInterface;

	/**
	 * @param string $identifier
	 *
	 * @return StepInterface|null
	 */
	public function get($identifier);

	/**
	 * @return CheckoutStorageInterface
	 */
	public function truncate(): CheckoutStorageInterface;
}
