<?php

namespace app\modules\checkout\services\checkout;

interface CheckoutInterface
{
	const EVENT_COMPLETED = 'completed';
}
