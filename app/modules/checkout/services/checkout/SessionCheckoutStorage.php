<?php

namespace app\modules\checkout\services\checkout;

use app\modules\checkout\services\checkout\step\StepInterface;
use app\services\di\ContainerAwareTrait;
use app\services\SessionInterface;
use Yii;

class SessionCheckoutStorage implements CheckoutStorageInterface
{
	use ContainerAwareTrait;

	/**
	 * @var SessionInterface
	 */
	protected $session;
	/**
	 * @var string
	 */
	protected $namespace;

	/**
	 * @param SessionInterface $session
	 * @param string $namespace
	 */
	public function __construct(SessionInterface $session, $namespace = 'checkout')
	{
		$this->session = $session;
		$this->namespace = $namespace;

		if (!$this->isValid()) {
			$this->rebuild();
		}
	}

	/**
	 * @return bool
	 */
	protected function isValid()
	{
		return
			isset($this->session[$this->namespace]) &&
			isset($this->session[$this->namespace]['current_step']) &&
			isset($this->session[$this->namespace]['steps']);
	}

	/**
	 * rebuild the inner session structure
	 */
	protected function rebuild()
	{
		$this->session[$this->namespace] = [
			'current_step' => null,
			'steps' => [],
		];
	}

	/**
	 * @param StepInterface $step
	 *
	 * @return CheckoutStorageInterface
	 */
	public function setCurrentStep(StepInterface $step): CheckoutStorageInterface
	{
		$session = $this->session[$this->namespace];
		$session['current_step'] = $step->getIdentifier();
		$this->session[$this->namespace] = $session;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getCurrentStep()
	{
		return $this->session[$this->namespace]['current_step'];
	}

	/**
	 * @return $this
	 */
	public function resetCurrentStep()
	{
		$this->session[$this->namespace] = array_merge($this->session[$this->namespace], ['current_step' => null]);

		return $this;
	}

	/**
	 * @param string $className
	 *
	 * @return StepInterface|null
	 */
	public function get($className)
	{
		$steps = $this->session[$this->namespace]['steps'];

		if (isset($steps[$className])) {
			/** @var StepInterface $step */
			$step = $this->container->get($className);
			$step->unserialize($steps[$className]);

			return $step;
		}

		return null;
	}

	/**
	 * @param StepInterface $step
	 *
	 * @return CheckoutStorageInterface
	 */
	public function store(StepInterface $step): CheckoutStorageInterface
	{
		$session = $this->session[$this->namespace];
		$session['steps'][get_class($step)] = $step->serialize();
		$this->session[$this->namespace] = $session;

		return $this;
	}

	/**
	 * @param StepInterface $step
	 *
	 * @return CheckoutStorageInterface
	 */
	public function remove(StepInterface $step): CheckoutStorageInterface
	{
		if (isset($this->session[$this->namespace]['steps'][$step->getIdentifier()])) {
			unset($this->session[$this->namespace]['steps'][$step->getIdentifier()]);
		}

		return $this;
	}

	/**
	 * @return CheckoutStorageInterface
	 */
	public function truncate(): CheckoutStorageInterface
	{
		$this->rebuild();

		return $this;
	}

	public static function getStepItems($step)
	{
		$steps = Yii::$app->controller->checkoutBuilder->build();
		$stepInformation = $steps[$step];

		return $stepInformation->getData()['items'];
	}
}
