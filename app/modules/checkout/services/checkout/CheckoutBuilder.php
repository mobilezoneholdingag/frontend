<?php

namespace app\modules\checkout\services\checkout;

use app\modules\checkout\services\cart\CartInterface;
use app\modules\checkout\services\checkout\step\CartStep;
use app\modules\checkout\services\checkout\step\ContractDataStep;
use app\modules\checkout\services\checkout\step\CustomerDataStep;
use app\modules\checkout\services\checkout\step\OverviewStep;
use app\modules\checkout\services\checkout\step\PaymentStep;
use app\modules\checkout\services\checkout\step\StepInterface;
use app\services\di\ContainerAwareTrait;

class CheckoutBuilder implements CheckoutBuilderInterface
{
	use ContainerAwareTrait;

	/**
	 * @var CartInterface
	 */
	protected $cart;

	/**
	 * @var CheckoutStorageInterface
	 */
	protected $storage;

	/**
	 * cache built steps, so they will not be created / unserialized every time build() is called
	 *
	 * @var null|StepInterface[]
	 */
	protected $cachedSteps = null;

	/**
	 * @param CartInterface $cart
	 * @param CheckoutStorageInterface $storage
	 */
	public function __construct(CartInterface $cart, CheckoutStorageInterface $storage)
	{
		$this->cart = $cart;
		$this->storage = $storage;
	}

	/**
	 * @param string $currentStep
	 * @return StepInterface[]
	 */
	public function build($currentStep = null)
	{
		// internal instance cache
		if (!$this->cachedSteps) {
			// if not current step given, retrieve current step from session
			if (!$currentStep && $this->storage->getCurrentStep()) {
				$currentStep = $this->storage->getCurrentStep();
			}

			$stepClasses = [
				CartStep::class,
				CustomerDataStep::class,
				$this->cart->hasTariff() ? ContractDataStep::class : null,
				PaymentStep::class,
				OverviewStep::class,
			];

			$this->cachedSteps = [];

			foreach ($stepClasses as $stepClass) {
				if($stepClass === null) {
					continue;
				}

				$this->cachedSteps[] = $this->storage->get($stepClass) ?? $this->container->get($stepClass);
			}

			$this->markAccessible($this->cachedSteps);

			if ($currentStep === null) {
				$this->cachedSteps[0]->setActive(true);
			} else {
				foreach ($this->cachedSteps as $step) {
					if ($step->getIdentifier() === $currentStep) {
						$step->setActive(true);
						break;
					}
				}
			}
		}

		return $this->cachedSteps;
	}

	/**
	 * @param StepInterface[] $steps
	 *
	 * @return $this
	 */
	protected function markAccessible($steps)
	{
		// first set always accessible
		$steps[0]->setAccessible(true);

		// steps should be accessible only if the previous step is valid
		for ($i=0; $i<=count($steps); $i++) {
			if (!$steps[$i]->isValid() || !isset($steps[$i+1])) {
				break;
			}

			$steps[$i+1]->setAccessible(true);
		}

		return $this;
	}

	/**
	 * @return $this
	 */
	public function next()
	{
		$steps = $this->build();
		$currentStep = $this->getCurrentStep();

		for ($i=0; $i<=count($steps); $i++) {
			if ($currentStep->getIdentifier() === $steps[$i]->getIdentifier() && isset($steps[++$i])) {
				$this->storage->setCurrentStep($steps[$i]);

				break;
			}
		}

		return $this;
	}

	/**
	 * @return $this
	 */
	public function back()
	{
		$steps = $this->build();
		$currentStep = $this->getCurrentStep();

		for ($i=1; $i<=count($steps); $i++) {
			if ($currentStep->getIdentifier() === $steps[$i]->getIdentifier() && isset($steps[--$i])) {
				$this->storage->setCurrentStep($steps[$i]);

				break;
			}
		}

		return $this;
	}

	/**
	 * @inheritdoc
	 */
	public function getCurrentStep(): StepInterface
	{
		$steps = $this->build();

		foreach ($steps as $step) {
			if ($step->isActive()) {
				return $step;
			}
		}

		return null;
	}

	/**
	 * @return bool
	 */
	public function isFinished(): bool
	{
		foreach ($this->build() as $step) {
			if (!$step->isValid()) {
				return false;
			}
		}

		return true;
	}
}
