<?php

namespace app\modules\checkout\services\checkout;

use app\modules\checkout\services\checkout\step\StepInterface;

interface CheckoutBuilderInterface
{
	/**
	 * @param string $currentStep   Unique step identifier
	 * @return StepInterface[]
	 */
	public function build($currentStep = null);

	/**
	 * @return StepInterface
	 */
	public function getCurrentStep(): StepInterface;

	/**
	 * step forward to next step
	 *
	 * @return $this
	 */
	public function next();

	/**
	 * step back in the step list
	 *
	 * @return $this
	 */
	public function back();

	/**
	 * @return bool
	 */
	public function isFinished(): bool;
}
