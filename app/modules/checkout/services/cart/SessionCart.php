<?php

namespace app\modules\checkout\services\cart;

use app\models\AbstractApiModel;
use app\models\CouponModel;
use app\modules\checkout\services\checkout\CheckoutStorageInterface;
use app\services\SessionInterface;
use deinhandy\yii2events\dispatcher\EventDispatcherInterface;

class SessionCart extends AbstractCart
{
	protected $storage;
	protected $namespace;
	const COMMISSION_ARTICLE_ID = 12529;

	public function __construct(SessionInterface $sessionStorage, EventDispatcherInterface $eventDispatcher, $namespace = 'cart')
	{
		parent::__construct($eventDispatcher);

		$this->storage = $sessionStorage;
		$this->namespace = $namespace;

		// initialize if not done yet, truncate will create proper structure
		if (!$this->getCart()) {
			$this->truncate();
		}
	}

	private function getNewPosition($items)
	{
		if (count($items) === 0) {
			return 0;
		}

		$positions = array_map(function($item) {
			return $item['data']['position'];
		}, $items);

		return max($positions) + 1;
	}

	public function addItem(ItemInterface $item)
	{
		$cart = $this->getCart();
		$position = $this->getNewPosition($cart['items']);
		$item->position = $position;

		$cart['items'][] = [
			'identifier' => $item->getIdentifier(),
			'class_name' => get_class($item),
			'data' => $item->serialize()
		];

		$this->setCart($cart);

		return parent::addItem($item);
	}

	public function updateItem(ItemInterface $item)
	{
		$items = $this->getItems();

		foreach ($items as &$cartItem) {
			if($cartItem->getIdentifier() === $item->getIdentifier()) {
				$cartItem = $item;
			}
			$cartItem = [
				'identifier' => $cartItem->getIdentifier(),
				'class_name' => get_class($cartItem),
				'data' => $cartItem->serialize()
			];
		}

		$this->setCart([
			'items' => $items
		]);

		return $this;
	}

	public function removeItem(ItemInterface $item, bool $removeSameItems = false)
	{
		// remove the item by filtering all items and compare the identifier
		$items = array_filter($this->getCart()['items'], function ($itemData) use ($item, $removeSameItems) {

			// compare the "id" ('12345'), not the "identifier" ('article-12345-1')
			if ($removeSameItems) {
				return $item->id !== $itemData['data']['id'];
			}

			// compare the "identifier" ('article-12345-1'), not the "id" ('12345')
			return $itemData['identifier'] != $item->getIdentifier();
		});

		$this->setCart([
			'items' => $items
		]);

		return parent::removeItem($item);
	}

	public function truncate()
	{
		$this->setCart([
			'items' => []
		]);

		return parent::truncate();
	}

	public function getItems()
	{
		$items = array_map(function($itemData) {
			/** @var ItemInterface $item */
			$item = new $itemData['class_name'];
			$item->unserialize($itemData['data']);

			return $item;
		}, $this->getCart()['items']);

		// display coupons always as last item
		usort($items, function(ItemInterface $a) {
			if ($a->getItemType() === ItemInterface::TYPE_COUPON) {
				return 1;
			}

			return 0;
		});

		return $items;
	}

	private function getCart()
	{
		return isset($this->storage[$this->namespace]) ? $this->storage[$this->namespace] : null;
	}

	private function setCart($cart)
	{
		$this->storage[$this->namespace] = $cart;

		return $this;
	}

	public function saveBackup()
	{
		$this->storage[$this->namespace . '_backup'] = $this->getCart();
	}

	public function restoreBackup()
	{
		$this->storage[$this->namespace] = $this->storage[$this->namespace . '_backup'];
	}

	/**
	 * @inheritdoc
	 */
	public function count()
	{
		$items = $this->getCart()['items'];

		return count(array_filter($items, function($item) {
			return $item['class_name'] != CouponModel::class;
		}));
	}

	public function addCommisionArticle()
	{
		$item = di(ItemInterface::TYPE_ARTICLE);
		/** @var $item ItemInterface|AbstractApiModel */
		$item->fetch(self::COMMISSION_ARTICLE_ID);

		if ($item->validate()) {
			$this->addItem($item);
		}
	}

	public function removeCommisionArticle()
	{
		$item = di(ItemInterface::TYPE_ARTICLE);
		/** @var $item ItemInterface|AbstractApiModel */
		$item->fetch(self::COMMISSION_ARTICLE_ID);

		if ($this->cartContains($item)) {
			$this->removeItem($item, true);

			/** @var CheckoutStorageInterface $checkoutStorage */
			$checkoutStorage = di(CheckoutStorageInterface::class);
			$checkoutStorage->resetCurrentStep();
		}
	}

	public function cartContains(ItemInterface $item)
	{
		foreach ($this->getItems() as $cartItem) {
			if ($cartItem->id === $item->id) {
				return true;
			}
		}

		return false;
	}

	/** @inheritdoc */
	public function getItem($id)
	{
		foreach ($this->getItems() as $item) {
			if ($item->getIdentifier() === $id) {
				return $item;
			}
		}

		return null;
	}
}
