<?php

namespace app\modules\checkout\services\cart;

use app\models\ArticleModel;
use app\models\ArticleTariffModel;

class ArticleExtractor implements DataExtractorInterface
{
	protected $cart;

	public function __construct(CartInterface $cart)
	{
		$this->cart = $cart;
	}

	/**
	 * extracts data from the cart
	 *
	 * @return mixed
	 */
	public function extract()
	{
		foreach ($this->cart->getItems() as $item) {
			/** @var ArticleModel $item */
			if ($item->getItemType() === ItemInterface::TYPE_ARTICLE) {
				return $item;
			}

			if ($item->getItemType() === ItemInterface::TYPE_ARTICLE_TARIFF) {
				/** @var ArticleTariffModel $item */
				return $item->article;
			}
		}

		return null;
	}
}
