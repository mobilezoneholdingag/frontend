<?php

namespace app\modules\checkout\services\cart;

interface CartInterface extends \Countable
{
	const EVENT_ADD = 'add';
	const EVENT_REMOVE = 'remove';
	const EVENT_TRUNCATE = 'truncate';

	/**
	 * @param ItemInterface $item
	 * @return $this
	 */
	public function addItem(ItemInterface $item);

	/**
	 * @param ItemInterface $item
	 * @return $this
	 */
	public function updateItem(ItemInterface $item);

	/**
	 * @param ItemInterface $item
	 * @return $this
	 */
	public function removeItem(ItemInterface $item);

	/**
	 * @return $this
	 */
	public function truncate();

	/**
	 * @return ItemInterface[]
	 */
	public function getItems();

	/**
	 * @param $id
	 *
	 * @return ItemInterface
	 */
	public function getItem($id);

	/**
	 * @return bool
	 */
	public function hasTariff();

	public function saveBackup();

	public function restoreBackup();
}
