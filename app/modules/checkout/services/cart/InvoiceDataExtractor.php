<?php

namespace app\modules\checkout\services\cart;

use app\helper\AuthHelper;
use app\models\AddressModel;
use app\models\ArticleModel;
use app\models\ArticleTariffModel;
use app\models\PassportModel;
use app\modules\checkout\services\checkout\CheckoutStorageInterface;
use app\modules\checkout\services\checkout\step\ContractDataStep;
use app\modules\checkout\services\checkout\step\CustomerDataStep;
use DateTime;
use Yii;

class InvoiceDataExtractor implements DataExtractorInterface
{
	protected $cart;
	protected $cartTotalsCalculator;

	/** @var CheckoutStorageInterface */
	private $checkoutStorage;

	public function __construct(CartInterface $cart, CheckoutStorageInterface $checkoutStorage, CartTotalsCalculator $totalsCalculator)
	{
		$this->cart = $cart;
		$this->checkoutStorage = $checkoutStorage;
		$this->cartTotalsCalculator = $totalsCalculator;
	}

	/**
	 * extracts data from the cart
	 *
	 * @return mixed
	 */
	public function extract()
	{
		$data = [];
		$this->processCartItems($data);
		$this->processPassportData($data);
		$this->processCustomerData($data);
		$data['customer_ip'] = Yii::$app->request->userIP;
		$data['customer_id'] = AuthHelper::user()->id;

		return $data;
	}
	
	private function processCartItems(array &$data)
	{
		$total = $this->cartTotalsCalculator->calculate();
		$data['total'] = $total['total']['once'];
		$data['commission'] = $total['commission'] ?? 0.0;
		$items = [];

		foreach (ItemInterface::TYPES as $type) {
			$items[$type] = [];
		}

		foreach ($this->cart->getItems() as $item) {
			if (in_array(
					$item->getItemType(),
					[
						ItemInterface::TYPE_ARTICLE,
						ItemInterface::TYPE_ARTICLE_TARIFF,
					]
				)
				&& ($item instanceof ArticleModel || $item instanceof ArticleTariffModel)
				&& isset($item->insurance_id)
			) {
				$items[ItemInterface::TYPE_ARTICLE][] = $item->insurance_id;
			}
			$itemData = $item->getData();
			$items[$item->getItemType()][] = $itemData['id'];
		}

		$data['items'] = array_filter($items);
	}

	private function processPassportData(array &$data)
	{
		/** @var ContractDataStep $contractStep */
		$contractStep = $this->checkoutStorage->get(ContractDataStep::class);
		/** @var CustomerDataStep $customerStep */
		$customerStep = $this->checkoutStorage->get(CustomerDataStep::class);

		if ($contractStep) {
			$contractStepData = $contractStep->getData();
			/** @var PassportModel $passport */
			$passport = $contractStepData['passport'];
			$data['birthday'] = (new DateTime($passport->birthday))->format('Y-m-d');
			$data['customerPhoneNumber'] = $contractStepData['porting']->toArray();
		} else {
			$data['birthday'] = (new DateTime($customerStep->getData()['defaultAddress']->birthday))->format('Y-m-d');
		}
	}

	private function processCustomerData(array &$data)
	{
		/** @var CustomerDataStep $step */
		$step = $this->checkoutStorage->get(CustomerDataStep::class);
		/** @var PassportModel $passport */
		$stepData = $step->getData();
		/** @var AddressModel $deliveryAddress */
		$deliveryAddress = $stepData['deliveryAddress'];
		/** @var AddressModel $billingAddress */
		$billingAddress = $stepData['billingAddress'];

		$data['delivery_address'] = $deliveryAddress->toArray();
		$data['billing_address'] = $billingAddress->toArray();

		if (empty($data['delivery_address']['firstname'])) {
			$data['delivery_address'] = [];
		}
	}
}
