<?php

namespace app\modules\checkout\services\cart;

use app\models\ArticleModel;
use app\models\ArticleTariffModel;
use app\models\CouponModel;
use app\models\PaymentModel;
use app\modules\checkout\services\checkout\CheckoutStorageInterface;
use app\modules\checkout\services\checkout\step\PaymentStep;
use app\services\api\ApiClientInterface;

class CartTotalsCalculator
{
	protected $cart;
	protected $apiClient;
	protected $checkoutStorage;

	public function __construct(
		CartInterface $cart,
		ApiClientInterface $apiClient,
		CheckoutStorageInterface $checkoutStorage
	) {
		$this->cart = $cart;
		$this->apiClient = $apiClient;
		$this->checkoutStorage = $checkoutStorage;
	}

	public function calculate()
	{
		$data = [
			'products' => [],
			'coupon' => [],
			'payment_method' => '',
		];

		foreach ($this->cart->getItems() as $item) {
			if ($item->getItemType() === ItemInterface::TYPE_COUPON) {
				/** @var CouponModel $item */
				$data['coupon']['code'] = $item->getCode();
			} else {
				$itemData = $item->getData();
				$data['products'][] = [
					'id' => $itemData['id'],
					'type' => $this->totalsTypeMapping($item->getItemType()),
				];
			}

			// if insurance id is set, then put the insurance as article into the request
			$insurancePossible = in_array(
				$item->getItemType(),
				[ItemInterface::TYPE_ARTICLE, ItemInterface::TYPE_ARTICLE_TARIFF]
			);

			/** @var ArticleModel|ArticleTariffModel $item */
			if ($insurancePossible && $item->has_insurance) {
				$data['products'][] = [
					'id' => $item->current_insurance->id,
					'type' => ItemInterface::TYPE_ARTICLE,
				];
			}
		}

		if ($this->checkoutStorage->get(PaymentStep::class)) {
			$paymentMethod = $this->checkoutStorage->get(PaymentStep::class)->getData()['payment']->getType();
			$data['payment_method'] = $paymentMethod;
		}

		return $this->apiClient->get('cart/totals', null, $data);
	}

	public function getTotalOnce(): float
	{
		$totalPrices = $this->calculate();

		return isset($totalPrices['total']['once']) ? $totalPrices['total']['once'] : 0;
	}

	protected function totalsTypeMapping($type)
	{
		if ($type === ItemInterface::TYPE_ACCESSORIES) {
			return ItemInterface::TYPE_ARTICLE;
		}

		return $type;
	}
}
