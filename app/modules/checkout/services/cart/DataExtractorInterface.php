<?php

namespace app\modules\checkout\services\cart;

interface DataExtractorInterface
{
	/**
	 * extracts data from the cart
	 *
	 * @return mixed
	 */
	public function extract();
}
