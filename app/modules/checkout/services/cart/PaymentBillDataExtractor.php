<?php

namespace app\modules\checkout\services\cart;

use app\helper\ArrayHelper;
use app\models\CustomerModel;
use app\models\GenderInterface;
use app\models\PassportModel;
use app\modules\checkout\services\checkout\CheckoutStorageInterface;
use app\modules\checkout\services\checkout\step\CartStep;
use app\modules\checkout\services\checkout\step\ContractDataStep;
use app\modules\checkout\services\checkout\step\CustomerDataStep;
use Yii;

class PaymentBillDataExtractor implements DataExtractorInterface
{
	protected $checkoutStorage;

	public function __construct(CheckoutStorageInterface $checkoutStorage)
	{
		$this->checkoutStorage = $checkoutStorage;
	}

	/**
	 * extracts data from the cart
	 *
	 * @return mixed
	 */
	public function extract()
	{
		$data = [];

		$this->processCustomerAddress($data);
		$this->processPassportData($data);
		$this->processCart($data);

		// replace test data
		$data['email'] = '0711234567';
		$data['phone_number'] = '0711234567';
		$data['mobile_number'] = '0797654321';

		return [
			'address' => $data,
		];
	}

	protected function processCart(&$data)
	{
		/** @var CartStep $step */
		$step = $this->checkoutStorage->get(CartStep::class);
		$data['amount'] = $step->getData()['totals']['once'];
	}

	protected function processPassportData(&$data)
	{
		/** @var ContractDataStep $step */
		$step = $this->checkoutStorage->get(ContractDataStep::class);
		if (null === $step) { // This step is optional
			return;
		}

		/** @var PassportModel $passport */
		$passport = $step->getData()['passport'];

		$data['language'] = explode('-', Yii::$app->language)[0];
		$data['country'] = 'CH';
		$data['birthdate'] = (new \DateTime($passport->birthday))->format('Y-m-d');
	}

	protected function processCustomerAddress(&$address)
	{
		/** @var CustomerDataStep $step */
		$step = $this->checkoutStorage->get(CustomerDataStep::class);
		/** @var CustomerModel $customer */
		$customer = $step->getData()['customer'];
		$address = $customer->getBillingAddress()->toArray();
		$address = ArrayHelper::mapKeys($address, [
			'sex' => 'gender',
			'firstname' => 'first_name',
			'lastname' => 'last_name',
			'contactPhoneNumber' => 'phone_number',
		]);
		$address['street'] .= ' ' . $address['houseNumber'];
		$address['gender'] = $address['gender'] == GenderInterface::MALE ? 'male' : 'female';

		$address['email'] = $customer->email;

		unset($address['houseNumber'], $address['formName']);
	}
}
