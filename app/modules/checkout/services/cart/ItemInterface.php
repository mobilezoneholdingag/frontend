<?php

namespace app\modules\checkout\services\cart;

interface ItemInterface extends \Serializable
{
	const TYPE_ARTICLE = 'article';
	const TYPE_TARIFF = 'tariff';
	const TYPE_ARTICLE_TARIFF = 'article_tariff';
	const TYPE_COUPON = 'coupon';
	const TYPE_ACCESSORIES = 'accessories';

	const TYPES = [
		self::TYPE_ARTICLE,
		self::TYPE_TARIFF,
		self::TYPE_ARTICLE_TARIFF,
		self::TYPE_COUPON,
		self::TYPE_ACCESSORIES,
	];

	public function getIdentifier();
	public function getData();
	public function getItemType();
}
