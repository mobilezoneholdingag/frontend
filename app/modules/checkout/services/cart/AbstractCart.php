<?php

namespace app\modules\checkout\services\cart;

use app\modules\checkout\events\CartEvent;
use deinhandy\yii2events\dispatcher\EventDispatcherInterface;

abstract class AbstractCart implements CartInterface
{
	protected $eventDispatcher;

	public function __construct(EventDispatcherInterface $eventDispatcher)
	{
		$this->eventDispatcher = $eventDispatcher;
	}

	public function addItem(ItemInterface $item)
	{
		$this->eventDispatcher->dispatch($this->createEvent(CartInterface::EVENT_ADD, $item));

		return $this;
	}

	public function removeItem(ItemInterface $item)
	{
		$this->eventDispatcher->dispatch($this->createEvent(CartInterface::EVENT_REMOVE, $item));

		return $this;
	}

	public function truncate()
	{
		$this->eventDispatcher->dispatch(new CartEvent([
			'name' => CartInterface::EVENT_TRUNCATE
		]));

		return $this;
	}

	protected function createEvent($name, ItemInterface $item)
	{
		$event = new CartEvent(compact('name'));
		$event->setItem($item);

		return $event;
	}

	public function hasTariff()
	{
		foreach ($this->getItems() as $item) {
			if(in_array($item->getItemType(), [ItemInterface::TYPE_TARIFF, ItemInterface::TYPE_ARTICLE_TARIFF])) {
				return true;
			}
		}

		return false;
	}
}
