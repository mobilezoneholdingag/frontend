<?php

namespace app\modules\checkout\services\cart;

use app\helper\ArrayHelper;
use app\models\CustomerModel;
use app\models\PassportModel;
use app\models\TariffModel;
use app\modules\checkout\services\checkout\CheckoutStorageInterface;
use app\modules\checkout\services\checkout\step\CartStep;
use app\modules\checkout\services\checkout\step\ContractDataStep;
use app\modules\checkout\services\checkout\step\CustomerDataStep;
use Yii;

class TariffProviderExtractor implements DataExtractorInterface
{
	protected $cart;

	public function __construct(CartInterface $cart)
	{
		$this->cart = $cart;
	}

	/**
	 * extracts data from the cart
	 *
	 * @return mixed
	 */
	public function extract()
	{
		foreach ($this->cart->getItems() as $item) {
			if ($item instanceof TariffModel) {
				return $item->getData()['provider'];
			}
		}

		return null;
	}
}
