<?php

namespace app\modules\checkout\services\cart;

class PaymentBillMockDataExtractor implements DataExtractorInterface
{

	/**
	 * extracts data from the cart
	 *
	 * @return mixed
	 */
	public function extract()
	{
		return [
			'gender' =>  'male',
		    'first_name' =>  'Maria Käthi',
		    'last_name' =>  'Test',
		    'street' =>  'Dorstrasse 77',
		    'city' =>  'Küsnacht',
		    'zip' =>  '8700',
		    'country' =>  'CH',
		    'language' =>  'de',
		    'phone_number' =>  '0711234567',
		    'mobile_number' =>  '0797654321',
		    'email' =>  'test@mfgroup.ch',
		    'birthdate' =>  '1960-07-07',
		    'amount' =>  10,
		];
	}
}