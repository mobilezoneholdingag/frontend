<?php

namespace app\modules\checkout\services\cart;

use app\models\AbstractItemApiModel;
use app\models\AccessoriesModel;
use app\models\AddressModel;
use app\models\ArticleModel;
use app\models\ArticleTariffModel;
use app\models\CouponModel;
use app\models\CustomerModel;
use app\models\ArticleInsuranceModel;
use app\models\TariffModel;
use app\modules\checkout\services\checkout\CheckoutStorageInterface;
use app\modules\checkout\services\checkout\step\CartStep;
use app\modules\checkout\services\checkout\step\ContractDataStep;
use app\modules\checkout\services\checkout\step\CustomerDataStep;
use app\modules\checkout\services\checkout\step\PaymentStep;
use app\services\auth\AuthProviderInterface;
use Yii;

/**
 * Class CheckoutDataExtractor
 *
 * @package app\modules\checkout\services\cart
 */
class CheckoutDataExtractor implements DataExtractorInterface
{
	protected $checkoutStorage;

	/**
	 * CheckoutDataExtractor constructor.
	 *
	 * @param CheckoutStorageInterface $checkoutStorage
	 */
	public function __construct(CheckoutStorageInterface $checkoutStorage)
	{
		$this->checkoutStorage = $checkoutStorage;
	}

	/**
	 * extracts data from the cart
	 *
	 * @return mixed
	 */
	public function extract()
	{
		$data = [];

		$this->processCustomer($data);
		$this->processCart($data);
		$this->processContractData($data);
		$this->processPayment($data);
		$this->processSwisscom($data);
		$this->processRenewalData($data);

		return $data;
	}

	/**
	 * @param $data
	 */
	protected function processCustomer(&$data)
	{
		/** @var CustomerDataStep $step */
		$step = $this->checkoutStorage->get(CustomerDataStep::class);

		/** @var CustomerModel $customer */
		$customerStepData = $step->getData();

		/* @var AuthProviderInterface $authProvider */
		$authProvider = di('auth.provider');

		// @TODO: USER 1 as default is stupid!!! just needed it - for reasons
		$data['customer']['customer_id'] = $authProvider->user()->id ?? 1;

		/* @var $defaultAddress AddressModel */
		$defaultAddress = $customerStepData['defaultAddress'] ?? null;

		if($defaultAddress) {
			$data['customer']['defaultAddress']['sex'] = $defaultAddress->sex;
			$data['customer']['defaultAddress']['firstname'] = $defaultAddress->firstname;
			$data['customer']['defaultAddress']['lastname'] = $defaultAddress->lastname;
			$data['customer']['defaultAddress']['company'] = $defaultAddress->company ?? null;
			$data['customer']['defaultAddress']['street'] = $defaultAddress->street;
			$data['customer']['defaultAddress']['house_number'] = $defaultAddress->houseNumber;
			$data['customer']['defaultAddress']['zip'] = $defaultAddress->zip;
			$data['customer']['defaultAddress']['city'] = $defaultAddress->city;
			$data['customer']['defaultAddress']['country'] = $defaultAddress->country ?? null;
			$data['customer']['defaultAddress']['contact_phone_number'] = $defaultAddress->getContactPhoneNumber();
		}

		$data['customer']['birthday'] = $defaultAddress->birthday;
		
		/* @var $billingAddress AddressModel */
		$billingAddress = $customerStepData['defaultAddress'];
		if ($customerStepData['billingAddress']) {
			$billingAddress = $customerStepData['billingAddress'];
		}
		$data['customer']['billingAddress']['sex'] = $billingAddress->sex;
		$data['customer']['billingAddress']['firstname'] = $billingAddress->firstname;
		$data['customer']['billingAddress']['lastname'] = $billingAddress->lastname;
		$data['customer']['billingAddress']['company'] = $billingAddress->company ?? null;
		$data['customer']['billingAddress']['street'] = $billingAddress->street;
		$data['customer']['billingAddress']['house_number'] = $billingAddress->houseNumber;
		$data['customer']['billingAddress']['zip'] = $billingAddress->zip;
		$data['customer']['billingAddress']['city'] = $billingAddress->city;
		$data['customer']['billingAddress']['country'] = $billingAddress->country ?? null;
		$data['customer']['billingAddress']['contact_phone_number'] = $billingAddress->getContactPhoneNumber();

		$data['customer']['useDeliveryAddress'] = $customerStepData['customer']['useDeliveryAddress'] ?? false;

		if($data['customer']['useDeliveryAddress']) {
			/* @var $deliveryAddress AddressModel */
			$deliveryAddress = $customerStepData['deliveryAddress'];
			$data['customer']['deliveryAddress']['sex'] = $deliveryAddress->sex;
			$data['customer']['deliveryAddress']['firstname'] = $deliveryAddress->firstname;
			$data['customer']['deliveryAddress']['lastname'] = $deliveryAddress->lastname;
			$data['customer']['deliveryAddress']['company'] = $deliveryAddress->company ?? null;
			$data['customer']['deliveryAddress']['street'] = $deliveryAddress->street;
			$data['customer']['deliveryAddress']['house_number'] = $deliveryAddress->houseNumber;
			$data['customer']['deliveryAddress']['zip'] = $deliveryAddress->zip;
			$data['customer']['deliveryAddress']['city'] = $deliveryAddress->city;
			$data['customer']['deliveryAddress']['country'] = $deliveryAddress->country ?? null;
			$data['customer']['deliveryAddress']['contact_phone_number'] = $deliveryAddress->getContactPhoneNumber();
		}
	}

	/**
	 * @param $data
	 */
	protected function processCart(&$data)
	{
		/** @var CartStep $step */
		$step = $this->checkoutStorage->get(CartStep::class);
		$cartData = $step->getData();

		$customer_id = $data['customer']['customer_id'];
		$data['cart']['customer_id'] = $customer_id;

		foreach ($cartData['items'] as $item) {
			$singleItem = [];

			switch (get_class($item)) {
				case AccessoriesModel::class:
				case ArticleModel::class:
					/* @var $item ArticleModel */
					$singleItem['item_id'] = $item->data['id'];
					$singleItem['item_product_type_id'] = $this->getCorrespondingProductType($item);
					$singleItem['title'] = $item->data['product_name'];
					$singleItem['vat_rate'] = (float) $item->data['prices']['vat_rate'];
					$singleItem['mat_no'] = $item->data['mat_no'];

					if ($item->has_insurance) {
						$singleItem['insurance'] = $this->getInsurance($item);
					}

					$data['cart']['items'][] = $singleItem;
					break;

				case TariffModel::class:
					/* @var $item TariffModel */
					$singleItem['item_id'] = $item->data['id'];
					$singleItem['item_product_type_id'] = $this->getCorrespondingProductType($item);
					$singleItem['title'] = $item->data['title'];
					$singleItem['erp_title'] = $item->data['erp_title'];
					$data['cart']['items'][] = $singleItem;
					break;

				case ArticleTariffModel::class:
					/* @var $item ArticleTariffModel */
					$singleItem['item_id'] = $item->data['id'];
					$singleItem['item_product_type_id'] = $this->getCorrespondingProductType($item);
					$singleItem['title'] = '(AT) ' . $item->article['product_name'] . ' & ' . $item->tariff['title'];
					$singleItem['erp_title'] = $item->tariff['erp_title'];
					$singleItem['mat_no'] = $item->article['mat_no'];

					if ($item->has_insurance) {
						$singleItem['insurance'] = $this->getInsurance($item, ItemInterface::TYPE_ARTICLE);
					}

					if ($item->sim_card) {
						$singleItem['sim_card'] = $item->sim_card->id;
					}

					$data['cart']['items'][] = $singleItem;
					break;

				case CouponModel::class:
					/* @var $item CouponModel */
					$singleItem['item_id'] = $item->data['id'];
					$singleItem['item_product_type_id'] = $this->getCorrespondingProductType($item);
					$singleItem['title'] = '(CODE) ' . $item->data['code'];
					$data['cart']['items'][] = $singleItem;
					break;

			}
		}
	}

	/**
	 * @param ArticleModel|ArticleTariffModel|AccessoriesModel $item
	 * @param string|null $type
	 *
	 * @return array
	 */
	private function getInsurance($item, string $type = null): array
	{
		return [
			'item_id' => $item->current_insurance->id,
			'title' => $item->current_insurance->product_name,
			'item_product_type_id' => $type ?? $item::ITEM_TYPE,
		];
	}

	/**
	 * @param ItemInterface|AbstractItemApiModel $item
	 *
	 * @return mixed
	 */
	protected function getCorrespondingProductType($item)
	{
		return $item::ITEM_TYPE === ArticleInsuranceModel::ITEM_TYPE ? ArticleModel::ITEM_TYPE : $item::ITEM_TYPE;
	}

	/**
	 * @param $data
	 */
	private function processPayment(&$data)
	{
		/** @var PaymentStep $step */
		$step = $this->checkoutStorage->get(PaymentStep::class);

		$data['payment']['type'] = $step->getData()['payment']['type'];
		$data['payment']['payload'] = $step->getData()['payment']['payload'][$data['payment']['type']][$data['payment']['type'] . '-id'] ?? null;
	}

	private function processSwisscom(&$data)
	{
		$data['swisscom']['token'] = Yii::$app->session->get('swisscom_access_token');
		$data['swisscom']['subscription_key'] = Yii::$app->session->get('swisscom_subscription_key');
		$data['swisscom']['proposed_subscription_start_date'] = Yii::$app->session->get('swisscom_proposed_subscription_start_date');
	}

	private function processContractData(&$data)
	{
		/** @var ContractDataStep $step */
		$step = $this->checkoutStorage->get(ContractDataStep::class);

		if(!$step) {
			return;
		}

		$data['customer']['passport_id'] = $step->getData()['passport']['passport_id'] ?? null;
		$data['customer']['passport_type'] = $step->getData()['passport']['passport_type'] ?? null;
		$data['customer']['nationality'] = $step->getData()['passport']['nationality'] ?? null;
		$data['customer']['birthday'] = $step->getData()['passport']['birthday'] ?? null;
	}

	private function processRenewalData(&$data)
	{
		$data['article_hash'] = Yii::$app->session->has('article_hash')
			? Yii::$app->session->get('article_hash')
			: null;
		$data['renewal_request_id'] = Yii::$app->session->has('renewal_request_id')
			? Yii::$app->session->get('renewal_request_id')
			: null;
	}
}
