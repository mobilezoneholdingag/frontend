{% set interactive = interactive|default(false) %}
{% set prices = prices|default(item.prices) %}
{% set commisionArticleId = constant('app\\modules\\checkout\\services\\cart\\SessionCart::COMMISSION_ARTICLE_ID') %}
{% set sim_only_id = constant('app\\models\\ArticleModel::SIM_ONLY') %}

{% if item.id != sim_only_id %}
    <tr class="article-row">
        <td>
            <div class="float-right product-img-wrapper text-center">
                {# TODO this is not a cool way to exclude the commision article's image #}
                {% if item.id != commisionArticleId %}
                    <img class="img-fluid img-mh mb-3"
                         src="{{ item.images[0]|image('small')|default(asset('/img/no_image_big.png')) }}"
                         alt="{{ item.product_name }}">
                {% endif %}
            </div>

            <div class="hidden-xs-down">
                {# TODO this is not a cool way to exclude the commision article's image #}
                {% if item.id != commisionArticleId %}
                    <img class="product-brand-image"
                         alt="{{ item.manufacturer.title }}"
                         src="{{ item.manufacturer.image|image('product-brand-image')|default(asset('/img/no_image_big.png')) }}">
                {% endif %}
            </div>

            <h2 class="item-title">{{ item.product_name }}</h2>

            {% set delivery_label_class %}
                {% spaceless %}
                    {% if item.delivery_status_id == 3 %}
                        text-danger
                    {% else %}
                        text-success
                    {% endif %}
                {% endspaceless %}
            {% endset %}

            <p class="{{ delivery_label_class }}">
                {% include '@views/stock/stock.twig' with {stock: item.stock} %}
            </p>

            {% if interactive %}
                <a href="{{ Url.to('/checkout/cart/remove/' ~ item_identifier|default(item.identifier)) }}">
                    <span class="icon icon-sm icon-cancel"> {{ 'Remove article'|trans('checkout') }}</span>
                </a>
            {% endif %}
        </td>
        <td class="text-center hidden-md-down price-monthly">
            {{ utils.price(prices.monthly, 'lg') }}
        </td>
        <td class="text-center hidden-md-down price-once">
            {% if item.id == commisionArticleId %}
                {{ utils.price(commission, 'lg') }}
            {% else %}
                {{ utils.price(prices.once, 'lg') }}
            {% endif %}
        </td>
    </tr>
{% endif %}

{% set insurance_item = insurance_item ?? item %}
{% if insurance_item.insurances|length > 0 %}
    {% include '@checkout/steps/items/extended/insurance.twig' with {
        interactive: interactive,
        article: insurance_item ?? item,
        insurance_count: insurance_count,
    } %}
{% endif %}
