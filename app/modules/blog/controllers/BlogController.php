<?php

namespace app\modules\blog\controllers;

use app\modules\BaseModuleController;
use app\services\Seo;
use yii\helpers\ArrayHelper;


class BlogController extends BaseModuleController
{

    private $api;

    public function beforeAction($action)
    {

        $this->api = \Yii::$app->luyaApi;

        return parent::beforeAction($action);

    }

    public function actionIndex($slug = null)
    {
        // Get Page Tree
        $tree = $this->api->getTree();

        if(!array_key_exists('blog', $tree)) {
            return $this->redirect('/');
        }

        if(empty($slug)) {
            // return $this->redirect('/');
            Seo::setTitle('Blog | mobilezone');
            $tree = $tree['blog'];

            foreach($tree as $k => $t) {

                if($t['is_offline']) {
                    unset($tree[$k]);
                    continue;
                }

                $tree[$k]['navItem'] = $this->api->getPage($t['item']['nav_item_id'], null, false);

            }

            ArrayHelper::multisort($tree, ['sortIndex'], [SORT_DESC]);

            return $this->renderFile('@blog/tree.twig', ['tree' => $tree]);
        }

        // Find Rewrite for given Language
        foreach($tree['blog'] as $trow => $t) {
            if($t['item']['alias'] == $slug) {

                if($t['is_offline']) {
                    return $this->redirect('/');
                }

                Seo::setTitle($t['item']['title'] . ' | mobilezone');
                $page = $this->api->getPage($t['item']['nav_item_id']);
                Seo::setMetaDescription($page['navItem']['description']);
                Seo::setKeywords($page['navItem']['keywords']);
            }

        }

        if(empty($page)) {
            return $this->redirect('/');
        }

        return $this->renderFile('@blog/index.twig', ['page' => $page]);

    }
}
