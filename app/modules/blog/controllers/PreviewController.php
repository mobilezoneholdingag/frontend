<?php

namespace app\modules\blog\controllers;

use app\modules\BaseModuleController;
use app\services\Seo;


class PreviewController extends BaseModuleController
{

    private $api;

    public function beforeAction($action)
    {
        $this->api = \Yii::$app->luyaApi;
        return parent::beforeAction($action);
    }

    public function actionIndex($itemId, $version) {
        $page = $this->api->getPage($itemId, $version);
        return $this->renderFile('@blog/index.twig', ['page' => $page]);
    }

}