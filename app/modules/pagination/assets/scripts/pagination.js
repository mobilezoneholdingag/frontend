class Pagination {
  constructor(FilterLord) {
    this.filterLord = FilterLord;

    this.$buttonContainer = $('.load-more-button');
    this.$loadMoreButton = this.$buttonContainer.find('.btn');
    this.$loadMoreText = this.$loadMoreButton.find('.load-more-text');
    this.$currentItemCount = this.$loadMoreButton.find('.current');
    this.page = 1;
    this.perPage = parseInt(this.$buttonContainer.data('per-page'));
    this.total = parseInt(this.$buttonContainer.data('total'));

    this.addEventListener();
  }

  addEventListener() {
    this.$loadMoreButton.on('click', (event) => {
      if (!this.$loadMoreButton.hasClass('disabled')) {
        this.onClickButton(event)
      }
    });
    $('body').on('filter:selected.' + this.filterLord.eventNamespace, this.onFilterSelected.bind(this));
  }

  onFilterSelected() {
    this.$buttonContainer.toggleClass('d-block', this.filterLord.noFilterSelected);
    this.$buttonContainer.toggleClass('d-none', !this.filterLord.noFilterSelected);
    this.page = 1;
  }

  onClickButton(event) {
    event.preventDefault();
    this.$loadMoreButton.addClass('disabled');
    this.page++;
    this.filterLord.requestor.request(this.filterLord.collectRequestData())
      .then((result) => {
        this.filterLord.renderer.update(result, false);
        this.$loadMoreButton.removeClass('disabled');
        this.setItemCount(this.page * this.perPage);
      })
      .catch((e) => {
        console.log('oops', e);
      });
  }

  setItemCount(itemCount) {
    if (itemCount > this.total) {
      this.$loadMoreButton.addClass('disabled');
      itemCount = this.total;
      this.$loadMoreText.remove();
    }

    this.$currentItemCount.html(itemCount);
  }

  static loadMore(url, data) {
    return $.get(url, data);
  }

  getPage() {
    return this.page;
  }
}

export default Pagination;
