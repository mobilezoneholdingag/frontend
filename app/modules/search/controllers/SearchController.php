<?php

namespace app\modules\search\controllers;

use app\modules\BaseModuleController;
use app\modules\search\models\SearchModel;
use Yii;

/**
 * Class SearchController
 *
 * @package app\modules\search\controllers
 */
class SearchController extends BaseModuleController
{
	/**
	 * @return string
	 */
	public function actionIndex()
	{
		$request = Yii::$app->request;
		$searchModel = new SearchModel();
		$searchResult = [];

		if ($query = $request->get()) {
			$searchResult = $searchModel->fetch(null, $query);
		}

		return $this->renderFile('@search/search.twig', ['searchResult' => $searchResult]);
	}
}
