<?php

namespace app\modules\search\models;

use app\models\AbstractApiModel;
use app\models\ArticleModel;
use app\modules\shopFinder\models\ShopModel;
use Illuminate\Support\Collection;

/**
 * @property Collection|ShopModel[] $shops
 * @property Collection|ArticleModel[] $articles
 */
class SearchModel extends AbstractApiModel
{
	const API_ENTITY_NAME = 'search/search';

	protected $relations = [
		'shops' => ['class' => ShopModel::class, 'many' => true],
		'articles' => ['class' => ArticleModel::class, 'many' => true],
	];
}
