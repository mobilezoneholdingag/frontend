<?php

namespace app\modules;

use app\behaviors\DataLayerBehavior;
use app\behaviors\GoogleDataLayerBehavior;
use app\models\OrderModel;
use app\services\application\WebController;
use Yii;

/**
 * Class BaseModuleController
 */
class BaseModuleController extends WebController
{
	/**
	 * BaseModuleController constructor.
	 *
	 * @param string $id
	 * @param \yii\base\Module $module
	 * @param array $config
	 */
	public function __construct($id, $module, $config = [])
	{
		parent::__construct($id, $module, $config);

		Yii::$app->view->attachBehaviors(
			[
				DataLayerBehavior::GOOGLE => GoogleDataLayerBehavior::className(),
			]
		);
	}
}
