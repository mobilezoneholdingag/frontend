<?php

namespace app\modules\swisscom\controllers;

use app\models\ApiRepository;
use app\models\ArticleModel;
use app\models\ProviderModel;
use app\models\SwisscomRepository;
use app\models\SwisscomRetentionModel;
use app\models\TariffModel;
use app\modules\BaseModuleController;
use app\modules\checkout\services\cart\CartInterface;
use app\modules\checkout\services\cart\SessionCart;
use app\services\Languages;
use DateTime;
use Yii;
use yii\helpers\Url;

/**
 * Class SwisscomController
 *
 * @package app\modules\swisscom\controllers
 */
class SwisscomController extends BaseModuleController
{
	/** @var SessionCart CartInterface */
	protected $cart;

	public function __construct($id, $module, CartInterface $cart, $config = [])
	{
		$this->cart = $cart;

		parent::__construct($id, $module, $config);
	}

	public function beforeAction($action)
	{


		$request = Yii::$app->request;
		list($route, $queryParameters) = $request->resolve();

		if (isset($queryParameters['lang'])) {
			return $this->redirectToLocalizedUrl($route, $queryParameters);
		}

		return parent::beforeAction($action);
	}

	private function redirectToLocalizedUrl(string $route, array $queryParameters): bool
	{
		$language = $queryParameters['lang'];
		unset($queryParameters['lang']);
		$url = Languages::localizedBaseUrl($language) . Url::to(array_merge(["/{$route}"], $queryParameters));
		$this->redirect($url);

		return false;
	}

	public function actionIndex()
	{
		$this->handleCode();
		$this->handleSelectedContract();
		return $this->renderRetentionOffers();
	}

	private function handleCode()
	{
		$request = Yii::$app->request;

		if ($request->get('code')) {
			Yii::$app->session->set('swisscom_code', $request->get('code'));

			$accessToken = di(SwisscomRepository::class)->getAccessToken($request->get('code'));
			Yii::$app->session->set('swisscom_access_token', $accessToken);

			$subscriptionSelectUrl = di(SwisscomRepository::class)->getUrlForContractSelection($accessToken);
			Yii::$app->response->redirect($subscriptionSelectUrl);
		}
	}

	private function handleSelectedContract()
	{
		$request = Yii::$app->request;

		if ($request->get('subscriptionKey')) {
			Yii::$app->session->set('swisscom_subscription_key', $request->get('subscriptionKey'));
		}
	}

	private function renderRetentionOffers()
	{
		$token = Yii::$app->session->get('swisscom_access_token');
		$key = Yii::$app->request->get('subscriptionKey');

		if ($token && $key) {
			Yii::$app->session->set('swisscom_subscription_key', $key);

			$articleId = Yii::$app->session->get('swisscom_article_id');
			/** @var SwisscomRetentionModel $data */
			$data = di(SwisscomRepository::class)->getRetentionOffers($token, $key, $articleId);

			if ($data) {
				if (isset($data->simCardIndeterminable) && $data->simCardIndeterminable) {
					Yii::$app->session->addFlash('danger', Yii::t('view', 'Retention_Offers_New_Sim_Card_Needed'));
					Yii::$app->session->set('simCardIndeterminable', true);
				}

				Yii::$app->session->set('renewal_request_id', $data->renewalRequestId ?? null);

				if(isset($data->simCard)) {
					/** @var ApiRepository $articleRepository */
					$articleRepository = di(ApiRepository::class)->model(ArticleModel::class);
					/** @var ArticleModel $article */
					$article = $articleRepository->find($data->simCard, [], true);
					if ($article) {
						$article->setData(['id' => $article->id]);
						if (!$this->cart->cartContains($article)) {
							$this->cart->addItem($article);
						}
					}
				}

				Yii::$app->session->set('swisscom_proposed_subscription_start_date', $data->proposedSubscriptionStartDate ?? null);
				Yii::$app->session->set("eligible_tariffs_for_{$articleId}", $data->articleTariffs ?? []);

				$success = $data->eligible ? true : false;
				$subscriptionEndDate = '';
				if (!$success && isset($data->subsciption_end_date)) {
					$subscriptionEndDate = '&subscription_end_date=' . $data->subsciption_end_date;
				}
				$articleSelectUrl = Yii::$app->session->get('swisscom_article_url');

				$this->addFlashMessages($data, $success);

				if (!$articleSelectUrl && count($data->tariffs) > 0) {
					return $this->renderFile('@tariff/overview.twig', [
						'offer_type' => TariffModel::OFFER_TYPE_RENEW,
						'contract_mode' => 'contract-renewal',
						'tariffs' => $data->tariffs,
						'per_page' => 100,
					]);
				}

				if (!$articleSelectUrl) {
					return Yii::$app->response->redirect([
						'/product/renewal-request/index',
						'mode' => 'contract-renewal'
					]);
				}

				return Yii::$app->response->redirect(
					Url::to([
						"/{$articleSelectUrl}",
						'id' => $articleId,
						'offer_type' => TariffModel::OFFER_TYPE_RENEW,
						'mode' => 'contract-renewal',
						'provider' => 2,
						'eligible' => $success . $subscriptionEndDate,
					]) . '#subscriptions'
				);
			}
		}
		return null;
	}

	private function addFlashMessages($data, $success)
	{
		$endDate = $data->subsciption_end_date ?? null;
		$endDateTimeStamp = new DateTime($endDate);
		$endDateFormatted = $endDateTimeStamp->format('d.m.Y');

		if (!$success && $endDate !== null) {
			Yii::$app->session->addFlash(
				'danger',
				Yii::t(
					'contract-renewal',
					'Contract_Renewal_Possible_At{possible_renewal_date}',
					['possible_renewal_date' => $endDateFormatted]
				)
			);
		} elseif (Yii::$app->request->get('renewal_provider') !== null) {
			Yii::$app->session->addFlash(
				'success',
				Yii::t('product', 'Pdp_Renewal_Message_Title') .
				Yii::t('product', 'Pdp_Renewal_Message_Text')
			);
		}

		if ($success && (empty($data->tariffs) && empty($data->articleTariffs))) {
			Yii::$app->session->addFlash('danger', Yii::t('view', 'No_Eligible_Tariffs_For_Article'));
		}
	}
}
