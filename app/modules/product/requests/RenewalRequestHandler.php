<?php

namespace app\modules\product\requests;

use app\modules\product\models\RenewalRequestForm;
use Yii;
use yii\web\Request;

class RenewalRequestHandler
{
	public function handle(Request $request): RenewalRequestForm
	{
		$keepNumberForm = new RenewalRequestForm([
			'article_id' => $request->get('id', null),
		]);

		if ($request->isPost) {
			$keepNumberForm->load($request->post());
			$keepNumberForm->url = $request->getUrl();

			$scenario = $keepNumberForm->determineScenario();
			$keepNumberForm->scenario = $scenario;
			$keepNumberForm->is_valid = $keepNumberForm->validate();
			$scenariosWithPersonalData = [RenewalRequestForm::SCENARIO_PERSONAL_DATA, RenewalRequestForm::SCENARIO_CONTRACT_RENEWAL];
			if ($keepNumberForm->is_valid && in_array($scenario, $scenariosWithPersonalData)) {
				$keepNumberForm->create();
				$keepNumberForm->created = true;
			}
		}

		return $keepNumberForm;
	}
}
