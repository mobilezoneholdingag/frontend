<?php

namespace app\modules\product\models;

use app\models\AbstractApiModel;
use app\models\ApiRepository;
use app\models\ArticleModel;
use app\models\GenderInterface;
use app\models\PassportTypeInterface;
use app\models\ProviderModel;
use Illuminate\Support\Collection;
use Yii;

/**
 * @property Collection $providers
 * @property array $passport_types
 * @property array $genders
 */
class RenewalRequestForm extends AbstractApiModel
{
	const API_ENTITY_NAME = 'renewal-request';

	public $gender = GenderInterface::MALE;
	public $phone_number;
	public $firstname;
	public $lastname;
	public $birthday;
	public $passport_type;
	public $passport_number;
	public $email;
	public $provider_id;
	public $article_id;
	public $nationality;
	public $url;

	public $monthly_national_data_volume;
	public $monthly_international_data_volume;
	public $monthly_national_phone_minutes;
	public $monthly_international_phone_minutes;
	public $monthly_phone_minutes_from_abroad;

	public $created = false;
	public $is_valid = false;

	public $step_phone_number;
	public $step_personal_data;
	public $step_contract_renewal;
	public $step_swisscom_redirect;

	const SCENARIO_PHONE_NUMBER = 'step_phone_number';
	const SCENARIO_PERSONAL_DATA = 'step_personal_data';
	const SCENARIO_CONTRACT_RENEWAL = 'step_contract_renewal';
	const SCENARIO_SWISSCOM_REDIRECT = 'step_swisscom_redirect';

	protected $relations = [
		'provider' => ['class' => ProviderModel::class, 'field' => 'provider_id'],
		'article' => ['class' => ArticleModel::class, 'field' => 'article_id'],
	];

	public function scenarios()
	{
		return [
			static::SCENARIO_DEFAULT => $this->fields(),
			static::SCENARIO_PHONE_NUMBER => ['phone_number', 'provider_id', 'article_id'],
			static::SCENARIO_PERSONAL_DATA => ['phone_number', 'provider_id', 'article_id', 'gender', 'firstname', 'lastname', 'birthday', 'passport_type', 'passport_number', 'email', 'nationality'],
			static::SCENARIO_CONTRACT_RENEWAL => ['phone_number', 'provider_id', 'article_id', 'gender', 'firstname', 'lastname', 'birthday', 'passport_type', 'passport_number', 'email', 'monthly_national_data_volume', 'monthly_international_data_volume', 'monthly_national_phone_minutes', 'monthly_international_phone_minutes', 'monthly_phone_minutes_from_abroad', 'nationality'],
			static::SCENARIO_SWISSCOM_REDIRECT => ['provider_id', 'article_id'],
		];
	}

	public function rules()
	{
		return [
			[['gender', 'phone_number', 'provider_id', 'firstname', 'lastname', 'birthday', 'nationality', 'passport_type', 'passport_number', 'email'], 'required'],
			[['monthly_national_data_volume',
				'monthly_international_data_volume',
				'monthly_national_phone_minutes',
				'monthly_international_phone_minutes',
				'monthly_phone_minutes_from_abroad',], 'integer'],
			['provider', 'in', 'range' => $this->providers->pluck('id')],
			['gender', 'in', 'range' => array_keys(GenderInterface::ALL)],
		];
	}

	public function attributeLabels()
	{
		return [
			'phone_number' => Yii::t('view', 'Contract_Box_Which_Number_To_Prolong').'*',
			'provider' => Yii::t('view', 'Contract_Box_Prolong_With_Which_Provider'),
			'gender' => Yii::t('view','Gender'),
			'firstname' => Yii::t('checkout','Firstname'),
			'lastname' => Yii::t('checkout','Lastname'),
			'birthday' => Yii::t('checkout','Birthday'),
			'passport_type' => Yii::t('checkout','Passport_Type'),
			'passport_number' => Yii::t('checkout','Passport_Number'),
			'email' => Yii::t('checkout','Email'),
			'nationality' => Yii::t('checkout','Nationality'),
			'monthly_national_data_volume' => Yii::t('app', 'monatl. Datenvolumen - national'),
			'monthly_international_data_volume' => Yii::t('app', 'monatl. Datenvolumen - international'),
			'monthly_national_phone_minutes' => Yii::t('app', 'monatl. Gesprächsdauer - national'),
			'monthly_international_phone_minutes' => Yii::t('app', 'monatl. Gesprächsdauer - international'),
			'monthly_phone_minutes_from_abroad' => Yii::t('app', 'monatl. Gesprächsdauer - aus dem Ausland'),
		];
	}

	public function getProviders()
	{
		/** @var ApiRepository $repository */
		$repository = di(ApiRepository::class);

		return $repository->model(ProviderModel::class)->find();
	}

	public function getProvider()
	{
		/** @var ApiRepository $repository */
		$repository = di(ApiRepository::class);

		return $repository->model(ProviderModel::class)->find($this->provider_id);
	}

	public function determineScenario() {
		$possibleScenarios = $this->scenarios();
		unset($possibleScenarios['default']);

		foreach($possibleScenarios as $key => $value) {
			if($this->{$key} != null) {
				return $key;
			}
		}
		return 'default';
	}
}
