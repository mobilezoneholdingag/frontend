<?php

namespace app\modules\product\models;

use app\models\AbstractApiModel;
use app\models\ArticleTariffModel;

/**
 * @property ArticleTariffModel $offer
 * @property array $passport_types
 * @property array $genders
 * @property string $valid_until_date
 * @property string $phone_number
 * @property int $customer_id
 * @property string $firstname
 * @property string $lastname
 */
class RenewalRequestModel extends AbstractApiModel
{
	const API_ENTITY_NAME = 'renewal-request';

	protected $relations = [
		'offer' => ['class' => ArticleTariffModel::class, 'field' => 'offer_id'],
	];

	public function getFullName()
	{
		return $this->firstname . ' ' . $this->lastname;
	}
}
