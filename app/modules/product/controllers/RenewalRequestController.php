<?php

namespace app\modules\product\controllers;

use app\models\ApiRepository;
use app\models\ProviderModel;
use app\models\SwisscomRepository;
use app\modules\BaseModuleController;
use app\modules\product\models\RenewalRequestModel;
use app\modules\product\requests\RenewalRequestHandler;
use Illuminate\Support\Collection;
use Yii;
use yii\web\NotFoundHttpException;

class RenewalRequestController extends BaseModuleController
{
	protected $handler;

	public function __construct($id, $module, RenewalRequestHandler $renewalRequestHandler, array $config = [])
	{
		parent::__construct($id, $module, $config);

		$this->handler = $renewalRequestHandler;
	}

	public function actionView()
	{
		$id = Yii::$app->request->get('id');
		/** @var RenewalRequestModel $renewalRequest */
		$renewalRequest = di(ApiRepository::class)
			->model(RenewalRequestModel::class)->find($id);

		$offer = $renewalRequest->offer;

		if (!$renewalRequest || !$offer) {
			throw new NotFoundHttpException();
		}

		return $this->renderFile('@product/product/renewal-request.twig', [
			'article_tariff' => $offer,
			'article' => $offer->article,
		]);
	}

	public function actionIndex()
	{
		$request = Yii::$app->request;
		$form = $this->handler->handle($request);

		/** @var ApiRepository $repository */
		$repository = di(ApiRepository::class)->model(ProviderModel::class);
		/** @var Collection|ProviderModel[] $providers */
		$providers = $repository->find()->where('renewable', '==', true);

		return $this->renderFile('@product/contract-renewal/index.twig', [
			'contract_mode' => ProductController::getContractMode(),
			'renewal_request_form' => $form,
			'providers' => $providers,
			'swisscom_login_url' => di(SwisscomRepository::class)->getLoginUrl(),
			'selected_provider' => $repository->find($request->get('selected_provider')),
		]);
	}
}
