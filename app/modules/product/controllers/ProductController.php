<?php

namespace app\modules\product\controllers;

use app\models\ApiRepository;
use app\models\ArticleGroupModel;
use app\models\ArticleModel;
use app\models\ProductCategoryModel;
use app\models\ProviderModel;
use app\models\SetSearchModel;
use app\models\SetSearchRepository;
use app\models\SwisscomRepository;
use app\models\ArticleTariffFilterModel;
use app\models\TariffModel;
use app\modules\BaseModuleController;
use app\modules\checkout\services\cart\ItemInterface;
use app\modules\product\requests\RenewalRequestHandler;
use Illuminate\Support\Collection;
use Yii;
use yii\helpers\Url;

/**
 * Class ProductController
 *
 * @package app\modules\product\controllers
 */
class ProductController extends BaseModuleController
{
	protected $renewalHandler;
	protected $cachedSelectedProvider;
	private $productCategoryId = null;
	const ITEMS_PER_PAGE = 20;

	public function __construct($id, $module, RenewalRequestHandler $renewalRequestHandler, array $config = [])
	{
		parent::__construct($id, $module, $config);

		$this->renewalHandler = $renewalRequestHandler;
	}

	/**
	 * @return string
	 */
	public function productWithSubscriptionOption($categoryName)
	{
		$request = Yii::$app->request;
		$id = $request->get('id');

		if ($request->isAjax && $request->get('product_card', false)) {
			return $this->renderProductCard($id);
		}

		$eligible = $request->get('eligible');
		if (self::getContractMode() == 'contract-renewal') {
			$route = Yii::$app->request->resolve()[0];
			Yii::$app->session->set('swisscom_article_url', $route);
			Yii::$app->session->set('swisscom_article_id', $id);
			if (isset($eligible) && $eligible == 0) {
				return $this->redirect(
					[
						Url::current(
							[
								'renewal_provider' => ProviderModel::getSwisscomProvider()->id,
								'mode' => 'porting',
								'eligible' => false,
								'subscription_end_date' => $request->get('subscription_end_date', null),
							]
						) . '#subscriptions',
					]
				);
			}
		}

		$renewalForm = $this->renewalHandler->handle($request);

		if ($renewalForm->created) {
			return $this->redirect([
				Url::current(
					[
						'renewal_provider' => $renewalForm->provider_id,
						'mode' => 'contract-renewal',
						'eligible' => true,
					]
				) . '#subscriptions',
			]);
		}

		/** @var Collection|ArticleModel[] $article */
		$article = di(ApiRepository::class)->model(ArticleModel::class)->find($id, ['expand' => 'articleDetails']);


		if(!isset($article['group_id'])){
			return $this->redirect(Url::base(true));
		}

		/** @var Collection|ArticleGroupModel $articleGroup */
		$articleGroup = $article['group_id'] ? di(ApiRepository::class)->model(ArticleGroupModel::class)->find($article['group_id']) : null;

		$this->productCategoryId = $article->product_category_id ?? null;

		$offerType = $this->getOfferType();

		$eligibleTariffs = null;
		if ($offerType == TariffModel::OFFER_TYPE_RENEW) {
			$eligibleTariffs = Yii::$app->session->get("eligible_tariffs_for_{$id}");
		}

		/** @var Collection|TariffModel[] $tariffs */
		$filterModel = di(ApiRepository::class)->model(ArticleTariffFilterModel::class)->findOne([
			'facets' => 1, // show available filter options
			'article' => $id, // only load tariffs belonging to this article
			'offer_type' => $offerType, // new phone number or contract renewal?
			'article_tariff_id_included' => $eligibleTariffs,
			'per-page' => self::ITEMS_PER_PAGE,
			'page' => $request->get('page', 1),
			'group_by' => 'tariff_id',
			'provider' => Yii::$app->request->get('provider'),
		]);

		/** @var Collection $articleTariffs */
		$articleTariffs = $filterModel->article_tariffs;
		$standardTariffs = $filterModel->standard_tariffs;
		$standardTariffProviders = $this->getStandardTariffProvider($standardTariffs);
		$facets = $filterModel->facets;

		$accessories = $this->getAccessories($articleGroup);
		$accessoryCount = $this->getAccessoryCount($accessories);

		$allProvider = di(ApiRepository::class)->model(ProviderModel::class)->find()->where('renewable', '==', true);

		$selectedProvider = $this->getSelectedProvider();

		$showSubscriptions = false;
		if (isset($selectedProvider) && $selectedProvider->contract_renewal_direct_offer && $request->get('eligible', false)) {
			$showSubscriptions = true;
		}

		$eligible = Yii::$app->request->get('eligible');

		if ($eligible && is_array($eligibleTariffs) && (empty($eligibleTariffs) || !$articleTariffs->count())) {
			$showSubscriptions = false;
		}

		$params = [
			'current_box' => $request->get('mode', 'new-number'),
			'contract_mode' => self::getContractMode(),
			'no_contract' => $articleTariffs->count() === 0,
			'article' => $article,
			'article_group' => $articleGroup,
			'article_tariffs' => $articleTariffs,
			'standard_tariffs' => $standardTariffs,
			'standard_tariff_providers' => $standardTariffProviders,
			'renewal_request_form' => $renewalForm,
			'swisscom_login_url' =>  $request->get('mode') === 'contract-renewal' ? di(SwisscomRepository::class)->getLoginUrl() : null,
			'providers' => $allProvider,
			'selected_provider' => $selectedProvider,
			'filters' => $facets,
			'selected_standard_tariff' => $this->getSelectedStandardTariff($standardTariffs),
			'initial_accessory_articles' => $accessoryCount,
			'hide_accessory_button' => $accessories ? $this->hideAccessoryButton($accessories) : false,
			'show_subscriptions' => $showSubscriptions,
			'total' => $filterModel->total ?? 0,
			'per_page' => self::ITEMS_PER_PAGE,
			'sim_only' => $id == ArticleModel::SIM_ONLY,
			'delivery_items' => $this->getDeliveryItemArray($article['delivery_includes']),
		];

		return $this->renderFile('@product/product/'.$categoryName.'.twig', $params);
	}

	private function getAccessories(ArticleGroupModel $articleGroup = null)
	{
		if (!$articleGroup) {
			return null;
		}

		$accessorySet = di(SetSearchRepository::class)->findAccessorySets($articleGroup->accessory_set_id);

		if (!$accessorySet) {
			return null;
		}

		/** @var SetSearchModel $accessories */
		return di(SetSearchRepository::class)->findByAccessorySets($accessorySet->items);
	}

	private function getAccessoryCount(SetSearchModel $accessories = null)
	{
		if (!$accessories) {
			return null;
		}

		$accessoryCount = Yii::$app->request->get('all_accessories')
			? $accessories->getArticlesOnly()
			: $accessories->getArticlesOnly()->take(SetSearchModel::INITIAL_ITEM_LIMIT);

		return $accessoryCount;
	}

	/**
	 * @return string
	 */
	public function actionSmartphones()
	{
		return $this->productWithSubscriptionOption('smartphone');
	}

	/**
	 * @return string
	 */
	public function actionAccessories()
	{
		return $this->productWithoutSubscriptionOption('accessorie');
	}

	/**
	 * @return string
	 */
	public function actionTablets()
	{
		return $this->productWithSubscriptionOption('tablet');
	}

	/**
	 * @return string
	 */
	public function actionSmartwatches()
	{
		return $this->productWithoutSubscriptionOption('smartwatch');
	}


    /**
     * @return string
     */
    public function actionRefurbished()
    {
        return $this->productWithoutSubscriptionOption('refurbished');
    }


	protected function productWithoutSubscriptionOption($categoryName)
	{
		$id = Yii::$app->request->get('id');

		/** @var Collection|ArticleModel[] $article */
		$article = di(ApiRepository::class)->model(ArticleModel::class)->find($id, ['expand' => 'articleDetails']);

		/** @var Collection|ArticleGroupModel[] $articleGroup */
		$articleGroup = di(ApiRepository::class)->model(ArticleGroupModel::class)->find($article->group_id);

		$params = [
			'article' => $article,
			'article_group' => $articleGroup,
			'no_contract' => true,
			'accessory_id' => $categoryName === 'accessorie' ? $article->id : null // TODO there has to be a better way to detect the article type
		];

		return $this->renderFile('@product/product/' . $categoryName . '.twig', $params);
	}

	/**
	 * json enpdpoint for requesting a filtered tariff list
	 *
	 * @return string
	 */
	public function actionFilteredTariffs()
	{

		$filter = Yii::$app->request->get();

		/** @var ApiRepository $filterRepositor */
		$filterRepository = di(ApiRepository::class)->model(ArticleTariffFilterModel::class);

		if(isset($filter['id']) || isset($filter['article'])) {
			/** @var ArticleModel $article */
			$article = di(ApiRepository::class)->model(ArticleModel::class)->find($filter['id'] ?? $filter['article'] ?? null);

			$this->productCategoryId = $article->product_category_id ?? null;
		}

		$page = $filter['page'] ?? 1;
		$filterModel = $filterRepository->findOne(array_merge($filter, [
			'facets' => 1,
			'article' => $filter['article'] ?? null,
			'offer_type' => $this->getOfferType(),
			'per-page' => self::ITEMS_PER_PAGE,
			'page' => $page,
			'group_by' => 'tariff_id',
		]), true);
		$articleTariffs = $filterModel->article_tariffs;
		$standardTariffs = $filterModel->standard_tariffs;
		$renderedList = $this->renderFile('@product/subscriptions/subs-list.twig', [
			'article_tariffs' => $articleTariffs,
			'standard_tariffs' => $page > 1 ? null : $standardTariffs,
			'filter' => $filter,
            'isAjax' => Yii::$app->request->isAjax
		]);
		return $renderedList;
	}

	private function getOfferType()
	{
		$mode = Yii::$app->request->get('mode');
		if ($this->productCategoryId === ProductCategoryModel::TABLET) {
			if ($mode === 'contract-renewal') {
				return TariffModel::OFFER_TYPE_RENEW;
			}
			return TariffModel::OFFER_TYPE_DATA;
		}

		// by contract box
		switch ($mode) {
			case 'new-number':
				return TariffModel::OFFER_TYPE_NEW;
			case 'porting':
				return TariffModel::OFFER_TYPE_PORTING;
			case 'contract-renewal':
				return TariffModel::OFFER_TYPE_RENEW;
		}

		// by "get" parameter
		if(Yii::$app->request->get('offer_type')) {
			return Yii::$app->request->get('offer_type');
		}

		// default
		return TariffModel::OFFER_TYPE_NEW;
	}

	private function getSelectedProvider()
	{
		$providerId = Yii::$app->request->get('provider');

		if($providerId) {
			/** @var ProviderModel[] $provider */
			$this->cachedSelectedProvider = di(ApiRepository::class)->model(ProviderModel::class)->find($providerId);
		}

		return $this->cachedSelectedProvider;
	}

	private function getSelectedStandardTariff($standardTariffs)
	{

		$providerId = $this->cachedSelectedProvider['id'] ?? null;
		$selectedStandardTariff = [];

		foreach ($standardTariffs as $standardTariff) {
			if($standardTariff['tariff']['provider_id'] == $providerId) {
				$selectedStandardTariff = $standardTariff;
			}
		}

		return $selectedStandardTariff;
	}

	private function getStandardTariffProvider($standardTariffs)
	{
		$StandardTariffProvider = [];

		foreach ($standardTariffs as $standardTariff) {
			$StandardTariffProvider[] = $standardTariff->tariff->provider;
		}

		return Collection::make($StandardTariffProvider);
	}

	public static function getContractMode()
	{
		$contractMode = Yii::$app->request->get('mode');

		if (!$contractMode) {
			$contractMode = 'new-number';
		}

		// TODO we need this information later-on in the checkout, but is this really the correct place and way of doing it?
		Yii::$app->session->set('selected_contract_mode', $contractMode);

		return $contractMode;
	}

	private function hideAccessoryButton($accessories)
	{
		return Yii::$app->request->get('all_accessories') ||
			$accessories->items->count() <= SetSearchModel::INITIAL_ITEM_LIMIT;
	}

	private function renderProductCard(int $articleId): string
	{
		$tariffId = Yii::$app->request->get('tariff_id');

		/** @var ArticleTariffFilterModel $tariffs */
		$filterModel = di(ApiRepository::class)->model(ArticleTariffFilterModel::class)->findOne(
			[
				ItemInterface::TYPE_ARTICLE => $articleId,
				ItemInterface::TYPE_TARIFF => $tariffId,
			]
		);

		$articleTariff = $filterModel->article_tariffs->first();

		$params = [
			'article' => $articleTariff->article,
			'article_tariff' => $articleTariff
		];

		return $this->renderFile('@category/product-card.twig', $params);
	}

	public function getDeliveryItemArray($items)
	{
		return explode(', ', $items);
	}
}
