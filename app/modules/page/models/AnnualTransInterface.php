<?php

namespace app\modules\page\models;

interface AnnualTransInterface
{
	const ALL = [

		//Navi
		'kpi-title' => [
			'de' => 'Kennzahlen',
			'en' => 'Key Figures',
		],
		'profile-title' => [
			'de' => 'Portrait',
			'en' => 'Profile',
		],
		'letter-title' => [
			'de' => 'Aktionärsbrief',
			'en' => 'Letter to Shareholders',
		],
		'governance-title' => [
			'de' => 'Corporate Governance',
			'en' => 'Corporate Governance',
		],
		'compensation-title' => [
			'de' => 'Vergütungsbericht',
			'en' => 'Compensation Report',
		],
		'financial-title' => [
			'de' => 'Finanzbericht',
			'en' => 'Financial Report',
		],


		//PDF Titles
		'pdf-title-full' => [
			'de' => 'Geschäftsbericht 2016',
			'en' => 'Annual report 2016',
		],
		'pdf-title-overview' => [
			'de' => 'Kurzporträt 2016',
			'en' => 'Brief profile 2016',
		],
		'pdf-title-letter' => [
			'de' => 'Aktionärsbrief 2016',
			'en' => 'Letter to Shareholders 2016',
		],
		'pdf-title-highlights' => [
			'de' => 'Highlights 2016',
			'en' => 'Highlights report 2016',
		],
		'pdf-title-department' => [
			'de' => 'Bereichsberichte 2016',
			'en' => 'Department report 2016',
		],
		'pdf-title-corporate' => [
			'de' => 'Corporate Governance 2016',
			'en' => 'Corporate Governance report 2016',
		],
		'pdf-title-compensation' => [
			'de' => 'Vergütungsbericht 2016',
			'en' => 'Compensation report 2016',
		],
		'pdf-title-financial' => [
			'de' => 'Finanzbericht 2016',
			'en' => 'Financial report 2016',
		],


		//Common
		'title' => [
			'de' => 'Geschäftsbericht',
			'en' => 'Annual Report',
		],
		'read-more' => [
			'de' => 'Hier mehr lesen',
			'en' => 'Read more',
		],
		'read-pdf' => [
			'de' => 'Im PDF lesen',
			'en' => 'Read full PDF',
		],


		//Start
		'profile-teaser' => [
			'de' => 'Die mobilezone Gruppe ist ein Telekommunikationsunternehmen mit Hauptsitz in Regensdorf, Zürich',
			'en' => 'mobilezone Group is a telecommunications company headquartered in Regensdorf, Zurich,',
		],
		'kpi-profit' => [
			'de' => 'Konzerngewinn (CHF Mio.)',
			'en' => 'Net consolidated profit (m CHF)',
		],
		'kpi-shareprice' => [
			'de' => 'Aktienkursentwicklung',
			'en' => 'Share Price Development',
		],
		'kpi-ebit' => [
			'de' => 'Betriebsgewinn / EBIT (CHF Mio.)',
			'en' => 'Operating profit / EBIT (m CHF)',
		],
		'kpi-ebitda' => [
			'de' => 'Konzerngewinn (CHF Mio.)',
			'en' => 'Net consolidated profit (m CHF)',
		],
		'kpi-employees' => [
			'de' => 'Personalbestand',
			'en' => 'Employees',
		],
		'kpi-net-sales' => [
			'de' => 'Nettoverkaufserlös (CHF Mio.)',
			'en' => 'Net Sales (m CHF)',
		],
		'kpi-dividend' => [
			'de' => 'Dividende (CHF)',
			'en' => 'Dividend (CHF)',
		],
		'letter-teaser' => [
			'de' => 'Brief an die Aktionärinnen und Aktionäre',
			'en' => 'Letter to our shareholders from CEO and advisory board',
		],
		'highlights-teaser' => [
			'de' => 'Die Highlights im Jahre 2016',
			'en' => 'Our highlights in 2016',
		],
		'governance-teaser' => [
			'de' => 'Grundsätze und Bestimmungen der Corporate Governance...',
			'en' => 'The principles and provisions of Corporate Governance...',
		],
		'compensation-teaser' => [
			'de' => 'Der Vergütungsbericht legt in Übereinstimmung mit den gesetzlichen Bestimmungen...',
			'en' => 'The compensation report outlines the compensation guidelines...',
		],
		'financial-teaser' => [
			'de' => 'Der Finanzbericht enthält die ausführliche Konzernrechnung der mobilezone Gruppe...',
			'en' => 'The financial report comprises full audited financial records of the mobilezone group...',
		],


		//Key Figures
		'kpi-text' => [
			'de' => 'mobilezone erzielte einen Rekordumsatz und erneut einen höheren Konzerngewinn.',
			'en' => 'mobilezone reached a new record turnover and was able to increase its profits.',
		],
		'groupreport-subtitle' => [
			'de' => 'Konzernrechnung mobilezone Gruppe',
			'en' => 'Accounts mobilezone group',
		],


		//Profile
		'profile-subtitle' => [
			'de' => 'Das passende Angebot für jeden Kunden',
			'en' => 'The right offer for every customer',
		],
		'profile-text' => [
			'de' => 'Die mobilezone Gruppe, ein Telekommunikationsunternehmen mit Hauptsitz in Regensdorf/Zürich, 
				umfasst zwei Geschäftsbereiche (Handel und Service Providing). Zum Geschäftsbereich ­Handel gehören die 
				mobilezone ag, die einsAmobile GmbH und die mobilezone business ag. Der Geschäftsbereich Service 
				Providing umfasst die TalkTalk AG, die mobiletouch ag und die ­mobiletouch austria gmbH. Die mobilezone 
				Gruppe zeichnet sich neben einer kompetenten und unabhängigen Beratung von Privat- und Geschäftskunden 
				zu Tarifplänen für Handy, Festnetztelefonie, Internet und Digital TV der wichtigen Anbieter sowie im 
				Reparaturwesen auch durch eine grosse Service-Palette aus: eigene Handyversicherungen, die Rücknahme von 
				gebrauchten und die Reparaturannahme defekter Handys in allen Shops. An ausgewählten Standorten werden 
				in Service-Centern Express-Reparaturen vor Ort angeboten. Ergänzt wird das Portfolio mit dem Canon 
				Repair-Center, welches Reparaturen von Canon-Kameras und Home-Office-Geräten anbietet.  Mit einsAmobile 
				ist mobilezone in Deutschland vertreten und vertreibt Mobilfunkprodukte über diverse Webportale online 
				und via Partner-Shops. Zusätzlich ist einsAmobile im Grosshandel und in der Belieferung des Fachhandels 
				in Deutschland tätig. Die mobilezone Gruppe beschäftigt mehr als 900 Mitarbeitende an den Standorten 
				Regensdorf, Urnäsch, Zweidlen, Zug, Wien (A) und Obertshausen (D).',
			'en' => 'mobilezone Group is a telecommunications company headquartered in Regensdorf/Zurich. Its two 
				business segments are Trade and Service Providing. The segment Trade includes the companies mobilezone 
				ag, einsAmobile GmbH, and mobilezone business ag. The business segment Service Providing consists of 
				TalkTalk AG, mobiletouch ag, and mobiletouch austria gmbH. In addition to offering competent and 
				independent advice for private and business customers regarding the price plans of all major providers 
				for mobile phones, fixed-line telephony, Internet, and digital TV, as well as regarding repairs, 
				mobilezone stands out for its extensive range of services: the company offers its own mobile phone 
				insurance, buys back used mobile phones, and accepts mobile phones for repair in all its shops. In 
				selected shops, express repairs are available in on-site service centers. To this extensive portfolio, 
				the company has added the Canon Repair Center; it offers repairs of Canon cameras and home office 
				equipment. Thanks to the acquisition of einsAmobile, mobilezone also has a presence in Germany and sells 
				mobile phone products online via various web portals and Partner shops since April. In addition, 
				einsAmobile is active in the wholesale trade and also supplies specialist stores in Germany. Overall, 
				mobilezone Group employs more than 900 people at its offices and shops in Regensdorf, Urnäsch, Zweidlen, 
				Zug, Wien (A), and Obertshausen (D). ',
		],
		'retail-subtitle' => [
			'de' => 'Handel',
			'en' => 'Retail',
		],
		'retail-mz-text' => [
			'de' => 'mobilezone ist der führende unabhängige Schweizer Telekomspezialist im Bereich Mobil- und 
				Festnetztelefonie. Als Marktführer bietet mobilezone ein vollständiges Handy-Sortiment und Tarifpläne 
				für Mobil- und Festnetztelefonie, Digital TV und Internet der wichtigen Anbieter (Swisscom, Sunrise und 
				UPC) inklusive unabhängiger Beratung und Services. Kunden finden bei mobilezone das grösste Angebot an 
				Mobiltelefonen und Zubehör.',
			'en' => 'mobilezone is the leading independent telecommunication specialist for mobile and landline 
				connection in Switzerland. The market leader mobilezone offers a complete assortment of smartphones, 
				tariff plans for mobile and landline communication, digital TV and internet of the major providers 
				(Swisscom, Sunrise and UPC) as well as independent advice and service. Customers are welcomed with the 
				largest offering of mobile devices and accessories at mobilezone.',
		],
		'retail-eam-text' => [
			'de' => 'einAmobile ist auf die Vermittlung von Mobilverträgen sowie der Verkauf von Hardware spezialisiert. 
				Die Dienstleistungen und Produkte werden in 55 Ashop-Standorten (Partner-Shops) sowie online über 
				diverse Webportale angeboten. Zusätzlich ist einsAmobile in der Belieferung des Fachhandels tätig.',
			'en' => 'einsAmobile is specialized on brokering mobile telecommunication contracts and wholesaling phone 
				hardware. Services and products are offered in 55 Ashop locations (partner shops) as well as online via 
				several digital platforms. In addition, einsAmobile is wholeseller for specialty retailers.',
		],
		'retail-mzbiz-text' => [
			'de' => 'mobilezone business bietet unabhängige Beratung und individuelle Kundenlösungen im Bereich 
				Telekommunikation für KMU und Grossunternehmen. mobilezone fungiert dabei als Schnittstelle zwischen 
				Telekomanbietern, Geräteherstellern und Unternehmen und unterstützt Firmen jeder Branche bei 
				individuellen Gesamtlösungen in den Bereichen Fleet-Management und Outsourcing.',
			'en' => 'mobilezone business provides independent advice and custom solutions for telecommunication needs of 
				small, medium and large corporations. mobilezone acts as an intermediary between companies, contract 
				providers and device manufacturers and supports business clients from all industries by developing 
				holistic solutions which are tailored to the individual requirements including fleet management and 
				outsourcing.',
		],
		'service-talktalk-text' => [
			'de' => 'TalkTalk bietet Dienstleistungen in den Bereichen Mobil-, Festnetz-, Grundanschluss und Internet an. 
				Alle Produkte von TalkTalk sind schweizweit in den 129 mobilezone-Shops erhältlich.',
			'en' => 'TalkTalk offers mobile, landline and internet communication services across Switzerland in all 129 
				shops run by mobilezone.',
		],
		'service-mobiletouch-text' => [
			'de' => 'mobiletouch bietet als führendes Service-Center in der Schweiz und in Österreich Reparatur- und 
				Logistikdienstleistungen für mobile Produkte wie Handys, Digitalkameras und andere Geräte an.',
			'en' => 'mobiletouch is the leading service center in Switzerland and Austria providing repair and logistics 
				solutions for mobile products such as smartphones, digital cameras and other devices.',
		],


		//Letter to Shareholders
		'letter-subtitle' => [
			'de' => 'mobilezone erzielt erneut einen höheren Gewinn und einen Rekordumsatz',
			'en' => 'mobilezone achieves record turnover and higher profit yet again',
		],
		'letter-fischer-title' => [
			'de' => 'Verwaltungsratspräsident',
			'en' => 'President Board of Directors',
		],
		'letter-content' => [
			'de' => '<p><strong>Sehr geehrte Aktionärinnen und Aktionäre</strong></p><br>
				<p>
					<strong>mobilezone steigert den Betriebsgewinn um 20 Prozent auf CHF 48.5 Mio.</strong><br>
					mobilezone hat im Geschäftsjahr 2016 einen Rekordumsatz von CHF 1 088 Mio. erzielt, der 27 Prozent 
					über dem Vorjahr liegt. Bei einer Pro-forma-Konsolidierung der einsAmobile im Vorjahr ab dem 1. 
					Januar 2015 beträgt der Umsatz 2015 CHF 997 Mio. gegenüber CHF 1 088 Mio. im abgeschlossenen 
					Berichtsjahr.
				</p>
				<p>
					Diese sehr erfreuliche Entwicklung spiegelt sich in einem um 20 Prozent höheren operativen ­Gewinn 
					(EBIT) von CHF 48.5 Mio. (Vorjahr: CHF 40.4 Mio.) wider. Die Finanzaufwendungen ­stiegen um CHF 0.4 
					Mio. auf CHF 1.8 Mio. Der Steueraufwand betrug CHF 10.7 Mio. (Vorjahr: CHF 8.6 Mio.). Der 
					Konzerngewinn konnte um 17 Prozent auf CHF 36.1 Mio. (Vorjahr: CHF 30.8 Mio.) ge­steigert werden. 
					Der Pro-forma-Gewinn 2015 betrug CHF 32 Mio.
				</p>
				<p>
					Der Gewinn pro Aktie konnte von CHF 0.96 auf CHF 1.12 im Berichtsjahr erhöht werden. Der ­Aktienkurs 
					lag am 6. März 2017 bei CHF 14.55 gegenüber CHF 14.35 Ende Dezember 2015.
				</p>
				<p>
					Die Bilanz von mobilezone weist am 31. Dezember 2016 flüssige Mittel von CHF 27.0 Mio. (Vorjahr: CHF 
					14.3 Mio.) aus. Das Nettoumlaufvermögen belief sich am Jahresende auf CHF 27.2 Mio. (Vorjahr: CHF 
					42.6 Mio.). Das Eigenkapital im Einzelabschluss der mobilezone holding ag beträgt CHF 54.7 Mio. 
					(Vorjahr: CHF 58.5 Mio.).
				</p>
				<p>
					Beide Geschäftsbereiche Handel und Service Providing haben zu diesem erfolgreichen Geschäftsresultat 
					beigetragen.
				</p>
				<br>
				<p>
					<strong>Segment Handel mit sehr grossem Umsatzanstieg und Steigerung der Profitabilität</strong><br>
					Das Segment Handel beinhaltet die Aktivitäten im schweizweiten Filialnetz, das Online-Geschäft, das 
					B2B-Geschäft und seit April 2015 die Aktivitäten von einsAmobile in Deutschland.
				</p>
				<p>
					Das Marktumfeld im Retailgeschäft in der Schweiz bleibt anspruchsvoll und herausfordernd. Es hat 
					sich im ersten Halbjahr gezeigt, dass die Geschäftsstrategien von mobilezone und Salt zu 
					unterschiedlich sind, um unseren Kunden weiterhin attraktive Salt-Produkte anbieten zu können. 
					mobilezone setzt auf die Partner Swisscom, Sunrise, UPC und TalkTalk und bietet seit Juli 2016 keine 
					Salt-Produkte mehr an.
				</p>
				<p>
					An den Standorten Luzern Bahnhof, Bern Waaghaus, Hinwil, Genf Rue de Carouge, Spreitenbach, Emmen 
					Center, Zug Metalli und Zürich Löwenstrasse wurde das neue Ladenbaukonzept, in komplett neuem 
					Design, eingeführt. Im Bahnhof Zürich (ShopVille) wurde ein neuer Shop im November eröffnet. Weitere 
					Shops werden im laufenden Jahr nach dem neuen Konzept umgebaut.
				</p>
				<p>
					Insbesondere das Online-Geschäft, aber auch das Grosshandelsgeschäft in Deutschland, konnte mit 
					Rekordergebnissen sowohl hinsichtlich Umsatz als auch im Betriebsergebnis aufwarten. So wurden in 
					Deutschland über 200 000 (Vorjahr: 150 000) Mobilfunkverträge online abgeschlossen. Mit der 
					Lancierung von deinhandy.ch im Jahr 2016 hat mobilezone in der Schweiz einen wichtigen Schritt 
					gemacht, um die deutschen Online-Kenntnisse im Schweizer Markt zu nutzen.
				</p>
				<p>
					Im Segment Handel konnte der Umsatz von CHF 785 Mio. auf CHF 1 012 Mio. oder um 29 Prozent 
					gesteigert werden. Der Anteil des Grosshandelsumsatzes beträgt CHF 618 Mio. (Vorjahr: CHF 422 Mio.). 
					Das EBIT stieg von CHF 27.8 Mio. auf CHF 40.6 Mio. oder um 46 Prozent an. Diese sehr erfreuliche 
					Entwicklung wurde im Wesentlichen in Deutschland erzielt.
				</p>
				<br>
				<p>
					<strong>Segment Service Providing mit Steigerung der Profitabilität</strong><br>
					Das Segment Service Providing beinhaltet die eigenen TalkTalk Mobil- und Festnetzangebote der 
					Gesellschaft und das Servicegeschäft in der Schweiz und in Österreich.
				</p>
				<p>
					Der Umsatz im Service Providing nahm von CHF 73.6 Mio. auf CHF 75.5 Mio. oder um 2,6 Prozent zu. Das 
					EBIT stieg um 6,6 Prozent auf CHF 8.1 Mio. an.
				</p>
				<p>
					Die Profitabilität des Schweizer und des österreichischen Reparaturgeschäfts verlief im zweiten 
					Halbjahr 2016 äusserst positiv, obwohl die Auftragsvolumen im 2016 um insgesamt 6 Prozent abnahmen.
				</p>
				<p>
					Umsatz und EBIT bei TalkTalk liegen leicht unter den Vorjahreszahlen. In der Kundengewinnung mit 
					TalkTalk-­Produkten fokussiert sich mobilezone seit Sommer 2014 auf Mobilangebote. Der Umsatzanteil 
					der Mobilkunden bei TalkTalk beträgt 37,5 Prozent (Vorjahr: 17,5 Prozent). Im Bereich Mobiltelefonie 
					konnte die Kundenzahl auf 39‘000 (Vorjahr: 32 000) ausgebaut werden.
				</p>
				<br>
				<p>
					<strong>Dividendenantrag an die Generalversammlung</strong><br>
					Der Generalversammlung vom 6. April 2017 wird eine Dividende von CHF 0.60 pro Namenaktie beantragt 
					werden. Bei Annahme dieses Antrages wird die Dividende, unter Abzug der schweizerischen 
					Verrechnungssteuer von 35 Prozent, am 13. April 2017 ausbezahlt. Ab dem 11. April 2017 wird die 
					Aktie ex Dividende gehandelt. Auf Basis des Aktienkurses von CHF 14.55 (Vorjahr: CHF 14.35) am 
					Bilanzstichtag entspricht dies einer Dividendenrendite von 4,1 Prozent und einer Gesamtperformance 
					im Berichtsjahr von 5,2 Prozent.
				</p>
				<br>
				<p>
					<strong>Veränderung in der Konzernleitung</strong><br>
					Im November 2016 hat die Gesellschaft bekannt gegeben, dass die Konzernleitung von drei auf fünf 
					Mitglieder mit kompetenten, jüngeren Personen aus den eigenen Reihen erweitert wird. Ab dem 1. 
					Januar 2017 sind Murat Ayhan, Akin Erdem und Roger Wassmer neu in der Konzernleitung der 
					Gesellschaft. Werner Waldburger hat die Konzernleitung auf eigenen Wunsch verlassen und wird 
					mobilezone weiterhin im Bereich Business Steering Schweiz zur Verfügung stehen.
				</p>
				<p>
					Unser Unternehmen hat im Geschäftsjahr 2016 ein sehr gutes Ergebnis erzielt. Wir sind überzeugt, 
					auch im Jahr 2017 auf diesem erfolgreichen Weg weiterfahren zu können. Zusätzliche Omnichannel- und 
					Online-Aktivitäten werden diesen Weg unterstützen. Ein besonderer Dank gilt unseren Mitarbeitenden, 
					die es mit ihrem täglichen Einsatz ermöglicht haben, ein weiteres erfolgreiches Geschäftsjahr 
					abzuschliessen. Für Ihre Treue und das Vertrauen in unser Unternehmen möchten wir uns an dieser 
					Stelle herzlich bedanken.
				</p>
				<br>
				<p>Regensdorf, 7. März 2017</p>',
			'en' => '<p><strong>Dear Shareholders</strong></p><br>
				<p>
					<strong>mobilezone has increased its operating profit by 20 percent to CHF 48.5 million</strong><br>
					In fiscal year 2016 mobilezone achieved record sales of CHF 1,088 million, an increase of 27 percent 
					over the previous year’s figure. Based on a pro-forma consolidation in the previous year of the 
					company einsAmobile as of January 1, 2015, sales for fiscal year 2015 totaled CHF 997 million 
					compared to CHF 1,088 million in fiscal year 2016.
				</p>
				<p>
					This very positive development is reflected in the 20 percent increase in operating profit (EBIT) to 
					CHF 48.5 million (2015: CHF 40.4 million). Financial expenses rose by CHF 0.4 million to CHF 1.8 
					million. The company’s tax expense amounted to CHF 10.7 million (2015: CHF 8.6 million). 
					Consolidated profit was increased by 17 percent to CHF 36.1 million (2015: CHF 30.8 million). In 
					2015 the pro-forma profit amounted to CHF 32 million.
				</p>
				<p>
					Earnings per share were increased in the reporting year from CHF 0.96 to CHF 1.12. As of March 6, 
					2017, the mobilezone share price was CHF 14.55 compared to CHF 14.35 at the end of December 2015.
				</p>
				<p>
					As of December 31, 2016, mobilezone‘s balance sheet shows cash and cash equivalents totalling CHF 
					27.0 million (2015: CHF 14.3 million ). At the end of the year, net current assets amounted to CHF 
					27.2 million (2015: CHF 42.6 million ). In the mobilezone holding ag individual financial 
					statements, shareholders’ equity amounts to CHF 54.7 million (2015: CHF 58.5 million).
				</p>
				<p>
					The two business segments Trade and Service Providing have contributed to the positive result of 
					fiscal year 2016.
				</p>
				<br>
				<p>
					<strong>Segment Trade reports significant increase in sales and higher profitability</strong><br>
					The segment Trade includes activities in the company’s branch network extending throughout 
					Switzerland, in the online business, in the B2B sector, and since April 2015 also the activities of 
					the einsAmobile in Germany.
				</p>
				<p>
					The market environment in the retail business in Switzerland remains very demanding and challenging. 
					In the first six months of fiscal year 2016 it became clear that the business strategies of 
					mobilezone and Salt differ too widely to continue offering our customers attractive Salt products. 
					Therefore, mobilezone focuses on its partners Swisscom, Sunrise, UPC, and TalkTalk and does no 
					longer offer any Salt products since July 2016.
				</p>
				<p>
					A new shop concept with a completely new design was introduced in the mobilezone shops at Lucerne’s 
					central station as well as in Bern Waaghaus, Hinwil, Geneva Rue de Carouge, Spreitenbach, 
					Emmencenter, Zug Metalli, and Zurich Löwenstrasse. In November 2016 a new shop was opened at the 
					Zurich central station (Shopville). In the current year several other shops will be remodeled in 
					line with the new concept.
				</p>
				<p>
					Above all the online business but also the wholesale business in Germany showed record results both 
					in terms of sales as well as operating profit. In particular, in Germany more than 200,000 (2015: 
					150,000) mobile phone contracts were concluded online. With the launch of deinhandy.ch in May 2016, 
					mobilezone took an important step in Switzerland toward applying its successful experience in 
					Germany to the Swiss market.
				</p>
				<p>
					The segment Trade achieved an increase in sales of 29 percent, from CHF 785 million to CHF 1,012 
					million. The wholesale portion of this total amounts to CHF 618 million (2015: CHF 422 million). 
					EBIT rose from CHF 27.8 million to CHF 40.6 million, an increase of 46 percent. This positive 
					development is primarily due to the results in Germany.
				</p>
				<br>
				<p>
					<strong>Segment Service Providing reports increased profitability</strong><br>
					The segment Service Providing includes the company’s own TalkTalk mobile and fixed-line offers as 
					well as the service business in Switzerland and Austria.
				</p>
				<p>
					Sales in the segment Service Providing grew from CHF 73.6 million to CHF 75.5 million, an increase 
					of 2.6 percent. EBIT rose by 6.6 percent to CHF 8.1 million.
				</p>
				<p>
					The profitability of the repair business in Switzerland and Austria developed very positively in the 
					second half of fiscal year 2016 despite a decrease in the overall order volume by a total of 6 
					percent.
				</p>
				<p>
					TalkTalk’s sales and EBIT came in slightly below the previous year’s figures. Since summer 2014, 
					mobilezone has been focusing its customer acquisition efforts regarding TalkTalk products on mobile 
					offers. The share of mobile phone customers of TalkTalk’s total sales amounts to 37.5 percent (2015: 
					17.5 percent). In the area of mobile telephony the number of customers grew to 39,000 (2015: 32,000).
				</p>
				<br>
				<p>
					<strong>Dividend proposal to the General Meeting</strong><br>
					A dividend of CHF 0.60 will be proposed to the General Meeting on April 6, 2017. Based on the share 
					price at balance sheet date of CHF 14.55 (2015: CHF 14.35), this proposed dividend represents a 
					dividend yield of 4.1 percent and an overall performance in the reporting year of 5.2 percent. If 
					this proposal is approved, the ­dividend of CHF 0.60 per registered share, less 35 percent Swiss 
					withholding tax (anticipatory tax), will be paid out on April 13, 2017. Starting on April 11, 2017, 
					the shares will be traded ex-dividend. 
				</p>
				<br>
				<p>
					<strong>Change in Group management</strong><br>
					In November 2016 mobilezone Group announced that the Group management will be expanded from three to 
					five members by adding experienced, younger persons from within the company’s own ranks. Effective 
					January 1, 2017, Murat Ayhan, Akin Erdem, and Roger Wassmer are new members of the Group management. 
					Werner Waldburger has left the Group management at his own request and will continue to be available 
					to mobilezone in the area Business Steering Schweiz.
				</p>
				<p>
					In summary, mobilezone has achieved a very positive result in fiscal year 2016, and we are convinced 
					that the company will be able to continue on this positive course in 2017. Additional Omnichannel 
					and online efforts will further support this positive development. At this point we wish to thank 
					especially our employees; with their ­daily commitment they have made a successful fiscal year 
					possible. Finally, we want to take this opportunity to thank you sincerely for your loyalty and your 
					confidence in our company.
				</p>
				<br>
				<p>Regensdorf, March 7 2017</p>',
		],


		//Highlights
		'highlights-subtitle' => [
			'de' => 'Die Höhepunkte des Jahres',
			'en' => 'Highlights of our year',
		],
		'month-april' => [
			'de' => 'APRIL',
			'en' => 'APRIL',
		],
		'month-may' => [
			'de' => 'MAI',
			'en' => 'MAY',
		],
		'month-june' => [
			'de' => 'JUNI',
			'en' => 'JUNE',
		],
		'month-july' => [
			'de' => 'JULI',
			'en' => 'JULY',
		],
		'month-november' => [
			'de' => 'NOVEMBER',
			'en' => 'NOVEMBER',
		],
		'highlight1-title' => [
			'de' => 'Shoperöffnung von mobilezone im Bahnhof Luzern',
			'en' => 'Shop opening at Lucerne train station',
		],
		'highlight1-text' => [
			'de' => 'mobilezone optimiert seine Standorte weiter und eröffnet einen neuen Shop an bester Lage im Bahnhof 
				Luzern im komplett neuen Design.',
			'en' => 'mobilezone continues to optimize ist retail locations and opens a new shop in prime location at the 
				train station of Lucerne using a completely new design.',
		],
		'highlight2-title' => [
			'de' => 'mobilezone launched die Webseite DeinHandy.ch',
			'en' => 'mobilezone launches website DeinHandy.ch',
		],
		'highlight2-text' => [
			'de' => 'Die erfolgreiche Onlineplattform DeinHandy.de gibt es dank mobilezone auch in der Schweiz. Darauf 
				finden Kunden das richtige Handy mit dem für die passenden Tarif und können ihr gewünschtes Handy 
				inklusive Vertrag kaufen.',
			'en' => 'The successful German online plattform DeinHandy.de expands into Switzerland thanks to mobilezone. 
				It allows visitors to identify the best device for them and shows all applicable tariff options across 
				providers to be bought alongside with the smartphone.',
		],
		'highlight3-title' => [
			'de' => 'Breel Embolo wird Markenbotschafter von mobilezone',
			'en' => 'Breel Embolo becomes mobilezone brand ambassador',
		],
		'highlight3-text' => [
			'de' => 'mobilezone und der Fussballer Breel Embolo gehen gemeinsame Wege. Das Ausnahmetalent von Schalke 04 
				und der Schweizer Fussballnationalmannschaft wird zukünftig als Markenbotschafter von mobilezone zu 
				sehen sein.',
			'en' => 'mobilezone and Swiss football player Breel Embolo join forces. The professional striker of Schalke 
				04 and the Swiss national team will serve as brand ambassador for mobilezone in the future.',
		],
		'highlight4-title' => [
			'de' => 'mobilezone führt Apple Pay ein',
			'en' => 'mobilezone introduces Apple Pay',
		],
		'highlight4-text' => [
			'de' => 'mobilezone-Kunden können seit Juli in allen mobilezone-Shops alle Produkte und Dienstleistungen 
				einfach, sicher und vertraulich mit ihrem iPhone bezahlen. Apple Pay vereinfacht den Zahlungsprozess 
				massgeblich.',
			'en' => 'Since July, mobilezone’s customers can pay for all products and services in any mobilezone-shops 
				simply, ­securely, and confidentially with their iPhone. Apple Pay simplifies the payment process 
				significantly. ',
		],
		'highlight5-title' => [
			'de' => 'mobilezone erweitert die Konzernleitung auf fünf Mitglieder',
			'en' => 'mobilezone expands ist group management team to 5 members',
		],
		'highlight5-text' => [
			'de' => 'mobilezone erweitert die Konzernleitung per 1. Januar 2017 von drei auf fünf Mitglieder und trägt 
				damit der neuen Konzernstruktur Rechnung. Die Konzernleitung wird mit kompetenten, jüngeren Personen aus 
				den eigenen Reihen erweitert.',
			'en' => 'mobilezone expands its Group management at January 1, 2017 from three to five members, a step that 
				takes into account the new Group structure. The company will add competent younger persons from within 
				its own ranks to the Group management. ',
		],
		'highlight6-title' => [
			'de' => 'Shoperöffnung im ShopVille-Zürich Hauptbahnhof',
			'en' => 'Shop opening in ShopVille - Zurich main station',
		],
		'highlight6-text' => [
			'de' => 'mobilezone ist nun an drei der fünf Schweizer Bahnhöfe mit den höchsten Personenfrequenzen präsent. 
				Neben Zürich sind das die Hauptbahnhöfe in Basel und Luzern.',
			'en' => 'mobilezone is now present at three of the five Swiss train stations with highest passenger volume. 
				Apart from Zurich main station these are the main stations in Basel and Lucerne.',
		],


		// Corporate Governance
		'governance-text' => [
			'de' => 'Die Grundsätze und Bestimmungen der Corporate Governance sind in den Statuten und im 
				Organisationsreglement der mobilezone holding festgehalten. Die veröffentlichten Informationen 
				entsprechen den Corporate-Governance-Richtlinie der SIX Swiss Exchange und den gültigen 
				Offenlegungsbestimmungen des Schweizerischen Obligationenrechts. Stichtag ist der 31. Dezember 2016, 
				sofern nicht anders vermerkt.',
			'en' => 'The principles and provisions of Corporate Governance are fixated via statutes and the 
				organizational rules of mobilezone holding ag. The published information are in accordance with the SIX 
				Corporate Governance principles provided by the Swiss Exchange and the legal framework applicable in 
				Switzerland. Documents are as of December 31, 2016 unless explicitly stated otherwise.',
		],


		// Compensation Report
		'compensation-text' => [
			'de' => 'Der Vergütungsbericht legt in Übereinstimmung mit den gesetzlichen Bestimmungen und den Statuten 
				die Vergütungspolitik für den Verwaltungsrat sowie die Konzernleitung der mobilezone Gruppe dar.',
			'en' => 'The compensation report outlines the compensation guidelines and policy of advisory board and 
				executive management of the mobilezone group in accordance with the applicable legal regulations.',
		],


		// Financial Report
		'financial-text' => [
			'de' => 'Der Finanzbericht enthält die ausführliche Konzernrechnung der mobilezone Gruppe sowie die 
				Jahresrechnung der mobilezone holding ag.',
			'en' => 'The financial report comprises full audited financial records of the mobilezone group as well as 
				financial statements of the mobilezone holding ag.',
		],
	];
}
