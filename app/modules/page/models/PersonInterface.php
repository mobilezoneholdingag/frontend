<?php

namespace app\modules\page\models;

interface PersonInterface
{
	const DIRECTORS = [
		[
			'name' => 'Urs T. Fischer',
			'subtitle' => 'Person_Sub_Fischer',
			'text' => 'Person_Text_Fischer',
			'img' => 'person_fischer.jpg',
		],
		[
			'name' => 'Andreas M. Blaser',
			'subtitle' => 'Person_Sub_Blaser',
			'text' => 'Person_Text_Blaser',
			'img' => 'person_blaser.jpg',
		],
        [
            'name' => 'Gabriela Theus',
            'subtitle' => 'Person_Sub_Theus',
            'text' => 'Person_Text_Theus',
            'img' => 'person_theus.png'
        ],
        [
            'name' => 'Christian Petit',
            'subtitle' => 'Person_Sub_Petit',
            'text' => 'Person_Text_Petit',
            'img' => 'person_petit.png'
        ]
	];

	const DIRECTORS_EN = [
		[
			'name' => 'Urs T. Fischer',
			'subtitle' => 'President',
			'text' => 'Urs T. Fischer (1954, Swiss national) has been chairman of the Board of Directors of the mobilezone Group 
				since April 2009. After graduating with a diploma in engineering from the ETH Zurich, he held 
				various management positions at IBM Switzerland and Digital Equipment Corporation, Switzerland. He 
				was the CEO of Sunrise Communication AG in Zurich and was CEO and member of the Board of Directors 
				of Ascom Group, Bern. From 2004 to 2007 Urs T. Fischer was managing director of Hewlett-Packard 
				(Switzerland) GmbH in Dübendorf, and since 2009 he has been CEO of the international IT-systems 
				company ACP in Vienna. He is on the Board of Directors of various corporations that are not listed 
				on the stock exchange.',
			'img' => 'person_fischer.jpg',
		],
		[
			'name' => 'Andreas M. Blaser',
			'subtitle' => 'Member',
			'text' => 'Andreas M. Blaser (1962, Swiss national) has been member of the Board of Directors of the mobilezone Group since 2016. He is an entrepreneur and management consultant. After studying 
				business administration and IT, he held various leadership positions in national and international IT 
				companies. He was a cofounder of TVD AG, and from 1999 to 2003 he also served on its board of directors. 
				As a member of corporate management and project director, he headed the project business of Computer 
				Sciences Corporation (CSC) in Switzerland from 2003 to 2007. He is cofounder of the management 
				consulting company Blaser Meewes & Partner AG and has been a member of its board of directors since 
				2012. In addition, Andreas Blaser serves as president of the foundation Sternwarte Uecht for 
				astronomical research and education.',
			'img' => 'person_blaser.jpg',
		],
        [
            'name' => 'Gabriela Theus',
            'subtitle' => 'Member',
            'text' => 'Gabriela Theus (1973, Swiss national) has been member of the Board of Directors of the mobilezone Group since April 2018. After obtaining a degree in Economics at HSG St. Gallen, she worked from 1999 on as a consultant at Knorr Capital Partner and then at Ernst & Young and Sal. Oppenheim in the real estate sector, before moving to Zug Estates Holding AG as CFO. She has been Director of Fund Management at the company in Zug since September 2017  ',
            'img' => 'person_theus.png',
        ],
        [
            'name' => 'Christian Petit',
            'subtitle' => 'Member',
            'text' => 'Christian Petit (1963, French national) has been member of the Board of Directors of the mobilezone Group since 2018. Following business training at the ESSEC in Cergy-Pontoise (France) he worked in various positions at Paribas Group in France, Belgium and Germany. From 1993 to 1999, he held various management positions (up to Director General) at debitel France (today La Poste Mobile), a telecommunications company with Mercedes-Benz and Metro as shareholders, in Paris. Most recently he worked for Swisscom AG for 17 years. Between 2007 and 2017, as a member of the Group Board of Swisscom Group, he was responsible for the private clients business (2007-2013) and then for key accounts (2013-2017). ',
            'img' => 'person_petit.png',
        ]
	];

	const GROUP_MANAGEMENT = [
		[
			'name' => 'Markus Bernhard',
			'subtitle' => 'CEO',
			'text' => 'Person_Text_Bernhard',
			'img' => 'person_bernhard.jpg',
		],
		[
			'name' => 'Andreas Fecker',
			'subtitle' => 'CFO',
			'text' => 'Person_Text_Fecker',
			'img' => 'person_fecker.jpg',
		],
		[
			'name' => 'Murat Ayhan',
			'subtitle' => 'Subtitle_Text_Ayhan',
			'text' => 'Person_Text_Ayhan',
			'img' => 'person_ayhan.jpg',
		],
		[
			'name' => 'Akin Erdem',
			'subtitle' => 'Subtitle_Text_Erdem',
			'text' => 'Person_Text_Erdem',
			'img' => 'person_erdem.jpg',
		],
		[
			'name' => 'Roger Wassmer',
			'subtitle' => 'Subtitle_Text_Wassmer',
			'text' => 'Person_Text_Wassmer',
			'img' => 'person_wassmer.jpg',
		],
	];

	const GROUP_MANAGEMENT_EN = [
		[
			'name' => 'Markus Bernhard',
			'subtitle' => 'CEO',
			'text' => 'Markus Bernhard (1964, Swiss) has led mobilezone Group as its CEO since 2014. From 2007 to 2013 
				he was mobilezone Group’s CFO. Following his graduation from the University of St. Gallen (HSG St. 
				Gallen) with a degree in economics, Markus Bernhard received his diploma as certified public accountant. 
				From 1991 to 1997 he worked as auditor at Revisuisse Price Waterhouse AG in Zurich. He was CFO of Cope 
				Inc. in Rotkreuz until 2000 and subsequently was CFO of Mount10 Holding AG, also in Rotkreuz. Markus 
				Bernhard is a member of the board of directors of Novavisions AG in Rotkreuz.',
			'img' => 'person_bernhard.jpg',
		],
		[
			'name' => 'Andreas Fecker',
			'subtitle' => 'CFO',
			'text' => 'Andreas Fecker (1972, Swiss) joined mobilezone Group’s management as CFO in 2014. From 2008 to 
				2013 he was Head of Finance & Controlling of mobilezone Group. After graduating with a Swiss federal 
				certificate as financial expert in accounting and finance, Andreas Fecker earned his Master of Advanced 
				Studies degree in Controlling in 2013 at the Lucerne University of Applied Sciences and Arts.',
			'img' => 'person_fecker.jpg',
		],
		[
			'name' => 'Murat Ayhan',
			'subtitle' => 'Managing Director Germany',
			'text' => 'Murat Ayhan (1977, German), responsible for the area of e-Commerce and Operator Relations as of 
				01/01/2017 and founder and Managing Director of einsAmobile GmbH. He completed his studies in Darmstadt 
				and gained a degree in Electrical Engineering. During his studies, he opened three mobile phone shops in 
				1999, which he then sold in 2001. From 2002 to 2005, he worked as a managing director for a 
				telecommunications company in Offenbach. In 2005, he founded einsAmobile GmbH in Obertshausen which has 
				been 100 percent owned by the mobilezone Group since 2015.',
			'img' => 'person_ayhan.jpg',
		],
		[
			'name' => 'Akin Erdem',
			'subtitle' => 'Managing Director Germany',
			'text' => 'Akin Erdem (1974, German), responsible for the areas of Purchase and Wholesale across the Group 
				as of 01/01/2017 as well as Managing Director of einsAmobile GmbH. After completing his studies in 1994, 
				he qualified with a Bachelors in Business Administration. In 1998, he founded his own company in the 
				telecommunications sector, which he expanded into a leading wholesaler in Germany. In 2010, he switched 
				to become CEO and 50 percent co-partner in einsAmobile GmbH, which has been 100 percent owned by the 
				mobilezone Group since 2015.',
			'img' => 'person_erdem.jpg',
		],
		[
			'name' => 'Roger Wassmer',
			'subtitle' => 'COO Germany & Austria',
			'text' => 'Roger Wassmer (1973, Swiss), COO Switzerland and Austria as of 01/01/2017. Has been heading up 
				mobiletouch Switzerland as Managing Director since 2014 as well as mobiletouch Austria since 2015. He is 
				also responsible for the services of the mobilezone Group. From 2012 to 2014, Roger Wassmer was Country 
				Manager for zanox Switzerland AG. He previously worked at Publimedia and EurotaxGlass as Sales and 
				Marketing Manager as well as CEO of OmniMedia AG and Managing Director of Car4you. After further 
				training as a Swiss government-recognised graduate in marketing management, he obtained an Executive MBA 
				FH.',
			'img' => 'person_wassmer.jpg',
		],
	];

	const MANAGEMENT_TEAM = [
		[
			'name' => 'Thomas Gülünay',
			'subtitle' => 'Person_Sub_Gülünay',
			'text' => 'Person_Text_Gülünay',
			'img' => 'person_gueluenay.jpg',
		],
		[
			'name' => 'Fritz Hauser',
			'subtitle' => 'Person_Sub_Hauser',
			'text' => 'Person_Text_Hauser',
			'img' => 'person_hauser.jpg',
		],
		[
			'name' => 'Nicola Lippolis',
			'subtitle' => 'Person_Sub_Lippolis',
			'text' => 'Person_Text_Lippolis',
			'img' => 'person_lippolis.jpg',
		],
		[
			'name' => 'Philipp Müller',
			'subtitle' => 'Person_Sub_Müller',
			'text' => 'Person_Text_Müller',
			'img' => 'person_mueller.jpg',
		],
		[
			'name' => 'Daniel Ringger',
			'subtitle' => 'Subtitle_Text_Ringger',
			'text' => 'Person_Text_Ringger',
			'img' => 'person_ringger.jpg',
		],
		[
			'name' => 'Karl Steinke',
			'subtitle' => 'Managing Director TalkTalk',
			'text' => 'Person_Text_Steinke',
			'img' => 'person_steinke.jpg',
		],
		[
			'name' => 'Gregor Vogt',
			'subtitle' => 'Head of Marketing & Communication',
			'text' => 'Person_Text_Vogt',
			'img' => 'person_vogt.jpg',
		],
		[
			'name' => 'Werner Waldburger',
			'subtitle' => 'Subtitle_Text_Waldburger',
			'text' => 'Person_Text_Waldburger',
			'img' => 'person_waldburger.jpg',
		],
	];

	const MANAGEMENT_TEAM_EN = [
		[
			'name' => 'Thomas Gülünay',
			'subtitle' => 'Head of BU Business customers',
			'text' => 'Thomas Gülünay (a Swiss national, born in 1979) has been CEO of mobilezone business ag since 
				2011. In the period from 2008 to 2011, he was Head of B2B Administration at mobilezone business ag. In 
				2003 he founded his own company, tojaco Trading GmbH, which he headed as CEO and co-owner. After earning 
				a Swiss Federal Diploma in Sales Coordination, he obtained an Executive Diploma in SME Management from 
				the University of St. Gallen in 2012.',
			'img' => 'person_gueluenay.jpg',
		],
		[
			'name' => 'Fritz Hauser',
			'subtitle' => 'Head of IT & Logistics',
			'text' => 'Fritz Hauser (a Swiss national, born in 1971) has been Head of IT & Logistics at mobilezone since 
				2007. In 1999, the year when mobilezone ag was established, he was responsible for Product Management 
				and in 2000 he took over the development of the IT division. Following his apprenticeship as an 
				electronics technician, he continued his education in technical business administration and information 
				technology, gaining a Swiss Federal Diploma. From 1991 to 1997 he was Product Manager in charge of the 
				trading division of a Swiss IT distributor. From 1997 to 1999 he worked as a project manager for mobile 
				solutions ag in Dübendorf. He is currently completing the Executive MBA programme at the University of 
				St. Gallen.',
			'img' => 'person_hauser.jpg',
		],
		[
			'name' => 'Nicola Lippolis',
			'subtitle' => 'Head of Sales',
			'text' => 'Nicola Lippolis (an Italian national, born in 1976) has been Head of Sales at mobilezone since 
				2014. Following his training as a retail specialist, he worked in various management positions at Fust 
				AG and Interdiscount. From 2009 to 2014 he was Head of Regional Sales at Interdiscount, responsible for 
				all the branches in Central Switzerland and Ticino.',
			'img' => 'person_lippolis.jpg'
		],
		[
			'name' => 'Philipp Müller',
			'subtitle' => 'Head of Controlling',
			'text' => 'Philipp Müller (a Swiss national, born in 1978) has been Head of Business Controlling for the 
				mobilezone Group since 2012. After studying Economics at the University of St. Gallen, he obtained his 
				first professional experience as a trainee and strategy controller at Swisscom between 2004 and 2008. He 
				then spent just under 2 years as a Senior Controller at Sunrise, before working from 2010 to 2012 as a 
				Financial Controller in the digital sector at Adconion Media Group in Baar and Munich. During this 
				period, he completed his MAS Controlling at the IFZ (Financial Services Institute) in Zug.',
			'img' => 'person_mueller.jpg',
		],
		[
			'name' => 'Daniel Ringger',
			'subtitle' => 'Head of Operator Relations Switzerland',
			'text' => 'Daniel Ringger (1983, Swiss), Head of Operator Relations Switzerland as of 01/01/2017, has been 
				working for the mobilezone Group since 2004. He was Product Manager from 2007 – 2013 in the Purchasing 
				and Operator Relations department and since 2014 Head of Operator Relations. In 2013, he completed his 
				in-service training to gain a Swiss state-recognised degree-level qualification as a business economist 
				at the state college of higher education in Zurich. He is currently completing studies to gain an 
				Executive MBA in General Management at Zurich University of Applied Sciences.',
			'img' => 'person_ringger.jpg',
		],
		[
			'name' => 'Karl Steinke',
			'subtitle' => 'Managing Director TalkTalk',
			'text' => 'Karl Steinke (Swiss / US dual citizenship) has headed TalkTalk AG since it was established in 
				1999. This firm has been part of the mobilezone Group since 2013. After obtaining a Bachelor\'s degree 
				at the University of Wisconsin (Madison, USA), he completed his MBA at McGill University in Montreal, 
				Canada. He then attended further education courses at INSEAD in France and at IMD in Lausanne. Before 
				taking his post at mobilezone, he worked for five years for Lufthansa in the USA in various positions. 
				After that Karl Steinke was a co-founder of NewTel Communications GmbH in Düsseldorf, where worked as 
				Head of Sales.',
			'img' => 'person_steinke.jpg',
		],
		[
			'name' => 'Gregor Vogt',
			'subtitle' => 'Head of Marketing & Communication',
			'text' => 'Gregor Vogt (a Swiss national, born in 1979) has been leading the Marketing department at 
				mobilezone as Head of Marketing & Communication since 2010. After getting his degree in Communication at 
				the University of Fribourg, he worked as a brand consultant at Hotz & Hotz from 2005 to 2008. He then 
				worked as an advisor and deputy CEO at Screen Concept before moving to the mobilezone Group. Gregor Vogt 
				obtained his Executive MBA from the University of St. Gallen in 2014.',
			'img' => 'person_vogt.jpg',
		],
		[
			'name' => 'Werner Waldburger',
			'subtitle' => 'Head of Business Steering Switzerland',
			'text' => 'Werner Waldburger (1963, Swiss) joined mobilezone Group’s management in 1999; from October 2012 
				until the end of 2016, he was the Group’s CPO. Werner Waldburger left the Group management as of 
				January 1, 2017, and now heads the area Business Steering Schweiz. Following his apprenticeship as radio 
				and television electrician, he graduated from the commercial college and passed the advanced 
				examinations in retailing. He held various positions in sales, both in the office and in the field, 
				before working as Head of Consumer Electronics Purchasing at Dipl. Ing. Fust AG from 1989 to 1999. In 
				2009 Werner Waldburger received an advanced education diploma in marketing from the university in 
				St. Gallen.',
			'img' => 'person_waldburger.jpg',
		],
	];
}
