<?php

namespace app\modules\page\models;

interface ReportInterface
{
    const ALL = [
        [
            'title' => 'Page_Reports_Half_Year_18',
            'title_en' => 'Half-year Report 2018',
            'presentation_de' => 'http://ad67fd46e0f74a24d6c6-813fb3d75440f466a8b71726e0d62b63.r67.cf3.rackcdn.com/downloads/Pr%C3%A4sentation%20der%20Halbjahreszahlen%202018.pdf',
            'downloads_de' => '2018_mobilezone_Halbjahresbericht_DE.pdf',
            'downloads_en' => '2018_mobilezone_Half-year-report_EN.pdf',
        ],
        [
            'title' => 'Page_Reports_Annual_17',
            'title_en' => 'Annual Report 2017',
            'downloads_de' => 'mobilezone_GB2017_DE.pdf',
            'downloads_en' => 'mobilezone_GB2017_EN.pdf',
        ],
        [
            'title' => 'Page_Reports_Half_Year_17',
            'title_en' => 'Half-year Report 2017',
            'downloads_de' => '2017_mobilezone_Halbjahresbericht_DE.pdf',
            'downloads_en' => '2017_mobilezone_Half-year-report_EN.pdf',
            'publishing_date' => '20170818065500',
        ],
        [
            'title' => 'Page_Tree_Annual_Reports_16',
            'title_en' => 'Annual Report 2016',
            'downloads_de' => 'https://www.mobilezone.ch/ueber-uns/investoren/geschaeftsberichte-16/start',
            'downloads_en' => 'https://www.mobilezone.ch/about-us/investors/annual-reports-16/start',
            'no_pdf' => true,
        ],
        [
            'title' => 'Page_Reports_Half_Year_16',
            'title_en' => 'Half-year Report 2016',
            'downloads_de' => '2016_mobilezone_Halbjahresbericht_DE.pdf',
            'downloads_en' => '2016_mobilezone_Half-year-report_EN.pdf',
        ],
        [
            'title' => 'Page_Reports_Annual_15',
            'title_en' => 'Annual Report 2015',
            'downloads_de' => '2015_mobilezone_Geschaeftsbericht_DE.pdf',
            'downloads_en' => '2015_mobilezone_Geschaeftsbericht_EN.pdf',
        ],
        [
            'title' => 'Page_Reports_Half_Year_15',
            'title_en' => 'Half-year Report 2015',
            'downloads_de' => '2015_mobilezone_Halbjahresbericht_DE.pdf',
            'downloads_en' => '2015_mobilezone_Half-year-report_EN.pdf',
        ],
        [
            'title' => 'Page_Reports_Annual_14',
            'title_en' => 'Annual Report 2014',
            'downloads_de' => '2014_mobilezone_Geschaeftsbericht_DE.pdf',
            'downloads_en' => '2014_mobilezone_Geschaeftsbericht_EN.pdf',
        ],
        [
            'title' => 'Page_Reports_Half_Year_14',
            'title_en' => 'Half-year Report 2014',
            'downloads_de' => '2014_mobilezone_Halbjahresbericht_DE.pdf',
            'downloads_en' => '2014_mobilezone_Half-year-report_EN.pdf',
        ],
        [
            'title' => 'Page_Reports_Annual_13',
            'title_en' => 'Annual Report 2013',
            'downloads_de' => '2013_mobilezone_Geschaeftsbericht_DE.pdf',
            'downloads_en' => '2013_mobilezone_Geschaeftsbericht_EN.pdf',
        ],
        [
            'title' => 'Page_Reports_Half_Year_13',
            'title_en' => 'Half-year Report 2013',
            'downloads_de' => '2013_mobilezone_Halbjahresbericht_DE.pdf',
            'downloads_en' => '2013_mobilezone_Half-year-report_EN.pdf',
        ],
        [
            'title' => 'Page_Reports_Annual_12',
            'title_en' => 'Annual Report 2012',
            'downloads_de' => '2012_mobilezone_Geschaeftsbericht_DE.pdf',
            'downloads_en' => '2012_mobilezone_Geschaeftsbericht_EN.pdf',
        ],
        [
            'title' => 'Page_Reports_Half_Year_12',
            'title_en' => 'Half-year Report 2012',
            'downloads_de' => '2012_mobilezone_Halbjahresbericht_DE.pdf',
            'downloads_en' => '2012_mobilezone_Half-year-report_EN.pdf',
        ],
        [
            'title' => 'Page_Reports_Annual_11',
            'title_en' => 'Annual Report 2011',
            'downloads_de' => '2011_mobilezone_Geschaeftsbericht_DE.pdf',
            'downloads_en' => '2011_mobilezone_Geschaeftsbericht_EN.pdf',
        ],
        [
            'title' => 'Page_Reports_Half_Year_11',
            'title_en' => 'Half-year Report 2011',
            'downloads_de' => '2011_mobilezone_Halbjahresbericht_DE.pdf',
            'downloads_en' => '2011_mobilezone_Half-year-report_EN.pdf',
        ],
        [
            'title' => 'Page_Reports_Annual_10',
            'title_en' => 'Annual Report 2010',
            'downloads_de' => '2010_mobilezone_Geschaeftsbericht_DE.pdf',
            'downloads_en' => '2010_mobilezone_Geschaeftsbericht_EN.pdf',
        ],
        [
            'title' => 'Page_Reports_Half_Year_10',
            'title_en' => 'Half-year Report 2010',
            'downloads_de' => '2010_mobilezone_Halbjahresbericht_DE.pdf',
            'downloads_en' => '2010_mobilezone_Half-year-report_EN.pdf',
        ],
        [
            'title' => 'Page_Reports_Annual_09',
            'title_en' => 'Annual Report 2009',
            'downloads_de' => '2009_mobilezone_Geschaeftsbericht_DE.pdf',
            'downloads_en' => '2009_mobilezone_Geschaeftsbericht_EN.pdf',
        ],
        [
            'title' => 'Page_Reports_Half_Year_09',
            'title_en' => 'Half-year Report 2009',
            'downloads_de' => '2009_mobilezone_Halbjahresbericht_DE.pdf',
            'downloads_en' => '2009_mobilezone_Half-year-report_EN.pdf',
        ],
        [
            'title' => 'Page_Reports_Annual_08',
            'title_en' => 'Annual Report 2008',
            'downloads_de' => '2008_mobilezone_Geschaeftsbericht_DE.pdf',
            'downloads_en' => '2008_mobilezone_Geschaeftsbericht_EN.pdf',
        ],
        [
            'title' => 'Page_Reports_Half_Year_08',
            'title_en' => 'Half-year Report 2008',
            'downloads_de' => '2008_mobilezone_Halbjahresbericht_DE.pdf',
            'downloads_en' => '2008_mobilezone_Half-year-report_EN.pdf',
        ],
        [
            'title' => 'Page_Reports_Annual_07',
            'title_en' => 'Annual Report 2007',
            'downloads_de' => '2007_mobilezone_Geschaeftsbericht_DE.pdf',
            'downloads_en' => '2007_mobilezone_Geschaeftsbericht_EN.pdf',
        ],
        [
            'title' => 'Page_Reports_Half_Year_07',
            'title_en' => 'Half-year Report 2007',
            'downloads_de' => '2007_mobilezone_Halbjahresbericht_DE.pdf',
            'downloads_en' => '2007_mobilezone_Half-year-report_EN.pdf',
        ],
        [
            'title' => 'Page_Reports_Annual_06',
            'title_en' => 'Annual Report 2006',
            'downloads_de' => '2006_mobilezone_Geschaeftsbericht_DE.pdf',
            'downloads_en' => '2006_mobilezone_Geschaeftsbericht_EN.pdf',
        ],
        [
            'title' => 'Page_Reports_Half_Year_06',
            'title_en' => 'Half-year Report 2006',
            'downloads_de' => '2006_mobilezone_Halbjahresbericht_DE.pdf',
            'downloads_en' => '2006_mobilezone_Half-year-report_EN.pdf',
        ],
        [
            'title' => 'Page_Reports_Annual_05',
            'title_en' => 'Annual Report 2005',
            'downloads_de' => '2005_mobilezone_Geschaeftsbericht_DE.pdf',
            'downloads_en' => '2005_mobilezone_Geschaeftsbericht_EN.pdf',
        ],
        [
            'title' => 'Page_Reports_Half_Year_05',
            'title_en' => 'Half-year Report 2005',
            'downloads_de' => '2005_mobilezone_Halbjahresbericht_DE.pdf',
            'downloads_en' => '2005_mobilezone_Half-year-report_EN.pdf',
        ],
        [
            'title' => 'Page_Reports_Annual_04',
            'title_en' => 'Annual Report 2004',
            'downloads_de' => '2004_mobilezone_Geschaeftsbericht_DE.pdf',
            'downloads_en' => '2004_mobilezone_Geschaeftsbericht_EN.pdf',
        ],
        [
            'title' => 'Page_Reports_Half_Year_04',
            'title_en' => 'Half-year Report 2004',
            'downloads_de' => '2004_mobilezone_Halbjahresbericht_DE.pdf',
            'downloads_en' => '2004_mobilezone_Half-year-report_EN.pdf',
        ],
        [
            'title' => 'Page_Reports_Annual_03',
            'title_en' => 'Annual Report 2003',
            'downloads_de' => '2003_mobilezone_Geschaeftsbericht_DE.pdf',
            'downloads_en' => '2003_mobilezone_Geschaeftsbericht_EN.pdf',
        ],
        [
            'title' => 'Page_Reports_Half_Year_03',
            'title_en' => 'Half-year Report 2003',
            'downloads_de' => '2003_mobilezone_Halbjahresbericht_DE.pdf',
            'downloads_en' => '2003_mobilezone_Half-year-report_EN.pdf',
        ],
        [
            'title' => 'Page_Reports_Annual_02',
            'title_en' => 'Annual Report 2002',
            'downloads_de' => '2002_mobilezone_Geschaeftsbericht_DE.pdf',
            'downloads_en' => '2002_mobilezone_Geschaeftsbericht_EN.pdf',
        ],
        [
            'title' => 'Page_Reports_Half_Year_02',
            'title_en' => 'Half-year Report 2002',
            'downloads_de' => '2002_mobilezone_Halbjahresbericht_DE.pdf',
            'downloads_en' => '2002_mobilezone_Half-year-report_EN.pdf',
        ],
        [
            'title' => 'Page_Reports_Annual_01',
            'title_en' => 'Annual Report 2001',
            'downloads_de' => '2001_mobilezone_Geschaeftsbericht_DE.pdf',
            'downloads_en' => '2001_mobilezone_Geschaeftsbericht_EN.pdf',
        ],
    ];
}
