<?php
namespace app\modules\page\models;

interface EventInterface
{
	const DE = [
		[
            'title' => 'Veröffentlichung Geschäftsbericht 2017',
			'date' => '02.03.2018',
			'download_title_one' => '',
			'download_link_one' => '',
			'download_title_two' => '',
			'download_link_two' => '',
		],
		[
            'title' => 'Medienkonferenz Geschäftsjahr 2017',
			'date' => '02.03.2018',
			'download_title_one' => '',
			'download_link_one' => '',
			'download_title_two' => '',
			'download_link_two' => '',
		],
		[
			'title' => 'Generalversammlung 2018',
			'date' => '05.04.2018',
			'download_title_one' => '',
			'download_link_one' => '',
			'download_title_two' => '',
			'download_link_two' => '',
		],
	];

	const FR = [
		[
            'title' => 'Publication de rapport de gestion 2017',
			'date' => '02.03.2018',
			'download_title_one' => '',
			'download_link_one' => '',
			'download_title_two' => '',
			'download_link_two' => '',
		],
		[
            'title' => 'Conférence de presse 2017',
			'date' => '02.03.2018',
			'download_title_one' => '',
			'download_link_one' => '',
			'download_title_two' => '',
			'download_link_two' => '',
		],
		[
			'title' => 'Assemblée générale 2018',
			'date' => '05.04.2018',
			'download_title_one' => '',
			'download_link_one' => '',
			'download_title_two' => '',
			'download_link_two' => '',
		],
	];

	const IT = [
		[
            'title' => 'Pubblicazione del rapporto di gestione 2017',
			'date' => '02.03.2018',
			'download_title_one' => '',
			'download_link_one' => '',
			'download_title_two' => '',
			'download_link_two' => '',
		],
		[
            'title' => 'Conferenza stampa 2017',
			'date' => '02.03.2018',
			'download_title_one' => '',
			'download_link_one' => '',
			'download_title_two' => '',
			'download_link_two' => '',
		],
		[
			'title' => 'Assemblea generale 2018',
			'date' => '05.04.2018',
			'download_title_one' => '',
			'download_link_one' => '',
			'download_title_two' => '',
			'download_link_two' => '',
		],
	];

	const EN = [
		[
            'title' => 'Publication Annual Report 2017',
			'date' => '02.03.2018',
			'download_title_one' => '',
			'download_link_one' => '',
			'download_title_two' => '',
			'download_link_two' => '',
		],
		[
            'title' => 'Media conference 2017',
			'date' => '02.03.2018',
			'download_title_one' => '',
			'download_link_one' => '',
			'download_title_two' => '',
			'download_link_two' => '',
		],
		[
			'title' => 'General Meeting 2018',
			'date' => '05.04.2018',
			'download_title_one' => '',
			'download_link_one' => '',
			'download_title_two' => '',
			'download_link_two' => '',
		],
	];
}
