<?php

namespace app\modules\page\controllers;

use app\models\ContractConfigurator;
use app\modules\BaseModuleController;
use app\services\SessionInterface;
use Yii;
use yii\base\InlineAction;

class TvController extends BaseModuleController
{
	protected $session;

	public function __construct(
		$id,
		$module,
		SessionInterface $session,
		$config = []
	) {
		$this->session = $session;

		parent::__construct($id, $module, $config);
	}

	public function beforeAction($action)
	{
		// necessary hack to allow sending POST data from forms
		if ($action instanceof InlineAction) {
			// JSON actions w/o csrf tokens
			switch ($action->actionMethod) {
				case 'actionSendContractConfigurator':
					Yii::$app->request->enableCsrfValidation = false;
					break;
			}
		}

		return parent::beforeAction($action);
	}

	public function actionTv()
	{
		return $this->renderFile('@page/tv.twig');
	}

	public function actionTvOld()
	{
		return $this->renderFile('@page/tv/tv-old.twig');
	}

	public function actionContractConfigurator()
	{
		$model = new ContractConfigurator();

		if (Yii::$app->request->isPost) {
			if ($model->load(Yii::$app->request->post())) {
				if ($model->validate()) {

					$response = $model->sendEmail();

					if ($response == 200) {
						$this->session->addFlash('success', Yii::t('app', 'Form_Success_Message'));
						return $this->renderFile('@page/tv/cc-success.twig');
					} else {
						$this->session->addFlash('error', Yii::t('app', 'Form_Error_Message'));
					}
				}
			}
		}

		return $this->renderFile(
			'@page/tv/contract-configurator.twig',
			[
				'model' => $model,
			]
		);
	}
}
