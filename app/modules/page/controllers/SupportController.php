<?php

namespace app\modules\page\controllers;

use app\modules\BaseModuleController;

class SupportController extends BaseModuleController
{
	public function actionTerms()
	{
		return $this->renderFile('@page/support/terms.twig');
	}

	public function actionDataProtection()
	{
		return $this->renderFile('@page/support/data-protection.twig');
	}

	public function actionServiceSupport()
	{
		return $this->renderFile('@page/support/service-support.twig');
	}

	public function actionRepair()
	{
		return $this->renderFile('@page/support/repair.twig');
	}

	public function actionServiceGarantie()
	{
		return $this->renderFile('@page/support/service-garantie.twig');
	}

	public function actionDeviceInsurance()
	{
		return $this->renderFile('@page/support/device-insurance.twig');
	}

	public function actionShipment()
	{
		return $this->renderFile('@page/support/shipment.twig');
	}

	public function actionImprint()
	{
		return $this->renderFile('@page/support/imprint.twig');
	}
}
