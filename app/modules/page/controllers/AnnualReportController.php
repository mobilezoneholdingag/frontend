<?php

namespace app\modules\page\controllers;

use app\modules\BaseModuleController;

class AnnualReportController extends BaseModuleController
{
	public function annualReportDe($file)
	{
		$lang = \Yii::$app->language;
		$langKey = $lang == 'de-CH' ? 'de' : 'en';

		$params = [
			'lang_key' => $langKey,
			'file' => $file,
		];

		return $this->renderFile('@page/annual-reports/' . $file . '.twig', $params);
	}

	public function annualReportEn($file)
	{
		$params = [
			'lang_key' => 'en',
			'file' => $file,
		];

		return $this->renderFile('@page/annual-reports/' . $file . '.twig', $params);
	}

	public function actionStartDe()
	{
		return $this->annualReportDe('start');
	}

	public function actionStartEn()
	{
		return $this->annualReportEn('start');
	}

	public function actionKeyFiguresDe()
	{
		return $this->annualReportDe('key-figures');
	}

	public function actionKeyFiguresEn()
	{
		return $this->annualReportEn('key-figures');
	}

	public function actionPortraitDe()
	{
		return $this->annualReportDe('portrait');
	}

	public function actionPortraitEn()
	{
		return $this->annualReportEn('portrait');
	}

	public function actionLetterToShareholdersDe()
	{
		return $this->annualReportDe('letter-to-shareholders');
	}

	public function actionLetterToShareholdersEn()
	{
		return $this->annualReportEn('letter-to-shareholders');
	}

	public function actionHighlightsDe()
	{
		return $this->annualReportDe('highlights');
	}

	public function actionHighlightsEn()
	{
		return $this->annualReportEn('highlights');
	}

	public function actionCorporateGovernanceDe()
	{
		return $this->annualReportDe('corporate-governance');
	}

	public function actionCorporateGovernanceEn()
	{
		return $this->annualReportEn('corporate-governance');
	}

	public function actionCompensationReportDe()
	{
		return $this->annualReportDe('compensation-report');
	}

	public function actionCompensationReportEn()
	{
		return $this->annualReportEn('compensation-report');
	}

	public function actionFinancialReportDe()
	{
		return $this->annualReportDe('financial-report');
	}

	public function actionFinancialReportEn()
	{
		return $this->annualReportEn('financial-report');
	}
}
