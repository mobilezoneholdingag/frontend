<?php

namespace app\modules\page\controllers;

use app\models\ApiRepository;
use app\models\JobCategoryModel;
use app\models\JobModel;
use app\modules\BaseModuleController;

class JobsController extends BaseModuleController
{
	public function actionJobs()
	{
		return $this->renderFile('@page/jobs/karriere-jobs.twig');
	}

	public function actionWork()
	{
		return $this->renderFile('@page/jobs/work.twig');
	}

	public function actionEducation()
	{
		return $this->renderFile('@page/jobs/education.twig');
	}

	public function actionPositions()
	{
		/** @var ApiRepository $repository */
		$repository = di(ApiRepository::class)->model(JobCategoryModel::class);

		return $this->renderFile('@page/jobs/positions.twig', [
			'jobCategories' => $repository->find()
		]);
	}

	public function actionJob($slug)
	{
		/** @var ApiRepository $repository */
		$repository = di(ApiRepository::class)->model(JobModel::class);

		return $this->renderFile('@page/jobs/job.twig', [
			'job' => $repository->find($slug)
		]);
	}
}
