<?php

namespace app\modules\page\controllers;

use app\models\ContactForm;
use app\modules\BaseModuleController;
use app\modules\page\models\EventInterface;
use app\modules\page\models\PersonInterface;
use app\modules\page\models\ReportInterface;
use app\services\NewsFeed;
use app\services\SessionInterface;
use Yii;
use yii\base\InlineAction;
use yii\captcha\Captcha;
use app\services\Seo;
class AboutController extends BaseModuleController
{
	protected $session;

	public function __construct(
		$id,
		$module,
		SessionInterface $session,
		$config = []
	) {
		$this->session = $session;

		parent::__construct($id, $module, $config);
	}

	public function beforeAction($action)
	{
		// necessary hack to allow sending POST data from forms
		if ($action instanceof InlineAction) {
			// JSON actions w/o csrf tokens
			switch ($action->actionMethod) {
				case 'actionSendContact':
					Yii::$app->request->enableCsrfValidation = false;
					break;
			}
		}

		return parent::beforeAction($action);
	}

	public function actions()
	{
		return [
			'captcha' => [
				'class' => 'yii\captcha\CaptchaAction',
			],
		];
	}

	public function actionAboutUs()
	{
		return $this->renderFile('@page/about/about.twig');
	}

	public function actionAboutUsEn()
	{
		return $this->renderFile('@page/about/en/about-en.twig');
	}

	public function actionBusinessUnits()
	{
		return $this->renderFile('@page/about/business.twig');
	}

	public function actionBusinessUnitsEn()
	{
		return $this->renderFile('@page/about/en/business-en.twig');
	}

	public function actionNews()
	{
		return $this->renderFile('@page/about/news.twig');
	}

	public function actionNewsEn()
	{
		return $this->renderFile('@page/about/en/news-en.twig');
	}

	public function actionFeeds($id = null)
	{
        $feed = NewsFeed::items(null);
        foreach($feed[0]->feed->data["child"] as $itm){
            foreach($itm["feed"] as $feed) {
                foreach($feed["child"] as $child){
                    foreach($child["entry"] as $entry){
                        if($entry["child"]["http://www.w3.org/2005/Atom"]["id"][0]["data"] == $id){
                            Seo::setTitle(strip_tags($entry["child"]["http://www.w3.org/2005/Atom"]["title"][0]["data"])." | mobilezone");
                        }
                    }
                }
            }
        }
		return $this->renderFile('@page/about/feeds.twig', ['feedID' => $id]);
	}

	public function actionFeedsEn($id = null)
	{
		$lang = 'en-EN';

		return $this->renderFile('@page/about/en/feeds-en.twig',
			[
				'feedID' => $id,
				'lang' => $lang
			]
		);
	}

	public function actionMediaService()
	{
		return $this->renderFile('@page/about/media.twig');
	}

	public function actionMediaServiceEn()
	{
		return $this->renderFile('@page/about/en/media-en.twig');
	}

	public function actionKeyFigures()
	{
		return $this->renderFile('@page/about/key-figures.twig');
	}

	public function actionKeyFiguresEn()
	{
		return $this->renderFile('@page/about/en/key-en.twig');
	}

    private function findPage()
    {
        $tree = Yii::$app->luyaApi->getTree();

        if(!array_key_exists('pages', $tree)) {
            return false;
        }

        foreach($tree['pages'] as $trow => $t) {

            if($t['item']['alias'] == 'fk') {
                return Yii::$app->luyaApi->getPage($t['item']['nav_item_id']);
            }

        }

        return false;

    }


	public function actionCalendar()
	{

	    $calendar = Yii::$app->luyaApi->getCalendar(1);
        return $this->renderFile('@page/about/calendar.twig', ['calendar' => $calendar, 'page' => $this->findPage()]);

        /*
         * DEPRECATED

		$lang = Yii::$app->language;
		$events = [];

		if ($lang == 'de-CH') {
			$events = EventInterface::DE;
		} elseif ($lang == 'fr-FR') {
			$events = EventInterface::FR;
		} elseif ($lang == 'it-IT') {
			$events = EventInterface::IT;
		}

		return $this->renderFile('@page/about/calendar.twig', ['events' => $events]);

        */

	}

	public function actionCalendarEn()
	{
	    Yii::$app->luyaApi->fakeLang('en');
        $calendar = Yii::$app->luyaApi->getCalendar(1);
        return $this->renderFile('@page/about/calendar.twig', ['calendar' => $calendar, 'title' => 'Financial Calendar', 'page' => $this->findPage()]);

	    /*
		$events = EventInterface::EN;

		return $this->renderFile('@page/about/en/calendar-en.twig', ['events' => $events]);
	    */
	}

	public function actionReports()
	{
		$reports = ReportInterface::ALL;
		$currentTime = date("YmdHis", strtotime('+2 hours'));

		$params = [
			'reports' => $reports,
			'current_time' => $currentTime,
		];

		return $this->renderFile('@page/about/reports.twig', $params);
	}

	public function actionReportsEn()
	{
		$reports = ReportInterface::ALL;

		$params = [
			'reports' => $reports,
			'lang_key' => 'en'
		];

		return $this->renderFile('@page/about/en/reports-en.twig', $params);
	}

	public function actionIrService()
	{
		return $this->renderFile('@page/about/ir-service.twig');
	}

	public function actionIrServiceEn()
	{
		return $this->renderFile('@page/about/en/ir-en.twig');
	}

	public function actionStructure()
	{
		return $this->renderFile('@page/about/structure.twig');
	}

	public function actionStructureEn()
	{
		return $this->renderFile('@page/about/en/structure-en.twig');
	}

	public function actionBoardOfDirectors()
	{

        $people = Yii::$app->luyaApi->getPeople('board-of-directors');
        $title = Yii::t('staticcontent', 'Page_Tree_Board_of_Directors');

        return $this->renderFile('@page/about/luya/people.twig', ['people' => $people, 'showCompany' => false, 'title' => $title]);

	}

	public function actionBoardOfDirectorsEn()
	{
        Yii::$app->luyaApi->fakeLang('en');
        $people = Yii::$app->luyaApi->getPeople('board-of-directors');
        $title = "Board of Directors";

        return $this->renderFile('@page/about/luya/people.twig', ['people' => $people, 'showCompany' => false, 'title' => $title]);

	}

	public function actionGroupManagement()
	{
	    $people = Yii::$app->luyaApi->getPeople('group-management');
        $title = Yii::t('staticcontent', 'Page_Tree_Group_Management');

	    return $this->renderFile('@page/about/luya/people.twig', ['people' => $people, 'showCompany' => false, 'title' => $title]);
	}

	public function actionGroupManagementEn()
	{
        Yii::$app->luyaApi->fakeLang('en');
        $people = Yii::$app->luyaApi->getPeople('group-management');
        $title = 'Group Management';

        return $this->renderFile('@page/about/luya/people.twig', ['people' => $people, 'showCompany' => false, 'title' => $title]);
	}

    public function actionManagementTeamTrade()
    {
        $people = Yii::$app->luyaApi->getPeople('management-trade');
        $title = Yii::t('staticcontent', 'Page_Tree_Management_Team') . " (" . Yii::t('staticcontent', 'Page_Tree_Management_Trade') . ")";

        return $this->renderFile('@page/about/luya/people.twig', ['people' => $people, 'showCompany' => true, 'title' => $title]);
    }

    public function actionManagementTeamTradeEn()
    {
        Yii::$app->luyaApi->fakeLang('en');
        $people = Yii::$app->luyaApi->getPeople('management-trade');
        $title = "Management Team (Trade)";

        return $this->renderFile('@page/about/luya/people.twig', ['people' => $people, 'showCompany' => true, 'title' => $title]);
    }

    public function actionManagementTeamServiceProviding()
    {
        $people = Yii::$app->luyaApi->getPeople('management-service-providing');
        $title = Yii::t('staticcontent', 'Page_Tree_Management_Team') . " (" . Yii::t('staticcontent', 'Page_Tree_Management_Service_Providing') . ")";

        return $this->renderFile('@page/about/luya/people.twig', ['people' => $people, 'showCompany' => true, 'title' => $title]);
    }

    public function actionManagementTeamServiceProvidingEn()
    {
        Yii::$app->luyaApi->fakeLang('en');
        $people = Yii::$app->luyaApi->getPeople('management-service-providing');
        $title = "Management Team (Service Providing)";

        return $this->renderFile('@page/about/luya/people.twig', ['people' => $people, 'showCompany' => true, 'title' => $title]);
    }

	public function actionDownloads()
	{
		return $this->renderFile('@page/about/downloads.twig');
	}

	public function actionDownloadsEn()
	{
		return $this->renderFile('@page/about/en/downloads-en.twig');
	}

	public function actionContact()
	{
		$model = new ContactForm();

		if (Yii::$app->request->isPost) {
			if ($model->load(Yii::$app->request->post())) {
				if ($model->validate()) {

					$response = $model->sendEmail();

					if ($response == 200) {
						$this->session->addFlash('success', Yii::t('app', 'Form_Success_Message'));
						return $this->renderFile('@page/about/contact-success.twig');
					} else {
						$this->session->addFlash('error', Yii::t('app', 'Form_Error_Message'));
					}
				}
			}
		}

		$sidebarExtension = true;
		$model->captcha = '';
		$captcha = new Captcha(['name' => 'captcha', 'captchaAction' => 'page/about/captcha', 'model' => $model]);

		return $this->renderFile(
			'@page/about/contact.twig',
			[
				'sidebarExtension' => $sidebarExtension,
				'model' => $model,
				'captcha' => $captcha,
				'pre_selected_question' => ['options' => [Yii::$app->request->get('id') => ['selected' => true]]],
			]
		);
	}

	public function actionContactEn()
	{
		$model = new ContactForm();

		if (Yii::$app->request->isPost) {
			if ($model->load(Yii::$app->request->post())) {
				if ($model->validate()) {

					$response = $model->sendEmail();

					if ($response == 200) {
						$this->session->addFlash('success', 'Contact form sent');
						return $this->renderFile('@page/about/en/contact-success-en.twig');
					} else {
						$this->session->addFlash('error', 'Contact form not sent');
					}
				}
			}
		}

		$sidebarExtension = true;
		$model->captcha = '';
		$captcha = new Captcha(['name' => 'captcha', 'captchaAction' => 'page/about/captcha', 'model' => $model]);

		return $this->renderFile(
			'@page/about/en/contact-en.twig',
			[
				'sidebarExtension' => $sidebarExtension,
				'model' => $model,
				'captcha' => $captcha,
				'pre_selected_question' => ['options' => [Yii::$app->request->get('id') => ['selected' => true]]],
			]
		);
	}
}
