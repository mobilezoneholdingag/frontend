<?php

namespace app\console\controllers;

use app\services\api\ApiClientInterface;
use app\services\i18n\TranslationManager;
use app\services\Locale;
use Exception;
use Yii;
use yii\console\Controller;
use yii\helpers\FileHelper;

/**
 * Class LanguageController
 *
 * @package console\controllers
 */
class LanguageController extends Controller
{
	/**
	 * @var TranslationManager
	 */
	protected $translationManager;
	protected $apiClient;

	public function __construct($id, $module, TranslationManager $translationManager, ApiClientInterface $apiClient, $config = [])
	{
		parent::__construct($id, $module, $config);

		$this->translationManager = $translationManager;
		$this->apiClient = $apiClient;
	}

	/**
	 * Pull content from api for existing translation files.
	 * ---
	 * 1) Lookup in cache, if lifetime of files has exceeded.
	 * 2) case 1 - expired:
	 *      2.1) Get all local translation file names for recent country setting.
	 *      2.2) Api call to get alle files content.
	 *      2.3) Overwrite all files according to response.
	 *      2.4) Create new cache entry for expiry time - default 3600s
	 *    case 2 - still valid:
	 *      2.1) skip method - proceed with existing files
	 *
	 * @param string $language
	 *
	 * @return null
	 */

	public function actionUpdate($language = 'de-CH')
	{
		$config = Yii::$app->params['translations'];

		if (!in_array($language, $config['languages'])) {
			throw new \InvalidArgumentException('Language not defined: `' . $language . '`.');
		}
		
		Locale::setLanguage($language);
		
		// set cache state: in progress
		$this->translationManager->setCacheState(
			TranslationManager::CACHE_STATE_FILES_UNDER_CONSTRUCTION,
			TranslationManager::CACHE_KEY . $language . gethostname()
		);

		$languageDir = Yii::getAlias($config['path'] . '/' . $language);

		foreach ($config['categories'] as $category) {
			$filePath = $languageDir . '/' . $category . '.php';

			if ($data = $this->getTranslationData($language, $category)) {
				FileHelper::createDirectory($languageDir);
				file_put_contents($filePath, $this->transformTranslationData($data));
			}
		}

		// set cache state: valid/done
		$this->translationManager->setCacheState(
			TranslationManager::CACHE_STATE_VALID,
			TranslationManager::CACHE_KEY . $language . gethostname()
		);
	}

	/**
	 * @param string $language
	 * @param string $category
	 *
	 * @return null
	 */
	private function getTranslationData($language, $category)
	{
		try {
			return $this->apiClient->get('language/translations', null, [
				'language_code' => $language,
				'category' => $category,
			]);
		} catch (Exception $e) {
			// remove cache elem
			$this->translationManager->setCacheState(false, TranslationManager::CACHE_KEY . $language . gethostname());

			return null;
		}
	}

	/**
	 * transform translation data to executable php code
	 *
	 * @param array $data
	 * @return string
	 */
	private function transformTranslationData($data)
	{
		$translations = [];

		foreach ($data as $trans) {
			$translations[$trans['message']] = $trans['translation'];
		}

		return '<?php return ' . var_export($translations, true) . ';';
	}
}
