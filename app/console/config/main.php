<?php

$params = array_replace_recursive(
	require(__DIR__ . '/../../config/params.php'),
	require(__DIR__ . '/../../config/params-local.php'),
	require(__DIR__ . '/params.php')
);

return [
	'id' => 'app-console',
	'basePath' => dirname(dirname(__DIR__)),
	'controllerNamespace' => 'app\console\controllers',
	'bootstrap' => ['log'],
	'components' => [
		'log' => [
			'targets' => [
				[
					'class' => yii\log\FileTarget::class,
					'levels' => ['error', 'warning'],
				],
			],
		],
		'redis' => [
			'class' => yii\redis\Connection::class,
			'hostname' => 'redis',
		],
		'redis-session' => [
			'class' => yii\redis\Connection::class,
			'hostname' => 'redis',
			'database' => 1,
		],
		'cache' => [
			'class' => yii\redis\Cache::class,
		],
		'urlManager' => [
			'class' => app\services\i18n\UrlManager::class,
			'enablePrettyUrl' => true,
			'showScriptName' => false,
			'baseUrl' => 'https://www.deinhandy.de/',
		],
	],
	'params' => $params,
];
