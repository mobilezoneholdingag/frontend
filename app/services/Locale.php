<?php

namespace app\services;

use Yii;
use yii\web\Request;

/**
 * Class Locale
 * @package components
 */
class Locale
{
	const GERMANY = 'de-DE';
	const SUISSE = 'de-CH';
	const ITALY = 'it-IT';
	const FRANCE = 'fr-FR';

	/**
	 * Check a locale if it's the current locale on the server.
	 *
	 * @param string $locale    ISO Alpha 4 code for a language.
	 *
	 * @return bool
	 */
	static public function in($locale)
	{
		return $locale == Yii::$app->language;
	}

	/**
	 * Returns the current language on the server.
	 *
	 * @return string
	 */
	static public function active()
	{
		return Yii::$app->language;
	}

	/**
	 * True, if the server is configured for Switzerland.
	 *
	 * @return bool
	 */
	static public function inSuisse()
	{
		return Yii::$app->language === self::SUISSE;
	}

	/**
	 * Setup environment configs according to language.
	 *
	 * @param $directory
	 */
	static public function setEnvironmentConfigurations($directory)
	{
		$request    = Yii::$app->request;

		//@TODO: check if console needs language setting too
		if ($request->isConsoleRequest)
			return;

		$config = self::getConfigByIndication($request);

		$url = isset($config['url']) ? $config['url'] : null;
		$language = isset($config['language']) ? $config['language'] : null;
		$currency = isset($config['currency']) ? $config['currency'] : null;

		if (!$language)
			$language = self::getLocaleByIndication($request);

		if ($directory && $request)
			self::setDebugLanguage($request);

		if ($directory && $language && self::isFrontend())
			self::setFrontendPaths($directory);

		if ($url)
			self::setUrlManagerHostInfo($url);

		if ($currency)
			self::setCurrency($currency);
	}

	/**
	 * Checks on the base path of Yii::$app, if the environment is frontend.
	 *
	 * @return bool True, if the base path of the application is named 'frontend'
	 */
	static protected function isFrontend()
	{
		return strpos(Yii::$app->getBasePath(), 'frontend') !== false;
	}

	/**
	 * Returns a config array for debugging by guessing the environment.
	 *
	 * @param Request $request
	 *
	 * @return array
	 */
	static protected function getConfigByIndication($request)
	{
		return [
			'url'       => $request->getHostInfo(),
			'language' => self::getLocaleByIndication($request),
			'currency' => 'CHF',
			'database' => 'deinhandy'
		];
	}

	/**
	 * @param string $language  4-alpha ISO code of language.
	 */
	static public function setLanguage($language)
	{
		Yii::$app->language = $language;
	}

	/**
	 * @param $directory
	 */
	static public function setFrontendPaths($directory)
	{
		Yii::$app->viewPath    = $directory.'/frontend/views/';
		Yii::$app->layoutPath  = Yii::$app->viewPath.'/layouts';
	}

	/**
	 * @param string $currency  Currency code.
	 */
	static public function setCurrency($currency = null)
	{
		if ($currency)
			Yii::$app->formatter->currencyCode = $currency;
	}

	/**
 * @param string $hostInfo  Host info for urlManager.
 */
	static public function setUrlManagerHostInfo($hostInfo = null)
	{
		if ($hostInfo)
			Yii::$app->urlManager->setHostInfo($hostInfo);
	}

	/**
	 * Sets the language for debug environments.
	 *
	 * @param Request   $request
	 */
	static public function setDebugLanguage($request = null)
	{
		if (!$request)
			$request = Yii::$app->request;

		//@TODO: make dynamic!
		self::setLanguage(Yii::$app->params['languages'][self::getCountryTld($request)]);
		Yii::$app->params['google.country'] = array_flip(Yii::$app->params['languages'])[Yii::$app->language];
	}

	/**
	 * Indicates the locale by domain.
	 *
	 * @param Request $request
	 * @return string 4-alpha ISO code of language.
	 */
	static public function getLocaleByIndication($request)
	{
		if ($request->isConsoleRequest) {
			return Locale::SUISSE;
		}
		$country = self::getCountryTld($request);

		return Yii::$app->params['languages'][$country];
	}

	/**
	 * @return bool|null    Null, if it cannot be determined.
	 */
	public static function isDev()
	{
		if (false == ($request = Yii::$app->request) || $request->isConsoleRequest)
			return null;

		return strpos(parse_url(Yii::$app->request->hostInfo, PHP_URL_HOST), '.dev') !== false;
	}

	/**
	 * Returns the TLD of the request.
	 *
	 * @param Request $request
	 *
	 * @return string   TLD
	 */
	public static function getCountryTld($request)
	{
		$host = parse_url($request->hostInfo, PHP_URL_HOST);

		$domainComponents = explode('.', $host);
		$country = '';

		// detecting language
		foreach ($domainComponents as $v) {
			if ($v == Yii::$app->params['localeMatchName']) {
				break;
			}

			$country = $v;
		}

		// if none found matching lang array --> replace with default app language
		if(!isset(Yii::$app->params['languages'][$country])) {
			$country = array_flip(Yii::$app->params['languages'])[Yii::$app->language];
		}

		return $country;
	}
}
