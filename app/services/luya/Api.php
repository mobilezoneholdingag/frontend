<?php

namespace app\services\luya;

use GuzzleHttp\Client as HttpClient;
use yii\base\Component;

class Api extends Component
{
    private $http;

    private $langMapping = [
        'de-CH' => "de",
        'fr-FR' => "fr",
        'it-IT' => "it",
    ];

    public $token;
    public $endpoint;

    public function init()
    {

        $endpoint = $this->endpoint . $this->langMapping[\Yii::$app->language] . '/api/';

        $this->http = new HttpClient([
            'base_uri' => $endpoint,
            'headers' => [
                'Authorization' => 'Bearer ' . $this->token,
                'Accept' => 'application/json'
            ]
        ]);
    }

    public function fakeLang($lang)
    {
        $endpoint = $this->endpoint . $lang . '/api/';

        $this->http = new HttpClient([
            'base_uri' => $endpoint,
            'headers' => [
                'Authorization' => 'Bearer ' . $this->token,
                'Accept' => 'application/json'
            ]
        ]);
    }

    public function getTree() {
        return $this->get('nav/tree');
    }

    public function getPage($id, $version = null, $full = true) {

        if($full) {
            $full = '/full';
        } else {
            $full = '';
        }

        return $this->get('nav-items/' . $id . $full . (!empty($version) ? '?version=' . $version : ''));
    }

    public function getCalendar($id) {
        return $this->get('calendar-groups/' . $id . '?expand=upcoming');
    }

    public function getPeople($slug) {
        return $this->get('people/by-slug?slug=' . $slug);
    }

    // PRIVATE METHODS

    private function get($ep) {


        /*

        if(\Yii::$app->params['environment_stage'] == 'live') {
            // CACHE EVERYTHING ON LIVE
            $cache = \Yii::$app->cache->get(base64_encode(\Yii::$app->language . $ep));

            if($cache !== false) {
                return $cache;
            }
        }
        */

        $res = $this->http->request('GET', $ep);

        $result = $this->result($res);

        /*

        if(\Yii::$app->params['environment_stage'] == 'live') {
            \Yii::$app->cache->set(base64_encode(\Yii::$app->language . $ep), $result);
        }

        */

        return $result;

    }

    private function result($res) {
        return json_decode($res->getBody()->getContents(), true);
    }


}