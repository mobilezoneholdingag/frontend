<?php

namespace app\services;

use app\models\GeneralNewsModel;
use GeneralNews;
use Yii;
use app\models\ApiRepository;

class GeneralNewsItems
{
    public static function items($count, $lang = null)
    {
        $items = [];
        $apiRepository = di(ApiRepository::class)->model(GeneralNewsModel::class);
        $items = $apiRepository->model(GeneralNewsModel::class)->find(null,["sort"=>"order by date"]);
        if(!empty($items)) {
            $items = $items->sortBy("date")->reverse()->slice(0,5);
        }

        return $items;
    }

}
