<?php

namespace app\services;

use app\modules\seo\models\SeoService;
use Yii;

class Seo
{

    private static $title;
    private static $keywords;
    private static $metaDescription;

	public static function getMetaRobots($path = null)
	{
		$seo = self::getSeoInformation($path);

		return $seo->getMetaRobots();
	}

	public static function setKeywords($keywords)
    {
        self::$keywords = $keywords;
    }

	public static function getKeywords($path = null)
	{

	    if(!empty(self::$keywords)) {
	        return self::$keywords;
        }

		$seo = self::getSeoInformation($path);

		return $seo->getKeywords();
	}

	public static function setTitle($title)
    {
        self::$title = $title;
    }

	public static function getTitle($path = null)
	{

	    if(!empty(self::$title)) {
	        return self::$title;
        }

		$seo = self::getSeoInformation($path);

		return Yii::t('seo', $seo->getTitle(), $seo->getEntityInformation());
	}

	public static function setMetaDescription($description)
    {
        self::$metaDescription = $description;
    }

	public static function getMetaDescription($path = null)
	{

	    if(!empty(self::$metaDescription)) {
	        return self::$metaDescription;
        }

		$seo = self::getSeoInformation($path);

		return Yii::t('seo', $seo->getDescription(), $seo->getEntityInformation());
	}

	public static function getSeoText($path = null)
	{
		$seo = self::getSeoInformation($path);

		return $seo->getSeoText();
	}

	public static function getCanonical($path = null)
	{
		$seo = self::getSeoInformation($path);

		return $seo->getCanonical();
	}

	private static function getSeoInformation($path = null)
	{
		$path = $path ?? Yii::$app->request->url;
		$canonical = SeoService::getCanonical($path); // aka RoutingService

		// TODO redet später mit dem echten SEO Service
		$seoInformation = SeoService::getSeoInformation($canonical);

		return $seoInformation;
	}
}
