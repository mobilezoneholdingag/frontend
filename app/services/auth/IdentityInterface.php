<?php

namespace app\services\auth;

interface IdentityInterface extends \yii\web\IdentityInterface
{
	/**
	 * @return bool
	 */
	public function checkLogin(): bool;
}
