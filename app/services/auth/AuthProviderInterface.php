<?php

namespace app\services\auth;

use app\models\CustomerModel;

interface AuthProviderInterface
{
	/**
	 * returns current logged in user
	 *
	 * @return CustomerModel|null
	 */
	public function user();

	/**
	 * @return $this
	 */
	public function logout();

	/**
	 * @param IdentityInterface $user
	 *
	 * @return $this;
	 */
	public function login(IdentityInterface $user);

	/**
	 * @param IdentityInterface $user
	 *
	 * @return bool
	 */
	public function check(IdentityInterface $user): bool;
}
