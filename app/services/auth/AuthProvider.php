<?php

namespace app\services\auth;

use Yii;

class AuthProvider implements AuthProviderInterface
{
	/**
	 * returns current logged in user
	 *
	 * @return \yii\web\IdentityInterface
	 */
	public function user()
	{
		return Yii::$app->user->getIdentity();
	}

	/**
	 * @return $this
	 */
	public function logout()
	{
		Yii::$app->user->logout();

		return $this;
	}

	/**
	 * @param IdentityInterface $user
	 *
	 * @return $this;
	 */
	public function login(IdentityInterface $user)
	{
		return Yii::$app->user->login($user);
	}

	/**
	 * @param IdentityInterface $user
	 *
	 * @return bool
	 */
	public function check(IdentityInterface $user): bool
	{
		return $user->checkLogin();
	}
}
