<?php

namespace app\services\api;

use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\TransferStats;
use Yii;
use yii\helpers\Url;

class ApiClient implements ApiClientInterface
{
	/**
	 * @var string
	 */
	protected $endpoint;
	/**
	 * @var string
	 */
	protected $secureEndpoint;
	/**
	 * @var
	 */
	protected $client;
	protected $config;
	protected $logger;

	/**
	 * @param string $endpoint
	 * @param string $secureEndpoint
	 * @param array $config
	 * @param ApiClientLoggerInterface $logger
	 */
	public function __construct($endpoint, $secureEndpoint, $config, ApiClientLoggerInterface $logger)
	{
		$this->endpoint = $endpoint;
		$this->secureEndpoint = $secureEndpoint;
		$this->config = $config;
		$this->logger = $logger;

		$fqdnHeader = Yii::$app->request instanceof \yii\console\Request ? null : Url::home(true);

		$userIp = 'NOT_DEFINED';
        $userAgent = 'NOT_DEFINED';
        $userHost = 'NOT_DEFINED';

        if(!Yii::$app->request->isConsoleRequest) {
            $userIp = Yii::$app->request->headers->get('X-Forwarded-For', Yii::$app->request->userIp);
            $userAgent = Yii::$app->request->userAgent;
            $userHost = Yii::$app->request->userHost;
        }

		$this->client = new HttpClient([
			'headers' => [
				'Accept-Language' => Yii::$app->language,
				'X-Stardust-Frontend-FQDN' => $fqdnHeader,
                'X-Client-Ip' => $userIp,
                'X-Client-Agent' => $userAgent,
                'X-Client-Host' => $userHost,
			],
			'auth' => ['deinhandy', 'applepie42'],
			'base_uri' => $this->endpoint,
			'timeout' => $config['timeout'],
		]);
	}

	protected function getCacheDuration($entity)
	{
		return $this->config['caching']['entities'][$entity] ?? $this->config['caching']['duration'];
	}

	protected function shouldCache($entity)
	{
		return $this->config['caching']['enabled']
		&& ($this->config['caching']['entities'][$entity] ?? true) != false;
	}

	public function get($entity, $id = null, $params = [])
	{
		$duration = 0;
		$effectiveUri = null;
		$uri = $entity . ($id ? '/' . $id : '');
		$cache = Yii::$app->cache;
		$cacheKey = $entity . $id . json_encode($params) . Yii::$app->language;
		$shouldCache = $this->shouldCache($entity);
		$result = null;

		try {
			if ($shouldCache && $cache->exists($cacheKey)) {
				$this->logger->log(__FUNCTION__, $uri, $params, $duration);

				return $cache->get($cacheKey);
			}

			$response = $this->client->get($uri, [
				'query' => $params,
				'on_stats' => function(TransferStats $stats) use (&$duration, &$effectiveUri) {
					$duration = $stats->getTransferTime();
					$effectiveUri = $stats->getEffectiveUri();
				},
			]);

			if ($response->getStatusCode() === 200) {
				$result = json_decode((string) $response->getBody(), true);
			}

			if ($shouldCache) {
				$cache->set($cacheKey, $result, $this->getCacheDuration($entity));
			}

			$this->logger->log(__FUNCTION__, $effectiveUri ?? $uri, $params, $duration);

			return $result;

		} catch (ClientException $e) {
			if ($e->getResponse()->getStatusCode() === 404) {
				if ($this->shouldCache($entity)) {
					$cache->set($cacheKey, null, $this->getCacheDuration($entity));
				}
			}

			return null;
		}
	}

	public function post($entity, $params = [])
	{
		try {
			return $this->doRequest('post', $entity, $params);
		} catch (ConnectException $e) {
			return null;
		} catch (ClientException $e) {
			return null;
		}
	}

	public function delete($entity, $id = null, $params = [])
	{
		try {
			return $this->doRequest('delete', $entity . ($id ? '/' . $id : ''), $params);
		} catch (ConnectException $e) {
			return null;
		}
	}

	public function put($entity, $id, $params = [])
	{
		try {
			return $this->doRequest('put', $entity . '/' . $id, $params);
		} catch (ConnectException $e) {
			return null;
		}
	}

	/**
	 * @param string $method
	 * @param string $uri
	 * @param array $params
	 *
	 * @return mixed
	 */
	private function doRequest($method, $uri, $params)
	{
		$duration = 0;
		$effectiveUri = null;

		/** @var Response $response */
		$response = $this->client->$method($uri, [
			'form_params' => $params,
			'on_stats' => function(TransferStats $stats) use (&$duration, &$effectiveUri) {
				$duration = $stats->getTransferTime();
				$effectiveUri = $stats->getEffectiveUri();
			},
		]);

		$this->logger->log(__FUNCTION__, $effectiveUri ?? $uri, $params, $duration);

		return json_decode($response->getBody(), true);
	}
}
