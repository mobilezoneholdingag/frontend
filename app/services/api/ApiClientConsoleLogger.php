<?php

namespace app\services\api;

use deinhandy\yii2events\WebApplication;
use app\services\SessionInterface;
use Yii;
use yii\base\ActionEvent;

class ApiClientConsoleLogger implements ApiClientLoggerInterface
{
	protected $session;

	const SESSION_KEY = 'api-console-logs';

	public function __construct(SessionInterface $session)
	{
		$this->session = $session;

		Yii::$app->on(WebApplication::EVENT_AFTER_ACTION, function(ActionEvent $event) {
			if (!Yii::$app->request->isAjax && is_string($event->result)) {
				$logs = $this->session[self::SESSION_KEY] ?? [];

				$console = Yii::$app->view->renderFile('@views/api/console.twig', ['logs' => $logs]);
				$event->result .= $console;

				$this->session[self::SESSION_KEY] = [];
			}
		});
	}

	/**
	 * @param string $method
	 * @param string $uri
	 * @param mixed $params
	 * @param float $duration
	 *
	 * @return $this
	 */
	public function log(string $method, string $uri, $params, $duration)
	{
		$logs = $this->session[self::SESSION_KEY] ?? [];
		$logs[] = compact('method', 'uri', 'params', 'duration');
		$this->session[self::SESSION_KEY] = $logs;

		return $this;
	}
}
