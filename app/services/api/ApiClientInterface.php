<?php

namespace app\services\api;

interface ApiClientInterface
{
    public function get($entity, $id = null, $params = []);
    public function post($entity, $params = []);
    public function put($entity, $id, $params = []);
    public function delete($entity, $id, $params = []);
}
