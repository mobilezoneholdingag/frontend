<?php

namespace app\services\api;

interface ApiClientLoggerInterface
{
	/**
	 * @param string $method
	 * @param string $uri
	 * @param mixed $params
	 * @param float $duration
	 *
	 * @return $this
	 */
	public function log(string $method, string $uri, $params, $duration);
}
