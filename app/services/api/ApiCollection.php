<?php

namespace app\services\api;

use Illuminate\Support\Collection;

class ApiCollection extends Collection
{
	public function get($key, $default = null)
	{
		return value(parent::get($key, $default));
	}

	public function first(callable $callback = null, $default = null)
	{
		return value(parent::first($callback, $default));
	}

	public function offsetGet($key)
	{
		return value(parent::offsetGet($key));
	}

	public function getIterator()
	{
		foreach ($this->items as &$item) {
			$item = value($item);
		}

		return parent::getIterator();
	}

	protected function operatorForWhere($key, $operator, $value)
	{
		foreach ($this->items as &$item) {
			$item = value($item);
		}

		return parent::operatorForWhere($key, $operator, $value);
	}
}
