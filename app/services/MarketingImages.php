<?php

namespace app\services;

use app\models\MarketingImagesModel;

class MarketingImages {
	public static function get($tag)
	{
		$marketingImages = new MarketingImagesModel();
		return $marketingImages->fetch($tag)->getData();
	}
}
