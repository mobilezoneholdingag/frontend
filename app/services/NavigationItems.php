<?php

namespace app\services;

use app\models\AccessoryCategoryModel;
use app\models\AccessoryCategorySetModel;
use app\models\ApiRepository;
use app\models\ManufacturerNaviModel;
use app\models\SetModel;

class NavigationItems
{

    const AccessoryCategoriesMap = [
        "compatible",
        "protection",
        "charge",
        "audio",
        "misc",
        "car",
    ];

	public static function getManufacturer($id)
	{
		/** @var ApiRepository $apiRepository */
		$apiRepository = di(ApiRepository::class)->model(ManufacturerNaviModel::class);
		$manufacturer = $apiRepository->find(null, ['product_category_id' => $id], true);

		return $manufacturer->elements;
	}

	public static function getAccessoryCategories()
	{
		/** @var ApiRepository $apiRepository */
		$apiRepository = di(ApiRepository::class);
        $accessoryCategories = $apiRepository->model(AccessoryCategoryModel::class)->find()->sort(function($item) {
            return array_search($item->title, self::AccessoryCategoriesMap);
        })->reverse();

		return $accessoryCategories;
	}

	public static function getAccessorySetsByCategoryId($id = null)
	{
		$sets = [];
		/** @var ApiRepository $apiRepository */
		$repository = di(ApiRepository::class);
		$allSets = $repository
			->model(AccessoryCategorySetModel::class)
			->find(null, ['accessory_category_id' => $id], false);

		foreach ($allSets as $set) {
			$setId = $set->set_id;
			$setModel = $repository->model(SetModel::class)->find($setId, [], true);
			$setTitle = $setModel->system_title;
			array_push($sets, $setTitle);
		}

		return $sets;
	}
}
