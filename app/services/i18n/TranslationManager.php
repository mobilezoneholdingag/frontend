<?php

namespace app\services\i18n;

use Symfony\Component\Process\Process;
use Yii;
use yii\caching\Cache;

/**
 * Class TranslationManager
 */
class TranslationManager
{
	const CACHE_KEY = 'TranslationManager_language_cache_flag';
	const CACHE_STATE_FILES_UNDER_CONSTRUCTION = 'still_writing_files';
	const CACHE_STATE_VALID = 'files_written';
	/**
	 * @var Cache
	 */
	protected $cache;
	/**
	 * @var int
	 */
	protected $cacheDuration;

	/**
	 * TranslationManager constructor.
	 *
	 * @param Cache $cache
	 * @param $cacheDuration
	 */
	public function __construct(Cache $cache, $cacheDuration)
	{
		$this->cache = $cache;
		$this->cacheDuration = $cacheDuration;
	}

	/**
	 * Spawn async process calling api to download language files periodically.
	 */
	public function updateTranslationFiles()
	{
		if (!$this->isValid() && !$this->isFilesUnderContruction()) {
			(
			new Process(
				'php ./yii language/update ' . Yii::$app->language,
				Yii::getAlias('@app/..')
			)
			)->start();
		}
	}

	/**
	 * @return bool
	 */
	public function isValid()
	{
		return $this->queryFrontendCache(static::CACHE_STATE_VALID);
	}

	/**
	 * @param bool $state
	 *
	 * @return bool
	 */
	protected function queryFrontendCache($state = false)
	{
		$ckey = $this->getCacheKey();
		$res = $state === $this->cache->get($ckey);

		return $res;
	}

	/**
	 * @return string
	 */
	protected function getCacheKey()
	{
		return static::CACHE_KEY . Yii::$app->language . gethostname();
	}

	/**
	 *
	 */
	public function isFilesUnderContruction()
	{
		return $this->queryFrontendCache(static::CACHE_STATE_FILES_UNDER_CONSTRUCTION);
	}

	/**
	 * @param string|boolean $state
	 * @param null $overwriteKey
	 */
	public function setCacheState($state = false, $overwriteKey = null)
	{
		$key = $this->getCacheKey();
		if (null !== $overwriteKey) {
			$key = $overwriteKey;
		}

		if (false === $state) {
			$this->cache->delete($key);
		} else {
			$this->cache->set($key, $state, $this->cacheDuration);
		}
	}

	/**
	 * @return array
	 * @throws \yii\base\InvalidConfigException
	 */
	public function getLanguageSelectItems()
	{
		$navArr = [];

		foreach (Yii::$app->params['languages'] as $langShort => $langCode) {
			$baseUrl = Yii::$app->urlManager->getHostInfo();
			$navArr[] = [
				'route' => preg_replace(
					'/(https?:\/\/)([a-z]{2,3})\.(.*)/',
					'${1}' . ((array_flip(
							Yii::$app->params['languages']
						)['de-CH'] !== $langShort) ? $langShort : 'www') . '.${3}',
					$baseUrl 
				),
				'active' => $langCode === Yii::$app->language,
				'name' => strtoupper($langShort == 'ch' ? 'de' : $langShort),
				'lang_code' => $langCode,
			];
		}

		return $navArr;
	}
}
