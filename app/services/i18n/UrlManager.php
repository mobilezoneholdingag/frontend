<?php

namespace app\services\i18n;

use app\services\Locale;
use Yii;
use yii\web\UrlRule;

class UrlManager extends \yii\web\UrlManager
{
	public function init()
	{
		$this->cacheKey = $this->createLocalizedCacheKey();
		parent::init();
	}

	private function createLocalizedCacheKey()
	{
		$countryGuess = Yii::$app->params['languages'][Locale::getCountryTld(Yii::$app->request)];

		return $this->cacheKey . $countryGuess;
	}

	public function createUrlFromRules(array $rules, string $path, array $args)
	{
		$url = '';
		/** @var UrlRule[] $urlRules */
		$urlRules = $this->buildRules($rules);
		foreach ($urlRules as $rule) {
            if (($url = $rule->createUrl($this, $path, $args)) !== false) {
                break;
            }
        }

        return $url;
	}
}
