<?php

namespace app\services\i18n;

use app\services\Locale;
use Yii;
use yii\web\Request;
use yii\web\UrlManager;
use yii\web\UrlRule;
use yii\web\UrlRuleInterface;

/**
 * Class UrlRules
 *
 * @package app\services\i18n
 */
class UrlRules implements UrlRuleInterface
{
	protected $rules = [];
	const CACHE_PREFIX = 'urlrules';

	public function __construct()
	{
		$cache = Yii::$app->cache;
		$countryGuess = Yii::$app->params['languages'][Locale::getCountryTld(Yii::$app->request)];
		$cacheKey = self::CACHE_PREFIX . $countryGuess;
		$rules = Yii::$app->params['urlRules'];

		// create rules & cache them
		if (!$cache->exists($cacheKey)) {
			$rules = $this->createRules($rules[$countryGuess] ?? []);
			$cache->set($cacheKey, $rules);
		}

		$this->rules = $cache->get($cacheKey);
	}

	/**
	 * Parses the given request and returns the corresponding route and parameters.
	 *
	 * @param \yii\web\UrlManager $manager the URL manager
	 * @param \yii\web\Request $request the request component
	 *
	 * @return array|boolean the parsing result. The route and the parameters are returned as an array.
	 * If false, it means this rule cannot be used to parse this path info.
	 */
	public function parseRequest($manager, $request)
	{
		return $this->checkRulesForRequestParsing($manager, $request);
	}

	/**
	 * Create new UrlRules from config.
	 *
	 * @param array $localizedRules
	 */
	private function createRules($localizedRules)
	{
		$rules = [];

		foreach ($localizedRules as $pattern => $route) {
			if (is_array($route)) {
				$rule = new UrlRule($route);
				if (!in_array($rule, $this->rules)) {
					$rules[] = $rule;
				}
			} else {
				$rule = new UrlRule(
					[
						'pattern' => $pattern,
						'route' => $route,
					]
				);
				if (!in_array($rule, $this->rules)) {
					$rules[] = $rule;
				}
			}
		}

		return $rules;
	}

	/**
	 * Check rules on match and return result Array|false.
	 *
	 * @param UrlManager $manager
	 * @param Request $request
	 *
	 * @return bool|array
	 */
	private function checkRulesForRequestParsing($manager, $request)
	{
		foreach ($this->rules as $rule) {
			/* @var $rule UrlRule */
			if (($result = $rule->parseRequest($manager, $request)) !== false) {
				return $result;
			}
		}

		return false;
	}

	/**
	 * Creates a URL according to the given route and parameters.
	 *
	 * @param \yii\web\UrlManager $manager the URL manager
	 * @param string $route the route. It should not have slashes at the beginning or the end.
	 * @param array $params the parameters
	 *
	 * @return bool|string the created URL, or false if this rule cannot be used for creating this URL.
	 */
	public function createUrl($manager, $route, $params)
	{
		return $this->checkRulesForUrlCreation($manager, $route, $params);
	}

	/**
	 * @param $manager
	 * @param $route
	 * @param $params
	 *
	 * @return bool|string
	 */
	private function checkRulesForUrlCreation($manager, $route, $params)
	{
		foreach ($this->rules as $rule) {
			/* @var $rule UrlRule */

			// Fix for wrong yii implementation forcing us to declare ALL keys listed in defaults for createUrl() --> parseRequest() works as expected!
			$mergedParams = array_merge($rule->defaults, $params);

			if (($result = $rule->createUrl($manager, $route, $mergedParams)) !== false) {
				return $result;
			}
		}

		return false;
	}
}
