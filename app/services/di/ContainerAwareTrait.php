<?php

namespace app\services\di;

use deinhandy\yii2container\Container;

trait ContainerAwareTrait
{
	/**
	 * @var Container
	 */
	protected $container;

	/**
	 * @return Container
	 */
	public function getContainer(): Container
	{
		return $this->container;
	}

	/**
	 * @param Container $container
	 *
	 * @return $this
	 */
	public function setContainer(Container $container)
	{
		$this->container = $container;

		return $this;
	}
}
