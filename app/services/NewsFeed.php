<?php

namespace app\services;

use SimplePie;
use Yii;
use yii\helpers\FileHelper;

class NewsFeed
{
	public static function items($count, $lang = null)
	{
		$feed = self::getFeed($lang);
		$items = $feed->get_items(0, $count);

		return $items;
	}

	private static function getFeed($language = null)
	{
		$feed = new SimplePie();

		FileHelper::createDirectory(Yii::getAlias('@runtimePath/cache'), $mode = 0775, $recursive = true);
		$feed->set_cache_location(Yii::getAlias('@runtimePath/cache'));

		if ($language) {
			$lang = $language;
		} else {
			$lang = \Yii::$app->language;
		}

		if ($lang == 'de-CH') {
			$feed->set_feed_url(
				[
					'https://xml.newsbox.ch/corporate_web/che/mobilezoneholding/press_release/digest_complete_de_press_release_atom.xml'
				]
			);
		} elseif ($lang == 'fr-FR') {
			$feed->set_feed_url(
				[
					'https://xml.newsbox.ch/corporate_web/che/mobilezoneholding/press_release/digest_complete_fr_press_release_atom.xml'
				]
			);
		} elseif ($lang == 'it-IT') {
			$feed->set_feed_url(
				[
					'https://xml.newsbox.ch/corporate_web/che/mobilezoneholding/press_release/digest_complete_it_press_release_atom.xml'
				]
			);
		} elseif ($lang == 'en-EN') {
			$feed->set_feed_url(
				[
					'https://xml.newsbox.ch/corporate_web/che/mobilezoneholding/press_release/digest_complete_en_press_release_atom.xml'
				]
			);
		}

		$feed->init();
		$feed->handle_content_type();

		return $feed;
	}
}
