<?php

namespace app\services;

use app\models\ArticleTariffModel;
use app\models\TariffModel;
use app\modules\checkout\services\cart\CartInterface;
use app\modules\checkout\services\cart\ItemInterface;

class Basket
{
	public static function itemCount() {
		$cart = di('cart');

		return $cart->count();
	}

	public static function getTariff() {
		/** @var $cart CartInterface */
		$cart = di('cart');
		foreach($cart->getItems() as $item) {
			if ($item->getItemType() == ItemInterface::TYPE_ARTICLE_TARIFF) {
				/** @var $item ArticleTariffModel */
				return $item->tariff;
			} elseif ($item->getItemType() == ItemInterface::TYPE_TARIFF) {
				/** @var $item TariffModel */
				return $item;
			}
		}

		return null;
	}
}
