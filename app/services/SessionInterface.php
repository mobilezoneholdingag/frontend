<?php

namespace app\services;

interface SessionInterface extends \IteratorAggregate, \ArrayAccess, \Countable
{
	/**
	 * @param string $key
	 * @param bool|string $value
	 * @param bool $removeAfterAccess
	 *
	 * @return mixed
	 */
	public function addFlash($key, $value = true, $removeAfterAccess = true);
	public function getAllFlashes($delete = false);
    public function removeAllFlashes();
    public function hasFlash($key);
	public function setFlash($key, $value = true, $removeAfterAccess = true);
}
