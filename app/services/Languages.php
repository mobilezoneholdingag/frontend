<?php

namespace app\services;

use app\services\i18n\TranslationManager;
use Yii;

class Languages
{
	public static function languages()
	{
		/** @var TranslationManager $translationManager */
		$translationManager = di(TranslationManager::class);

		$translationManager->updateTranslationFiles();

		return $translationManager->getLanguageSelectItems();
	}

	public static function urls()
	{
		$languages = self::languages();

		$urls = [];
		foreach ($languages as $language) {
			$urls[$language['lang_code']] = $language['route'];
		}

		return $urls;
	}

	public static function localizedBaseUrl(string $language = Locale::GERMANY)
	{
		$urls = self::urls();

		return $urls[$language] ?? $urls[Locale::GERMANY];
	}
}
