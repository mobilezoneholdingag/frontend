<?php

namespace app\services\application;

use Yii;
use yii\web\Controller;

class WebController extends Controller
{
	public function beforeAction($action)
	{
		if ($action->id === 'index' && $action->controller->id === 'homepage') {
			$this->enableCsrfValidation = false;
		}

		return parent::beforeAction($action);
	}

	protected function trans($value, $category = null)
	{
		return Yii::t($category ?? $this->module->id, $value);
	}
}
