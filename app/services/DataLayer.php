<?php
namespace app\services;

use app\models\ApiRepository;
use app\models\ArticleModel;
use app\models\OrderModel;
use app\models\OrderItemModel;
use app\models\TariffModel;
use app\modules\BaseModuleController;
use app\modules\checkout\services\cart\SessionCart;
use app\modules\search\models\SearchModel;
use Yii;

class DataLayer extends BaseModuleController
{
	protected $authProvider;

	public function __construct(
		$id,
		$module,
		$config = []
	) {
		parent::__construct($id, $module, $config);
	}

	public static function getDataLayerInformation($event)
	{
		$controller = Yii::$app->controller->id;
		$action = Yii::$app->controller->action->id;
		$request = Yii::$app->request;
		$params = $event->params ?? null;

		switch ("{$controller}/{$action}") {
			case 'product/smartphones':
			case 'product/tablets':
			case 'product/accessories':
			case 'product/smartwatches':
            case 'product/refurbished':
				if (!isset($params['article'])) {
					return [];
				}
				$articleInformation = $params['article'];
				$propertyExtension = [
					'deviceId' => $articleInformation->id,
					'deviceName' => $articleInformation->product_name,
					'devicePrice' => $articleInformation->prices['once'],
					'deviceBrand' => $articleInformation->manufacturer['title'],
					'deviceCategory' => $articleInformation->product_category_frontend_route_name,
					'deviceVariant' => $articleInformation->color_label ? $articleInformation->color_label : "",
				];

				return [
					'dataLayerInformation' => $articleInformation,
					'dataLayerType' => 'articleInformation',
					'propertyExtension' => $propertyExtension,
				];
			case 'search/index':
				if (!isset($params['searchResult'])) {
					break ;
				}

				/** @var SearchModel $searchModel */
				$searchModel = $params['searchResult'];

				$propertyExtension = [
					'searchQuery' => $request->get('q'),
					'searchResults' => array_key_exists('articles', $searchModel->getData()) ?
						count($searchModel->getData()['articles']) :
						0,
				];

				return [
					'propertyExtension' => $propertyExtension,
				];
			case 'checkout/success':
				$orderId = $request->get('orderId');
				$customer = Yii::$app->user;
				$orderInformation = $customer->identity->getOrders()->where('id', $orderId)->first();
				$contractDuration = self::getContractDuration($orderInformation) ?? null;
				$transactionSum = self::getTransactionSum($orderInformation);
				$tax = $transactionSum - ($transactionSum / OrderModel::TAX_CALCULATION_VALUE * 100);

				$orderInformation->transaction_sum = $transactionSum;
				$orderInformation->tax = round($tax, 2);
				$orderInformation->coupon = self::getCouponTitle($orderInformation);
				$orderInformation->contract_duration = $contractDuration;
				$propertyExtension = [
					'checkoutModus' => self::getCheckoutModus($orderInformation),
				];

				return [
					'dataLayerInformation' => $orderInformation,
					'dataLayerType' => 'orderInformation',
					'propertyExtension' => $propertyExtension,
				];
		}

		switch ($controller) {
			case 'checkout':
				$steps = Yii::$app->controller->checkoutBuilder->build();
				$checkoutInformation = $steps[0];
				$propertyExtension = [
					'checkoutModus' => self::getCheckoutModus($checkoutInformation),
				];

				return [
					'dataLayerInformation' => $checkoutInformation,
					'dataLayerType' => 'checkoutInformation',
					'propertyExtension' => $propertyExtension,
				];
		}
	}

	public static function getCheckoutModus($itemInformation)
	{
		$isSuccess = Yii::$app->request->get('orderId') > 0;
		$checkoutModus = '';
		$items = $itemInformation->getData()['items'];
		$onlyAccessories = true;
		$noTariff = true;

		foreach ($items as $item) {
			if (is_numeric($item)) {
				/** @var ApiRepository $repository */
				$repository = di(ApiRepository::class);
				$item = $repository->model(OrderItemModel::class)->find($item);
			}
			if ($item['item_id'] == SessionCart::COMMISSION_ARTICLE_ID) continue;

			$itemType = $isSuccess ? $item['item_product_type_id'] : $item->itemType;
			$offerType = $isSuccess ? OrderItemModel::getOfferTypeById($item['item_id']) : $item->tariff['offer_type_id'];

			if($itemType != 'accessories') {
				$onlyAccessories = false;
			}

			if($itemType == 'article_tariff' || $itemType == 'tariff') {
				$noTariff = false;

				switch($offerType) {
					case TariffModel::OFFER_TYPE_RENEW:
						$checkoutModus = 'bundle_renewal';
						break;
					case TariffModel::OFFER_TYPE_NEW:
						$checkoutModus = 'bundle_new';
						break;
				};
			}
		}

		if($noTariff) {
			$checkoutModus = 'hardware_device';
		}

		if($onlyAccessories) {
			$checkoutModus = 'hardware_accessories-only';
		}

		return $checkoutModus;
	}

	private static function getCouponTitle($orderInformation)
	{
		foreach($orderInformation->items as $item) {
			if($item->item_product_type_id == 'coupon') {
				return $item->title;
			}
		}
	}

	private static function getContractDuration($orderInformation)
	{
		foreach($orderInformation->items as $item) {
			if($item->item_product_type_id == 'tariff') {
				return $item->tariff->contract_duration;
			}
		}
	}

	private static function getTransactionSum($orderInformation)
	{
		$transactionSum = 0;
		$contractDuration = self::getContractDuration($orderInformation) ?? 1;

		foreach($orderInformation->items as $item) {
			$transactionSum = $transactionSum + $item->prices['once'];

			if ($item->prices['monthly'] > 0) {
				$transactionSum = $transactionSum + ($item->prices['monthly'] * $contractDuration);
			}
		}

		return $transactionSum;
	}
}
