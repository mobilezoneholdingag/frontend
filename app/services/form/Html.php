<?php

namespace app\services\form;

use Yii;

class Html
{
	static public function range($options = [])
	{
		return Yii::$app->view->renderFile('@views/forms/range.twig', $options);
	}

	static public function checkbox($options = [])
	{
		$options['attrs'] = array_merge(
			['class' => 'custom-control-input', 'value' => 1],
			$options['attrs'] ?? [],
			['type' => 'checkbox']
		);

		return Yii::$app->view->renderFile('@views/forms/checkbox.twig', $options);
	}

	static public function radio($options = [])
	{
		$options['attrs'] = array_merge(['class' => 'custom-control-input'], $options['attrs'] ?? [], ['type' => 'radio']);

		return Yii::$app->view->renderFile('@views/forms/radio.twig', $options);
	}
}
