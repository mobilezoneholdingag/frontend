<?php

namespace app\services\form;

class ActiveForm extends \yii\bootstrap\ActiveForm
{
	public $errorCssClass = 'has-danger';

	public function __construct(array $config = [])
	{
		parent::__construct($config);

		$this->fieldConfig['class'] = ActiveField::class;
	}
}
