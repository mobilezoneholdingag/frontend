<?php

namespace app\services\form;

use app\models\ApiRepository;
use app\models\CountryInterface;
use app\models\GenderInterface;
use app\models\PassportTypeInterface;
use app\models\ProviderModel;
use app\modules\shopFinder\models\ShopModel;
use Yii;
use yii\bootstrap\Html as YiiHtml;
use app\services\form\Html as Bootstrap4Html;

class ActiveField extends \yii\bootstrap\ActiveField
{
	public $size;

	public function range()
	{
		return Bootstrap4Html::range([
			'min' => [
				'name' => 'from',
				'limit' => 0,
				'value' => 20,
			],
			'max' => [
				'name' => 'to',
				'limit' => 150,
				'value' => 90,
			],
		]);
	}

	public function hiddenInput($options = [])
	{
		$this
			->label(false)
			->noFormGroup();

		return parent::hiddenInput($options);
	}

	public function submit($type = 'danger')
	{
		$options = [
			'class' => "btn btn-{$type}"
		];

		return $this->input('submit', $options);
	}

	public function render($content = null)
	{
		$this->errorOptions = [
			'tag' => 'div',
			'class' => 'form-control-feedback',
			'encode' => false,
		];

		return parent::render($content);
	}

	/**
	 * @return bool
	 */
	protected function hasError()
	{
		return $this->model->hasErrors($this->attribute);
	}

	public function inlineGroup()
	{
		$this->options['class'] .= ' form-group-inline';

		return $this;
	}

	public function date($options = [], $noLabel = false)
	{
		if ($this->isRequired()) {
			$this->label($this->model->getAttributeLabel($this->attribute) . '*');
		}

		if ($this->hasError()) {
			$this->inputOptions['class'] .= ' form-control-danger';
		}

		// beware that the datepicker class actually will be picked by the
		// datepicker.js and there a second field will be generated
		$this->inputOptions['class'] .= ' datepicker';
		$this->inputOptions['data-placeholder'] = $noLabel ?
			Yii::t('checkout', 'Birthday') :
			Yii::t('view', 'Date_Format');
		$this->inputOptions['placeholder'] = 'YYYY-MM-DD'; //noscript fallback
		$options = array_merge($this->inputOptions, $options);
		$this->adjustLabelFor($options);
		$this->parts['{input}'] = YiiHtml::activeInput('text', $this->model, $this->attribute, $options);

		return $this;
	}

	public function textInput($options = [])
	{
		if ($this->hasError()) {
			$this->inputOptions['class'] .= ' form-control-danger';
		}

		return parent::textInput($options);
	}

	public function checkbox($options = [], $enclosedByLabel = true)
	{
		return Bootstrap4Html::checkbox([
			'label' => $this->model->getAttributeLabel($this->attribute),
			'size' => $this->size,
			'errors' => $this->model->getErrors($this->attribute),
			'attrs' => array_merge([
				'id' => $this->getInputId(),
				'name' => "{$this->model->formName()}[{$this->attribute}]",
				'checked' => $this->model->{$this->attribute},
			], $options),
		]);
	}

	public function telInput($options = [])
	{
		$this->inputOptions['class'] .= ' tel-input';
		$options = array_merge($this->inputOptions, $options);
		$this->adjustLabelFor($options);
		$suffix = '<div class="input-group"><span class="input-group-addon">+41</span>';
		$this->parts['{input}'] = $suffix.YiiHtml::activeInput('number', $this->model, $this->attribute, $options).'</div>';

		return $this;
	}

	/**
	 * @param array $items
	 * @param array $options
	 *
	 * @return $this
	 */
	public function radioList($items, $options = [])
	{
		return Yii::$app->view->renderFile('@views/forms/radio-list.twig', [
			'items' => $items,
			'options' => $options,
			'field' => $this,
			'errors' => $this->model->getErrors($this->attribute),
		]);
	}

	public function checkboxList($items, $options = [])
	{
		return Yii::$app->view->renderFile('@views/forms/checkbox-list.twig', [
			'items' => $items,
			'options' => $options,
			'field' => $this,
			'errors' => $this->model->getErrors($this->attribute),
		]);
	}

	public function genderRadioList($options = [])
	{
		$en = Yii::$app->controller->action->id === 'contact-en';

		$genders = $en ? [1 => 'Mr.', 2 => 'Mrs.'] :
			array_map(function($label) {
			return Yii::t('app', $label);
		}, GenderInterface::ALL);

		return $this->radioList($genders, $options);
	}

	public function grid($labelColumn, $inputColumn)
	{
		$this->options['class'] .= ' row';
		$this->labelOptions['class'] .= ' col-form-label ' . $labelColumn;

		$this->template = "{label}\n<div class=\"{$inputColumn}\">{input}\n{hint}\n{error}</div>";

		return $this;
	}

	public function countryDropDown($options = [])
	{
		$countries = array_map(function($label) {
			return Yii::t('countries', $label);
		}, CountryInterface::ALL);

		$options['prompt'] = Yii::t('app', 'Please_Choose');

		return $this->dropDownList($countries, $options);
	}

	public function passportTypeDropDown($options = [])
	{
		$passportTypes = array_map(function($label) {
			return Yii::t('passport-types', $label);
		}, PassportTypeInterface::ALL);

		$options['prompt'] = Yii::t('app', 'Please_Choose');

		return $this->dropDownList($passportTypes, $options);
	}

	public function providerDropDown($title, $options = [])
	{
		$providers = di(ApiRepository::class)->model(ProviderModel::class)->find();
		$providerTitle = [];

		foreach($providers as $provider) {
			$providerTitle[$provider->title] = $provider->title;
		}

		$options['prompt'] = $title;

		return $this->dropDownList($providerTitle, $options);
	}

	public function shopDropDown($title, $options = [])
	{
		$shops = di(ApiRepository::class)->model(ShopModel::class)->find();
		$shopTitle = [];

		foreach($shops as $shop) {
			$shopTitle[$shop->name] = $shop->name;
		}

		$options['prompt'] = $title;

		return $this->dropDownList($shopTitle, $options);
	}

	/**
	 * @return $this
	 */
	public function inlineBlock()
	{
		$this->inputOptions['class'] .= ' d-inline-block';

		return $this;
	}

	/**
	 * @inheritdoc
	 */
	public function dropDownList($items, $options = [])
	{
		$this->inputOptions['class'] .= ' custom-select';

		if ($this->model->hasErrors($this->attribute)) {
			$this->inputOptions['class'] .= ' form-control-danger';
		}

		return parent::dropDownList($items, $options);
	}

	/**
	 * do not create a .form-group wrapper, will be a simple div without the class
	 *
	 * @return $this
	 */
	public function noFormGroup()
	{
		$this->options['class'] = '';

		return $this;
	}

	/**
	 * sets the control size class on the field
	 *
	 * @param string $size
	 *
	 * @return $this
	 */
	public function size($size)
	{
		$this->size = $size;

		if (!isset($this->inputOptions['class'])) {
			$this->inputOptions['class'] = '';
		}

		$this->inputOptions['class'] .= ' form-control-' . $size;
		$this->inputOptions['class'] = trim($this->inputOptions['class']);

		return $this;
	}

	/**
	 * does not display the label. instead puts it into the placeholder attribute
	 *
	 * @return $this
	 */
	public function labelAsPlaceholder()
	{
		$this->label(false);
		$this->inputOptions['placeholder'] = $this->model->getAttributeLabel($this->attribute);

		if ($this->isRequired()) {
			$this->inputOptions['placeholder'] .= '*';
		}

		return $this;
	}

	/**
	 * does not display the label. instead puts it into the placeholder attribute
	 *
	 * @return $this
	 */
	public function labelAsValue()
	{
		$this->label(false);
		$this->inputOptions['value'] = $this->model->getAttributeLabel($this->attribute);

		if ($this->isRequired()) {
			$this->inputOptions['value'] .= '*';
		}

		return $this;
	}

	/**
	 * @return $this
	 */
	public function labelAsPrompt()
	{
		$this->label(false);
		$this->inputOptions['prompt'] = $this->model->getAttributeLabel($this->attribute);

		if ($this->isRequired()) {
			$this->inputOptions['prompt'] .= '*';
		}

		return $this;
	}

	/**
	 * @return bool
	 */
	protected function isRequired():bool
	{
		return $this->model->isAttributeRequired($this->attribute);
	}
}
