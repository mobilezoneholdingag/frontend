<?php
namespace app\controllers;

use app\actions\ErrorAction;
use app\behaviors\DataLayerBehavior;
use app\behaviors\GoogleDataLayerBehavior;
use app\services\redirect\UrlRules;
use Yii;
use yii\web\Controller;
use yii\web\Cookie;

/**
 * Site controller
 */
class SiteController extends Controller
{
	/**
	 * @inheritdoc
	 */
	public function actions()
	{
		return [
			'error' => [
				'class' => ErrorAction::class,
			],
		];
	}

	/**
	 * @param \yii\base\Action $action
	 * @return bool
	 * @throws \yii\base\InvalidConfigException
	 * @throws \yii\web\BadRequestHttpException
	 */
	public function beforeAction($action)
	{
		$redirectRules = UrlRules::getRedirectUrls();
		$movedRules = UrlRules::getMovedUrls();
		$pathInfoFull = Yii::$app->request->url;
		$pathInfoClean = '/' . Yii::$app->request->pathInfo;

		if(isset($redirectRules[$pathInfoFull])) {
			return $this->redirect($redirectRules[$pathInfoFull], 301)->send();
		}
		elseif(isset($redirectRules[$pathInfoClean])) {
			return $this->redirect($redirectRules[$pathInfoClean], 301)->send();
		}
		elseif(isset($movedRules[$pathInfoClean])) {
			return $this->redirect($movedRules[$pathInfoClean])->send();
		}

		Yii::$app->view->attachBehaviors(
			[
				DataLayerBehavior::GOOGLE => GoogleDataLayerBehavior::className(),
			]
		);

		$getParameter = array_change_key_case(Yii::$app->request->get(), CASE_LOWER);

		if (isset($getParameter['bid'])) {
			$this->onTrackingIds('bid', $getParameter['bid']);
		}

		if (isset($getParameter['cid'])) {
			$this->onTrackingIds('cid', $getParameter['cid']);
		}

		if (Yii::$app->session->get('time', false) === false) {
			Yii::$app->session->set('time', time());
		}

		return parent::beforeAction($action);
	}

	/**
	 * @param $dataLayerName
	 * @param $name
	 * @param $value
	 *
	 * @return $this|bool
	 */
	public function setOnDataLayer($dataLayerName, $name, $value)
	{
		/**
		 * @var DataLayerBehavior $dataLayerBehavior
		 */
		if (false == ($dataLayerBehavior = Yii::$app->view->getBehavior($dataLayerName))) {
			return false;
		}

		return $dataLayerBehavior->setOnDataLayer($name, $value);
	}

	/**
	 * @param $dataLayerName
	 * @param array $values
	 *
	 * @return $this|bool
	 */
	public function mergeOnDataLayer($dataLayerName = '', array $values = [])
	{
		/**
		 * @var DataLayerBehavior $dataLayerBehavior
		 */
		if (false == ($dataLayerBehavior = Yii::$app->view->getBehavior($dataLayerName))) {
			return false;
		}

		return $dataLayerBehavior->mergeOnDataLayer($values);
	}

	/**
	 * Sets a new tracking id on data layer and on cookies.
	 *
	 * @param string $idKind The id you like to set.
	 * @param string $idValue The value you like to set.
	 */
	protected function onTrackingIds($idKind, $idValue)
	{
		$expiration = (new \DateTime())->add(new \DateInterval('P30D'))->getTimestamp();
		$idCookie = new Cookie([
			'name' => $idKind,
			'value' => $idValue,
			'expire' => $expiration,
			'httpOnly' => false
		]);

		$this->setOnDataLayer(DataLayerBehavior::GOOGLE, $idKind, $idValue);

		Yii::$app->response->cookies->add($idCookie);
	}
}

