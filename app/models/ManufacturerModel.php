<?php

namespace app\models;

class ManufacturerModel extends AbstractApiModel
{
	const API_ENTITY_NAME = 'manufacturer';
	const SIM_ONLY_MANUFACTURER_ID = 123;
}
