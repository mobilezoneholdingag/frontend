<?php

namespace app\models;

class ContractConfigurator extends AbstractApiModel
{
	public $service;
	public $tv;
	public $internet;
	public $land_line;
	public $mobile;
	public $provider;
	public $tv_provider;
	public $internet_provider;
	public $land_line_provider;
	public $kombi_provider;
	public $sex;
	public $firstname;
	public $lastname;
	public $street;
	public $number;
	public $zip;
	public $place;
	public $birthday;
	public $email;
	public $tel;
	public $recipient;
	public $get_in_touch_type;
	public $shop;
	public $remarks;

	public function rules() : array
	{
		return [
			// name, email, subject and body are required
			[['firstname', 'lastname', 'tel', 'email'], 'required'],
			['email', 'email'],
			[
				[
					'tv_provider',
					'internet_provider',
					'land_line_provider',
					'kombi_provider',
					'kombi_provider',
					'firstname',
					'lastname',
					'street',
					'place',
					'number',
					'shop',
					'remarks',
					'service',
					'tv',
					'internet',
					'land_line',
					'mobile',
					'provider',
					'get_in_touch_type',
				],
				'string',
			],
			[['zip'], 'number'],
			['birthday', 'date', 'format' => 'Y-m-d'],
			['sex', 'in', 'range' => array_keys(GenderInterface::ALL)],
		];
	}

	public function getAllServices() : array
	{
		return [
			'Digital TV' => \Yii::t('staticcontent', 'Page_CC_Service_1'),
			'Internet'  => \Yii::t('staticcontent', 'Page_CC_Service_2'),
			'Festnetz' => \Yii::t('staticcontent', 'Page_CC_Service_3'),
		];
	}

	public function getAllTvs() : array
	{
		return [
			'Keine' => \Yii::t('staticcontent', 'Page_CC_TV_1'),
			'30 Stunden' => \Yii::t('staticcontent', 'Page_CC_TV_2'),
			'7 Tage' => \Yii::t('staticcontent', 'Page_CC_TV_3'),
		];
	}

	public function getAllInternet() : array
	{
		return [
			'E-Mails' => \Yii::t('staticcontent', 'Page_CC_Internet_1'),
			'Musik' => \Yii::t('staticcontent', 'Page_CC_Internet_2'),
			'Filme' => \Yii::t('staticcontent', 'Page_CC_Internet_3'),
		];
	}

	public function getAllLandLine() : array
	{
		return [
			'Selten' => \Yii::t('staticcontent', 'Page_CC_Land_Line_1'),
			'Oft' => \Yii::t('staticcontent', 'Page_CC_Land_Line_2'),
			'Auslandtelefonie' => \Yii::t('staticcontent', 'Page_CC_Land_Line_3'),
		];
	}

	public function getAllMobile() : array
	{
		return [
			'Ja' => \Yii::t('view', 'Yes'),
			'Nein' => \Yii::t('view', 'No'),
		];
	}

	public function getAllProvider() : array
	{
		return [
			'Digital TV laufend' => \Yii::t('staticcontent', 'Page_CC_TV_Title'),
			'Internet laufend' => \Yii::t('staticcontent', 'Page_CC_Internet_Title'),
			'Festnetz laufend' => \Yii::t('staticcontent', 'Page_CC_Land_Line_Title'),
			'Kombie laufend' => \Yii::t('app', 'Bundle'),
			'Keine' => \Yii::t('app', 'None'),
		];
	}

	public function getAllGetInTouchTypes() : array
	{
		return [
			'0' => \Yii::t('staticcontent', 'Page_CC_Get_In_Touch_Question'),
			'E-Mail' => \Yii::t('staticcontent', 'Page_CC_Get_In_Touch_1'),
			'Shop' => \Yii::t('staticcontent', 'Page_CC_Get_In_Touch_2'),
		];
	}

	public function attributeLabels() : array
	{
		return [
			'remarks' => \Yii::t('staticcontent', 'Page_CC_Remark_Label'),
			'firstname' => \Yii::t('staticcontent', 'Page_Contact_Firstname'),
			'lastname' => \Yii::t('staticcontent', 'Page_Contact_Lastname'),
			'street' => \Yii::t('staticcontent', 'Page_Contact_Street'),
			'number' => \Yii::t('staticcontent', 'Page_Contact_Housenumber'),
			'zip' => \Yii::t('staticcontent', 'Page_Contact_Zip'),
			'place' => \Yii::t('staticcontent', 'Page_Contact_Place'),
			'tel' => \Yii::t('staticcontent', 'Page_Contact_Tel'),
			'email' => \Yii::t('staticcontent', 'Page_Contact_Email'),
		];
	}

	public function sendEmail()
	{
		return $this->getApiClient()->post('homer/mail/send-contract-configurator', $this->toArray());
	}
}
