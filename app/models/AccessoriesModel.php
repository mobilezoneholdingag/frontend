<?php

namespace app\models;

use app\modules\checkout\services\cart\ItemInterface;

class AccessoriesModel extends ArticleModel
{
	const ITEM_TYPE = ItemInterface::TYPE_ACCESSORIES;
}
