<?php

namespace app\models;

use Illuminate\Support\Collection;

/**
 * @property Collection|SetItemModel[] $items
 */
class SetAccessoryModel extends SetSearchModel
{
	const API_ENTITY_NAME = 'set/set/accessory';

	public $relations = [
		'items' => ['class' => SetItemModel::class, 'many' => true],
	];
}
