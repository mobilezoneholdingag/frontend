<?php

namespace app\models;

use Illuminate\Support\Collection;
use Yii;

/**
 * @property string $title
 *
 * @property Collection|JobModel[] $jobs
 * @property Collection|JobModel[] $localizedJobs
 */
class JobCategoryModel extends AbstractApiModel
{
	const API_ENTITY_NAME = 'job-category';

	protected $relations = [
		'jobs' => ['class' => JobModel::class, 'many' => true],
	];

	public function getLocalizedJobs()
	{
		$language = explode('-', Yii::$app->language)[0];

		return $this->jobs->where('language', '==', $language);
	}
}
