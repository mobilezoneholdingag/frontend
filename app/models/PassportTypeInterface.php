<?php

namespace app\models;

interface PassportTypeInterface
{
	const ALL = [
		1 => 'Passport',
		2 => 'Identity card',
		3 => 'Driver license',
		4 => 'Residence permit B',
		5 => 'Residence permit C',
		6 => 'Residence permit G',
		7 => 'Residence permit L',
		8 => 'Residence permit F',
		9 => 'Residence permit N',
		10 => 'Certificate of registration',
	];
}
