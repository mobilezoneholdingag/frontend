<?php

namespace app\models;

use app\services\api\ApiClientInterface;

trait RestApiAdapter
{
	protected $data = [];

	/**
	 * @return ApiClientInterface
	 */
	public function getApiClient()
	{
		return di('api.client');
	}

	protected function getApiEntityName()
	{
		return null;
	}

	public function getData()
	{
		return $this->data;
	}

	/**
	 * @param array $data
	 *
	 * @return $this
	 */
	public function setData(array $data)
	{
		$this->data = $data;

		return $this;
	}

	protected function getApiData()
	{
		return $this->toArray();
	}

	public function create($entity = null)
	{
		if (!($entity = $entity ?? $this->getApiEntityName())) {
			throw new \InvalidArgumentException('No api entity name given.');
		}

		$data = $this->getApiClient()->post($entity, $this->getApiData());

		return $data;
	}

	/**
	 * @param null $entity
	 * @param null $id
	 * @param array $params
	 *
	 * @return mixed|null
	 */
	public function get($entity = null, $id = null, $params = [])
	{
		if (!($entity = $entity ?? $this->getApiEntityName())) {
			throw new \InvalidArgumentException('No api entity name given.');
		}

		$data = $this->getApiClient()->get($entity, $id, $params);

		return $data;
	}

	public function update($entity = null, $id = null, $params = [])
	{
		if (!($entity = $entity ?? $this->getApiEntityName())) {
			throw new \InvalidArgumentException('No api entity name given.');
		}

		$data = $this->getApiClient()->put($entity, $id, $params);

		return $data;
	}

	public function delete($entity = null, $id = null, $params = [])
	{
		if (!($entity = $entity ?? $this->getApiEntityName())) {
			throw new \InvalidArgumentException('No api entity name given.');
		}

		$data = $this->getApiClient()->delete($entity, $id, $params);

		return $data;
	}
}
