<?php

namespace app\models;

use app\helper\StringHelper;
use app\validators\OfAgeValidator;
use Yii;

/**
 * @property CustomerModel $customer
 */
class AddressModel extends AbstractApiModel
{
	const API_ENTITY_NAME = 'customer-address';

	protected $relations = [
		'customer' => ['class' => CustomerModel::class],
	];

	public $sex = GenderInterface::MALE;
	public $firstname;
	public $lastname;
	public $street;
	public $houseNumber;
	public $zip;
	public $city;
	public $contactPhoneNumber;
	public $formName = null;
	public $isDefault = false;
	public $birthday;

	public function __construct(array $config = [])
	{
		foreach ($config as $name => $val) {
			unset($config[$name]);
			$name = StringHelper::snakeToCamel($name);

			if ($this->hasProperty($name)) {
				$config[$name] = $val;
			}
		}

		parent::__construct($config);
	}

	public function formName()
	{
		if ($this->formName !== null) {
			return $this->formName;
		}

		return parent::formName();
	}

	public function fields()
	{
		$fields = parent::fields();

		unset($fields['formName']);

		return parent::fields();
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		$noDigitsPattern = '/^[^0-9]+$/';
		$containsAtLeastOneDigitPattern = '/^.*[0-9]+.*$/';
		$validateZipMessage = Yii::t('checkout', 'Validate_Zip_Message');

		// streetnumber is a string as it could be "17a"
		return [
			[['sex', 'firstname', 'lastname', 'street', 'zip', 'city', 'contactPhoneNumber', 'isDefault'], 'required',],
			['sex', 'in', 'range' => array_keys(GenderInterface::ALL)],
			[['firstname', 'lastname', 'street', 'houseNumber', 'city'], 'string', 'max' => 64],
			[['houseNumber'], 'string', 'max' => 6],
			[
				['zip'], 'integer', 'min' => 1000, 'max' => 9999,
				'tooSmall' => $validateZipMessage,
				'tooBig' => $validateZipMessage
			],
			[['contactPhoneNumber'], 'integer'],
			[['firstname', 'lastname', 'city'], 'match', 'pattern' => $noDigitsPattern],
			[['houseNumber'], 'match', 'pattern' => $containsAtLeastOneDigitPattern],
			[['birthday'], 'required', 'when' => function(AddressModel $model) {
				return $model->birthday === '';
			}],
			['birthday', 'date', 'format' => 'Y-m-d'],
			['birthday', OfAgeValidator::class],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'sex' => Yii::t('checkout', 'Gender'),
			'firstname' => Yii::t('checkout', 'Firstname'),
			'lastname' => Yii::t('checkout', 'Lastname'),
			'street' => Yii::t('checkout', 'Street'),
			'houseNumber' => Yii::t('checkout', 'Nr.'),
			'zip' => Yii::t('checkout', 'Zip_Code'),
			'city' => Yii::t('checkout', 'City'),
			'contactPhoneNumber' => Yii::t('checkout', 'Contact_Phone_Number'),
			'isDefault' => Yii::t('customer-account', 'Use_As_Default_Address'),
			'birthday' => Yii::t('checkout', 'Birthday').'*',
		];
	}

	// TODO too hacky!
	public function getContactPhoneNumber()
	{
		return '41' . $this->contactPhoneNumber;
	}
}
