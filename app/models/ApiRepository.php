<?php

namespace app\models;

use app\services\api\ApiClientInterface;

class ApiRepository
{
	/**
	 * @var string
	 */
	protected $className;
	protected $apiClient;

	public function __construct(ApiClientInterface $apiClient)
	{
		$this->apiClient = $apiClient;
	}

	public function model($className)
	{
		$this->className = $className;

		return $this;
	}

	protected function getParams()
	{
		return [];
	}

	protected function request($id, $params)
	{
		$entity = constant("{$this->className}::API_ENTITY_NAME");
		$params = array_merge($this->getParams(), $params);

		return $this->apiClient->get($entity, $id, $params);
	}

	public function find($id = null, $params = [], $forceSingle = false)
	{
		if (!$data = $this->request($id, $params)) {
			return null;
		}

		if (!$id && !$forceSingle) {
			$result = collect();

			foreach ($data as $row) {
				$result[] = new $this->className($row);
			}

			return $result;
		} else {
			return empty($data) ?  null : new $this->className($data);
		}
	}

	public function findOne($params)
	{
		$result = $this->find(null, $params);

		return $result ? $result->first() : null;
	}

	public function delete(AbstractApiModel $model)
	{
		$entity = $model::API_ENTITY_NAME;

		return $this->apiClient->delete($entity, $model->id);
	}
}
