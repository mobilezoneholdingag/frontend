<?php

namespace app\models;

class ProviderRepository extends ApiRepository
{
	protected $className = ProviderModel::class;
}
