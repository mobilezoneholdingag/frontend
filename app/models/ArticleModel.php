<?php

namespace app\models;

use app\modules\checkout\services\cart\ItemInterface;
use deinhandy\yii2picasso\models\ImageCollection;
use yii\helpers\Url;

/**
 * @property int $id
 * @property string $product_name
 * @property string $color
 * @property string $color_label
 * @property string $memory
 * @property string $image_overlay_text
 * @property string $description_1
 * @property string $description_2
 * @property int $product_category_id
 * @property AccessoriesModel[] $accessories
 * @property ArticleGroupModel $group
 * @property ImageCollection $images
 * @property ArticleInsuranceModel $current_insurance // TODO Insurance Information belongs to ArticleGroup!
 * @property ArticleInsuranceModel[] $insurances // TODO Insurance Information belongs to ArticleGroup!
 * @property ManufacturerModel $manufacturer
 * @property ProductCategoryModel $product_category
 * @property StockModel $stock
 * @property boolean $has_insurance // TODO Insurance Information belongs to ArticleGroup!
 */
class ArticleModel extends AbstractItemApiModel
{
	const ITEM_TYPE = ItemInterface::TYPE_ARTICLE;
	const API_ENTITY_NAME = 'article';
	const SIM_ONLY = '5522';

	public $insurance_id;

	public $relations = [
		'accessories' => ['class' => AccessoriesModel::class, 'many' => true],
		'current_insurance' => ['class' => ArticleInsuranceModel::class, 'field' => 'insurance_id'], // TODO Insurance Information belongs to ArticleGroup!
		'group' => ['class' => ArticleGroupModel::class, 'field' => 'group_id'],
		'manufacturer' => ['class' => ManufacturerModel::class, 'field' => 'manufacturer_id'],
		'product_category' => ['class' => ProductCategoryModel::class, 'field' => 'product_category_id'],
		'stock' => ['class' => StockModel::class, 'field' => 'stock_id'],
	];

	public function getInsurances()
	{
		return $this->group->insurances;
	}

	public function getHasInsurance()
	{
		return !empty($this->insurance_id);
	}

	public function getUrl()
    {
        return Url::to(['/product/product/' . strtolower($this->product_category_frontend_route_name), 'id' => $this->id, 'manufacturerTitle' => strtolower($this->manufacturer->title), 'groupUrlText' => $this->group->title]);
    }

	/**
	 * @inheritdoc
	 */
	public function serialize()
	{
		return array_merge(parent::serialize(), [
			'insurance_id' => $this->insurance_id
		]);
	}

	/**
	 * @inheritdoc
	 */
	public function unserialize($serialized)
	{
		$this->fetch($serialized['id'], ['expand' => 'accessories']);
		$this->position = $serialized['position'];
		$this->insurance_id = $serialized['insurance_id'] ?? null;
	}
}
