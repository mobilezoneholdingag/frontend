<?php

namespace app\models;

class FacetsModel extends AbstractApiModel
{
	const API_ENTITY_NAME = 'facets';

	protected $relations = [
		'provider' => ['class' => ProviderModel::class, 'many' => true],
	];
}
