<?php

namespace app\models;

use Yii;
use yii\base\Model;

class CheckoutEmarsysModel extends Model
{

    public $register = false;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['register'], 'filter', 'filter' => 'boolval'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'register' => Yii::t('checkout', 'Checkout_Newsletter_Checkbox'),
        ];
    }
}
