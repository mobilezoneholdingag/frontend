<?php

namespace app\models;

class LandingPageModel extends AbstractApiModel
{
	const API_ENTITY_NAME = 'landing-page';
	const IS_ACTIVE = 1;
}
