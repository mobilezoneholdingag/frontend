<?php

namespace app\models;

use Exception;
use Yii;

/**
 * @property integer id
 * @property integer customer_id
 * @property string number
 * @property string provider
 */
class CustomerPhoneNumberModel extends AbstractApiModel
{
	const API_ENTITY_NAME = 'customer-phone-number';

	protected $porting = false;
	public $number;
	public $provider;
	public $type;
	public $waitUntilContractEnds = true;

	/**
	 * @var null|CustomerModel
	 */
	public $customer = null;

	const SCENARIO_PORTING = 'porting';
	const TYPE_SUBSCRIPTION = 1;
	const TYPE_PREPAID = 2;

	public function fields()
	{
		$fields = array_merge(parent::fields(), [
			'number',
			'provider',
			'type',
			'waitUntilContractEnds',
			'porting',
			'customer_id'
		]);

		return $fields;
	}

	public function first()
	{
		$data = $this->fetch()->getData();

		if (!is_array($data) || count($data) === 0) {
			return null;
		}

		$this->setAttributes($data[0], false);

		return $this;
	}

	public function getScenario()
	{
		return $this->getPorting() ? static::SCENARIO_PORTING : static::SCENARIO_DEFAULT;
	}

	public function getPorting()
	{
		return $this->porting;
	}

	public function setPorting($data)
	{
		$this->porting = (bool) $data;

		return $this;
	}

	public function fetch($id = null, $params = NULL)
	{
		if (!static::API_ENTITY_NAME) {
			throw new Exception('API Model: need to define `API_ENTITY_NAME` property.');
		}

		$result = $this->get(null, null, [
			'customer_id' => $this->customer->id,
		]);

		if($result) {
			$this->data = array_merge($this->data, $result);
		}

		return $this;
	}

	public function getProviders()
	{
		$providers = [];

		foreach ((new ProviderModel)->fetch()->getData() as $provider) {
			$providers[$provider['id']] = $provider['title'];
		}

		return $providers;
	}

	public function getTypes()
	{
		return [
			static::TYPE_SUBSCRIPTION => \Yii::t('checkout', 'Number_Type_Subscription'),
			static::TYPE_PREPAID => \Yii::t('checkout', 'Number_Type_Prepaid'),
		];
	}

	public function scenarios()
	{
		return [
			static::SCENARIO_DEFAULT => [],
			static::SCENARIO_PORTING => ['number', 'type', 'provider', 'waitUntilContractEnds'],
		];
	}

	public function rules()
	{
		return [
			[['number', 'provider', 'type', 'waitUntilContractEnds'], 'required'],
			['provider', 'in', 'range' => array_keys($this->getProviders())],
			['type', 'in', 'range' => array_keys($this->getTypes())],
			[['porting', 'waitUntilContractEnds'], 'boolean'],
		];
	}
	
	public function attributeLabels()
	{
		return [
			'number' => \Yii::t('product', 'Current_Mobile_Number'),
			'provider' => \Yii::t('product', 'Current_Provider'),
			'type' => \Yii::t('product', 'Current_Contract_Type'),
			'porting' => \Yii::t('view', 'Tariff_Selection_Porting'),
			'waitUntilContractEnds' => \Yii::t('product', 'Wait_Until_End_Of_Contract'),
		];
	}
}
