<?php

namespace app\models;

use app\modules\checkout\services\cart\ItemInterface;
use Illuminate\Support\Collection;
use Yii;

/**
 * @property ProviderModel $provider
 * @property Collection|ArticleTariffModel[] $articleTariffs
 * @property int $id
 * @property int $provider_id
 * @property int $offer_type_id
 * @property string $title
 * @property string $text_call
 * @property string $text_sms
 * @property string $text_internet
 * @property string $footnote_text
 * @property int $rate_payment_count
 * @property string $specific_legal_text
 */
class TariffModel extends AbstractItemApiModel
{
    const ITEM_TYPE = ItemInterface::TYPE_TARIFF;
	const OFFER_TYPE_NEW = 1;
	const OFFER_TYPE_PORTING = 1; // yes, "porting" has the same id like "new contract" *sigh*
	const OFFER_TYPE_RENEW = 3;
	const OFFER_TYPE_DATA = 4;
	const API_ENTITY_NAME = 'tariff';

	protected $relations = [
		'provider' => ['class' => ProviderModel::class],
		'articleTariffs' => ['class' => ArticleTariffModel::class, 'many' => true],
		'simOnlyArticleTariff' => ['class' => ArticleTariffModel::class, 'field' => 'sim_only_article_tariff_id'],
	];

	public function getTariffHighlights()
	{
		$highlights = [];

		if ($this->text_call) {
			$highlights[] = [
				'title' => Yii::t('view', 'Tariff_info_call'),
				'subtext' => $this->text_call,
			];
		}

		if ($this->text_internet) {
			$highlights[] = [
				'title' => Yii::t('view', 'Tariff_info_internet'),
				'subtext' => $this->text_internet,
			];
		}

		if ($this->text_sms) {
			$highlights[] = [
				'title' => Yii::t('view', 'Tariff_info_sms'),
				'subtext' => $this->text_sms,
			];
		}

		return $highlights;
	}
}
