<?php

namespace app\models;

/**
 * @property string $language
 * @property string $title
 * @property string $location
 * @property string $text
 * @property string $job_id
 */
class JobModel extends AbstractApiModel
{
	const API_ENTITY_NAME = 'job';
}
