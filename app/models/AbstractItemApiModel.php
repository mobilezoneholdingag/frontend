<?php

namespace app\models;

use app\modules\checkout\services\cart\ItemInterface;

abstract class AbstractItemApiModel extends AbstractApiModel implements ItemInterface
{
	const ITEM_TYPE = 'default';

	/**
	 * position inside of the cart
	 *
	 * @var int
	 */
	public $position;

	public function getData()
	{
		return parent::getData() + ['identifier' => $this->getIdentifier()];
	}

	/**
	 * @inheritdoc
	 */
	public function serialize()
	{
		return [
			'id' => $this->data['id'],
			'position' => $this->position,
		];
	}

	/**
	 * @inheritdoc
	 */
	public function unserialize($serialized)
	{
		$this->fetch($serialized['id']);
		$this->position = $serialized['position'];
	}

	/**
	 * @return string
	 */
	public function getIdentifier()
	{
		return implode('-', [
			static::ITEM_TYPE,
			$this->data['id'] ?? null,
			$this->position
		]);
	}

	public function getItemType()
	{
		return static::ITEM_TYPE;
	}
}
