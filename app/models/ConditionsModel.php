<?php

namespace app\models;

use app\modules\checkout\services\cart\CartInterface;
use app\modules\checkout\services\cart\ItemInterface;
use app\validators\ConfirmedValidator;
use Yii;
use yii\base\Model;
use yii\helpers\Url;

/**
 * @property string $insuranceLink
 * @property string $insuranceTitle
 * @property string $providerLink
 * @property string $providerTitle
 */
class ConditionsModel extends Model
{
	public $conditionsStore = false;
	public $conditionsProvider = false;
	public $conditionsInsurance = false;
	public $conditionsTariff = false;

	public $provider;

	public function getInsuranceLink()
	{
		$insurance = $this->getInsurance();

		return $insurance ? $insurance->term_link : null;
	}

	public function getInsuranceTitle()
	{
		$insurance = $this->getInsurance();

		return $insurance ? $insurance->manufacturer->title : null;
	}

	public function getProviderLink()
	{
		$provider = $this->getProvider();

		return $provider ? $provider->link_tos : null;
	}

	public function getProviderTitle()
	{
		$provider = $this->getProvider();

		return $provider ? $provider->title : null;
	}

	protected function getProvider()
	{
		/** @var CartInterface $cart */
		$cart = di(CartInterface::class);

		/** @var ItemInterface $item */
		foreach ($cart->getItems() as $item) {
			if ($item->getItemType() === ItemInterface::TYPE_ARTICLE_TARIFF) {
				/** @var ArticleTariffModel $item */
				return $item->tariff->provider;
			}

			if ($item->getItemType() === ItemInterface::TYPE_ARTICLE) {
				/** @var TariffModel $item */
				return $item->provider;
			}
		}

		return null;
	}

	protected function hasTariff() {
        $cart = di(CartInterface::class);
        return $cart->hasTariff();
    }

	protected function getInsurance()
	{
		/** @var CartInterface $cart */
		$cart = di(CartInterface::class);

		/** @var ItemInterface $item */
		foreach ($cart->getItems() as $item) {
			if (in_array($item->getItemType(), [ItemInterface::TYPE_ARTICLE_TARIFF, ItemInterface::TYPE_ARTICLE])) {
				/** @var ArticleTariffModel|ArticleModel $item */
				return $item->has_insurance ? $item->current_insurance : null;
			}
		}

		return null;
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['conditionsStore', 'conditionsProvider', 'conditionsInsurance', 'conditionsTariff'], 'filter', 'filter' => 'boolval'],

			['conditionsStore', ConfirmedValidator::class],

			['conditionsProvider', ConfirmedValidator::class, 'when' => function(ConditionsModel $model) {
				return $model->getProviderLink();
			}],

			['conditionsInsurance', ConfirmedValidator::class, 'when' => function(ConditionsModel $model) {
				return $model->getInsuranceLink();
			}],

            ['conditionsTariff', ConfirmedValidator::class, 'when' => function(ConditionsModel $model) {
                return $model->hasTariff();
            }],

		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		$conditionsLink = '
			<a class="text-danger" target="_blank" href="'.Url::toRoute('/page/support/terms').'">
				mobilezone
			</a>';

        if(is_object($this->getProvider())) {
            if ($this->getProvider()->id == 13) {
                if (Yii::$app->language == "de-CH") {
                    $providerlink = "https://www.upc.ch/de/allgemeine-geschaeftsbedingungen/";
                } elseif (Yii::$app->language == "fr-FR") {
                    $providerlink = "https://www.upc.ch/fr/conditions-generales/";
                } elseif (Yii::$app->language == "it-IT") {
                    $providerlink = "https://www.upc.ch/it/condizioni-generali/";
                }
                $conditionsProviderLink = '
            <a class="text-danger" target="_blank" href="' . $providerlink . '">
                ' . $this->providerTitle . '
            </a>';
            }else{
                $conditionsProviderLink = '
                <a class="text-danger" target="_blank" href="' . $this->providerLink . '">
                    ' . $this->providerTitle . '
                </a>';
            }
        } else {
            $conditionsProviderLink = '
			<a class="text-danger" target="_blank" href="' . $this->providerLink . '">
				' . $this->providerTitle . '
			</a>';
        }
        $conditionsInsuranceLink = '
        <a class="text-danger" target="_blank" href="' . $this->insuranceLink . '">
            ' . $this->insuranceTitle . '
        </a>';


		return [
			'conditionsStore' => Yii::t('checkout', 'Conditions_With_Link {link}!', ['link' => $conditionsLink]),
			'conditionsProvider' => Yii::t('checkout', 'Conditions_Provider_With_Link {link}!', ['link' => $conditionsProviderLink]),
			'conditionsInsurance' => Yii::t('checkout', 'Conditions_Insurance_With_Link {link}!', ['link' => $conditionsInsuranceLink]),
            'conditionsTariff' => Yii::t('checkout', 'Conditions_Tariff'),
		];
	}
}
