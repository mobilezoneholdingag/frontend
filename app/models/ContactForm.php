<?php

namespace app\models;

use Yii;

/**
 * ContactForm is the model behind the contact form.
 */
class ContactForm extends AbstractApiModel
{
	public $answere;
	public $question;
	public $request;
	public $firstname;
	public $lastname;
	public $street;
	public $number;
	public $zip;
	public $place;
	public $email;
	public $tel;
	public $captcha;
	public $recipient;
	private $emailOnlineshop = 'onlineshop@mobilezone.ch';
	private $emailMZHolding = 'mobilezoneholding@mobilezone.ch';
	private $emailMZInfo = 'info@mobilezone.ch';

	/**
	 * @inheritdoc
	 */
	public function rules() : array
	{
		return [
			// name, email, subject and body are required
			[['firstname', 'lastname', 'tel', 'email', 'captcha', 'answere'], 'required'],
			['captcha', 'captcha', 'captchaAction' => 'page/about/captcha'],
			['email', 'email'],
			[
				[
					'question',
					'request',
					'firstname',
					'lastname',
					'street',
					'place',
					'number',
					'answere',
					'recipient',
					'tel',
				],
				'string',
			],
			[['zip'], 'number'],
			['answere', 'in', 'range' => array_keys($this->getAllAnswers())],
			['question', 'in', 'range' => array_keys($this->getAllQuestions())],

		];
	}

	public function getAllAnswers() : array
	{
		return [
			1 => \Yii::t('staticcontent', 'Page_Contact_Tel'),
			2 => \Yii::t('staticcontent', 'Page_Contact_Email'),
		];
	}

	public function getAllAnswersEn() : array
	{
		return [
			1 => 'Telephone',
			2 => 'E-Mail',
		];
	}

	public function getAllQuestions() : array
	{
		return [
			2 => \Yii::t('staticcontent', 'Page_Contact_Topic_General'),
			3 => \Yii::t('staticcontent', 'Page_Contact_Topic_Order'),
			4 => \Yii::t('staticcontent', 'Page_Contact_Topic_Investor'),
			5 => \Yii::t('staticcontent', 'Page_Contact_Topic_Public'),
		];
	}

	public function getAllQuestionsEn() : array
	{
		return [
			1 => 'Topics',
			2 => 'Gerneral request',
			3 => 'Customer feedback',
			4 => 'Investor relations',
			5 => 'Media requests',
		];
	}

	public function getViewQuestions() : array
	{
		$questions = [1 => \Yii::t('staticcontent', 'Page_Contact_Topic')] + $this->getAllQuestions();

		return $questions;
	}

	public function attributeLabels() : array
	{
		$en = Yii::$app->controller->action->id === 'contact-en';
		$regularLabels = [
			'question' => \Yii::t('staticcontent', 'Page_Contact_Question_Label'),
			'request' => \Yii::t('staticcontent', 'Page_Contact_Request_Label'),
			'firstname' => \Yii::t('staticcontent', 'Page_Contact_Firstname'),
			'lastname' => \Yii::t('staticcontent', 'Page_Contact_Lastname'),
			'street' => \Yii::t('staticcontent', 'Page_Contact_Street'),
			'number' => \Yii::t('staticcontent', 'Page_Contact_Housenumber'),
			'zip' => \Yii::t('staticcontent', 'Page_Contact_Zip'),
			'place' => \Yii::t('staticcontent', 'Page_Contact_Place'),
			'tel' => \Yii::t('staticcontent', 'Page_Contact_Tel'),
			'email' => \Yii::t('staticcontent', 'Page_Contact_Email'),
			'answere' => \Yii::t('staticcontent', 'Page_Contact_Answere'),
			'captcha' => 'Captcha*',
		];
		$englishLabels = [
			'question' => 'Inquiry',
			'request' => 'State your issue',
			'firstname' => 'First name',
			'lastname' => 'Last name',
			'street' => 'Street',
			'number' => 'House number',
			'zip' => 'Zip code',
			'place' => 'Place',
			'tel' => 'Telephone',
			'email' => 'E-Mail',
			'answere' => 'Type of answere',
			'captcha' => 'Captcha*',
		];

		return $en ? $englishLabels : $regularLabels;
	}

	public function sendEmail()
	{
		$this->recipient = $this->emailOnlineshop;

		if ($this->question == 2) {
			$this->recipient = $this->emailMZInfo;
		}

		if ($this->question == 4 || $this->question == 5) {
			$this->recipient = $this->emailMZHolding;
		}

		$this->answere = $this->getAllAnswers()[$this->answere];
		$this->question = $this->getAllQuestions()[$this->question];

		return $this->getApiClient()->post('homer/mail/send-contact', $this->toArray());
	}
}
