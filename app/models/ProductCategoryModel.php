<?php

namespace app\models;

class ProductCategoryModel extends AbstractItemApiModel
{
	const SIM_CARD = 11;
	const SMARTPHONE = 1;
	const TABLET = 48;
	const SMARTWATCH = 69;
	const INSURANCE = 41;

	const API_ENTITY_NAME = 'product-category';
}
