<?php

namespace app\models;

use app\helper\ArrayHelper;
use Yii;

class CustomerRepository extends ApiRepository
{
	protected $className = CustomerModel::class;

	/**
	 * generates a token and sends an email in through the api
	 *
	 * @param CustomerModel $customer
	 *
	 * @return mixed
	 */
	public function forgotPassword(CustomerModel $customer)
	{
		return $this->apiClient->post('customer/send-password-reset-mail', [
			'email' => $customer->email,
			'frontendUrl' => Yii::$app->urlManager->hostInfo,
		]);
	}

	public function resetPassword($token, CustomerModel $customer)
	{
		return $this->apiClient->post('customer/reset-password', [
			'token' => $token,
			'password' => $customer->newPassword,
		]);
	}

	public function changePassword(CustomerModel $customer)
	{
		return $this->apiClient->put('customer', $customer->id, [
			'password_hash' => $customer->newPassword,
		]);
	}

	public function save(CustomerModel $customer)
	{
		$params = $customer->toArray();
		$params = ArrayHelper::keysToSnake($params);

		$params = collect($params)
			->only($customer->safeAttributes())
			->toArray();

		if ($customer->id) {
			$this->apiClient->put('customer', $customer->id, $params);
		} else {
			$data = $this->apiClient->post('customer', $params);
		}

		if (isset($data['id'])) {
			$customer->id = $data['id'];
		}

		return $customer;
	}

	public function getAllLanguages()
	{
		return $this->apiClient->get('customer/all-languages');
	}
}
