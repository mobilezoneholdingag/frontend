<?php

namespace app\models;

/**
 * @property ArticleTariffModel $article_tariff
 * @property float[] $prices
 */
class SimCardModel extends ArticleModel
{
	const ITEM_TYPE = null;

	public $relations = [
		'article_tariff' => ['class' => ArticleTariffModel::class],
	];
}
