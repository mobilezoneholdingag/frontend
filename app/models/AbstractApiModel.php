<?php

namespace app\models;

use app\helper\StringHelper;
use app\services\api\ApiCollection as Collection;
use Closure;
use yii\base\Exception;
use yii\base\Model;

abstract class AbstractApiModel extends Model
{
	use RestApiAdapter;

	const API_ENTITY_NAME = null;
	protected $relations = [];

	public function __construct($config = [])
	{
		foreach ($config as $name => $value) {
			if (isset($this->relations[$name])) {
				$this->data[$name] = $value;
				unset($config[$name]);
			}
		}

		parent::__construct($config);
	}

	/**
	 * @param string $name
	 *
	 * @return mixed
	 */
	public function __get($name)
	{
		// forward public calls to data attribute
		// array keys check because if using isset() it will not recognize null values
		if (in_array($name, array_keys($this->data)) && !isset($this->relations[$name])) {
			return $this->data[$name];
		}

		// check for getter methods
		$camelCase = ucfirst(StringHelper::snakeToCamel($name));
		if ($this->hasMethod('get' . $camelCase)) {
			return $this->{'get' . $camelCase}();
		}

		// check for bool getter
		if ($this->hasMethod('is' . $camelCase)) {
			return $this->{'is' . $camelCase}();
		}

		// resolve relations if needed
		if (isset($this->relations[$name])) {
			return $this->getRelationData($name);
		}

		try {
			return parent::__get($name);
		} catch (\Exception $exception) {
			return null;
		}
	}

	public function __set($name, $value)
	{
		try {
			parent::__set($name, $value);
		} catch (\Exception $exception) {
			$this->{$name} = $value;
		}
	}

	protected function getRelationData(string $name)
	{
		$relation = $this->relations[$name];
		$params = isset($relation['expand']) ? ['expand' => implode(',', $relation['expand'])] : [];

		if (isset($relation['many']) && $relation['many'] === true) {
			$this->resolveRelationMany($name, $params);
		} else {
			$this->resolveRelationOne($name, $params);
		}

		return $this->{$name};
	}

	// TODO: implement magic isset according to the __get logic

	protected function resolveRelationMany(string $name, array $params = []): self
	{
		$relation = $this->relations[$name];
		$className = $relation['class'];
		$lazyLoading = $relation['lazy'] ?? true;

		$data = $this->getValueForField($relation['field'] ?? $name);

		$collection = new Collection();
		$this->{$name} = &$collection;

		foreach ($data ?? [] as $item) {
			$collection->push($this->getModel($item, $className, $lazyLoading, $relation, $params));
		}

		return $this;
	}

	protected function getValueForField(string $field)
	{
		$parts = explode('.', $field);
		$first = array_shift($parts);

		$value = $this->{$first} ?? ($this->data[$first] ?? null);

		foreach ($parts as $part) {
			$value = $value[$part];
		}

		return $value;
	}

	private function getModel($data, string $className, bool $lazyLoading, array $relation, array $params = [])
	{
		$model = $data;

		if (is_array($data)) {
			/** @var AbstractApiModel $model */
			$model = new $className($data);
			$model->setData($data);
		} elseif (is_scalar($data)) {
			/** @var AbstractApiModel $model */
			$model = $lazyLoading
				? $this->lazyLoadingHolder($className, $data, $params)
				: $this->fetchModel($className, $data, $params);
		}

		// reverse relation
		if ($model !== null && isset($relation['reverse'])) {
			$model->{$relation['reverse']} = $this;
		}

		return $model;
	}

	protected function lazyLoadingHolder(string $className, int $id, $params = []): Closure
	{
		return function () use ($className, $id, $params) {
			/** @var AbstractApiModel $model */
			$model = new $className();
			$model->fetch($id, $params);

			return $model;
		};
	}

	public function fetch($id = null, $params = null)
	{
		if (!static::API_ENTITY_NAME) {
			throw new Exception('API Model: need to define `API_ENTITY_NAME` property.');
		}

		$data = $this->get(static::API_ENTITY_NAME, $id, $params);
		$this->data = array_merge($this->data, is_array($data) ? $data : []);

		// set public properties as well
		foreach ($data ?? [] as $key => $val) {
			if ($this->hasProperty($key)) {
				$this->{$key} = $val;
			}
		}

		return $this;
	}

	protected function fetchModel(string $className, int $id, array $params = []): self
	{
		/** @var AbstractApiModel $model */
		$model = new $className();
		$model->fetch($id, $params);

		return $model;
	}

	protected function resolveRelationOne(string $name, array $params = []): self
	{
		$relation = $this->relations[$name];
		$className = $relation['class'];
		$keyField = $relation['field'] ?? $name;

		$data = $this->getValueForField($keyField);

		if (is_array($data)) {
			$this->{$name} = new $className(['data' => $data]);
		} elseif (is_scalar($data)) {
			/** @var AbstractApiModel $model */
			$model = new $className();

			$this->{$name} = $model->fetch($data, $params);
		} else {
			$this->{$name} = null;
		}

		//reverse relation
		if ($this->{$name} !== null && isset($relation['reverse'])) {
			$this->{$name}->{$relation['reverse']} = $this;
		}

		return $this;
	}

	protected function getApiEntityName()
	{
		return static::API_ENTITY_NAME;
	}
}
