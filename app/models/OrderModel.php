<?php

namespace app\models;

use app\modules\checkout\services\cart\ItemInterface;
use Illuminate\Support\Collection;

/**
 * @property Collection|OrderModel $items
 * @property Collection|OrderModel $checkoutItems
 * @property string $status_code_label
 * @property string payment_method
 * @property int $status
 */
class OrderModel extends AbstractApiModel
{
	const API_ENTITY_NAME = 'order/order-info';
	const TAX_CALCULATION_VALUE = 108;

	const STATUS_NEW = 1;
	const STATUS_RECEIVED = 2;
	const STATUS_ON_HOLD = 3;
	const STATUS_PENDING_PSP_CAPTURE = 4;
	const STATUS_PAYMENT_SUCCESSFUL = 5;
	const STATUS_PAYMENT_INCOMPLETE = 6;
	const STATUS_DECLINED = 7;
	const STATUS_PAYMENT_FAILED = 8;
	const STATUS_COMMISSIONED = 9;
	const STATUS_SHIPPED = 10;
	const STATUS_AWAITING_PREPAYMENT = 11;

	protected $relations = [
		'items' => ['class' => OrderItemModel::class, 'many' => true],
	];

	public function orderHasTariff()
	{
		foreach ($this->items as $item) {
			if($item->item_product_type_id == ItemInterface::TYPE_TARIFF) {
				return true;
			}
		}

		return false;
	}

	public function getCoupon()
	{
		foreach ($this->items as $item) {
			if($item->item_product_type_id == ItemInterface::TYPE_COUPON) {
				return $item->article;

			}
		}

		return false;
	}

	public function getCheckoutItems()
	{
		return $this->items->where('item_product_type_id', '!=', ItemInterface::TYPE_ARTICLE_TARIFF)->all();
	}
}
