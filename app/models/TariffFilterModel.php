<?php

namespace app\models;

use Illuminate\Support\Collection;

/**
 * @property integer $total
 * @property Collection|TariffModel[] $tariffs
 * @property FacetsModel $facets
 */
class TariffFilterModel extends AbstractApiModel
{
	const API_ENTITY_NAME = 'tariff/search';

	protected $relations = [
		'facets' => ['class' => FacetsModel::class],
		'tariffs' => ['class' => TariffModel::class, 'many' => true, 'field' => 'items'],
	];
}
