<?php

namespace app\models;

use app\modules\checkout\services\cart\InvoiceDataExtractor;
use Yii;
use yii\web\Session;

class PaymentModel extends AbstractApiModel
{
	private $type;
	public $birthday;
	private $allowBill = true;
	private $types = null;
	public $payload = [];

	const TYPE_BILL = 'account-purchase';
	const TYPE_CREDIT_CARD = 'credit-card';
	const POSTFINANCE_E_FINANCE = 'pf-e-finance';
	const POSTFINANCE_CARD = 'pf-card';
	const PREPAYMENT = 'pre-payment';
	const NO_PAYMENT = 'no-payment-required';

	// TODO extend API endpoint so we can ask for the available payment types, but without doing a pre-screening
	protected $allowedTypes = [
		self::TYPE_BILL => 'Payment_Bill',
		self::TYPE_CREDIT_CARD => 'Payment_Credit_Card',
		self::POSTFINANCE_E_FINANCE => 'Payment_Postfinance',
		self::POSTFINANCE_CARD => 'Payment_E_Finance',
		self::PREPAYMENT => 'Pre_Payment',
		self::NO_PAYMENT => 'Payment_Not_Required',
	];

	const API_ENTITY_NAME = 'payment/methods';
	const SESSION_KEY_PAYMENT_TYPES = 'payment_types';
	const SESSION_KEY_PAYMENT_PAYLOAD = 'payment_payload';
	const FAILED_PAYMENT_METHODS_SESSION_KEY = 'failed_payment_methods';

	public function __construct(array $config = [])
	{
		parent::__construct($config);

		// TODO: extract saving logic for payment payload and handle it a better way
		/** @var Session $session */
		$session = di('session');
		if ($session->has(self::SESSION_KEY_PAYMENT_PAYLOAD)) {
			$this->payload = $session->get(self::SESSION_KEY_PAYMENT_PAYLOAD);
		}
		if ($session->has(self::SESSION_KEY_PAYMENT_TYPES)) {
			$this->types = $session->get(self::SESSION_KEY_PAYMENT_TYPES);
		}
	}

	/**
	 * @return array
	 */
	public function fields() {
		$fields = parent::fields();
		$fields[] = 'type';
		return $fields;
	}

	/**
	 * @return string
	 */
	public function getType(): string
	{
		if (!$this->type) {
			$this->type = array_keys($this->getTypes())[0];
		}

		return $this->type;
	}

	/**
	 * @param $type
	 *
	 * @return PaymentModel
	 */
	public function setType($type): PaymentModel
	{
		$this->type = $type;

		return $this;
	}

	public function getAllowBill()
	{
		return $this->allowBill;
	}

	// TODO: need to prevent from doing requests all the time
	public function getTypes()
	{
		if ($this->types === null) {
			$this->types = [];

			/** @var InvoiceDataExtractor $extractor */
			$extractor = di(InvoiceDataExtractor::class);
			$params = $extractor->extract();

			if ($params['total'] < 0.01) {
				$this->types = $this->allowedTypes;

				return $this->types;
			}

			$data = $this->getApiClient()->post(self::API_ENTITY_NAME, $params);

			if (is_array($data)) {
				foreach ($data as $type) {
					$name = $type['name'];

					if (isset($this->allowedTypes[$name])) {
						$this->types[$name] = $this->allowedTypes[$name];
					}

					// if payload is given, store it
					if (isset($type['payload'])) {
						$this->payload[$name] = $type['payload'];
						// TODO: see above, extract the storage of payment payload
						di('session')[self::SESSION_KEY_PAYMENT_PAYLOAD] = $this->payload;
					}
				}
			}

			di('session')[self::SESSION_KEY_PAYMENT_TYPES] = $this->types;
		}

		$this->removeFailedPaymentMethods();
		return $this->types;
	}

	private function removeFailedPaymentMethods()
	{
		foreach (Yii::$app->session->get(self::FAILED_PAYMENT_METHODS_SESSION_KEY, []) as $paymentMethodKey) {
			if (array_key_exists($paymentMethodKey, $this->types)) {
				unset($this->types[$paymentMethodKey]);
			}
		}
	}

	public function rules()
	{
		return [
			[['type'], 'required'],
			[['birthday'], 'required', 'when' => function(PaymentModel $model) {
				return $model->type == static::TYPE_BILL;
			}],
			['type', 'in', 'range' => array_keys($this->getTypes())],
		];
	}
	
	public function attributeLabels()
	{
		return [
			'type' => 'Payment_Method',
			'birthday' => Yii::t('checkout', 'Birthday'),
		];
	}
}
