<?php

namespace app\models;

use app\modules\checkout\services\cart\CartTotalsCalculator;
use app\modules\checkout\services\cart\ItemInterface;
use Yii;

/**
 * @property float $minimum_price
 */
class CouponModel extends AbstractItemApiModel
{
    const ITEM_TYPE = ItemInterface::TYPE_COUPON;

	const API_ENTITY_NAME = 'coupon';

	protected $relations = ['validate_code'];

	protected $code;
	protected $value = 0;

    public function fetch($code = null, $params = null)
    {
    	if (!$code && $this->code) {
    		$code = $this->code;
	    }

	    parent::fetch($code);

	    if (isset($this->data['id'])) {
	    	$this->setCode($this->data['code']);
	    	$this->setValue($this->data['discount_device_price']);
	    }

	    return $this;
    }

	public function validate($attributeNames = null, $clearErrors = true)
	{
		/** @var CartTotalsCalculator $cartTotalsCalculator */
		$cartTotalsCalculator = di(CartTotalsCalculator::class);

        if (!isset($this->data['id']) || $this->is_valid == 0 || !$this->validate_code) {
            Yii::$app->session->addFlash('danger', Yii::t('checkout','Invalid_Coupon'));

            return false;
        }

		if ($this->minimum_price && $cartTotalsCalculator->getTotalOnce() < $this->minimum_price) {
			Yii::$app->session->addFlash(
				'danger',
				Yii::t('checkout', 'Coupon_Total_Too_Low', ['amount' => $this->minimum_price])
			);

			return false;
		}

		return true;
	}

	/**
     * @return float|int
     */
    public function getValue()
    {
        return $this->value;
    }

	public function setValue($value)
	{
		$this->value = (float) $value;

		return $this;
    }

    /**
     * @return null|string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param null|string $code
     * @return CouponModel
     */
    public function setCode($code): CouponModel
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function serialize()
    {
        return ['position' => $this->position] + $this->getData();
    }

    /**
     * @inheritdoc
     */
    public function unserialize($serialized)
    {
        $this->fetch($serialized['code']);
    }

    public function getIdentifier()
    {
        return static::ITEM_TYPE . $this->code;
    }

    public function getItemType()
    {
        return static::ITEM_TYPE;
    }
}
