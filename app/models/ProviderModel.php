<?php

namespace app\models;

use deinhandy\yii2picasso\models\Image;

/**
 * @property integer $id
 * @property string $title
 * @property Image $image
 * @property boolean $renewable
 */
class ProviderModel extends AbstractApiModel
{
	const API_ENTITY_NAME = 'provider';
	const UPC_ID = '13';
	const WINGO_ID = '30';
	const SWISSCOM_ID = '2';

	public static function getSwisscomProvider()
	{
		return di(ProviderRepository::class)->findOne(['system_title' => 'swisscom']);
	}
}
