<?php

namespace app\models;

use Yii;

/**
 * @property int $id
 * @property string $email
 * @property int $status
 * @property CustomerModel $customer
 */
class NewsletterModel extends AbstractApiModel
{
	const API_ENTITY_NAME = 'newsletter';

	const STATUS_NEVER_SUBSCRIBED = 1;
	const STATUS_DOI_STARTED = 2;
	const STATUS_DOI_CONFIRMED = 3;
	const STATUS_UNSUBSCRIBED = 4;

	const SCENARIO_REGISTER = 'register';
	const SCENARIO_CUSTOMER_ACCOUNT = 'customer-account';
	const SCENARIO_HOMEPAGE = 'homepage';

	public $sex = GenderInterface::MALE;
	public $firstname;
	public $lastname;
	public $active = null;

	protected $relations = [
		'customer' => ['class' => CustomerModel::class],
	];

	public function fields()
	{
		return parent::fields();
	}

	public function scenarios()
	{
		$sharedRules = [
			'firstname',
			'lastname',
			'sex',
		];

		return [
			self::SCENARIO_CUSTOMER_ACCOUNT => array_merge($sharedRules, ['active']),
			self::SCENARIO_HOMEPAGE => array_merge($sharedRules, ['email']),
		];
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['firstname', 'lastname', 'active', 'sex'], 'required'],
			['sex', 'in', 'range' => array_keys(GenderInterface::ALL)],
			[['active'], 'boolean'],
			[['firstname', 'lastname'], 'string', 'max' => 64],
			['email', 'required'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'firstname' => Yii::t('checkout', 'Firstname'),
			'lastname' => Yii::t('checkout', 'Lastname'),
			'active' => Yii::t('customer-account', 'Newsletter_Confirmation_Advertisement'),
		];
	}

	public function getStatus()
	{
		if ($this->active === null) {
			return self::STATUS_NEVER_SUBSCRIBED;
		}

		return $this->active == true ? self::STATUS_DOI_STARTED : self::STATUS_UNSUBSCRIBED;
	}

	public function setStatus($status)
	{
		$this->active = $status === self::STATUS_DOI_STARTED;

		return $this;
	}
}
