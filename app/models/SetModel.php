<?php

namespace app\models;

use Illuminate\Support\Collection;

/**
 * @property Collection|SetItemModel[] $items
 */
class SetModel extends SetSearchModel
{
	const API_ENTITY_NAME = 'set/set';

	public $relations = [
		'items' => ['class' => SetItemModel::class, 'many' => true],
	];

}
