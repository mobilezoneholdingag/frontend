<?php

namespace app\models;

/**
 * @property int id
 * @property string title
 * @property string description_1
 * @property string description_2
 * @property ArticleModel articles
 * @property ArticleInsuranceModel insurances
 */
class ArticleGroupModel extends AbstractApiModel
{
	const API_ENTITY_NAME = 'article-group';

	public $relations = [
		'articles' => ['class' => ArticleModel::class, 'many' => true, 'field' => 'articles_in_group'],
		'insurances' => ['class' => ArticleInsuranceModel::class, 'many' => true],
	];
}
