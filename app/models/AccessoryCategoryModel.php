<?php

namespace app\models;

class AccessoryCategoryModel extends AbstractItemApiModel
{
	const API_ENTITY_NAME = 'accessory-category';

	public static function isAccessoryCategory($category): bool
	{
		$categories = di(ApiRepository::class)->model(AccessoryCategoryModel::class)->find();

		foreach ($categories as $value){
			if ($category === $value->title) {
				return true;
			}
		}

		return false;
	}
}
