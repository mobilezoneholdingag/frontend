<?php

namespace app\models;

use Illuminate\Support\Collection;

/**
 * @property Collection|FacetsModel $facets
 * @property Collection|SetItemModel[] $items
 * @property integer $total
 */
class SetSearchModel extends AbstractApiModel
{
	const API_ENTITY_NAME = 'set/set/search';
	const INITIAL_ITEM_LIMIT = 8;
	public $relations = [
		'facets' => ['class' => FacetsModel::class],
		'items' => ['class' => ArticleModel::class, 'many' => true],
	];

	public function getArticlesOnly()
	{
		return $this->items;
	}
}
