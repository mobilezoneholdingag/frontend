<?php

namespace app\models;

/**
 * @property ArticleModel $articles
 * @property ArticleGroupModel $group
 * @property ArticleModel $selected_article
 * @property string $system_title
 */
class SetItemModel extends AbstractApiModel
{
	const API_ENTITY_NAME = 'set/set';

	public $relations = [
		'articles' => ['class' => ArticleModel::class, 'many' => true],
		'group' => ['class' => ArticleGroupModel::class],
		'selected_article' => ['class' => ArticleModel::class],
	];
}
