<?php

namespace app\models;

use app\services\auth\AuthProvider;

class SwisscomRepository extends ApiRepository
{
	protected $className = SwisscomModel::class;

	public function getLoginUrl()
	{
		$response = $this->find('auth/start', []);

		return $response->url;
	}

	public function getAccessToken(string $code)
	{
		$response = $this->find('auth/request-token', ['code' => $code]);

		return $response->token;
	}

	public function getUrlForContractSelection(string $token)
	{
		$response = $this->find('auth/start-select-subscription', ['token' => $token]);

		return $response->url;
	}

	public function getRetentionOffers(string $token, string $key, int $articleId = null): SwisscomRetentionModel
	{
		/** @var CustomerModel $customer */
		$customer = di(AuthProvider::class)->user();

		$response = $this->model(SwisscomRetentionModel::class)->find(
			'check-retention',
			[
				'accessToken' => $token,
				'subscriptionKey' => $key,
				'articleId' => $articleId,
				'customerId' => $customer->id ?? null,
			]
		);

		return $response;
	}
}
