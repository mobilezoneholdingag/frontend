<?php

namespace app\models;

use app\modules\checkout\services\cart\ItemInterface;
use app\twig\CurrencyExtension;
use Yii;

/**
 * @property ArticleModel $article
 * @property TariffModel $tariff
 * @property ArticleInsuranceModel[] $insurances
 * @property ArticleInsuranceModel $current_insurance
 * @property SimCardModel $simCard
 * @property SimCardModel $sim_card
 * @property boolean $has_insurance
 * @property array $prices
 * @property int $price_article_monthly
 * @property int $price_article_first_month
 * @property int $price_tariff_monthly
 * @property int $price_article_total
 */
class ArticleTariffModel extends AbstractItemApiModel
{
	const ITEM_TYPE = ItemInterface::TYPE_ARTICLE_TARIFF;
	const API_ENTITY_NAME = 'article-tariff';
	public $insurance_id;
	protected $relations = [
		'article' => ['class' => ArticleModel::class, 'expand' => ['accessories']],
		'tariff' => ['class' => TariffModel::class],
		'sim_card' => ['class' => SimCardModel::class, 'reverse' => 'article_tariff'],
		'current_insurance' => ['class' => ArticleInsuranceModel::class, 'field' => 'insurance_id'],
	];

	public function getInsurances()
	{
		return $this->article->insurances;
	}

	public function getHasInsurance()
	{
		return !empty($this->insurance_id);
	}

	/**
	 * @inheritdoc
	 */
	public function serialize()
	{
		return array_merge(
			parent::serialize(),
			[
				'insurance_id' => $this->insurance_id,
			]
		);
	}

	/**
	 * @inheritdoc
	 */
	public function unserialize($serialized)
	{
		parent::unserialize($serialized);

		$this->insurance_id = $serialized['insurance_id'] ?? null;
	}

	public function getLegalText()
	{
		$currency = new CurrencyExtension();
		$specific_legal_text = $this->tariff->specific_legal_text;

		if($specific_legal_text) {
			return '* ' . $specific_legal_text;
		}
		elseif ($this->isRegularMonthlyCosts()) {
			return Yii::t(
				'product',
				'legal_text_rate_payment_no_deviating_first_month {rate_payment_count} {monthly_price} {tariff_title} {monthly_price_tariff} {applicable_sim_card_price} {total_calculated_article_price}',
				[
					'rate_payment_count' => $this->tariff->rate_payment_count,
					'monthly_price_tariff' => $currency->price($this->price_tariff_monthly),
					'tariff_title' => $this->tariff->title,
					'applicable_sim_card_price' => $currency->price($this->sim_card['prices']['once']),
					'total_calculated_article_price' => $currency->price($this->price_article_total),
					'monthly_price' => $currency->price($this->price_article_monthly),
				]
			);
		} elseif ($this->isCheaperFirstMonth()) {
			return Yii::t(
				'product',
				'legal_text_rate_payment_deviating_first_month {rate_payment_count} {monthly_price} {first_monthly} {tariff_title} {monthly_price_tariff} {applicable_sim_card_price} {total_calculated_article_price}',
				[
					'rate_payment_count' => $this->tariff->rate_payment_count,
					'monthly_price_tariff' => $currency->price($this->price_tariff_monthly),
					'tariff_title' => $this->tariff->title,
					'applicable_sim_card_price' => $currency->price($this->sim_card['prices']['once']),
					'total_calculated_article_price' => $currency->price($this->price_article_total),
					'monthly_price' => $currency->price($this->price_article_monthly),
					'first_monthly' => $currency->price($this->price_article_first_month),
				]
			);
		} else {
			return Yii::t(
				'product',
				'legal_text_no_rate_payment {tariff_title} {monthly_price_tariff} {applicable_sim_card_price}',
				[
					'tariff_title' => $this->tariff->title,
					'monthly_price_tariff' => $currency->price($this->price_tariff_monthly),
					'applicable_sim_card_price' => $currency->price($this->sim_card['prices']['once']),
				]
			);
		}
	}

	public function getLegalTextAddition()
	{
		if ($this->isNewUpc()) {
			return Yii::t('product', 'legal_text_addition_upc_new_contract');
		}
	}

	private function isRegularMonthlyCosts(): bool
	{
		$firstRate = $this->price_article_first_month;
		$rate = $this->price_article_monthly;

		return $rate > 0 && ($firstRate == 0 || $rate == $firstRate);
	}

	private function isCheaperFirstMonth(): bool
	{
		$rate = $this->price_article_monthly;
		$firstRate = $this->price_article_first_month;

		return $rate > 0 && ($firstRate > 0 && $rate != $firstRate);
	}

	private function isNewUpc(): bool
	{
		$tariff = $this->tariff;

		return $tariff->provider_id == ProviderModel::UPC_ID && $tariff->offer_type_id == TariffModel::OFFER_TYPE_NEW;
	}

	public function getContractInformation(): bool
	{
		$tariff = $this->tariff;

		return $tariff->text_call || $tariff->text_sms || $tariff->text_internet || $tariff->footnote_text;
	}

	public function getPriceArticleMonthly()
	{
		return $this->prices['article']['monthly'];
	}

	public function getPriceArticleFirstMonth()
	{
		return $this->prices['article']['first_month'];
	}

	public function getPriceArticleOnce()
	{
		return $this->prices['article']['once'];
	}

	public function getPriceTariffMonthly()
	{
		return $this->prices['tariff']['monthly'];
	}

	public function getPriceArticleTotal()
	{
		return $this->prices['article']['total'];
	}
}
