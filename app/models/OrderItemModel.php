<?php

namespace app\models;

/**
 * @property int $item_id
 * @property string $item_product_type_id
 */
class OrderItemModel extends AbstractApiModel
{
	const API_ENTITY_NAME = 'order/item';

	public function getItemType()
	{
		return $this->item_product_type_id;
	}

	public function getArticle(): ArticleModel
	{
		return di(ApiRepository::class)->model(ArticleModel::class)->find($this->item_id);
	}

	public function getTariff(): TariffModel
	{
		return di(ApiRepository::class)->model(TariffModel::class)->find($this->item_id);
	}

	public static function getOfferTypeById($id)
	{
		$tariff = di(ApiRepository::class)->model(TariffModel::class)->find($id);

		return $tariff['offer_type_id'];
	}
}
