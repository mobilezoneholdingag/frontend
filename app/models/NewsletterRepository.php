<?php

namespace app\models;

use app\helper\ArrayHelper;

class NewsletterRepository extends AbstractApiModel
{
	protected $attributes = [
		'id',
		'status',
		'sex',
		'firstname',
		'lastname',
		'customer_id',
	];

	public function save(NewsletterModel $newsletter)
	{
		$attributes = $newsletter->scenario === NewsletterModel::SCENARIO_REGISTER ? ['status', 'customer_id'] : $this->attributes;

		$params = $newsletter->toArray();
		$params = ArrayHelper::keysToSnake($params);

		$params['customer_id'] = $newsletter->customer->id;
		$params['status'] = $newsletter->getStatus();

		$params = collect($params)
			->only($attributes)
			->toArray();

		if ($newsletter->id) {
			$this->getApiClient()->put('newsletter', $newsletter->id, $params);
		} else {
			$data = $this->getApiClient()->post('newsletter', $params);
			$newsletter->id = $data['id'];
		}

		return $newsletter;
	}
}
