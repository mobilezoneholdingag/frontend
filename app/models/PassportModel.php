<?php

namespace app\models;

use app\validators\OfAgeValidator;
use Yii;

class PassportModel extends AbstractApiModel
{
	public $customer_id;
	public $passport_type;
	public $passport_id;
	public $nationality;
	public $birthday;
	public $remindWhenContractEnds = true;

	const API_ENTITY_NAME = 'customer/save-passport-information';

	public function getTypes()
	{
		return array_map(function($label) {
			return Yii::t('passport-types', $label);
		}, PassportTypeInterface::ALL);
	}

	public function rules()
	{
		return [
			[['passport_id', 'nationality', 'passport_type', 'birthday', 'remindWhenContractEnds'], 'required'],
			['nationality', 'in', 'range' => array_keys(CountryInterface::ALL)],
			['passport_type', 'in', 'range' => array_keys(PassportTypeInterface::ALL)],
			['birthday', 'date', 'format' => 'Y-m-d', 'message' => \Yii::t('checkout', 'Correct_Format_Message')],
			['birthday', OfAgeValidator::class],
			['remindWhenContractEnds', 'boolean'],
		];
	}
	
	public function attributeLabels()
	{
		return [
			'passport_id' => Yii::t('checkout', 'Passport_Number'),
			'passport_type' => Yii::t('checkout', 'Passport_Type'),
			'nationality' => Yii::t('checkout', 'Nationality'),
			'birthday' => Yii::t('checkout', 'Birthday'),
			'remindWhenContractEnds' => Yii::t('checkout', 'Remind_When_Contract_Expires'),
		];
	}
}

