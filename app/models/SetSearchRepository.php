<?php

namespace app\models;

use app\modules\category\controllers\CategoryController;
use app\services\api\ApiClientInterface;
use app\services\api\ApiCollection;

class SetSearchRepository extends ApiRepository
{
	protected $apiRepository;
	protected $className = SetSearchModel::class;

	public function __construct(ApiClientInterface $apiClient, ApiRepository $apiRepository)
	{
		parent::__construct($apiClient);

		$this->apiRepository = $apiRepository;
	}

	public function findAccessorySets($setName = null)
	{
		/** @var SetAccessoryModel|ApiCollection $set */
		$set = $this->apiRepository->model(SetAccessoryModel::class)->find($setName, [], true);

		return $set;
	}

	public function getAllSetsSystemTitles($sets)
	{
		$systemTitles = [];

		foreach ($sets as $set) {
			$systemTitles[] = $set->system_title;
		}

		return $systemTitles;
	}

	public function findByAccessorySets($accessorySets)
	{
		$systemTitles = [];

		foreach ($accessorySets as $key => $set) {
			$systemTitles[] = $set->system_title;
		}

		return $this->findOne([
			'id' => $systemTitles,
			'facets' => 1,
			'per-page' => CategoryController::ITEMS_PER_PAGE,
		]);
	}
}
