<?php

namespace app\models;

use app\behaviors\CustomerModelBehavior;
use app\services\auth\IdentityInterface;
use Illuminate\Support\Collection;
use Yii;

/**
 * @property Collection|AddressModel[] $addresses
 * @property AddressModel $address
 * @property Collection|AddressModel[] $other_addresses
 * @property Collection|NewsletterModel[] $newsletters
 * @property Collection|OrderModel[] $orders
 * @property AddressModel $defaultAddress
 * @property AddressModel $billingAddress
 * @property AddressModel $deliveryAddress
 */
class CustomerModel extends AbstractApiModel implements IdentityInterface
{
	const API_ENTITY_NAME = 'customer';

	protected $relations = [
		'addresses' => ['class' => AddressModel::class, 'many' => true, 'lazy' => false],
		'newsletters' => ['class' => NewsletterModel::class, 'many' => true],
		'orders' => ['class' => OrderModel::class, 'many' => true],
	];

	const SCENARIO_LOGIN = 'login';
	const SCENARIO_REGISTER = 'register';
	const SCENARIO_INFORMATION = 'information';
	const SCENARIO_NEWSLETTER = 'newsletter';
	const SCENARIO_FORGOT_PASSWORD = 'forgot_password';
	const SCENARIO_RESET_PASSWORD = 'reset_password';
	const SCENARIO_CHANGE_PASSWORD = 'change_password';

	public $gender;
	public $first_name;
	public $last_name;
	public $email;
	public $password;
	public $newEmail;
	public $newPassword;
	public $newPasswordRepeat;
	public $adHocErrors = [];
	public $id;
	public $via_newsletter;

	public $language;
	public $birthday;
	public $nationality;
	public $passport_type;
	public $passport_id;

	private $useDeliveryAddress = false;
	private $loginAttempt;
	private $registerAttempt = true;

	/**
	 * @var AddressModel
	 */
	private $defaultAddress;
	/**
	 * @var AddressModel
	 */
	private $billingAddress;
	/**
	 * @var AddressModel
	 */
	private $deliveryAddress;
	/**
	 * @var bool
	 */
	private $defaultAddressConfirmation = false;


	public function __construct(array $config = [])
	{
		parent::__construct($config);

		if($this->id != null) {
			$this->setDefaultAddresses();
		}
	}

	public function getAddress()
	{
		return $this->addresses->where('is_default', '==', true)->first();
	}

	public function getOtherAddresses()
	{
		return $this->addresses->where('is_default', '==', false);
	}

	/**
	 * Finds an identity by the given ID.
	 *
	 * @param string|integer $id the ID to be looked for
	 *
	 * @return IdentityInterface the identity object that matches the given ID.
	 * Null should be returned if such an identity cannot be found
	 * or the identity is not in an active state (disabled, deleted, etc.)
	 */
	public static function findIdentity($id)
	{
		return new static(
			[
				'id' => $id,
			]
		);
	}

	/**
	 * Finds an identity by the given token.
	 *
	 * @param mixed $token the token to be looked for
	 * @param mixed $type the type of the token. The value of this parameter depends on the implementation.
	 * For example, [[\yii\filters\auth\HttpBearerAuth]] will set this parameter to be `yii\filters\auth\HttpBearerAuth`.
	 *
	 * @return IdentityInterface the identity object that matches the given token.
	 * Null should be returned if such an identity cannot be found
	 * or the identity is not in an active state (disabled, deleted, etc.)
	 */
	public static function findIdentityByAccessToken($token, $type = null)
	{
		return new static(
			[
				'id' => $token,
			]
		);
	}

	public function fields()
	{
		$fields = parent::fields();

		$fields[] = 'loginAttempt';
		$fields[] = 'registerAttempt';
		$fields[] = 'defaultAddressConfirmation';
		$fields[] = 'useDeliveryAddress';

		// remove password from model export, so it will be not stored in the session
		unset($fields['password']);

		return $fields;
	}

	public function behaviors()
	{
		return [
			CustomerModelBehavior::class,
		];
	}

	public function scenarios()
	{
		return [
			self::SCENARIO_LOGIN => ['email', 'password'],
			self::SCENARIO_REGISTER => ['newEmail', 'newPassword', 'newPasswordRepeat'],
			self::SCENARIO_DEFAULT => [],
			self::SCENARIO_INFORMATION => ['gender', 'first_name', 'last_name', 'language', 'birthday', 'nationality', 'passport_type', 'passport_id'],
			self::SCENARIO_NEWSLETTER => ['gender', 'first_name', 'last_name', 'email', 'language', 'via_newsletter'],
			self::SCENARIO_FORGOT_PASSWORD => ['email'],
			self::SCENARIO_RESET_PASSWORD => ['newPassword', 'newPasswordRepeat'],
			self::SCENARIO_CHANGE_PASSWORD => ['password', 'newPassword', 'newPasswordRepeat'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		$validateNewPasswordRepeatMessage = Yii::t('customer-account', 'Validate_New_Password_Repeat_Message');

		return [
			[['via_newsletter'], 'safe'],
			[['email', 'newEmail'], 'email'],
			[['password', 'email', 'newEmail'], 'required'],
			[
				'gender',
				'required',
				'message' => Yii::t('customer-account', 'Validation_Please_Insert_Gender')
			],
			[
				'first_name',
				'required',
				'message' => Yii::t('customer-account', 'Validation_Please_Insert_First_Name')
			],
			[
				'last_name',
				'required',
				'message' => Yii::t('customer-account', 'Validation_Please_Insert_Last_Name')
			],
			[
				'newPassword',
				'required',
				'message' => Yii::t('customer-account', 'Validate_New_Password_Message')
			],
			[
				'newPasswordRepeat',
				'required',
				'message' => $validateNewPasswordRepeatMessage
			],
			[
				'newPasswordRepeat',
				'compare',
				'compareAttribute' => 'newPassword',
				'message' => $validateNewPasswordRepeatMessage
			],
			[
				['last_name', 'first_name'],
				'string',
				'max' => 255
			],
			['gender', 'in', 'range' => array_keys(GenderInterface::ALL)],
            ['sex', 'in', 'range' => array_keys(GenderInterface::ALL)],
			['nationality', 'in', 'range' => array_keys(CountryInterface::ALL)],
			['language', 'validateLanguage'],
			['passport_type', 'in', 'range' => array_keys(PassportTypeInterface::ALL)],
			['birthday', 'date', 'format' => 'Y-m-d'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'gender' => Yii::t('customer-account', 'Gender'),
			'first_name' => Yii::t('customer-account', 'First_Name'),
			'last_name' => Yii::t('customer-account', 'Last_Name'),
			'email' => Yii::t('checkout', 'Email'),
			'password' => Yii::t('checkout', 'Password'),
			'newEmail' => Yii::t('checkout', 'Email'),
			'newPassword' => Yii::t('checkout', 'Password'),
			'newPasswordRepeat' => Yii::t('checkout', 'Password_Repeat'),
			'loginAttempt' => Yii::t('checkout', 'Login_Now'),
			'useDeliveryAddress' => Yii::t('checkout', 'Other_Delivery_Address'),
			'language' => Yii::t('checkout', 'Language'),
			'nationality' => Yii::t('checkout', 'Nationality'),
			'birthday' => Yii::t('checkout', 'Birthday'),
			'passport_type' => Yii::t('checkout', 'Passport_Type'),
			'passport_id' => Yii::t('checkout', 'Passport_Number'),
		];
	}

	public function getDefaultAddressConfirmation()
	{
		return $this->defaultAddressConfirmation;
	}

	public function setDefaultAddressConfirmation($data)
	{
		$this->defaultAddressConfirmation = (bool)$data;

		return $this;
	}

	/**
	 * @return AddressModel
	 */
	public function getDefaultAddress()
	{
		if (!$this->defaultAddress) {
			$data = $this->getCustomerDefaultAddress()->getData();

			$this->defaultAddress = new AddressModel($data);
		}

		$this->defaultAddress->formName = 'DefaultAddress';

		return $this->defaultAddress;
	}

	/**
	 * @param array|AddressModel $data
	 *
	 * @return $this
	 */
	public function setDefaultAddress($data)
	{
		if (!$data instanceof AddressModel) {
			$data = new AddressModel($data);
		}

		$this->defaultAddress = $data;

		return $this;
	}

	public function getCustomerDefaultAddress()
	{
		// TODO: use proper rest resource endpoint
		$this->data = array_merge(
			$this->data,
			$this->get(
				'customer-address/default-address',
				null,
				[
					'id' => $this->id,
				]
			)
		);

		return $this;
	}

	public function getLoginAttempt()
	{
		return $this->loginAttempt;
	}

	public function setLoginAttempt($data)
	{
		$this->loginAttempt = (bool) $data;

		return $this;
	}

	public function getRegisterAttempt()
	{
		return $this->registerAttempt;
	}

	public function setRegisterAttempt($data)
	{
		$this->registerAttempt = (bool) $data;

		return $this;
	}

	/**
	 * @return bool
	 */
	public function checkLogin(): bool
	{
		$params = [
			'email' => $this->email,
			'password' => $this->password,
		];

		$this->data = array_merge($this->data, $this->getApiClient()->post('customer/login', $params) ?? []);

		if (isset($this->data['customerId'])) {
			$this->id = $this->data['customerId'];
			$this->setLoginAttempt(false);

			return true;
		}

		return false;
	}

	public function create($entity = null)
	{
		try {
			$lang = Yii::$app->language ?? 'de-CH';

			$data = $this->getApiClient()->post(
				'customer',
				[
					'gender' => $this->gender,
					'first_name' => $this->first_name,
					'last_name' => $this->last_name,
					'email' => $this->newEmail,
					'password_hash' => $this->newPassword,
					'language' => $lang,
					'birthday' => $this->birthday,
					'via_newsletter' => $this->via_newsletter,
				]
			);

			$this->id = $data['id'];
			$this->setNewAddress($this->getBillingAddress(), true);

			if ($this->getUseDeliveryAddress()) {
				$address = $this->getDeliveryAddress();
				$this->setNewAddress($address, false);
			} else {
				$address = $this->getBillingAddress();
			}

			$this->setDefaultAddress($address);
		} catch (\Exception $e) {
			$this->adHocErrors = [
				['newEmail', \Yii::t('checkout', 'Can_Not_Register')],
			];
		}

		return $this;
	}

	public function setNewAddress($params = null, $default = false)
	{
		if ($params instanceof AddressModel) {
			$params = $params->toArray(['sex', 'firstname', 'lastname', 'street', 'zip', 'city', 'houseNumber', 'contactPhoneNumber']);
			$params['house_number'] = $params['houseNumber'];
			$params['contact_phone_number'] = $params['contactPhoneNumber'];

			unset($params['houseNumber'], $params['contactPhoneNumber']);
		} elseif (!is_array($params)) {
			$params = (array)$params;
		}

		$params += [
			'customer_id' => $this->id,
			'is_default' => $default,
			'is_active' => true,
		];

		$this->data = array_merge($this->data, $this->getApiClient()->post('customer-address', $params) ?? []);

		return $this;
	}

	public function getBillingAddress()
	{
		if (!$this->billingAddress) {
			$this->billingAddress = new AddressModel;
		}

		$this->billingAddress->formName = 'BillingAddress';

		return $this->billingAddress;
	}

	/**
	 * @param array|AddressModel $data
	 *
	 * @return $this
	 */
	public function setBillingAddress($data)
	{
		if (!$data instanceof AddressModel) {
			$data = new AddressModel($data);
		}

		$this->billingAddress = $data;

		return $this;
	}

	public function getUseDeliveryAddress()
	{
		return $this->useDeliveryAddress;
	}

	public function setUseDeliveryAddress($data)
	{
		$this->useDeliveryAddress = (bool)$data;

		return $this;
	}

	public function getDeliveryAddress()
	{
		if (!$this->deliveryAddress) {
			$this->deliveryAddress = new AddressModel;
		}

		$this->deliveryAddress->formName = 'DeliveryAddress';

		return $this->deliveryAddress;
	}

	/**
	 * @param array|AddressModel $data
	 *
	 * @return $this
	 */
	public function setDeliveryAddress($data)
	{
		if (!$data instanceof AddressModel) {
			$data = new AddressModel($data);
		}

		$this->deliveryAddress = $data;

		return $this;
	}

	public function validate($attributeNames = null, $clearErrors = true)
	{
		$validate = true;

		if (is_array($this->adHocErrors) && count($this->adHocErrors) > 0) {
			foreach ($this->adHocErrors as $error) {
				$this->addError($error[0], $error[1]);
			}

			$validate = false;
		}

		return $validate && parent::validate($attributeNames, $clearErrors);
	}

	public function getCustomerData()
	{
		$this->data = array_merge($this->data, $this->get('customer', $this->id));

		return $this;
	}

	public function setNewCustomerPassword($params = null)
	{
		$params['id'] = $this->id;

		// TODO: use proper rest resource endpoint
		$this->data = array_merge($this->data, $this->getApiClient()->post('customer/change-password', $params) ?? []);

		return $this;
	}

	public function deleteAddress($addressId = null)
	{
		$this->data = array_merge(
			$this->data,
			$this->delete(
				'customer-address',
				$addressId,
				[
					'customer_id' => $this->id,
				]
			)
		);

		return $this;
	}

	public function setInformations($params = null)
	{
		$params['customer_id'] = $this->id;

		$this->data = array_merge($this->data, $this->getApiClient()->post('customer/set-informations', $params) ?? []);

		return $this;
	}

	public function getPasswordResetEmail($params = null)
	{
		$this->data = array_merge(
			$this->data,
			$this->getApiClient()->post('customer/send-password-reset-mail', $params) ?? []
		);

		return $this;
	}

	public function getPasswordReset($params = null)
	{
		$this->data = array_merge($this->data, $this->getApiClient()->post('customer/reset-password', $params) ?? []);

		return $this;
	}

	public function getOrders()
	{
		/** @var ApiRepository $repository */
		$repository = di(ApiRepository::class);

		$orders = $repository->model(OrderModel::class)->find(null, [
			'id' => Yii::$app->request->get('id'),
			'customer_id' => $this->id,
		]);

		return $orders;
	}

	/**
	 * Returns an ID that can uniquely identify a user identity.
	 *
	 * @return string|integer an ID that uniquely identifies a user identity.
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Returns a key that can be used to check the validity of a given identity ID.
	 * The key should be unique for each individual user, and should be persistent
	 * so that it can be used to check the validity of the user identity.
	 * The space of such keys should be big enough to defeat potential identity attacks.
	 * This is required if [[User::enableAutoLogin]] is enabled.
	 *
	 * @return string a key that is used to check the validity of a given identity ID.
	 * @see validateAuthKey()
	 */
	public function getAuthKey()
	{
		// TODO: Implement getAuthKey() method.
	}

	/**
	 * Validates the given auth key.
	 * This is required if [[User::enableAutoLogin]] is enabled.
	 *
	 * @param string $authKey the given auth key
	 *
	 * @return boolean whether the given auth key is valid.
	 * @see getAuthKey()
	 */
	public function validateAuthKey($authKey)
	{
		// TODO: Implement validateAuthKey() method.
	}

	public function validatePassword($password)
	{
		return Yii::$app->security->validatePassword($password, $this->password_hash);
	}

	public function setDefaultAddresses()
	{
		$defaultAddress = $this->getCustomerDefaultAddress()->getData();

		if (!$defaultAddress instanceof AddressModel) {
			$defaultAddress = new AddressModel($defaultAddress);
		}

		if ($defaultAddress->validate()) {
			$this->setDefaultAddress($defaultAddress);
			$this->setBillingAddress($this->getDefaultAddress());
		}
	}

	public function getLanguageUppercase(): string
	{
		return strtoupper(explode('-', $this->language ?? 'de-CH')[0]);
	}

	public function validateLanguage($attribute)
	{
		/** @var CustomerRepository $customerRepository */
		$customerRepository = di(CustomerRepository::class);
		$languages = array_keys($customerRepository->getAllLanguages());
		if (!in_array($this->{$attribute}, $languages)) {
			$message = Yii::t('yii', '{attribute} is invalid.', ['attribute' => $this->getAttributeLabel($attribute)]);
			$this->addError($attribute, $message);
		}
	}

	public function emailExists()
	{
		$repository = di(ApiRepository::class);

		return $repository->model(self::class)->find(null, [
			'email' => Yii::$app->request->get('email'),
		]);
	}
}
