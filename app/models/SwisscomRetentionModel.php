<?php

namespace app\models;

class SwisscomRetentionModel extends AbstractApiModel
{
	const API_ENTITY_NAME = 'swisscom/retention';

	protected $relations = [
		'tariffs' => ['class' => TariffModel::class, 'many' => true],
	];
}
