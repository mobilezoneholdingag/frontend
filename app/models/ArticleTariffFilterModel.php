<?php

namespace app\models;

use Illuminate\Support\Collection;

/**
 * @property Collection|FacetsModel[] $facets
 * @property Collection|TariffModel[] $article_tariffs
 * @property integer $total
 * @property bool $exists
 */
class ArticleTariffFilterModel extends AbstractApiModel
{
	const API_ENTITY_NAME = 'article-tariff/search';

	protected $relations = [
		'facets' => ['class' => FacetsModel::class],
		'article_tariffs' => ['class' => ArticleTariffModel::class, 'many' => true, 'field' => 'items'],
		'standard_tariffs' => ['class' => ArticleTariffModel::class, 'many' => true, 'field' => 'default_items'],
	];
}
