<?php

namespace app\models;

use app\helper\ArrayHelper;

class AddressRepository extends ApiRepository
{
	protected $attributes = [
		'id',
		'customer_id',
		'sex',
		'firstname',
		'lastname',
		'street',
		'house_number',
		'zip',
		'city',
		'contact_phone_number',
		'is_default',
	];

	public function save(AddressModel $address, int $customerId = null)
	{
		$params = $address->toArray();
		$params = ArrayHelper::keysToSnake($params);

		$params['customer_id'] = $customerId;

		$params = collect($params)
			->only($this->attributes)
			->toArray();

		// add phone prefix (+41)
		// In the frontend we always use phone numbers without prefix, but in the backend we need the number WITH prefix.
		// TODO this is definitely not the most stable implementation
		$params['contact_phone_number'] = '41' . $address->contactPhoneNumber;

		if ($address->id) {
			$address->getApiClient()->put('customer-address', $address->id, $params);
		} else {
			$address->getApiClient()->post('customer-address', $params);
		}

		return $this;
	}

	public function load($id = null)
	{
		$address = $this->model(AddressModel::class)->find($id);

		// remove phone prefix (+41)
		// From the backend we get the number WITH prefix, but we need to disply it without.
		// TODO this is definitely not the most stable implementation
		$address->contactPhoneNumber = substr($address->contactPhoneNumber, 2);

		return $address;
	}
}
