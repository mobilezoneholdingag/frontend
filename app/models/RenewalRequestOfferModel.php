<?php

namespace app\models;

use app\modules\checkout\services\cart\ItemInterface;
use Illuminate\Support\Collection;

/**
 * @property ArticleModel $article
 * @property Collection|TariffModel[] $tariffs
 */
class RenewalRequestOfferModel extends AbstractItemApiModel
{
	const API_ENTITY_NAME = 'renewal-request-offer';

	protected $relations = [
		'articles' => ['class' => ArticleModel::class],
		'tariffs' => ['class' => TariffModel::class],
	];
}
