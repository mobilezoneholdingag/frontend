<?php

namespace app\models;

class ManufacturerNaviModel extends AbstractApiModel
{
	const API_ENTITY_NAME = 'manufacturer/title';
}
