<?php

namespace app\twig;

use Twig_SimpleFilter;
use Yii;

class UnitExtension extends \Twig_Extension
{
	/**
	 * Returns a list of filters to add to the existing list.
	 *
	 * @return array An array of filters
	 */
	public function getFilters()
	{
		return [
			new Twig_SimpleFilter('unit', [$this, 'getUnit']),
		];
	}

	public function getUnit($value, $unit = '')
	{
		if ($unit == 'GB' && $value < 1) {
			return $value * 1000 . ' MB';
		}

		$unit = $unit ? Yii::t('units', $unit) : '';

		return $value . ' ' . $unit;
	}

	/**
	 * Returns the name of the extension.
	 *
	 * @return string The extension name
	 */
	public function getName()
	{
		return 'unit';
	}
}
