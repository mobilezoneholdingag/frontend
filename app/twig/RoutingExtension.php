<?php

namespace app\twig;

use app\services\i18n\UrlManager;
use Twig_SimpleFilter;
use Yii;
use yii\web\Request;

class RoutingExtension extends \Twig_Extension
{
	const LANG_MAP = [
		'DE' => 'de-CH',
		'FR' => 'fr-FR',
		'IT' => 'it-IT',
	];

	const REPLACE_MAP = [
		' ' => '-',
		'.' => '-',
		'ä' => 'ae',
		'ö' => 'oe',
		'ü' => 'ue',
		'ß' => 'ss',
	];

	/** @var UrlManager */
	private $urlManager;
	/** @var Request */
	private $request;

	public function __construct()
	{
		$this->urlManager = Yii::$app->urlManager;
		$this->request = Yii::$app->request;
	}

	/**
	 * Returns a list of filters to add to the existing list.
	 *
	 * @return array An array of filters
	 */
	public function getFilters()
	{
		return [
			new Twig_SimpleFilter('routing', [$this, 'translateRouting']),
			new Twig_SimpleFilter('slugify', [$this, 'normalizeStringForUrl']),
		];
	}

	/**
	 * Returns the name of the extension.
	 *
	 * @return string The extension name
	 */
	public function getName()
	{
		return 'routing';
	}

	public function normalizeStringForUrl($string): string
	{
		if (!$string) {
			return "";
		}

		$adjustedString = str_replace(
			array_keys(self::REPLACE_MAP),
			array_values(self::REPLACE_MAP),
			mb_strtolower($string)
		);

		return preg_replace('/[^A-Za-z0-9\-]/', '', $adjustedString);
	}

	public function translateRouting($targetDomain, $language): string
	{
		return $this->localizeRequestUrl($targetDomain, $language) . $this->getQueryString();
	}

	private function localizeRequestUrl($targetDomain, $language): string
	{
		$rules = $this->getUrlRulesForLanguage($language);
		$parsedRequest = $this->urlManager->parseRequest($this->request);

		return $targetDomain . '/' . $this->urlManager->createUrlFromRules(
				$rules,
				$parsedRequest[0],
				$parsedRequest[1]
			);
	}

	private function getUrlRulesForLanguage($language): array
	{
		$langKey = self::LANG_MAP[$language];

		return Yii::$app->params['urlRules'][$langKey];
	}

	private function getQueryString(): string
	{
		return $this->request->queryString ? "?{$this->request->queryString}" : "";
	}
}
