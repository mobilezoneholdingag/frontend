<?php

namespace app\twig;

use Twig_SimpleFilter;

class CurrencyExtension extends \Twig_Extension
{
	/**
	 * Returns a list of filters to add to the existing list.
	 *
	 * @return array An array of filters
	 */
	public function getFilters()
	{
		return [
			new Twig_SimpleFilter('price', [$this, 'price']),
		];
	}

	/**
	 * @param float|int $price
	 * @param string $currency
	 * @param string $format
	 * @return string
	 */
	public static function price($price, $currency = 'CHF', $format = '{currency} {price}')
	{
		$priceString = number_format($price, 2, '.', "'");

		return str_replace(['{price}', '{currency}'], [$priceString, $currency], $format);
	}

	/**
	 * Returns the name of the extension.
	 *
	 * @return string The extension name
	 */
	public function getName()
	{
		return 'currency';
	}
}
