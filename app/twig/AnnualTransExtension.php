<?php

namespace app\twig;

use app\modules\page\models\AnnualTransInterface;
use Twig_SimpleFilter;
use Yii;

class AnnualTransExtension extends \Twig_Extension
{
	/**
	 * Returns a list of filters to add to the existing list.
	 *
	 * @return array An array of filters
	 */
	public function getFilters()
	{
		return [
			new Twig_SimpleFilter('annualTrans', [$this, 'translate']),
		];
	}

	public function translate($key, $langKey)
	{
		$translation = AnnualTransInterface::ALL[$key][$langKey];

		return $translation;
	}

	/**
	 * Returns the name of the extension.
	 *
	 * @return string The extension name
	 */
	public function getName()
	{
		return 'annualTrans';
	}
}
