<?php

namespace app\twig;

use app\models\AccessoriesModel;
use app\models\ArticleModel;
use Yii;
use yii\base\Model;
use yii\helpers\Url;

class AssetsExtension extends \Twig_Extension
{
	/**
	 * @var string
	 */
	protected $basePath;
	protected $cdn;
	protected $manifest;

	public function __construct()
	{
		$this->cdn = Yii::$app->params['assets']['cdn'];
		$this->manifest = Yii::$app->params['assets']['manifest'];
		$this->basePath = preg_replace("/\/$/", "", Yii::$app->params['assets']['path']);
	}

	protected function slugify($string)
	{
		return strtolower(preg_replace("/[^\w-.]/mi", "", str_replace([' ', '.'], '-', $string)));
	}

	/**
	 * Returns a list of filters to add to the existing list.
	 *
	 * @return array An array of filters
	 */
	public function getFunctions()
	{
		return [
			new \Twig_SimpleFunction('asset', [$this, 'asset']),
			new \Twig_SimpleFunction('cdn', [$this, 'cdn']),
			new \Twig_SimpleFunction('routeModel', [$this, 'routeModel']),
		];
	}

	public function routeModel(Model $model)
	{
		$class = get_class($model);

		switch ($class) {
			case AccessoriesModel::class:
				/** @var AccessoriesModel|ArticleModel $model */
				$category = 'accessories';
				$group = $model->group['title'] ?? '';
				$manufacturer = $this->slugify($model->manufacturer->title);

				$paths = [
					"/product/product/{$category}",
					'id' => $model->id,
					'manufacturerTitle' => $manufacturer,
					'groupUrlText' => $this->slugify($group),
				];

				$url = Url::to($paths, true);

				return $url;
			default:
				throw new \InvalidArgumentException("There is not route definition for `$class`.");
		}
	}

	public function asset($filePath)
	{
		if ($filePath{0} === '/') {
			$filePath = substr($filePath, 1);
		}

		return $this->basePath . '/' . $this->pathToManifestPath($filePath);
	}

	protected function pathToManifestPath($filePath)
	{
		$manifestPath = base_path('web' . $this->basePath . '/' . $this->manifest);

		if (file_exists($manifestPath)) {
			$manifest = json_decode(file_get_contents($manifestPath), true);

			$filePath = $manifest[$filePath] ?? $filePath;
		}

		return $filePath;
	}

	public function cdn($filePath)
	{
		if ($filePath{0} === '/') {
			$filePath = substr($filePath, 1);
		}

		$url = Yii::$app->request->isSecureConnection ? $this->cdn['url_secure'] : $this->cdn['url'];

		return implode([$url, $filePath]);
	}

	/**
	 * Returns the name of the extension.
	 *
	 * @return string The extension name
	 */
	public function getName()
	{
		return 'assets';
	}
}
