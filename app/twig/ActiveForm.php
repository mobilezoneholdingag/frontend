<?php

namespace app\twig;

use app\services\form\ActiveForm as YiiActiveForm;

class ActiveForm extends \Twig_Extension
{
	/**
	 * {@inheritdoc}
	 */
	public function getName()
	{
		return 'active_form';
	}

	/**
	 * {@inheritdoc}
	 */
	public function getFunctions()
	{
		return [
			"form_begin" => new \Twig_Function_Method($this, 'begin'),
			"form_end" => new \Twig_Function_Method($this, 'end'),
		];
	}

	public function begin($args)
	{
		return YiiActiveForm::begin($args);
	}

	public function end()
	{
		YiiActiveForm::end();
	}
}
