<?php

namespace app\twig;

use app\models\ApiRepository;
use app\models\ArticleModel;
use app\models\SetAccessoryModel;
use app\models\SetModel;
use app\models\SetSearchModel;
use app\models\TariffModel;
use app\modules\shopFinder\models\ShopFinderForm;
use app\modules\shopFinder\models\ShopModel;
use app\modules\shopFinder\models\ShopRepository;
use app\modules\shopFinder\services\Geocoder;
use app\modules\shopFinder\services\GeocoderInterface;
use app\services\api\ApiClient;
use yii\base\View;
use yii\helpers\BaseArrayHelper;
use yii\twig\ViewRenderer;

class ModelExtension extends \Twig_Extension
{
    /**
     * {@inheritdoc}
     */

    public $view;

    public function __construct()
    {
        $this->view = \Yii::$app->getView();

    }


    public function getName()
    {
        return 'product_extension';
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return [
            "product" => new \Twig_Function_Method($this, 'product'),
            "tariff" => new \Twig_Function_Method($this, 'tariff'),
            "smartphones" => new \Twig_Function_Method($this, 'smartphones'),
            "shopfinder" => new \Twig_Function_Method($this, 'shopfinder'),
            "sets" => new \Twig_Function_Method($this, 'sets'),
        ];
    }

    public function product($id, $field = null)
    {

        $product = di(ApiRepository::class)->model(ArticleModel::class)->find($id, ['expand' => 'articleDetails']);

        if(empty($field)) {
            return $product;
        }

        return $product->$field;

    }

    public function tariff($id)
    {
        $tariff = di(ApiRepository::class)->model(TariffModel::class)->find($id);

        return $tariff;
    }

    public function smartphones($manufacturerId)
    {

        $requestParams = [];

        $requestParams['manufacturer_id'] = $manufacturerId;
        $requestParams['id'] = 'category-smartphones';
        $requestParams['facets'] = 1;
        $requestParams['per-page'] = 100;

        $api = di(ApiRepository::class);

        $result = $api->model(SetSearchModel::class)->findOne($requestParams);

        return $result->items;

    }

    public function shopfinder($query = null)
    {

        $shops = [];

        $shopFinder = new ShopFinderForm();

        $shopRepository = di(ShopRepository::class);
        $geocoder = di(GeocoderInterface::class);

        $shopFinder->fitbounds = true;

        if(!empty($query)) {
            $coords = $geocoder->search("{$query},Switzerland");

            // only set coords if there are some hits, otherwise default will be displayed
            if ($coords) {
                $shopFinder->latitude = $coords['lat'];
                $shopFinder->longitude = $coords['lng'];
                $shopFinder->fitbounds = false;
            }
        }

        if ($shopFinder->validate()) {
            $shops = $shopRepository->search($shopFinder);
        }

        $shops->each(function($shop) {
            $shop->html = $this->view->render('@shop-finder/google-maps-marker.twig', compact('shop'));
        });

        return [
            'shop_finder' => $shopFinder,
            'shops' => $shops,
        ];

    }

    public function sets($sets, $options = [])
    {

        $defaultOptions = [
            'sortBy' => 'position',
            'multidimensional' => false,
        ];

        $options = BaseArrayHelper::merge($defaultOptions, $options);

        $requestParams = [];
        $requestParams['id'] = $sets;
        $requestParams['facets'] = 1;
        $requestParams['per-page'] = 500;
        $requestParams['sort_by'] = $options['sortBy'];

        $sets = di(ApiRepository::class)->model(SetSearchModel::class)->findOne($requestParams);
        $articles = $sets->getArticlesOnly();

        if(is_array($options['multidimensional'])) {

            $return = [];

            $return = array_fill_keys($options['multidimensional'], []);
            $return['unordered'] = [];

            foreach($articles as $article) {

                if(!array_key_exists($article->product_category_id, $return)) {
                    $return['unordered'][] = $article;
                } else {
                    $return[$article->product_category_id][] = $article;
                }
            }

            return $return;

        }

        return $sets;

    }


}
