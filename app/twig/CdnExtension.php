<?php

namespace app\twig;

use Twig_SimpleFilter;

class CdnExtension extends \Twig_Extension
{
	/**
	 * Returns a list of filters to add to the existing list.
	 *
	 * @return array An array of filters
	 */
	public function getFilters()
	{
		return [
			new Twig_SimpleFilter('cdn', [$this, 'cdnUrl']),
		];
	}

	public function cdnUrl($path)
	{
		$cleanedCdnUrl = str_replace('images/', '', \Yii::$app->params['assets']['cdn']['url_secure']);

		return $cleanedCdnUrl . $path;
	}

	/**
	 * Returns the name of the extension.
	 *
	 * @return string The extension name
	 */
	public function getName()
	{
		return 'cdn';
	}
}
