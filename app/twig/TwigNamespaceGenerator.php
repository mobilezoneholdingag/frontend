<?php

namespace app\twig;

use deinhandy\yii2events\WebApplication;
use Yii;

class TwigNamespaceGenerator
{
	public function __construct()
	{
		$this->init();
	}

	public function init()
	{
		Yii::$app->on(WebApplication::EVENT_BEFORE_REQUEST, function() {
			$config = [
				'namespaces' => []
			];

			foreach (Yii::$app->modules as $module) {
				$config['namespaces'][] = [
					'namespace' => $module->id,
                    'path' => $this->relativePath($module->viewPath),
				];
			}

			$config['namespaces'][] = [
				'namespace' => 'views',
				'path' => $this->relativePath(Yii::$app->basePath . '/views'),
			];

			file_put_contents(base_path('ide-twig.json'), json_encode($config, JSON_UNESCAPED_SLASHES));
		});
	}

	protected function relativePath($path)
	{
		$path = str_replace(Yii::$app->basePath, '', $path);

		return 'app' . $path;
	}
}
