<?php

namespace app\behaviors;

use app\models\CustomerModel;
use yii\base\Behavior;

class CustomerModelBehavior extends Behavior
{
	public function events()
	{
		return [
			CustomerModel::EVENT_BEFORE_VALIDATE => 'beforeValidate',
		];
	}

	/**
	 * @param $event
	 */
	public function beforeValidate($event)
	{
		/** @var CustomerModel $customerModel */
		$customerModel = $event->sender;

		if (
			!$customerModel->loginAttempt && !in_array($customerModel->scenario, [
				CustomerModel::SCENARIO_NEWSLETTER,
				CustomerModel::SCENARIO_FORGOT_PASSWORD,
				CustomerModel::SCENARIO_RESET_PASSWORD,
				CustomerModel::SCENARIO_CHANGE_PASSWORD,
			])
		) {
			$customerModel->email = null;
			$customerModel->password = null;
		}
	}
}
