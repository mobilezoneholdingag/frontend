<?php

namespace app\behaviors;

use app\models\ApiRepository;
use app\models\ArticleModel;
use app\models\ArticleTariffModel;
use app\models\PassportTypeInterface;
use app\models\TariffModel;
use app\modules\checkout\services\checkout\SessionCheckoutStorage;
use app\services\DataLayer;
use kartik\helpers\Enum;
use Yii;
use yii\base\Event;

class GoogleDataLayerBehavior extends DataLayerBehavior
{
	protected $dataLayerProperties = [
		'pageType' => null,
		'language' => null,
		'environment' => null,
		'version' => '1',
		'bid' => null,
		'visitorLoginState' => null,
		'userId' => null,
		'isInternalIp' => null,
		'sessionID' => null,
		'deviceId' => null,
		'deviceName' => null,
		'devicePrice' => null,
		'deviceBrand' => null,
		'deviceCategory' => null,
		'deviceVariant' => null,
		'checkoutModus' => null,
		'searchQuery' => null,
		'searchResults' => null,
	];
	protected $dataLayerInitVar = 'var dataLayer';
	protected $dataFormat = 'array';
	// depth needed for distinction [controller-id][action-id]
	protected $controllerIdTranslations = [
		'homepage' => 'startpage',
		'category' => 'shop category',
		'product' => 'shop product detail pages',
		'checkout' => 'checkout page',
		'confirm' => 'confirmation page',
		'customer-account' => 'login portal',
		'search' => 'search result page',
		'page' => [
			// service & support
			'service-support' => 'service & support',
			'repair-warranty' => 'service & support',
			'express-repair' => 'service & support',
			'service-garantie' => 'service & support',
			'device-insurance' => 'service & support',
			// static pages
			'imprint' => 'static pages',
			'terms' => 'static pages',
			'data-protection' => 'static pages',
			'contact' => 'contact',
		],
		'shop-finder' => 'storepage',
		'site' => [
			'error' => '404',
		],
	];

	public function beginBody(Event $event = null)
	{
		parent::beginBody($event);

		Yii::$app->view->registerJs(
			Yii::$app->view->renderFile(
				'@views/marketing/gtm.twig',
				['id' => Yii::$app->params['GoogleTagManagerId']]
			),
			$this->getJsPositionOnPage()
		);

		echo Yii::$app->view->renderFile(
			'@views/marketing/gtm_noscript.twig',
			['id' => Yii::$app->params['GoogleTagManagerId']]
		);

		switch ($event->sender->context->id) {
			case 'checkout':
				$this->getCheckoutJs($event);
				break;
			case 'product':
				$this->renderProductJs();
				break;
		}

		if(Yii::$app->session['item-removed']) {
			$this->renderRemoveItemsJs();
		}

		$this->getConversionJs($event);
	}

	private function getCheckoutJs($event)
	{
		$currentStep = Yii::$app->session['checkout']['current_step'];
		$currentAction = $event->sender->context->action->id;

		if ($currentAction == 'success') {
			$this->renderPurchaseJs();
		} else {
			if ($currentStep == 'cart' || $currentStep == null) {
				$this->renderBasketJs();
			}

			if($currentStep && $currentStep != 'cart') {
				$this->renderCheckoutOptionsJs();
			}

			$this->renderCheckoutJs();
		}
	}

	private function getConversionJs($event)
	{
		$params = null;
		$request = Yii::$app->request;
		$actionId = Yii::$app->controller->action->id;
		$successMessage = Yii::$app->session['success'][0];

		switch ($event->sender->context->id) {
			case 'search':
				$params = [
					'category' => 'search',
					'action' => $request->get('q'),
				];
				break;
			case 'renewal-request':
				$params = [
					'category' => 'conversion',
					'action' => 'contract extension request',
				];
				break;
			case 'customer-account':
				if($actionId == 'index') {
					$params = [
						'category' => 'interaction',
						'action' => 'user login',
					];
				}
				break;
			case 'shop-finder':
				$storeSearch = $request->get('store');

				if($storeSearch) {
					$params = [
						'category' => 'storesearch',
						'action' => $storeSearch,
					];
				}
				break;
			case 'newsletter':
				if($successMessage == Yii::t('customer-account', 'Newsletter_Subscription_Saved')) {
					$params = [
						'category' => 'interaction',
						'action' => 'unsubscribe',
					];
				}
				break;
			case 'tv':
				if($actionId == 'contract-configurator') {
					$params = [
						'category' => 'user interaction',
						'action' => $successMessage ? 'form-sent' : 'viewed-configurator',
					];
				}
				break;
		}

		if($successMessage == Yii::t('customer-account', 'Newsletter_Subscription_Success')) {
			$params = [
				'category' => 'conversion',
				'action' => 'newsletter single opt in',
			];
		}

		return $params ? $this->renderConversionJs($params) : null;
	}

	private function renderPurchaseJs()
	{
		echo Yii::$app->view->renderFile(
			'@views/marketing/eePurchase.twig',
			['order_info' => $this->orderInformation]
		);
	}

	private function renderProductJs()
	{
		$props = $this->dataLayerProperties;

		echo Yii::$app->view->renderFile(
			'@views/marketing/eeProductView.twig',
			[
				'id' => $props['deviceId'],
				'name' => $props['deviceName'],
				'price' => $props['devicePrice'],
				'brand' => $props['deviceBrand'],
				'category' => $props['deviceCategory'],
				'variant' => $props['deviceVariant']
			]
		);
	}

	private function renderBasketJs()
	{
		echo Yii::$app->view->renderFile(
			'@views/marketing/eeAddToBasket.twig',
			['articles' => $this->checkoutInformation]
		);
	}

	private function renderCheckoutJs()
	{
		echo Yii::$app->view->renderFile(
			'@views/marketing/eeCheckout.twig', [
				'articles' => $this->checkoutInformation,
				'step' => self::getCurrentStepNr(),
			]
		);
	}

	private function renderCheckoutOptionsJs()
	{
		echo Yii::$app->view->renderFile(
			'@views/marketing/eeCheckoutOption.twig', [
				'step' => self::getCurrentStepNr() - 1,
				'option' => self::getCheckoutOption(),
			]
		);
	}

	public function renderConversionJs($params)
	{
		echo Yii::$app->view->renderFile(
			'@views/marketing/mkt-conversion.twig', [
				'params' => $params,
				'label' => Yii::$app->controller->id,
			]
		);
	}

	public function renderRemoveItemsJs()
	{
		echo Yii::$app->view->renderFile(
			'@views/marketing/eeRemoveFromCart.twig',
			['articles' => $this->getRemovedItems()]
		);
	}

	public function getDataLayer(Event $event): string
	{
		$this->prefillBaseVars($event);

		return parent::getDataLayer($event);
	}

	private function prefillBaseVars(Event $event)
	{
		$session = Yii::$app->session;
		$loginUser = Yii::$app->user;

		$prefillVars = [
			'pageType' => $this->getTranslatedControllerId($event),
			'language' => str_replace('-', '_', Yii::$app->language),
			'environment' => Yii::$app->params['environment_stage'] ?? 'stage_unknown',
			'bid' => Yii::$app->request->get('bid') ?? null,
			'visitorLoginState' => $loginUser->isGuest ? 'no' : 'yes',
			'userId' => $loginUser->isGuest ? null : md5($loginUser->getIdentity()->email),
			'isInternalIp' => $this->isInternalIp(),
			'sessionID' => $session->id,
		];

		$propertyExtension = DataLayer::getDataLayerInformation($event)['propertyExtension'] ?? null;
		$prefillVars = $propertyExtension ? array_merge($prefillVars, $propertyExtension) : $prefillVars;

		foreach ($this->dataLayerProperties as $propertyName => $value) {
			if (in_array($propertyName, array_keys($prefillVars)) && empty($value)) {
				$this->dataLayerProperties[$propertyName] = $prefillVars[$propertyName];
			}
		}
	}

	/**
	 * Current implementation won´t switch back to controllerId if no match is found with actionId.
	 */
	private function getTranslatedControllerId($event, $baseArray = [], $recursionId = null)
	{
		$id = $event->sender->context->id ?? $recursionId;

		if ($event) {
			$baseArray = $this->controllerIdTranslations;
		}

		if (isset($baseArray[$id])) {
			if (is_array($baseArray[$id])) {
				return $this->getTranslatedControllerId(null, $baseArray[$id], $event->sender->context->action->id) ?? 'site';
			}

			return $baseArray[$id];
		}

		return $id;
	}

	private function isInternalIp()
	{
		return in_array(Enum::userIP(), Yii::$app->params['mobilezone_internal_ips']);
	}

	private function getCurrentStepNr()
	{
		$steps = [
			'cart',
			'customer_data',
			'contract_data',
			'payment',
			'overview',
		];
		$currentStep = Yii::$app->controller->checkoutStorage->getCurrentStep();
		$currentStepNr = array_search($currentStep, $steps);

		return $currentStepNr ? $currentStepNr + 1 : 1;
	}

	private function getCheckoutOption()
	{
		$option = '';
		$steps = Yii::$app->controller->checkoutBuilder->build();
		$currentStep = Yii::$app->controller->checkoutStorage->getCurrentStep();

		switch ($currentStep) {
			case 'customer_data':
				$option = 'Insurance: ' . (self::getHasInsurance() ? 'Yes' : 'No');
				break;
			case 'contract_data':
				$option = 'Kundendaten: ' . self::getUserState();
				break;
			case 'payment':
				$stepInformation = $steps[2];
				$passportTypes = PassportTypeInterface::ALL;

				if(property_exists($stepInformation, 'passportModel')) {
					$passportType = Yii::t('passport-types', $passportTypes[$stepInformation->getData()['passport']->passport_type]);
					$option = 'Ausweisart: ' . $passportType;
				} else {
					$option = 'Kundendaten: ' . self::getUserState();
				}
				break;
			case 'overview':
				$stepInformation = count($steps) == 5 ? $steps[3] : $steps[2];
				$option = 'Zahlart: ' . $stepInformation->getData()['payment']->type;
		}

		return $option;
	}

	private function getHasInsurance()
	{
		$items = SessionCheckoutStorage::getStepItems(1);

		foreach ($items as $item) {
			if($item->insurance_id) {
				return true;
			}
		}

		return false;
	}

	private static function getUserState()
	{
		$userRegistration = Yii::$app->user->isGuest ? '' : Yii::$app->user->getIdentity()->data['created_at'];

		if((time()-(60*60*24)) < strtotime($userRegistration)) {
			return 'Neukunde';
		}

		return 'Bestandskunde';
	}

	private function getRemovedItems()
	{
		$identifier = Yii::$app->session['item-removed'];
		$itemInformation = explode('-', $identifier[0]);
		$category = $itemInformation[0];
		$id = $itemInformation[1];
		$articles = [];

		if($category == 'article_tariff') {
			$articleTariff = di(ApiRepository::class)->model(ArticleTariffModel::class)->find($id);
			$articles[0] = di(ApiRepository::class)->model(ArticleModel::class)->find($articleTariff->data['article']);
			$articles[1] = di(ApiRepository::class)->model(TariffModel::class)->find($articleTariff->data['tariff']);
		} else {
			$articles[0] = di(ApiRepository::class)->model(ArticleModel::class)->find($id);
		}

		return $articles;
	}
}
