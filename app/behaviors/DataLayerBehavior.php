<?php

namespace app\behaviors;

use app\services\DataLayer;
use Yii;
use yii\base\Behavior;
use yii\base\Event;
use yii\web\View;

abstract class DataLayerBehavior extends Behavior
{
	const GOOGLE = 'dataLayer_google';
	/* constant needs to get overwritten with value != null in extending classes to work as expected */
	const ACTIVE_STATE_PARAM_NAME = null;
	protected $dataLayerProperties = [];
	protected $dataLayerInitPrefix = '';
	protected $dataLayerInitVar = '';
	protected $dataFormat = '';
	protected $orderInformation = '';
	protected $articleInformation;
	protected $checkoutInformation;

	public function events(): array
	{
		return [
			View::EVENT_BEFORE_RENDER => 'beforeRender',
			View::EVENT_BEGIN_BODY => 'beginBody',
		];
	}

	public function beforeRender(Event $event)
	{
		/* if there is a activeState const set in params, only return if param is set true */
		if (!$this->isSetActiveAsGlobalParam()) {
			return null;
		}

		Yii::$app->view->registerJs($this->getDataLayer($event), $this->getJsPositionOnPage());
	}

	protected function isSetActiveAsGlobalParam(): bool
	{
		return empty(static::ACTIVE_STATE_PARAM_NAME) || (Yii::$app->params[static::ACTIVE_STATE_PARAM_NAME] ?? false);
	}

	public function getDataLayer(Event $event): string
	{
		// filter all keys having null values so GA won´t misinterprete thems
		$this->dataLayerProperties = array_filter($this->dataLayerProperties, function($value) {
			return $value !== null;
		});

		$data = json_encode($this->dataLayerProperties, JSON_PRETTY_PRINT);

		return
			$this->dataLayerInitPrefix . $this->dataLayerInitVar . ' = ' .
			$this->setDataType($data) . ';';
	}

	protected function setDataType($data): string
	{
		switch ($this->dataFormat) {
			case 'array':
				return "[{$data}]";
			default:
				return $data;
		}
	}

	protected function getJsPositionOnPage(): int
	{
		return View::POS_HEAD;
	}

	public function beginBody(Event $event = null)
	{
		$dataLayerInformation = DataLayer::getDataLayerInformation($event);
		$dataLayerInformation ? $this->setDataLayerInformation(
			$dataLayerInformation['dataLayerInformation'],
			$dataLayerInformation['dataLayerType'],
			self::GOOGLE
		) : null;

		if (!$this->isSetActiveAsGlobalParam()) {
			return null;
		}
	}

	public function setDataLayerInformation($dataLayerInformation, string $dataLayerType, string $dataLayerName = '')
	{
		$this->getDataLayerBehavior($dataLayerName)->setDataLayerType($dataLayerInformation, $dataLayerType);
	}

	public function setDataLayerType($dataLayerInformation, $dataLayerType)
	{
		$this->$dataLayerType = $dataLayerInformation;
	}

	private function getDataLayerBehavior(string $dataLayerName): DataLayerBehavior
	{
		if (false == ($dataLayerBehavior = Yii::$app->view->getBehavior($dataLayerName))) {
			return false;
		}

		return $dataLayerBehavior;
	}

	public function getFromDataLayer($name)
	{
		return $this->__get($name);
	}

	public function __get($name)
	{
		$dataLayerObject = new \ArrayObject($this->dataLayerProperties);

		return $dataLayerObject->offsetGet($name);
	}

	public function __set($name, $value)
	{
		$this->dataLayerProperties[$name] = $value;

		return $this;
	}

	public function setOnDataLayer($name, $value): self
	{
		$this->__set($name, $value);

		return $this;
	}

	public function mergeOnDataLayer(array $values = []): self
	{
		if (array_keys($this->dataLayerProperties) === range(0, count($this->dataLayerProperties) - 1)) {
			return null;
		}
		$this->dataLayerProperties = array_merge($this->dataLayerProperties, $values);

		return $this;
	}
}
