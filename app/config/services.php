<?php

use app\models\ApiRepository;
use app\services\api\ApiClientBlackholeLogger;
use app\services\api\ApiClientInterface;
use app\services\api\ApiClientLoggerInterface;
use app\services\auth\AuthProvider;
use app\services\auth\AuthProviderInterface;
use app\services\i18n\TranslationManager;
use deinhandy\yii2container\Container;

/** @var Container $container */
$container = di();

// common
$container->setSingleton(
	\app\services\SessionInterface::class, function() {
	return Yii::$app->session;
});
$container->addAlias('session', \app\services\SessionInterface::class);

// api
$container->setSingleton(ApiClientLoggerInterface::class, ApiClientBlackholeLogger::class);

$container->setSingleton(ApiClientInterface::class, function(Container $container) {
	return new \app\services\api\ApiClient(
		\Yii::$app->params['apiClient']['url'],
		\Yii::$app->params['apiClient']['url_secure'],
		\Yii::$app->params['apiClient'],
		$container->get(\Yii::$app->params['apiClient']['logger'])
	);
});
$container->addAlias('api.client', ApiClientInterface::class);

$container->set(ApiRepository::class, function(Container $container) {
	return new ApiRepository($container->get('api.client'));
});
$container->addAlias('api.repository', ApiRepository::class);

$container->setSingleton(AuthProviderInterface::class, AuthProvider::class);
$container->addAlias('auth.provider', AuthProviderInterface::class);

$container->setSingleton(TranslationManager::class, function() {
	return new TranslationManager(
		Yii::$app->cache,
		Yii::$app->params['language_cache_duration'] ?? 3600
	);
});

$container->setSingleton(\app\services\i18n\UrlRules::class, \app\services\i18n\UrlRules::class);
