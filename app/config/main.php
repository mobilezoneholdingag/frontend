<?php

$params = array_replace_recursive(
	require(app_path('config/params.php')),
	require(app_path('config/params-local.php')),
	require(app_path('modules/seo/config/routing-localized.php'))
);

require_once 'services.php';

return [
	'id' => 'frontend',
	'vendorPath' => base_path('vendor'),
	'runtimePath' => app_path('runtime'),
	'basePath' => app_path(),
	'language' => 'de-CH',
	'name' => 'Mobilezone',
	'bootstrap' => [
		'category',
		'checkout',
		'customer-account',
		'eventManager',
		'homepage',
		'landingpage',
		'log',
		'page',
		'pagination',
		'product',
		'search',
		'seo',
		'shop-finder',
		'swisscom',
		'tariff',
        'blog'
	],
	'modules' => [
		'category' => [
			'class' => \app\modules\category\CategoryModule::class,
		],
		'checkout' => [
			'class' => \app\modules\checkout\CheckoutModule::class,
		],
		'customer-account' => [
			'class' => \app\modules\customerAccount\CustomerAccountModule::class,
            'emarsys' => [
                'user' => 'mobilezone007',
                'secret' => 'Ik3WIOG2eofUBqOpThEW'
            ]
		],
		'homepage' => [
			'class' => \app\modules\homepage\HomepageModule::class,
		],
		'landingpage' => [
			'class' => \app\modules\landingpage\LandingpageModule::class,
		],
		'page' => [
			'class' => \app\modules\page\PageModule::class,
		],
		'pagination' => [
			'class' => \app\modules\pagination\PaginationModule::class,
		],
		'product' => [
			'class' => \app\modules\product\ProductModule::class,
		],
		'search' => [
			'class' => \app\modules\search\SearchModule::class,
		],
		'seo' => [
			'class' => \app\modules\seo\SeoModule::class,
		],
		'shop-finder' => [
			'class' => \app\modules\shopFinder\ShopFinderModule::class,
		],
		'swisscom' => [
			'class' => \app\modules\swisscom\SwisscomModule::class,
		],
		'tariff' => [
			'class' => \app\modules\tariff\TariffModule::class,
		],
        'blog' => [
            'class' => \app\modules\blog\BlogModule::class,
            'apiUrl' => $params['luya']['url'],
            'apiToken' => $params['luya']['token'],
        ],
	],
	'components' => [
		'user' => [
			'identityClass' => \app\models\CustomerModel::class,
			'loginUrl' => ['/customer-account/customer-account/login'],
		],
		'redis' => [
			'class' => yii\redis\Connection::class,
			'hostname' => 'redis',
		],
		'redis-session' => [
			'class' => yii\redis\Connection::class,
			'hostname' => 'redis',
			'database' => 1,
		],
		'session' => [
			'class' => app\services\Session::class,
			'redis' => 'redis-session',
            'timeout' => '7200'
		],
		'eventManager' => [
			'class' => 'events.manager',
		],
		'cache' => [
			'class' => yii\redis\Cache::class,
		],
        'luyaApi' => [
            'class' => app\services\luya\Api::class,
            'endpoint' => $params['luya']['url'],
            'token' => $params['luya']['token']
        ],
		'urlManager' => [
			'class' => app\services\i18n\UrlManager::class,
			'enablePrettyUrl' => true,
			'showScriptName' => false,
			'hostInfo' => 'https://www.deinhandy.de',
			'rules' => array_merge(
				$params['url']['rules'],
				$params['staticPages']
			),
		],
		'i18n' => [
			'translations' => [
				'*' => [
					'class' => yii\i18n\PhpMessageSource::class,
					'basePath' => '@app/messages',
					'fileMap' => [
						// these mappings are only needed for the sync process between backend and frontend - not needed
						// for accessing the translations in the frontend
						'app' => 'app.php',
						'seo' => 'seo.php',
						'view' => 'view.php',
						'staticcontent' => 'staticcontent.php',
						'search' => 'search.php',
						'filter' => 'filter.php',
						'shop-finder' => 'shop-finder.php',
						'customer-account' => 'customer-account.php',
						'checkout' => 'checkout.php',
						'countries' => 'countries.php',
						'passport-types' => 'passport-types.php',
						'languages' => 'languages.php',
						'contract-renewal' => 'contract-renewal.php',
					],
				],
			],
		],
		'log' => [
			'traceLevel' => 0,
			'targets' => [
				[
					'class' => yii\log\FileTarget::class,
					'levels' => ['error', 'warning'],
				],
			],
		],
		'errorHandler' => [
			'errorAction' => 'site/error',
		],
		'view' => [
			'defaultExtension' => 'twig',
			'renderers' => [
				'twig' => [
					'class' => 'yii\twig\ViewRenderer',
					'cachePath' => '@runtime/Twig/cache',
					'options' => YII_DEBUG ? [
						'debug' => true,
						'auto_reload' => true,
					] : [],
					'extensions' => array_merge(
						[
							\Twig_Extensions_Extension_Intl::class,
							\app\twig\TranslationExtension::class,
							\app\twig\UnitExtension::class,
							\app\twig\CurrencyExtension::class,
							\app\twig\CdnExtension::class,
							\app\twig\RoutingExtension::class,
							\app\twig\AnnualTransExtension::class,
							\app\twig\AssetsExtension::class,
							\app\twig\ActiveForm::class,
							\app\twig\ModelExtension::class,
							\deinhandy\yii2picasso\twig\ImageExtension::class,
                            Twig_Extension_StringLoader::class
						],
						!YII_DEBUG ? [] : [
							\Twig_Extension_Debug::class,
						]
					),
					'globals' => [
						'Auth' => \app\helper\AuthHelper::class,
						'Html' => \app\services\form\Html::class,
						'Article' => app\helper\ArticleHelper::class,
						'Tariff' => app\helper\TariffHelper::class,
						'Url' => yii\helpers\Url::class,
						'Locale' => app\services\Locale::class,
						'NewsFeed' => app\services\NewsFeed::class,
						'Languages' => app\services\Languages::class,
						'MarketingImages' => app\services\MarketingImages::class,
						'Basket' => app\services\Basket::class,
						'Seo' => app\services\Seo::class,
						'NavigationItems' => app\services\NavigationItems::class,
                        'Youtube' => app\helper\YoutubeHelper::class,
                        'GeneralNewsItems' => app\services\GeneralNewsItems::class,
					],
					'functions' => [
						't' => 'Yii::t',
						'uniqid' => 'uniqid',
					],
					'uses' => ['yii\bootstrap'],
				],
			],
		],
		// not used yet
		'request' => [
			// !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
			'cookieValidationKey' => 'blablubbrofllol',
		],
		// disable ActiveForm´ assets, since they got loaded again on View::endPage()
		'assetManager' => [
			'bundles' => [
				'yii\web\JqueryAsset' => [
					'sourcePath' => null,
					'js' => []
				],
				'yii\widgets\ActiveFormAsset' => [
					'sourcePath' => null,
					'js' => [],
					'depends' => [],
				],
			],
		],
	],
	'params' => $params,
];
