<?php

Yii::$container = new \deinhandy\yii2container\Container();

require 'helpers.php';

// backend, from server-side perspective
Yii::setAlias('common', dirname(__DIR__));
Yii::setAlias('components', dirname(dirname(__DIR__)) . '/components');
Yii::setAlias('utils', dirname(dirname(__DIR__)) . '/utils');
Yii::setAlias('frontend', dirname(dirname(__DIR__)) . '/frontend');
Yii::setAlias('backend', dirname(dirname(__DIR__)) . '/backend');
Yii::setAlias('console', dirname(dirname(__DIR__)) . '/console');
Yii::setAlias('tools', dirname(dirname(__DIR__)) . '/tools');
Yii::setAlias('imagesSavePath', dirname(dirname(__DIR__)) . '/frontend/web/images');
Yii::setAlias('mail', dirname(dirname(__DIR__)) . '/common/mail');
Yii::setAlias('mailAttachments', dirname(dirname(__DIR__)) . '/common/mail/attachments');
Yii::setAlias('rawData', dirname(dirname(__DIR__)) . '/common/rawData');
Yii::setAlias('orderUploadPath', dirname(dirname(__DIR__)) . '/backend/runtime/orders');
Yii::setAlias('runtimePath', dirname(dirname(__DIR__)) . '/app/runtime');

// frontend, from client-side perspective
Yii::setAlias('webImages', '/img/');
Yii::setAlias('providerImages', '/images/provider');
Yii::setAlias('manufacturerImages', '/images/manufacturer');

// used for sure, above probably not needed
Yii::setAlias('@views', base_path('app/views'));
