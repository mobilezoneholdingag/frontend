<?php

if (!function_exists('di')) {
	/**
	 * @param null|string $name
	 * @param array $params
	 *
	 * @return \deinhandy\yii2container\Container|mixed
	 */
	function di($name = null, $params = []) {
		if ($name === null) {
			return \Yii::$container;
		}

		return \Yii::$container->get($name, $params);
	}
}

if (!function_exists('base_path')) {
	/**
	 * @param string $path
	 * @return string
	 */
	function base_path($path = '') {
		// remove leading slash if exists
		$path = preg_replace("/^\//m", '', $path);

		return dirname(dirname(__DIR__)) . '/' . $path;
	}
}

if (!function_exists('app_path')) {
	/**
	 * @param string $path
	 * @return string
	 */
	function app_path($path = '') {
		// remove leading slash if exists
		$path = preg_replace("/^\//m", '', $path);

		return base_path('app/') . $path;
	}
}
