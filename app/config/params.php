<?php

use app\services\Locale;

return [
	'domain' => 'deinhandy.de',
	'titleSuffix' => ' | deinhandy.de',
	'currency' => 'CHF',
	'languages' => [
		'ch' => Locale::SUISSE,
		'fr' => Locale::FRANCE,
		'it' => Locale::ITALY,
	],
	'assets' => [
		'path' => '/build/',
		'manifest' => 'rev-manifest.json',
		'cdn' => [
			'url' => 'http://ad67fd46e0f74a24d6c6-813fb3d75440f466a8b71726e0d62b63.r67.cf3.rackcdn.com/images/',
			'url_secure' => 'https://55ac187de1ca161539c1-813fb3d75440f466a8b71726e0d62b63.ssl.cf3.rackcdn.com/images/',
		]
	],
	'translations' => [
		'path' => '@app/messages',
		'languages' => [
			Locale::SUISSE,
			Locale::FRANCE,
			Locale::ITALY,
		],
		'categories' => [
			'app',
			'checkout',
			'colors',
			'countries',
			'customer-account',
			'filter',
			'languages',
			'passport-types',
			'product',
			'search',
			'seo',
			'shop-finder',
			'staticcontent',
			'view',
			'contract-renewal',
			'article_details'
		],
	],
	'staticPages' => [
		'' => 'homepage',
		'dummy' => 'site/dummy',
		[
			'pattern' => '<landingpageUrl:[\w\-]+>',
			'route' => 'landingpage/landingpage/index',
			'defaults' => [
				'landingpageUrl' => '',
			],
		],
	],
	'apiClient' => [
		'url' => 'http://stardust.api/',
		'url_secure' => 'http://stardust.api/',
		'timeout' => 60,
		'caching' => [
			'enabled' => true,
			'duration' => 60*60*24,
			'entities' => [
				\app\models\PaymentModel::API_ENTITY_NAME => false,
				\app\models\CouponModel::API_ENTITY_NAME => false,
				\app\models\StockModel::API_ENTITY_NAME => 60,
				// TODO: enable suitable cache duration
				\app\modules\shopFinder\models\ShopModel::API_ENTITY_NAME => false,
				\app\models\CustomerModel::API_ENTITY_NAME => false,
				\app\models\OrderModel::API_ENTITY_NAME => false,
				\app\models\OrderItemModel::API_ENTITY_NAME => false,
				\app\models\NewsletterModel::API_ENTITY_NAME => false,
				\app\models\AddressModel::API_ENTITY_NAME => false,
				'customer-address/default-address' => false,
				\app\modules\product\models\RenewalRequestModel::API_ENTITY_NAME => false,
				\app\models\MarketingImagesModel::API_ENTITY_NAME => 60*60,
				\app\models\AccessoryCategoryModel::API_ENTITY_NAME => false,
                'cart/totals' => false,
			],
		],
		'logger' => \app\services\api\ApiClientBlackholeLogger::class,
	],
	'url' => [
		'rules' => [
			['class' => app\services\i18n\UrlRules::class],

			# aliases for dynamic pages (caching is not allowed here)
			'suche' => 'site/suche',
			'kontakt' => 'site/contact',
			'lp/<landingpageUrl:[\w\-]+>' => 'landingpage/landingpage/index',
            'blog/<slug:[\w\-]+>' => 'blog/blog/index',

			'offer/<id:[\w\-]+>' => 'checkout/cart/accept-offer',

			'<module:[\w-]+>/<controller:[\w-]+>/<action:[\w-]+>/<id:[\w-]+>/<type:[\w-]+>' => '<module>/<controller>/<action>',
			'<module:[\w-]+>/<controller:[\w-]+>/<action:[\w-]+>/<id:[\w-]+>' => '<module>/<controller>/<action>',
			'<module:[\w-]+>/<controller:[\w-]+>/<action:[\w-]+>' => '<module>/<controller>/<action>',

			'<controller:\w+>/<action:[\w-]+>/?' => '<controller>/<action>',
			'<controller:\w+>/<action:[\w\-]+>/<id:[\w\-]+>' => '<controller>/<action>',
		]
	],
	'language_cache_duration' => 3600,
	'GoogleTagManagerId' => 'GTM-K3GDQZ',
	'picasso' => [
		'http://picasso.vokky.net/storage',
		'http://picasso.vokky.net/api/v1/',
		'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6Ijg0ODdmNGI0N2UxY2RiMzI0YzI2MTU1MGM2Y2QwMWNkYTY1YzQ5OTJiMGEyNmE0N2FhOTMxOTQ4YjRhNmYzMDJmNmQ4YmY4ZTgyMjhhNjYzIn0.eyJhdWQiOiIxIiwianRpIjoiODQ4N2Y0YjQ3ZTFjZGIzMjRjMjYxNTUwYzZjZDAxY2RhNjVjNDk5MmIwYTI2YTQ3YWE5MzE5NDhiNGE2ZjMwMmY2ZDhiZjhlODIyOGE2NjMiLCJpYXQiOjE0ODExMzQzODksIm5iZiI6MTQ4MTEzNDM4OSwiZXhwIjo0NjM2ODA3OTg5LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.cQjNu88XB6_q_DeL1ZeSF2tl3OtBxljAva8F8MvG8yR9md7nsLiDTv3I3drIJEAvfJ1H9WUMflU5i5J2MuAgJ1LslOk6cfKbQ9sLSwn8CqY4htoIsGKxuhvNkMsIWizkwT_aCcKRR0isMNvwJbfLV6ZMzfHqoDDX1GdFmB7FEzEX8YzPDqF87cgrJ08N_n8u0p85V1kwyRC5SRAOARO3MwoQY0oF4mCGFvwY6GlCjtzTm8j4yTtpU67ExVdLd9ZOXEXOVVmqYyMZ5KdIOKkw10ca-FyG6tB3ZS5N93GEn93XjiZ5_1DHRrq-PRewLxNNBbV-UeCaYD7cg5P-ChhNFf59iPayc8zbPZpY3nkT-cxpObuAT6GogzJFJvAVrlmtqSG87QWrxl8v8AHgx2v0VvxNuAs45aLQjjBGMDD6NIKG6G53J17bPwaMFH5c8PbljBmfl3-dbmzT2tei0L05T4VyMUbvrGBqhdF15Kjke_3jEUiKfmcnxp524FoWv5ViwSgabwWGO1--NlIoj5GmQxmXJjyOn_lQuotRoqljnNGyvdYkvdgKxRRKkG8xwz1KF5Y1cDneNiw1A907EVXCBa0FSugXbh-hsktdQSyMZx7dvP0M98VxGdE7TW1Bd_i19JaD1IpR5Z0_XakLKAxTBZ4sQNFzNx_C0KGyx_H3eTc',
	],
	'picasso_namespace' => 'mobilezone',
	'mobilezone_internal_ips' => [
		'217.168.42.150',
		'62.12.179.152',
		'213.158.139.145',
		'46.140.129.164',
		'81.221.14.62',
		'62.96.85.130', // DH
	],
	'environment_stage' => 'dev',
	'timezone' => 'Europe/Zurich',
	'live_chat' => false, // we are doing this with google tag manager now
    'luya' => [
        'url' => 'http://luya.cms.mztech.ch:8080/',
        'token' => 'x5kPyHHmebfWT9gLdHOigvjgxGkm9bQRDvf5ErSKfmaOaB2Iqjvnhy0aMGbV2j6',
    ]
];
