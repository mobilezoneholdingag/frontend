<?php

namespace app\helper;

use app\models\ApiRepository;
use app\models\ArticleGroupModel;
use app\models\ArticleModel;
use app\models\ArticleTariffFilterModel;
use app\models\SetItemModel;
use Illuminate\Support\Collection;

class ArticleHelper
{
	/**
	 * @param ArticleModel $article
	 * @param $relationKey
	 * @param int $tariffId
	 *
	 * @return Collection $articles
	 * If the $relationKey is 'color' we are looking for articles in that article-group and same color
	 * and loop through the memory options that are available for that color. Vice versa if the $relationKey
	 * is 'memory'.
	 */
	public static function getArticlesByColorMemoryRelation(ArticleModel $article, $relationKey, int $tariffId = null)
	{
		$selectedArticleRelation = $article->$relationKey;
		$groupArticles = $article->group->articles;
		$articles = [];
		$baseArticleHasTariff = self::articleTariffExists($article->id);

		foreach($groupArticles as $groupArticle) {
			if (!$baseArticleHasTariff || !self::articleTariffExists($groupArticle->id, $tariffId ?? null)) {
				continue;
			}

			if($groupArticle->$relationKey == $selectedArticleRelation) {
				$articles[] = $groupArticle;
			}
		}

		return Collection::make($articles);
	}

	private static function articleTariffExists(int $articleId, int $tariffId = null)
	{
		$params = [
			'article' => $articleId,
			'check-if-exists' => true,
		];

		if ($tariffId !== null) {
			$params['tariff'] = $tariffId;
		}

		$filterModel = di(ApiRepository::class)->model(ArticleTariffFilterModel::class)->findOne($params);

		return $filterModel->exists;
	}

	public static function getArticleGroup(ArticleModel $article)
	{
		if ($article->group_id) {
			return $article->group;
		}

		return new ArticleGroupModel();
	}
}
