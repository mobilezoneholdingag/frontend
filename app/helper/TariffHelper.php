<?php

namespace app\helper;

use Yii;

class TariffHelper
{
	public static function getLabelForRecommended($tariff)
	{
		$recommendedId = $tariff['recommended_for'];

		if (!$recommendedId || $recommendedId > 4) {
			return;
		}

		return Yii::t('view', 'Tariff_Recommended_For_' . $recommendedId);
	}
}
