<?php

namespace app\helper;

use app\services\auth\AuthProviderInterface;

class AuthHelper
{
	static public function user()
	{
		return di(AuthProviderInterface::class)->user();
	}
}
