<?php

namespace app\helper;

class ArrayHelper extends \yii\helpers\ArrayHelper
{
	static public function keysToSnake($arr)
	{
		foreach ($arr as $name => $val) {
			unset($arr[$name]);
			$name = StringHelper::camelToSnake($name);
			$arr[$name] = $val;
		}

		return $arr;
	}

	static public function mapKeys($array, $mapping)
	{
		foreach ($mapping as $from => $to) {
			if (isset($array[$to])) {
				throw new \InvalidArgumentException("Field `{$from}` is mapped on an already existing key `{$to}`.");
			}

			$array[$to] = $array[$from];
			unset($array[$from]);
		}

		return $array;
	}
}
