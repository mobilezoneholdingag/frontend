<?php

namespace app\helper;

class StringHelper extends \yii\helpers\StringHelper
{
	static public function snakeToCamel($val)
	{
		$val = str_replace(' ', '', ucwords(str_replace('_', ' ', $val)));
		$val = strtolower(substr($val,0,1)).substr($val,1);

		return $val;
	}

	static public function camelToSnake($val)
	{
		$val = preg_replace('/([a-z])([A-Z])/', "\\1_\\2", $val);
		$val = strtolower($val);

		return $val;
	}
}
