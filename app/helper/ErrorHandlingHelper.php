<?php
/**
 * class for some additional functions executed on an error
 * @author Maik Manthey <maik.manthey@deinhandy.de>
 */

namespace app\helper;

use Yii;

class ErrorHandlingHelper
{
	public $requestUrl;
	public $urlParts;
	public $errorUrl;
	public $bidPart;

	public function __construct()
	{
		$this->requestUrl = $_SERVER['REQUEST_URI'];

		$this->urlParts = explode('?bid=', $this->requestUrl);
		$this->errorUrl = $this->urlParts[0];
		$this->bidPart = isset($urlParts[1]) ? '?bid=' . $this->urlParts[1] : null;
	}

	/**
	 * redirect requests with trailing slash to same url without trailing slash.
	 * We had some troubles with the nginx config handling trailing slashes.
	 */
	public function redirectTrailingSlashUrls()
	{
		if (substr($this->errorUrl, -1) == '/') {
			$cleanedUrl = substr($this->errorUrl, 0, -1) . $this->bidPart;
			header("Location: " . $cleanedUrl, false);
			exit;
		}
	}

	/**
	 * sends errorUrl and errorReferer to New Relic for tracking it in "Insights"
	 */
	public function sendErrorToNewRelic()
	{
		if (is_array(Yii::$app->params['errorPageTrackingException'])) {
			foreach (Yii::$app->params['errorPageTrackingException'] as $exceptionString) {
				if (strpos($_SERVER['REQUEST_URI'], $exceptionString) !== false) {
					return;
				}
			}
		}

		if (extension_loaded('newrelic') && $this->requestUrl != null) {
			newrelic_add_custom_parameter('errorUrl', $_SERVER['REQUEST_URI']);
			if(array_key_exists('HTTP_REFERER', $_SERVER))
				newrelic_add_custom_parameter('errorReferer', $_SERVER['HTTP_REFERER']);
		}
	}


}
