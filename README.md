# Stardust - Frontend

## Installation

* clone git repository https://bitbucket.org/deinhandydevs/stardust-frontend-mobilezone to location of your choice
* cd into subfolder `<project>/docker-frontend`
* run `docker login` and provide your _Docker Hub_ credentials
* run `docker-compose up` to get the project up and running
* update your hosts file: `127.0.0.1 stardust.api backend.stardust.dev www.stardust.dev fr.stardust.dev it.stardust.dev`
* you should see some output at http://frontend.stardust.dev - at least Yii error messages ;-)

## What you get

* This will install the `frontend` of the Stardust project, using `docker containers`.
* A database, containing enough test data to instantly start working on the project.
* Project is based on the `Yii2` PHP framework.
* All views are constructed out of pure `twig templates`. (No PHP involved, so we have the chance to switch to another application framework one day.)
* On startup the `sass compiler` will be run once (creating the css) 
* Also some file watcher will be registered which will
    * Rebuild the css bundle each time the source scss files change, and then reload the css bundle int the browser (so you don't have to hit _Refresh_ manually)
    * Rebuild the js bundle when the source js files have changed, and then reload the browser
    * Reload the page when a twig template has changed
* Build tasks are all orchestrated via `Gulp` 

## Manual Commands

* First, connect to the _node_ docker container `docker exec -it dockerfrontend_node_1 bash`...
* ...then run one of the following commands:
    * `npm run styles` to compile all scss files down to plain css
    * `npm run scripts` to compile and bundle all js files
    * `npm run watch` to start file watchers on all `scss`, `js` and `twig` files 
    * `npm run lint-sass` to check sass files for violation of coding standards
    * `npm run lint-html` to check twig templates for violation of coding standards
    * `npm run lint-js` to check js files for violation of coding standards
    * `npm start` to init the frontend for use in dev environment
    * `npm run start:prod` to init the frontend for use in live environment (no watcher, css and js minified)
* Alternatively, you can run these commands directly on the container:
    * `docker exec -it dockerfrontend_node_1 npm run styles`
    * `docker exec -it dockerfrontend_node_1 npm run scripts`
    * `...`
* Add parameter `--silent` to make npm's output less verbose
* Updating Composer packages:
    * `docker-compose run composer require yiisoft/yii2-twig:2.0.6`

## Potential Pitfalls

### Node Sass Error

* If an error occurs with the npm package `node-sass` try rebuilding it with `npm rebuild node-sass`.

### Windows

* Docker Beta will only work on Windows 10 __Pro__ (or higher). Reason: Docker depends on Hyper-V virtualization. This feature is not available (resp. not compatible with Docker) in previous Windows versions.
* Don't forget to activate `Shared Drives` in the Docker Beta user interface! Else the `/app` folder cannot be mounted, resulting in most of the containers to fail on load.
* `docker exec` does not work within the `Git Bash` - use the `PowerShell` or the default `cmd` commandline!
